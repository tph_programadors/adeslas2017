<?php
/* 
------------------
Language: Català
------------------
*/

$lang = array();

$lang['PAGE_TITLE'] = 'Disfruta una experiencia única';

// Menu

$lang['MENU_HOME'] = 'Inici';
$lang['MENU_REGALO'] = 'Regals';
$lang['MENU_COMO'] = 'Com sol·licitar el regal';
$lang['MENU_FAQ'] = 'Preguntes freqüents';
$lang['MENU_CONTACT'] = 'Contacte';
$lang['MENU_HOTEL'] = 'Hotel';
$lang['MENU_FUTBOL'] = 'Futbol';
$lang['MENU_EXPE'] = 'Experiències';


// Banner Home  

$lang['TEXT_BANNER'] = 'Viu una experiència única en renovar la teva pòlissa amb SegurCaixa Adeslas.';
$lang['BOTON_BANNER'] = 'Descobreix com';


// Slide Home  

$lang['TEXT_SLIDE1'] = 'Regal';
$lang['SUBTEXT_SLIDE1'] = '<h2>FUTBOL</h2>';
$lang['TEXT_SLIDEA'] = 'Regal';
$lang['SUBTEXT_SLIDEA'] = '<h2>EXPERIÈNCIES PER A DUES PERSONES</h2>';
$lang['TEXT_SLIDEB'] = 'Regal';
$lang['SUBTEXT_SLIDEB'] = '<h2>NIT D\'HOTEL PER A DUES PERSONES</h2>';
$lang['BOTON_SLIDE'] = 'LLEGIR MÉS';
$lang['TEXT_BANNER'] = 'LLEGIR MÉS';
$lang['BANNER_RES'] = '<img class="img-responsive" src="images/slide_res_hogar_cat.jpg" alt="SegurCaixa Adeslas">';
$lang['BANNER_PRIN'] = '<img class="img-responsive" src="images/slide_cat.jpg" alt="SegurCaixa Adeslas">';


// Panells Home  

$lang['TEXT_CUADRO1'] = 'Com sol·licitar el regal?';
$lang['BOTON_CUADRO1'] = 'VEURE';
$lang['TEXT_CUADRO2'] = 'Preguntes freqüents';
$lang['BOTON_CUADRO2'] = 'LLEGIR MÉS';
$lang['TEXT_CUADRO3'] = 'Contacta amb nosaltres';
$lang['BOTON_CUADRO3'] = 'Contacte';


// Hotel

$lang['TEXT_HOTEL1'] = 'L\'escapada perfecta per a dues persones ';
$lang['TEXT_HOTEL2'] = 'NIT D\'HOTEL';
$lang['TEXT_HOTEL3'] = '<H2>CONSULTA LES CATEGORIES</h2>
                       <P>Quan disposiss del teu codi regal podràs gaudir d\'una nit d\'hotel per a dues persones. En registrar-te hauràs d\'indicar tres categories d\'hotel on t\'agradaria allotjar-te, la província i tres dates. Nosaltres t\'assignarem una en funció de la disponibilitat.<br />
A què esperes? Consulta les categories que t\'oferim.</P>';

$lang['TEXT_HOTEL4'] = 'Tria una província';
$lang['TEXT_HOTEL5'] = 'Tria una categoria';
$lang['TEXT_SELEC'] = 'Selecciona';


// Futbol

$lang['TEXT_FUTBOL1'] = 'ENTRADES DE FUTBOL';
$lang['TEXT_FUTBOL2'] = 'Et regalem una entrada de futbol</br> de la Lliga 2016-2017 per a l\'equip que triis.
';
$lang['TEXT_FUTBOL3'] = '<H2>CONSULTA ELS EQUIPS</h2>
                       <P>Per consultar els equips tria primera o segona divisió. L\'entrada s\'assignarà de manera aleatòria per partits a casa de l\'equip triat, depenent de la disponibilitat d\'entrades del club.</P>';

$lang['TEXT_FUTBOL4'] = 'Selecciona la divisió *';
$lang['TEXT_FUTBOL5'] = 'Selecciona  l\'equip *';
$lang['TEXT_FUTBOL6'] = 'Primera Divisió';
$lang['TEXT_FUTBOL7'] = 'Segunda Divisió';

$lang['TEXT_TUPARTIDO'] = 'PARTIT OTORGAT';
$lang['TEXT_FUTBOL_CAN_1'] = 'Un moment si us plau';
$lang['TEXT_FUTBOL_CAN_2'] = 'ASSIGNANT PARTIT';
$lang['TEXT_FUTBOL_CAN_3'] = 'IMPORTANT!';
$lang['TEXT_FUTBOL_CAN_4'] = 'SI REBUTJAS AQUEST PARTIT NO TINDRÀS MÉS OPCIONS DE GAUDIR DEL REGAL.';
$lang['TEXT_FUTBOL_CAN_5'] = 'TORNAR';
$lang['TEXT_FUTBOL_CAN_6'] = 'REBUTJAR';
$lang['TEXT_FUTBOL_CAN_7'] = 'ENHORABONA!';
$lang['TEXT_FUTBOL_CAN_8'] = 'El Partit del que gaudiràs és el que correspon a la jornada';
$lang['TEXT_FUTBOL_CAN_9'] = 'el dia';
$lang['TEXT_FUTBOL_CAN_10'] = 'a';
$lang['TEXT_FUTBOL_CAN_11'] = 'entre';
$lang['TEXT_FUTBOL_CAN_12'] = 'i';
$lang['TEXT_FUTBOL_CAN_13'] = 'ACCEPTAR';

// Experiencias

$lang['TEXT_EXPE1'] = '<H2>CONSULTA LES EXPERIÈNCIES</h2>
                       <P>Quan disposis del teu codi regal podràs gaudir d\'una experiència per a dues persones. En registrar-te hauràs d\'indicar tres categories d\'experiències de les quals t\'agradaria gaudir, la província i tres dates. Nosaltres te n\'assignarem una en funció de la disponibilitat. <br />
A què esperes? Consulta les experiències que t\'oferim.</P>';
$lang['TEXT_EXPE2'] = '';
$lang['TEXT_EXPE3'] = 'Tria  una categoria';
$lang['TEXT_EXPE4'] = 'Tria  una experiència';
$lang['TEXT_EXPE5'] = 'BUSCAR';
$lang['TEXT_EXPE6'] = '<h1>PILATES</h1>
                    <p>Amb aquesta sessió descobreix els beneficis del pilates. Els exercicis de pilates enforteixen la musculatura sense augmentar excesibamente el volum muscular, especialment els músculs de l\'abdomen i esquena. No només enforteix el ventre muscular sinó també els lligaments i la flexibilitat d\'aquests.
                 Augmenta la flexibilitat articular el que es tradueix en una millora dels moviments i de les articulacions, també repercuteix en la possibilitat de lesions reduint dràsticament.</br>
                Corregeix la postura, ajudant a la coneguda higiene postural i això al teu torn redueix els dolors de coll, esquena i lumbars, no només per la tonificació muscular de l\'abdomen, glutis i esquena sinó també per la conscienciació en aspectes com les postures, la respiració o l\'equilibri.
                 També t\'ajudarà a establir la força central i estabilitat i augmentar la consciència cos-ment.
                     </p>';
$lang['TEXT_EXPE7'] = '<h1>PISCINA</h1>
                    <p>Submergeix-te en un mar de plaer, envoltat de tranquil·litat i recupera les teves millors sensacions ... així et sentiràs després de passar per una piscina climatitzada. També, sigui l\'època de l\'any que sigui, podràs practicar el teu esport favorit, entrenar o simplement divertir-te duent a terme tot tipus de jocs aquàtics.</br>
             L\'experiència consisteix en una, o dues entrades, a una piscina climatitzada.</p>';
$lang['TEXT_EXPE8'] = '<h1>REFLEXOLOGIA</h1>
                    <p>La reflexologia permet que el cos funcioni correctament mitjançant l\'estimulació d\'un sistema de punts de pressió específics, situats en els peus o a les mans i en què totes les terminacions nervioses del sistema nerviós central, incloent els òrgans i les estructures internes, són reflectides en miniatura.</p>';
$lang['TEXT_CATAS1'] = 'Restauració';
$lang['TEXT_CATAS'] = 'TASTOS I SOPARS PER A DUES PERSONES';
$lang['TEXT_ESPEC1'] = 'Oci i Cultura';
$lang['TEXT_ESPEC'] = 'ESPECTACLES PER A DUES PERSONES';
$lang['TEXT_SALUD1'] = 'Bellesa i Benestar ';
$lang['TEXT_SALUD'] = 'EXPERIÈNCIA PER A DUES PERSONES';
$lang['TEXT_AVENTURA1'] = 'Esport i Aventura ';
$lang['TEXT_AVENTURA'] = 'EXPERIÈNCIA PER A DUES PERSONES';
$lang['TEXT_CURSOS1'] = 'Cursos i Tallers ';
$lang['TEXT_CURSOS'] = 'EXPERIÈNCIA PER A DUES PERSONES';

$lang['TEXT_BELLEZA_2'] = 'Bellesa i Benestar';
$lang['TEXT_CURSOS_2'] = 'Cursos i tallers';
$lang['TEXT_OCIO_2'] = 'Oci i cultura';
$lang['TEXT_RESTAURACION_2'] = 'Restauració';
$lang['TEXT_DEPORTES_2'] = 'Esports i aventura';


////// contacto

$lang['FORM_CONTACTO1'] = 'Introdueix el teu nom';
$lang['FORM_CONTACTO2'] = 'Introdueix els teus cognoms';
$lang['FORM_CONTACTO3'] = 'Introdueix la teva adreça de correu electrònic.';
$lang['FORM_CONTACTO4'] = 'Introdueix un e-mail correcte';
$lang['FORM_CONTACTO5'] = 'Introdueix la teva consulta';
$lang['FORM_CONTACTO6'] = 'Has d&rsquo;acceptar la LOPD';

// Solicitar regalo paso 1 
$lang['MENU_TIEMPO'] = 'Disposes de 15 dies des del ';
$lang['MENU_TIEMPO2'] = ' per acabar el procés d\'inscripció i adjuntar la documentació.';
$lang['TEXT_MIDDLE'] = '
<H1> Ser client d\'Adeslas et permetrà gaudir d\'un regal, a triar entre una nit d\'hotel o una experiència per a dues persones.</H1>
<br />

                      <p><b style="font-size:16px;">1r.</b> Has de ser titular d\'una pòlissa de salut d\'Adeslas i haver rebut un SMS indicant la pòlissa de salut que renovaràs pròximament. </br>
<b style="font-size:16px;">2n.</b>   A continuació pots informar-te dels regals en aquesta pàgina web. No has de fer res més, només esperar que arribi la renovació.</br>
<b style="font-size:16px;">3r.</b>  Al mes següent de la renovació de la pòlissa (gener-febrer de 2018), sempre que s\'hagi renovat, rebràs un altre SMS amb un codi per sol·licitar el regal desitjat en aquesta pàgina web. En aquest moment ja podràs sol·licitar el teu regal indicant el codi regal, codi postal i número de pòlissa.
                      </p>';

$lang['BUBBLE_FORM1'] = '<img class="img-responsive" src="images/bubble_cat.jpg" alt="SegurCaixa Adeslas">';
$lang['TEXT_FORM'] = '<H1>
                     SOL·LICITA EL TEU REGAL
                     </H1>
                     <p>Tria el regal que prefereixis, una nit d\'hotel o una experiència. Després, sol·licita el teu regal introduint el
teu codi postal, codi regal i número de pòlissa.</br>
Després d\'introduir el codi regal disposes de 15 dies per acabar el registre i enviar la documentació
sol·licitada.</p>';

$lang['GLOBO_TEXT'] = 'Si ja disposes del</br> teu codi, sol·licita</br> el regal aquí.';
$lang['LABEL_CP'] = 'Codi postal*';
$lang['LABEL_REGALO'] = 'Codi regal*';
$lang['LABEL_NUM'] = 'Número de pòlissa*';
$lang['LABEL_LEGAL'] = 'He llegit i accepto les';
$lang['LABEL_CAMPOS'] = '* Tots els camps són obligatoris';
$lang['BOTON_TEXT'] = 'COMPROVAR';
$lang['ACEP_TEXT'] = 'ACCEPTAR';

$lang['FORM_1'] = 'Entrar codi postal.';
$lang['FORM_1_1'] = 'El codi postal ha de contenir 5 xifres.';
$lang['FORM_1_2'] = 'Introdueix el teu codi postall';
$lang['FORM_2'] = 'Entrar codi de regal.';
$lang['FORM_2_1'] = 'Introdueix el teu codi regal';
$lang['FORM_3'] = 'Entrar la pòlissa.';
$lang['FORM_3_1'] = 'Introdueix la teva pòlissa';
$lang['FORM_4'] = 'Has d\'acceptar les bases legals.';
$lang['FORM_5_1'] = 'Tria el tipus de regal';
$lang['FORM_5_2'] = 'Hotel';
$lang['FORM_5_3'] = 'Futbol';
$lang['FORM_5_4'] = 'Experiències';
$lang['BOTON_ACEPTAR'] = 'ACCEPTAR';



$lang['VIDEO'] = 'El navegador no soporta el video.';
/*$lang['VIDEO_TXT'] = '<a class="bla-1" href="https://www.youtube.com/watch?v=4FWK4TbI-0c"><img src="images/video.png" alt="SegurCaixa Adeslas"></a>
                   </br>
                   VEURE VÍDEO EXPLICATIU';*/
 $lang['VIDEO_TXT'] = ' <a href="#" data-toggle="modal" data-target="#video-cat-modal"><img src="images/video.png" alt="SegurCaixa Adeslas"></a>
                   </br>
                   VEURE VÍDEO EXPLICATIU';	
$lang['POPUP_FUTBOL'] = '<h4>"DISFRUTA UNA EXPERIÈNCIA ÚNICA"</h4>
             <h3>CALENDARI DE LLIGA NO DISPONIBLE</h3>
<p class="red">Benvolgut client.</br>

El sorteig del calendari de lliga, de primera i segona divisió de futbol, es realitzarà a mitjans de juliol de 2016, fins el dia 1 d\'Agost no podràs prosseguir amb el registre.</br>

Gràcies.

</p>';

// Solicitar regalo paso 2 

$lang['TEXT_MIDDLE_PASO1'] = '<H1>
                     <span class = "bold">TRIA TRES OPCIONS</span>
                     </H1>
                     <p>Hauràs de triar tres categories. Després tres experiències diferents per ordre de preferència.</p>';
$lang['TEXT_MIDDLE_PASO2'] = '<H1>
                     <span class = "bold">TRIA  TRES DATES </ span>
                     </H1>
                     <p> Selecciona tres dates diferents, almenys amb un mes de diferència entre si. La primera data no podràs sol·licitar-se en un termini inferior a 30 dies. </p>';		
$lang['TEXT_11'] = 'Categoria de la primera experiència *.';
$lang['TEXT_12'] = 'Primera experiència*.';
$lang['TEXT_13'] = 'Primera data (dd-mm-aaaa)*.';
$lang['TEXT_21'] = 'Categoria de la segona experiència*.';
$lang['TEXT_22'] = 'Segona experiència*.';
$lang['TEXT_23'] = 'Segona data (dd-mm-aaaa)*.';
$lang['TEXT_31'] = 'Categoria de la tercera experiència*.';
$lang['TEXT_32'] = 'Tercera experiència*.';
$lang['TEXT_33'] = 'Tercera data (dd-mm-aaaa)*.';
$lang['CONFIRM'] = 'CONFIRMAR';
$lang['RECUERDE'] = 'Si vols gaudir del regal acompanyat, has de disposar de les dades personals de tots dos en la sol·licitud (nom, adreça, DNI, data de naixement, telèfon i correu electrònic).';
$lang['TEXT_REQ'] = 'Camp requerit';
$lang['TEXT_DATA1'] = 'Primera data (dd-mm-aaaa)*';
$lang['TEXT_DATA2'] = 'Segona data (dd-mm-aaaa)*';
$lang['TEXT_DATA3'] = 'Tercera data (dd-mm-aaaa)*';
$lang['TEXT_IRSOLO'] = '<h3>ESTÀS SEGUR QUE DESITGES ANAR-HI SOL?</h3>
                 <h4>RECORDA! </h4>
                <p><strong>Si decideixes anar-hi sol, no podràs incorporar cap acompanyant.</strong></p>
                <div>
                <p><strong><span class="orange"></span></strong></p>';


$lang['TEXT_FORM2_1'] = 'Tria una Categoria';   
$lang['TEXT_FORM2_1_1'] = 'Tria una Província';  
$lang['TEXT_FORM2_2'] = 'Tria una Experiència';     
$lang['TEXT_FORM2_2_2'] = 'Tria una categoria';  
$lang['TEXT_FORM2_3'] = 'Introdueix una Data'; 

$lang['TEXT_FORM2_1_exp'] = 'Introdueix 3 experiències diferents'; 

$lang['TEXT_FORM2_3_comp'] = 'Selecciona una data amb mínim 30 dies després de la segona data triada'; 
$lang['TEXT_FORM2_2_comp'] = 'Selecciona una data amb mínim 30 dies després de la primera data triada'; 
$lang['TEXT_FORM2_1_comp'] = 'Selecciona una data amb mínim 30 dies després d\'avui';  


// Solicitar regalo paso 2 ( FUTBOL )
$lang['TEXT_MIDDLE_PASO1_FUTBOL'] = '<H1>
                    <span class="bold">
SELECCIONA UNA CATEGORIA I UN EQUIP</span>
                    </H1>
                    <p>L\'entrada s\'assignarà de manera aleatòria per partits a casa de l\'equip escollit , depenent de la disponibilitat d\'entrades del club.</p>';
$lang['RECUERDE_FUTBOL'] = 'Recorda que el procés de bescanvi s\'ha de completar sense interrupcions, en cas contrari el procés no serà vàlid.';
$lang['TEXT_FUTBOL_LIGA'] = 'Selecciona una categoria*';
$lang['TEXT_FUTBOL_EQUIPO'] = 'Selecciona un equip*';

// Solicitar regalo paso 2 ( HOTEL )

$lang['TEXT_MIDDLE_PASO1_HOTEL'] = '<H1>
                    <span class="bold">TRIA TRES OPCIONS</span>
                    </H1>
                    <p>Ha de triar la província o províncies que desitges. Després tres categories diferents per ordre de preferència.</p>';
$lang['TEXT_11_HOTEL'] = 'Tria  una província*';
$lang['TEXT_12_HOTEL'] = 'Tria  una categoria*';
$lang['TEXT_21_HOTEL'] = 'Tria  una província*';
$lang['TEXT_22_HOTEL'] = 'Tria  una categoria*';
$lang['TEXT_31_HOTEL'] = 'Tria  una província*';
$lang['TEXT_32_HOTEL'] = 'Tria  una categoria*';

// Solicitar regalo paso 3 

$lang['TEXT3_1'] = '<H1>FORMULARI DE REGISTRE</h1>
                   <P>Segueix amb el procés de bescanvi introduint les dades, de la persona que vulgui gaudir de l\'experiència, en el següent formulari.</P>';
$lang['TEXT3_2'] = '';
$lang['TEXT3_3'] = 'Nom*';
$lang['TEXT3_4'] = 'Cognoms*';
$lang['TEXT3_5'] = 'Data de naixement (dd-mm-aaaa)*';
$lang['TEXT3_6'] = 'DNI/NIE/Passaport*';
$lang['TEXT3_7'] = 'Correu electrònic*';
$lang['TEXT3_8'] = 'Telèfon*';
$lang['TEXT3_9'] = 'Direcció*';
$lang['TEXT3_10'] = 'Població*';
$lang['TEXT3_11'] = 'Codi postal*';
$lang['TEXT3_12'] = 'Selecciona una província *';
$lang['TEXT3_13'] = '*Tots els camps són obligatoris';
$lang['TEXT3_14'] = 'ANAR SOL';
$lang['TEXT3_15'] = 'ANAR ACOMPANYAT';
$lang['TEXT3_16'] = 'Si vols gaudir del regal acompanyat, has de disposar de les dades personals de l\'acompanyant en el moment de fer la sol·licitud (nom, adreça, DNI, data de naixement, telèfon i correu electrònic).
';
$lang['TEXT3_17'] = 'Camp nom requerit';
$lang['TEXT3_18'] = 'Camp cognoms requerit';
$lang['TEXT3_19'] = 'Camp DNI requerit';
$lang['TEXT3_20'] = 'Camp correu electrònic requerit';
$lang['TEXT3_21'] = 'Camp telèfon requerit';
$lang['TEXT3_22'] = 'Camp adreça postal requerit';
$lang['TEXT3_23'] = 'Camp codi postal requerit';
$lang['TEXT3_24'] = 'Sexe*';
$lang['TEXT3_25'] = 'Home';
$lang['TEXT3_26'] = 'Dona';

$lang['TEXT_FORM1_1'] = 'El códi';      
$lang['TEXT_FORM1_2'] = 'ja ha estat usat'; 
$lang['TEXT_FORM1_2_2'] = 'és erroni'; 
  
$lang['TEXT_FORM1_3'] = 'El número de pòlissa ';  
$lang['TEXT_FORM1_4'] = 'és erroni , revisa el teu número de pòlissa'; 
 
$lang['TEXT_FORM1_5'] = 'La pòlissa';  
$lang['TEXT_FORM1_6'] = 'ja ha estat usada';  

$lang['TEXT_FORM1_7'] = 'Introdueix un codi postal'; 
$lang['TEXT_FORM1_8'] = 'Introdueix un códi postal correcte'; 

$lang['TEXT_FORM1_9'] = 'Tria el tipus de regal'; 
$lang['TEXT_FORM1_10'] = 'Introdueix el número de pòlissa';

///// Pas3 ////
$lang['TEXT_FORM3_1'] = 'El DNI/NIE';
$lang['TEXT_FORM3_2'] = 'ja ha participat en aquesta promoció el';
$lang['TEXT_FORM3_3'] = 'Només es permet una participació per persona.';
$lang['TEXT_FORM3_4'] = 'Introdueix el teu nom';
$lang['TEXT_FORM3_5'] = 'Introdueix els teus cognoms';
$lang['TEXT_FORM3_6'] = 'Introdueix el teu sexe';
$lang['TEXT_FORM3_7'] = 'Introdueix la teva data de naixement';
$lang['TEXT_FORM3_7_menor'] = 'El participant ha de ser major d\'edat';
$lang['TEXT_FORM3_8'] = 'Introdueix el teu DNI';
$lang['TEXT_FORM3_9'] = 'Introdueix el teu e-mail';
$lang['TEXT_FORM3_9_format'] = 'Introdueix un e-mail correcte';
$lang['TEXT_FORM3_10'] = 'Introdueix el teu telèfon de contacte';
$lang['TEXT_FORM3_10_format'] = 'Introdueix un telèfon correcte (9 dígits ,sense espais)'; 
$lang['TEXT_FORM3_11'] = 'Introdueix la teva adreça';
$lang['TEXT_FORM3_12'] = 'Introdueix el teu municipi';
$lang['TEXT_FORM3_13'] = 'Introdueix la teva província';
$lang['TEXT_FORM3_14'] = 'Introdueix el teu codi postal';
// ACOMPANYANT ///
$lang['TEXT_FORM3_15'] = 'Introdueix el nom del teu acompanyant';
$lang['TEXT_FORM3_16'] = 'Introdueix els cognoms del teu acompanyant';
$lang['TEXT_FORM3_17'] = 'Selecciona el sexe del teu acompanyant';
$lang['TEXT_FORM3_18'] = 'Introdueix la data de naixement del teu acompanyant';
$lang['TEXT_FORM3_18_menor'] = 'Acompanyant menor d\'edat, la data de naixement és incorrecta'; 
$lang['TEXT_FORM3_18_mayor'] = 'Acompanyant major d\'edat, la data de naixement és incorrecta'; 
$lang['TEXT_FORM3_19'] = 'Selecciona si el teu acompanyant és menor d´edat';
$lang['TEXT_FORM3_20'] = 'Introdueix el DNI del teu acompanyant';
$lang['TEXT_FORM3_20_format'] = 'DNI incorrecte'; 
$lang['TEXT_FORM3_21'] = 'Introdueix el correu electrònic del teu acompanyant';
$lang['TEXT_FORM3_22'] = 'Introdueix el telèfon del teu acompanyant';
$lang['TEXT_FORM3_23'] = 'Introdueix la direcció del teu acompanyant';
$lang['TEXT_FORM3_24'] = 'Introdueix la província del teu acompanyant';
$lang['TEXT_FORM3_25'] = 'Introdueix la població del teu acompanyant';
$lang['TEXT_FORM3_26'] = 'Introdueix el codi postal del teu acompanyant';
$lang['TEXT_FORM3_27'] = 'DNI Incorrecte';

$lang['TEXT_MAIL_USER_22_TIT'] = 'disfrutaunaexperienciaunica.com';
$lang['TEXT_MAIL_USER_22_CONT'] = '
<p>Hemos recibido su documentaci&oacute;n y la hemos enviado a nuestro Departamento de validaci&oacute;n.</p>
<p>Si cumple con los requisitos de la promoci&oacute;n, el Departamento de reservas contactar&aacute; con Usted para la asignaci&oacute;n del regalo deseado.</p>
<p>Gracias por participar en &quot;DISFRUTA UNA EXPERIENCIA &Uacute;NICA&quot;.</p>';

$lang['TEXT_MAIL_USER_1_TIT'] = 'Registre a disfrutaunaexperienciaunica.com';

$lang['TEXT_MAIL_USER_1'] = 'Gr&agrave;cies per registrar-te a la Promoci&oacute; &quot;Disfruta una experiencia &uacute;nica&quot;.
<br />
<br />
Ara ha d\'enviar el teu formulari contractual que pot trobar '; 
$lang['TEXT_MAIL_USER_2'] = ' signat a mà pels participants juntament amb una fotoc&ograve;pia amb qualitat gr&agrave;fica dels documents identificatius (DNI/NIE) ambdues cares del titular i acompanyant, en un termini m&agrave;xim de 15 dies des del registre en la promoci&oacute;.
 '; 
$lang['TEXT_MAIL_USER_3'] = '
<br />
Adjuntant un arxiu en els seg&uuml;ents formats (.jpg .pdf .png .rar .zip) amb un pes total m&agrave;xim de 4 MB seguint el seg&uuml;ent enlla&ccedil;:
<br />
'; 
$lang['TEXT_MAIL_USER_4'] = '
<br />
Et recordem que el formulari ha d\'estar signat pel participant o participants, en cas d\'anar acompanyat, perqu&egrave; sigui v&agrave;lid.
<br />
<br />
Gr&agrave;cies per la teva participaci&oacute;.
<br />
<br />
Una salutaci&oacute; cordial,
'; 

// Sube archivos
$lang['TEXTARCH_1'] = 'Arxiu';
$lang['TEXTARCH_2'] = 'pujat correctament.';
$lang['TEXTARCH_3'] = 'Hem rebut correctament la teva documentació , si la documentació és correcta, ens posarem en contacte amb vostè en un termini màxim de 15 dies mitjançant correu electrònic per informar-lo de l´experiència confirmada';
//errores Sube archivos
$lang['TEXTARCH_4'] = 'Error al pujar l´arxiu.';
$lang['TEXTARCH_5'] = 'Error tipus d´arxiu';
$lang['TEXTARCH_6'] = 'Cap fitxer seleccionat.';
$lang['TEXTARCH_7'] = 'Error en pujar els arxius.';

// Solicitar regalo paso 3 Acompañante

$lang['TEXT_ACO1'] = '<h1>DADES DE L\'ACOMPANYANT</h1><p>Seguiu amb el procés de bescanvi introduint les dades de l\'acompanyant.</p>';
$lang['TEXT_ACO2'] = '¿És menor d\'edat? *';
$lang['TEXT_ACO3'] = 'Descarrega l\'autorització de menors ';
$lang['TEXT_ACO4'] = 'Descarregar';
$lang['TEXT_ACO5'] = 'Les següents dades poden ser els mateixos que els del participant';
$lang['TEXT_ACO6'] = '&nbsp;Els mateixos que el participant';
$lang['TEXT_ACO7'] = 'FINALITZAR';
$lang['TEXT3_27'] = 'Sí amb DNI';
$lang['TEXT3_28'] = 'Sí sense DNI';
$lang['TEXT3_29'] = 'No';


// Solicitar regalo paso 4 (La majoria de camps son els del paso 3) 

$lang['TEXT4_1'] = '<H1> Ara completi les dades del teu acompanyant </h1>
                   <P>Aquestes dades són necessàries per seguir amb el procés.</P>';
$lang['TEXT4_2'] = 'FINALIZAR';


// FAQ 

$lang['TEXT_FAQ'] = '
<H2> PREGUNTES FREQÜENTS </H2>
       <div class = "contenidors">
          <div class = "Accordion">
            <dl>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger"> 1. En què consisteix exactament el regal? </a>
              </dt>
              <dd class = "Accordion-content accordionItem is-collapsed" id="accordion1" ària-hidden = "true">
                <p>El regal consisteix en una experiència a triar entre les següents categories: Experiències per a dues persones (bellesa i benestar, cursos i tallers, esport i aventura, restauració i oci i cultura) i Nit d\'Hotel per a dues persones. Quan disposis del teu codi regal hauràs d\'indicar quina categoria prefereixes, la província i tres dates. Nosaltres te n\'assignarem una en funció de la disponibilitat.
</p>
              </dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger"> 2. Què he de fer per aconseguir el regal? </a>
              </dt>
              <dd class="Accordion-content accordionItem is-collapsed" id="accordion1" ària-hidden="true">
                <p>Un cop la renovació sigui efectiva, rebràs un altre SMS (gener-febrer 2018) amb un codi per sol·licitar gratuïtament l\'experiència desitjada en aquesta pàgina web.
</p>
              </dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger"> 3. Qui pot gaudir del regal? </a>
              </dt>
              <dd class="Accordion-content accordionItem is-collapsed" id="accordion1" ària-hidden="true">
                <p>El regal va dirigit al titular de la pòlissa, però pot gaudir-ne qualsevol persona que aquest triï. S\'haurà de registrar la persona que gaudirà del regal. S\'ha de facilitar mitjançant documentació oficial la identitat dels participants i sempre que (els acompanyants) no siguin majors d\'edat una autorització del seu tutor.
</p>
              </dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger"> 4. Puc canviar el regal triat una vegada confirmat? </a>
              </dt>
              <dd class="Accordion-content accordionItem is-collapsed" id="accordion1" ària-hidden="true">
                <p> Un cop confirmat el regal sol·licitat no serà possible fer cap tipus de modificació.</p>
              </dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger"> 5. Quina documentació haig d\'enviar per formalitzar el lliurament del regal? </a>
              </ dt>
              <dd class="Accordion-content accordionItem is-collapsed" id="accordion1" ària-hidden="true">
                <p>El client ha d\'enviar fotocòpia amb qualitat gràfica dels documents identificatius (DNI / NIE) ambdues cares d\'ell i l\'acompanyant juntament amb el formulari contractual de participació signat a mà.</p>
              </dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger"> 6. Quant de temps tinc per enviar la documentació? </a>
              </dt>
              <dd class = "Accordion-content accordionItem is-collapsed" id = "accordion1" ària-hidden = "true">
                <p>El termini per enviar la documentació és de 15 dies des de la data de la sol·licitud.</p>
              </dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger"> 7. Si ja he enviat la meva documentació, què he de fer ara? </a>
              </dt>
              <dd class = "Accordion-content accordionItem is-collapsed" id = "accordion1" ària-hidden = "true">
                <p>Un cop rebem la teva documentació, el departament de reserves en un termini màxim de 15 dies, contactarà amb el participant a través correu electrònic per informar-lo del regal assignat. A continuació, el client tindrà un termini de 5 dies per respondre, acceptant o rebutjant el regal.</P>
              </dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger"> 8. Com i quan rebré el val per gaudir del regal? </a>
              </dt>
              <dd class = "Accordion-content accordionItem is-collapsed" id = "accordion1" ària-hidden = "true">
                <P>5 dies laborables abans de la data de gaudi del regal confirmat es contactarà amb el client per fer-li lliurament de la documentació necessària.</p>
              </dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger"> 9. Què he de lliurar a l\'establiment per gaudir del regal?  </a>
              </dt>
              <Dd class = "Accordion-content accordionItem is-collapsed" id = "accordion1" ària-hidden = "true">
                <P>El mateix dia de l\'experiència, s\'haurà de lliurar a l\'establiment la documentació rebuda (VAL) i presentar el seu DNI.</p>
              </ Dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger"> 10. He d\'abonar alguna cosa? </a>
              </ dt>
              <Dd class = "Accordion-content accordionItem is-collapsed" id = "accordion1" ària-hidden = "true">
                <P>L\'experiència és totalment gratuïta, encara que seran per compte de la persona premiada totes les despeses de desplaçament, així com qualsevol extra que es generi en l\'assistència en el lloc de l\'experiència o de l\'allotjament a l\'hotel.</ P>
              </ Dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger"> 11. Quin termini tinc per a sol·licitar el regal? </a>
              </ dt>
              <Dd class = "Accordion-content accordionItem is-collapsed" id = "accordion1" ària-hidden = "true">
                <P>El termini de sol·licitud del regal és fins al 30 d\'abril de 2018. </ P>
              </Dd>
<dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger"> 12. Quin termini tinc per gaudir del regal </a>
              </ dt>
              <Dd class = "Accordion-content accordionItem is-collapsed" id = "accordion1" ària-hidden = "true">
                <P> El regal es podrà gaudir entre l\'1 de març de 2018 i el 31 de desembre de 2018, ambdós inclosos.</ p>
              </ Dd>
<dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger"> 13. Si tinc algun dubte o consulta què he de fer? </a>
              </ dt>
              <Dd class = "Accordion-content accordionItem is-collapsed" id = "accordion1" ària-hidden = "true">
                <P> Tens a la teva disposició el formulari de contacte a l\'apartat de contacte.</P>
              </ Dd>
       </ Dl>
          </div>
        </div> ';


// Contacto (La majoria de camps son els del paso 3) 

$lang['CONT_1'] = '<h2>FORMULARI DE CONTACTE</h2>
                  <p>Si tens algun dubte, pots omplir aquest formulari i enviar-nos la teva consulta.</p>';
$lang['CONT_2'] = '';
$lang['CONT_3'] = 'ENVIAR';
$lang['CONT_4'] = '<H1>LA TEVA CONSULTA HA ESTAT ENVIADA CORRECTAMENT</h1>
<P>Amb la major brevetat possible et donarem una resposta.</P>';
$lang['CONT_5'] = 'TORNAR';
$lang['CONT_6'] = 'Comentaris';
$lang['CONT_7'] = 'He llegit i accepto les ';
$lang['TEXT_LOPD'] = 'LOPD';


// Footer  

$lang['TEXT_BASES'] = 'Bases legals';
$lang['TEXT_POL_COOK'] = 'Política de cookies';
$lang['TEXT_POL_PRIV'] = 'Política de privacitat';


// Tu experiencia  

$lang['TEXT_TUEXPE1'] = '<H1> FORMULARI FINAL </h1>
                                 <P> Ara has d\'enviar aquest formulari contractual de participació signat i escanejat, juntament amb una fotocòpia amb qualitat gràfica dels documents identificatius (DNI / NIE ) ambdues cares, en un termini màxim de 15 dies des de la inscripció en la promoció.</p>';
$lang['TEXT_TUEXPE2'] = 'EXPERIÈNCIES';
$lang['TEXT_TUEXPE3'] = 'Primera experiència';
$lang['TEXT_TUEXPE4'] = 'Primera data';
$lang['TEXT_TUEXPE5'] = 'Segona experiència';
$lang['TEXT_TUEXPE6'] = 'Segona data';
$lang['TEXT_TUEXPE7'] = 'Tercera experiència';
$lang['TEXT_TUEXPE8'] = 'Tercera data';
$lang['TEXT_TUEXPE9'] = 'DADES DEL PARTICIPANT';
$lang['TEXT_TUEXPE10'] = 'Nom';
$lang['TEXT_TUEXPE11'] = 'Cognoms';
$lang['TEXT_TUEXPE12'] = 'Data de naixement';
$lang['TEXT_TUEXPE13'] = 'DNI/NIE/Passaport*';
$lang['TEXT_TUEXPE14'] = 'Correu electrònic';
$lang['TEXT_TUEXPE15'] = 'Telèfon';
$lang['TEXT_TUEXPE16'] = 'Direcció postal';
$lang['TEXT_TUEXPE17'] = 'Població';
$lang['TEXT_TUEXPE18'] = 'Codi postal';
$lang['TEXT_TUEXPE19'] = 'Província';
$lang['TEXT_TUEXP20'] = 'DADES DE L\'ACOMPANYANT';
$lang['TEXT_TUEXPE20'] = 'DADES DE L\'ACOMPANYANT';
$lang['TEXT_TUEXPE21'] = '<h2>ENVIAR DOCUMENTACIÓ</h2>
                                       <P>Envia la documentación per qualsevol dels mitjans que es detallen a continuació.</ p>';
$lang['TEXT_TUEXPE22'] = '
<strong> ADJUNTANT UN ARXIU </strong> </br>
Adjuntant un arxiu en un dels següents formats (.jpg .pdf .png .rar .zip). Amb un pes total màxim de 4MB.  ';
$lang['TEXT_EXPE23'] = 'Adjuntar DNI';
$lang['TEXT_EXPE24'] = 'Adjuntar formulari signat';
$lang['TEXT_EXPE25'] = 'Adjuntar Autorització de menors'; 
$lang['TEXT_EXPE26'] = 'Adjuntar DNI de l\'acompanyant';    
$lang['TEXT_TUEXPE27'] = '<strong>CORREU POSTAL ORDINARI  </strong> </br>
                                             Promoció “Disfruta una experiencia única"</br>
Apartat de correus 91</br>
08720 Vilafranca del Penedès</br>
Barcelona
';    
$lang['TEXT_TUEXPE28'] = '';    
$lang['TEXT_TUEXPE29'] = 'Guardar en PDF';  


// experiencia selecciona hotel mail recordatorio

$lang['MAIL_RECORDATOTIO_1'] = '<p>Et recordem la teva elecció:</p>'; 
$lang['MAIL_RECORDATOTIO_2'] = '<p>Per a la data:</p>'; 
$lang['MAIL_RECORDATOTIO_3'] = '<p>Cinc dies abans de la data, el departament de reserves contactarà amb tu perquè ens confirmis la teva assistència i t\'enviaran el teu número de reserva i / o localitzador, que hauràs d\'indicar a la recepció de l\'hotel a la teva arribada.</p>'; 
$lang['MAIL_RECORDATOTIO_3_ocio'] = '<p>Cinc dies abans de la data, el departament de reserves contactarà amb tu perquè ens confirmis la teva assistència, i t\'enviaran el teu número de reserva i / o localitzador, que hauràs d\'indicar a l\'establiment a la teva arribada.</p>'; 
$lang['MAIL_RECORDATOTIO_4'] = '<p>Trobareu més detalls a les bases de la promoció.</p>'; 
$lang['MAIL_RECORDATOTIO_5'] = '<p>Gràcies per la teva participació.</p>'; 


//Experiences description - the number is the crm id for this experience.

//Salud y Bienestar.
$lang['TEXT_EXP_DESCR_80']= '<H1>Mesoteràpia Facial i vitamines:</H1> La mesoteràpia és una tècnica d\'aplicació de medicaments a través de micro agulles en forma local, és a dir, sobre l\'àrea a tractar. En el cas de l\'envelliment facial s\'apliquen a la cara.';
$lang['TEXT_EXP_DESCR_4']= '<H1>Spinning:</H1> L\'spinning és un exercici aeròbic i de cames principalment, on el monitor o professor pot, mitjançant el canvi de la freqüència de pedaleig i de la resistència al moviment, realitzar tot tipus d\'intensitats. És una gimnàstica, molt adaptable al nivell de l\'alumne, podent ser tan senzilla com un passeig tranquil, o bé, tan esgotadora fins i tot per a un ciclista professional.';
$lang['TEXT_EXP_DESCR_86']= '<H1>Tractament Anti-Age:</H1> Es realitza una neteja de la pell profunda amb el "peeling", especialment, en les zones de les arrugues. Després es continua amb un petit drenatge facial per afavorir el treball en les zones més problemàtiques. S\'aplica una màscara especial sobre la superfície de la cara per fer la "fotoporació" i "electroporació" i afavorir la penetració dels principis actius a la pell.';
$lang['TEXT_EXP_DESCR_1']= '<H1>Massatge:</H1> El massatge és una forma de manipulació de les capes superficials i profundes dels músculs del cos utilitzant diverses tècniques, per millorar les seves funcions, ajudar en processos de curació, disminuir l\'activitat reflexa dels músculs, inhibir l\'excitabilitat motoneurona, promoure la relaxació i el benestar i com a activitat recreativa.';
$lang['TEXT_EXP_DESCR_3']= '<H1>Reflexologia:</H1> La reflexologia és la disciplina que promou el tractament de diverses afeccions a través de massatges a les mans o als peus. D\'acord amb aquesta filosofia, els massatges que s\'apliquen en certs punts del cos provoquen un reflex en altres regions corporals, permetent l\'alleujament d\'un malestar.';
$lang['TEXT_EXP_DESCR_5']= '<H1>Aeròbic:</H1> Coneguda com una de les disciplines esportives més populars i divertides de l\'actualitat, l\'aeròbic és una activitat física que implica una alta dosi de despesa d\'energia a través del ball o del seguiment de consignes i rutines ballables que posen en moviment a l\'organisme i permeten gastar moltes calories.';
$lang['TEXT_EXP_DESCR_176']= '<H1>Pilates:</H1> Pilates és un mètode d\'exercici i moviment físic dissenyat per estirar, enfortir i equilibrar el cos.';
$lang['TEXT_EXP_DESCR_177']= '<H1>Ioga:</H1> Ioga és un terme sànscrit que pot traduir-se com "esforç" o "unió". El concepte té dues grans aplicacions: d\'una banda, es tracta del conjunt de disciplines físiques i mentals que es van originar a l\'Índia i que busquen aconseguir la perfecció espiritual i la unió amb l\'absolut; d\'altra banda, el ioga està format per les pràctiques modernes que deriven de l\'esmentada tradició hindú i que promouen el domini del cos i una major capacitat de concentració.';
$lang['TEXT_EXP_DESCR_23']= '<H1>Reiki:</H1> El reiki es pot considerar com una disciplina pteudocientífica, una tècnica o una modalitat de medicina alternativa que cerca la sanació del pacient mitjançant la imposició de mans. Es basa en la canalització del que els teus practicants anomenen energia vital universal per aconseguir l\'equilibri.';
$lang['TEXT_EXP_DESCR_6']= '<H1>Zumba:</H1> La zumba és un tipus d\'activitat física (fitness) basada en ritmes i músiques llatinoamericanes. El teu origen és Colòmbia i està estesa per tot el món. Les coreografies de zumba inclouen ritmes com la samba, la salsa, el reggaeton, la cúmbia, el merengue i el mambo.';
$lang['TEXT_EXP_DESCR_7']= '<H1>Natació:</H1> Hauràs sentit mil vegades que la natació és ideal per a la salut: bona per a l\'esquena, poc agressiva amb les articulacions, una forma d\'entrenar tot el cos, apte per a tot tipus de persones... Relaxa\'t i gaudeix d\'una sessió d\'entrenament.';
$lang['TEXT_EXP_DESCR_8']= '<H1>Busseig:</H1> El busseig és una activitat subaquàtica que pot realitzar-se amb finalitats recreatives, d\'investigació o esportives. Consisteix a entrar a l\'aigua i submergir la totalitat del cos, de manera que, en general, es desenvolupa amb l\'ajuda d\'algun tipus d\'equipament que permet al bus no haver de sortir a la superfície a respirar.';
$lang['TEXT_EXP_DESCR_9']= '<H1>Urban dance:</H1> L\'expressió "ball urbà "o "dansa urbana", es refereix a certs esdeveniments, performances i creacions coreogràfiques, on els ballarins ballen en ple espai públic, com ho pot ser, al carrer, en una plaça, centre comercial...';
$lang['TEXT_EXP_DESCR_11']= '<H1>Ballet:</H1> La dansa clàssica, també coneguda com a ballet, és un tipus de dansa que compta amb diferents tècniques i moviments específics. Ballet és, a més, el nom que permet fer referència a la peça musical composta per a ser interpretada a través de la dansa.';
$lang['TEXT_EXP_DESCR_12']= '<H1>Flamenc:</H1> Gaudeixi d\'una classe de flamenc. El flamenc és un estil de música i dansa propi d\'Andalusia, Extremadura i Múrcia. Les seves principals característiques són el cant, el toc i el ball, comptant també amb les seves pròpies tradicions i normes';
$lang['TEXT_EXP_DESCR_15']= '<H1>Manicura:</H1> Llueix unes mans boniques, suaus i ben cuidades amb una manicura. A vegades no els donem gaire atenció, però per tenir i mantenir unes mans boniques és imprescindible cuidar-les, i per tant fer una bona manicura..';
$lang['TEXT_EXP_DESCR_14']= '<H1>Pedicura:</H1> Amb l\'arribada del bon temps comencen a aparèixer aquestes zones del cos que hem tingut oblidades durant l\'hivern. Els peus han estat tancats gran part de l\'any entre mitjons, botes i sabates, i no ens hem adonat que estaven maltractats que estaven. Cuidi i mimi els teus peus amb una pedicura professional.';
$lang['TEXT_EXP_DESCR_16']= '<H1>Canvi de look:</H1> No cal canviar el color del cabell i el pentinat cada mes. La suma de petites modificacions en la nostra aparença pot ajudar-nos a aconseguir un veritable canvi d\'imatge, que molts busquem però no ens animem a fer. Un expert l\'ajudarà i l\'assessorarà perquè el teu canvi sigui personalitzat.';
$lang['TEXT_EXP_DESCR_2']= '<H1>Spa:</H1> Permet evadir-te de la vida quotidiana i endinsa\'t en un exquisit santuari de la relaxació. Una teràpia amb aigua amb piscines, jacuzzis, hidromassatges, pressió, sauna...';
$lang['TEXT_EXP_DESCR_17']= '<H1>Fishing pedicure:</H1> Sent els teus peus frescos i sans amb la pedicura més natural! Submergeix els peus a l\'aigua tèbia i els peixos començaran a treballar netejant les seves impureses! Un treball exfoliant que et farà sentir un petit pessigolleig al principi i un agradable massatge final. Gràcies als peixos Garra rufa, originaris de Turquia, veuràs com les impureses i restes de pell morta desapareixen dels teus peus. La teva boca en forma de ventosa i sense dents realitza un tractament exfoliant alhora que fan massatges, millorant la circulació, ajudant-te a relaxar-te i hidratar la pell.';
$lang['TEXT_EXP_DESCR_18']= '<H1>Maquillatge professional:</H1> Descobreix tot el que pot arribar a fer amb l\'ajuda d\'un professional, aprendrà tècniques més avançades i el deixaran perfecte.';
$lang['TEXT_EXP_DESCR_202']= '<H1>Tint i permanent de pestanyes:</H1> Atraurà totes les mirades aquesta temporada, aconsegueixi embellir la teva mirada més encara donant-li un toc de sensualitat, desimboltura i joventut, perquè en tot moment estiguis perfecte de dia i de nit, vagi on vagi.';

//Espectáculos.
$lang['TEXT_EXP_DESCR_63']= '<H1>Cinema:</H1> Vés al cinema i gaudeix de l\'estrena que tria. Veu una pel•lícula en una gran pantalla i gaudeixi del teu so i només hauràs de pensar si les crispetes les vol dolces o salades. La durada serà, la de la pel•lícula escollida.';
$lang['TEXT_EXP_DESCR_144']= '<H1>Musical:</H1> Gaudeixi d\'un musical en què l\'acció es desenvolupa de manera cantada i ballada. Una forma de teatre que combina música, cançó, diàleg i ball, i que es representa en grans escenaris.';

$lang['TEXT_EXP_DESCR_142']= '<H1>Concerts:</H1> No es perdi l\'oportunitat de gaudir d\'un concert, una magnífica actuació en viu i en directe amb una ambient immillorable i la millor companyia, la de la música.';
$lang['TEXT_EXP_DESCR_141']= '<H1>Circ:</H1> Gaudeix d\'un espectacle artístic, normalment itinerant, que pot incloure acròbates, pallassos, mags, faquirs i molts més artistes.';
$lang['TEXT_EXP_DESCR_145']= '<H1>Teatre:</H1> Et convidem a gaudir d\'una obra de teatre. Diverteix-te amb un espectacle a partir de textos , idees i propostes de diversos àmbits i llenguatges escènics usant una combinació de discurs, gest, escenografia, música, so i espectacle.';

$lang['TEXT_EXP_DESCR_265']= '<H1>Monòleg:</H1> El monòleg una tècnica teatral interpretada sempre per una sola persona normalment sense cap tipus de decoració o vestuari especial . Generalment l\'intèrpret o monologuista exposa un tema o situació de la qual va fent diverses observacions sempre des d\'un punt de vista còmic amb la intenció de provocar el riure.';
$lang['TEXT_EXP_DESCR_266']= '<H1>Màgia:</H1> Deixa\'t sorprendre amb grans trucs de màgia i descobreix un espectacle que et deixarà sense paraules. Un xou que presenta als millors mags del panorama nacional, que et faran passar una nit inoblidable.';
$lang['TEXT_EXP_DESCR_267']= '<H1>Infantil:</H1> Un espectacle infantil on podràs gaudir d\'una obra de teatre, titelles, pallassos... on representessin un conte, història, aventura... aquesta divertida i original proposta d\'animació per a nens recupera l\'encant dels espectacles tradicional. Una bona manera d\'inculcar als més petits de la manera més lúdica i entretinguda l\'amor per la lectura i la fantasia.';

//Aventura.
$lang['TEXT_EXP_DESCR_29']= '<H1>RUTA BTT:</H1> Troba les millors rutes a la teva comarca, gaudeix del camp al damunt d\'una bicicleta. Aquesta experiència inclou el lloguer d\'una bicicleta.';
$lang['TEXT_EXP_DESCR_30']= '<H1>TREKING:</H1> Durant la ruta o excursió , un monitor li farà de guia i li explicarà tot el que vagi trobant en el camí.';
$lang['TEXT_EXP_DESCR_31']= '<H1>KAYAK:</H1> Passeig o descens en un caiac. Aquesta experiència inclou: ruta en caiac i el material tècnic per a realitzar l\'activitat.';
$lang['TEXT_EXP_DESCR_32']= '<H1>SENDERISME:</H1> Una experiència reconfortant. Allunyeu-vos dels sorolls de la ciutat. No oblidi roba còmoda i calçat esportiu.';
$lang['TEXT_EXP_DESCR_37']= '<H1>RAFTING:</H1> Un punt de trobada per a tots els aventurers i una experiència que no oblidaràs en molt de temps . Un dels esports d\'aventura de més de moda per a tots aquells que els agrada l\'aventura per l\'aigua.';
$lang['TEXT_EXP_DESCR_38']= '<H1>CIRCUIT MULTIAVENTURA:</H1> Sens dubte una jornada ideal! Inclou: un circuit complet de reptes en un parc d\'aventura.';
$lang['TEXT_EXP_DESCR_40']= '<H1>RUTA A CAVALL:</H1> Gaudeix d\'una ruta a cavall supervisada per un monitor especialitzat, per camins seleccionats amb cura perquè el premiat pugui gaudir de paisatges espectaculars.';
$lang['TEXT_EXP_DESCR_41']= '<H1>TIR AMB ARC:</H1> Un monitor li ensenyarà tot el que ha de saber per tensar l\'arc, col•locar la fletxa i afinar el tir perquè sigui precís. També repassaràs les postures i les tècniques bàsiques d\'aquest divertit esport.';
$lang['TEXT_EXP_DESCR_42']= '<H1>QUAD:</H1> L\'activitat consisteix en la realització d\'una ruta en quad per un entorn natural perfecte. La durada pot variar segons proveïdor.';

$lang['TEXT_EXP_DESCR_43']= '<H1>ESQUÍ O SNOWBOARD:</H1> Gaudeix d\'un dia de neu i muntanya amb una classe d\'esquí o surf de neu.';
$lang['TEXT_EXP_DESCR_44']= '<H1>AVENTURA EN ELS ARBRES:</H1> Realització de recorreguts d\'equilibri i habilitat entre els arbres en un circuit condicionat.';

$lang['TEXT_EXP_DESCR_45']= '<H1>ESPELEOLOGIA:</H1> Viu la natura des del teu interior. Iniciat en l\'aventura de l\'espeleologia, una experiència d\'allò més diferent.';
$lang['TEXT_EXP_DESCR_24']= '<H1>PITCH & PUTT:</H1> Descobreix tots els secrets del Pitch & Putt envoltat d\'un entorn natural fantàstic. ';
$lang['TEXT_EXP_DESCR_10']= '<H1>GIMNÀS:</H1> Posis en forma de la manera més sana. Una entrada gratuïta per a un dia a un gimnàs per a una persona.';
$lang['TEXT_EXP_DESCR_21']= '<H1>AQUAGYM:</H1> Apren a moure\'t dins l\'aigua amb exercicis aeròbics d\'allò més senzills.';
//$lang['TEXT_EXP_DESCR_4']= '<H1>SPINNING:</H1> Si li agrada la bicicleta, la música i l\'esport, aquesta és la teva experiència! Una classe de Spinning aconseguirà augmentar la teva adrenalina i alliberar l\'estrès acumulat durant el dia.';
//$lang['TEXT_EXP_DESCR_5']= '<H1>';
//$lang['TEXT_EXP_DESCR_176']= '<H1>';
//$lang['TEXT_EXP_DESCR_23']= '<H1>';
//$lang['TEXT_EXP_DESCR_6']= '<H1>';
$lang['TEXT_EXP_DESCR_33']= '<H1>ESCALADA:</H1> Aquesta experiència inclou una sessió d\'escalada, la supervisió d\'un monitor i el material tècnic.  ';
$lang['TEXT_EXP_DESCR_34']= '<H1>PIRAGÜISME:</H1> Viu una experiència entre paisatges inoblidables . No cal que hagis practicat aquest esport amb anterioritat, ja que les rutes són a l\'abast de tots els públics. Sent la llibertat de navegar sobre les aigües!';
$lang['TEXT_EXP_DESCR_35']= '<H1>PONTING:</H1> Un monitor et guiarà en aquesta al•lucinant experiència. Què inclou? Un salt al buit, la supervisió d\'un monitor, material tècnic, assegurança de responsabilitat civil i assistència mèdica privada.';
$lang['TEXT_EXP_DESCR_36']= '<H1>ALPINISME:</H1> Aquesta experiència inclou una sessió d\'alpinisme o escalada per a dues persones. Si és un amant dels esports de risc, gaudiràs d\'un dia ple d\'emocions adaptat al teu nivell.';

$lang['TEXT_EXP_DESCR_54']= '<H1>KARTS:</H1> Porta\'t el casc i aneu al circuit! Una experiència única per als amants del motor de competició en la qual podràs demostrar que, quan es tracta de karts, no hi ha qui el guanyi. Sentiràs la calor de l\'asfalt i la força dels revolts, però res no podràs aturar-lo fins a la línia d\'arribada.. ';
$lang['TEXT_EXP_DESCR_55']= '<H1>MINIGOLF:</H1> El golf en miniatura és una pràctica derivada del golf convencional; la gran diferència que aquest posseeix és que la teva pràctica es fa en una escala molt menor, tant en àrea com en camp actiu de golf. En general, les regles d\'aquest són les mateixes, només que el minigolf posseeix certes diferències al moment d\'executar i no cal tenir coneixements de golf ni un bon estat de forma per aconseguir-ho.';

$lang['TEXT_EXP_DESCR_114']= '<H1>ROCÒDROM:</H1> T\'agrada l\'escalada i no té temps? Tenim la solució. Passada una bona estona en un rocòdrom preparat específicament per practicar l\'escalada sense necessitat de desplaçar-te a la muntanya.';

//CATAS
$lang['TEXT_EXP_DESCR_220']= '<H1>TAST DE VINS:</H1> Gaudeix d\'una experiència plena de sensacions, en la que podràs tastar diferents tipus de vins de diverses zones vitivinícoles, vins mono varietals o de cupatge. Troba les diferències entre diferents varietats, macabeu, xarel•lo, parellada, Cabernet-Sauvignon, garnatxa, Pinot Noir...';
$lang['TEXT_EXP_DESCR_221']= '<H1>TAST DE CAVES:</H1> Triant aquest tast descobriràs el gran món del cava, tasti les exquisideses de la zona i diferents vins escumosos rosats, blancs, dolços, secs... Educa al teu paladar per distingir els diferents sabors que amaga cada copa.';
$lang['TEXT_EXP_DESCR_222']= '<H1>TAST DE CERVESES:</H1> La cervesa està plena de sabors i secrets, tant en el teu gust com en la teva elaboració, els quals podràs descobrir endinsant-se en cada un d\'ells mitjançant un tast de les cerveses.';
$lang['TEXT_EXP_DESCR_223']= '<H1>TAST D\'OLIS:</H1> Si és un amant dels bons olis, no dubti a triar aquesta experiència, en la qual podràs gaudir d\'una experiència de tast dels diferents olis segons la teva varietat o zona d\'origen. Descobrirà que la base d\'un bon plat és l\'oli.';
$lang['TEXT_EXP_DESCR_224']= '<H1>TAST DE CAFÈS:</H1> Si la teva passió és el cafè, no es perdi aquesta experiència. Podrà provar diferents varietats i descobrir que amaga aquesta llavor i les sensacions que ho pot experimentar.';
$lang['TEXT_EXP_DESCR_225']= '<H1>TAST DE FORMATGES:</H1> Una experiència en la qual podràs assaborir diferents varietats de formatges. Podrà educar el teu paladar i alhora aprendre una mica més del món de l\'elaboració dels formatges segons la zona i la procedència. Si és un amant dels formatges, amb aquest tast, gaudiràs d\'una explosió de sabors.';

//Cena para dos.
$lang['TEXT_EXP_DESCR_212']= '<H1>Cuina mediterrània:</H1> Si alguna cosa caracteritza la dieta mediterrània, és la intensitat dels teus sabors provinent de les seves terres, assaboreixi un sopar per a dues persones i gaudeixi de l\'experiència.';
$lang['TEXT_EXP_DESCR_214']= '<H1>Hamburgueseria:</H1> Assaboreixin l\'intens gust de la carn de les hamburgueses fetes a la graella o a la planxa acompanyant-lo amb la guarnició que més agradi.';
$lang['TEXT_EXP_DESCR_215']= '<H1>Vegetarià:</H1> Experimenta amb un sopar per a dues persones, els sabors més exquisits i saludables que proporciona la cuina vegetariana composta per vegetals cuinats de la manera més creativa.';
$lang['TEXT_EXP_DESCR_216']= '<H1>Pizzeria:</H1> Gaudeix d\'un fantàstic sopar per a dues persones  a l\'estil italià, amb un menú a bases de pizza acompanyat dels ingredients que més desitgesn. Margarita, quatre formatges, marinera...? Només t\'hauràs de preocupar de quina triar.';
$lang['TEXT_EXP_DESCR_219']= '<H1>Cuina Marroquina:</H1> Degusta un sopar, per a dues persones, ple de la diversitat i la riquesa que componen els plats de la cuina marroquina i la teva varietat de sabors que la caracteritzen. Hummus, tabules, cuscús...';
$lang['TEXT_EXP_DESCR_205']= '<H1>Braseria:</H1> Les braseries mantenen viu el foc d\'una llar d\'on surten tota mena de carns i verdures a la brasa. Gaudiu d\'una bona peça de carn amb la guarnició que més li agradi.';
$lang['TEXT_EXP_DESCR_206']= '<H1>Bar de tapes:</H1> Gaudeix de les tapes! A qui no li agrada teure en una terrassa i degustar les tapes més típiques del nostre país, patates braves, chocos, pop gallec, truita de patata... només t\'hauràs de preocupar per triar la beguda.';
$lang['TEXT_EXP_DESCR_209']= '<H1>Restaurant xinès:</H1> Pots gaudir d\'un menú per a dues persones on podràs degustar els teus plats més típics.';
$lang['TEXT_EXP_DESCR_217']= '<H1>Restaurant japonès:</H1> Gaudeix d\'una fantàstica experiència per a dues persones ben original, amb receptes que projecten, els sabors i el caràcter japonès, a la taula i al paladar. Podràs degustar els plats més saborosos de sushi o percebre les aromes dels condiments en el teu paladar.';
$lang['TEXT_EXP_DESCR_211']= '<H1>Marisqueria:</H1> Una experiència per gaudir amb la companyia que desitges i on podràs assaborir els peixos i mariscs més frescos del mercat.';

//ENTRADA DE FÚTBOL
$lang['TEXT_EXP_DESCR_79']= '<H1>ENTRADA DE FUTBOL:</H1> Si desitges viure un moment únic juntament amb el teu equip preferit, amb aquesta experiència gaudiràs d\'una entrada per a un partit de futbol de la Lliga BBVA de 1a o 2a divisió. Visca l\'afició d\'aquest esport! Podràs triar equip i aleatòriament se li assignarà un partit';

//NOCHE DE HOTEL
$lang['TEXT_EXP_DESCR_161']= '<H1>Nit en un hotel Rural:</H1> Gaudeix d\'una escapada per a dues persones al camp, gaudint de la tranquil•litat del paisatge i l\'entorn rural de la zona. Podràs fer llargues passejades en un entorn natural.';
$lang['TEXT_EXP_DESCR_268']= '<H1>Nit en un Hotel Design:</H1> És un amant de l\'art i el luxe? Escapis una nit a un hotel de disseny gaudint de les comoditats que aquest li ofereix dins de la zona desitjada.';
$lang['TEXT_EXP_DESCR_269']= '<H1>Nit en un Hotel Urbà:</H1> Si desitges realitzar llargs recorreguts per la ciutat descobrint els teus secrets i carrers més emblemàtics, tria una nit en un Hotel Urbà, experiència que et permetrà gaudir d\'un dia per la metròpoli tenint a l\'abast una gran varietat de bars, restaurants, botigues, parcs i tot tipus d\'activitats.';
$lang['TEXT_EXP_DESCR_270']= '<H1>Nit en un hotel de platja:</H1> Gaudeix d\'una nit al costat del mar i llargues passejades per la riba. Amb aquesta experiència gaudiràs d\'una nit en un hotel de la costa desitjada, on podràs degustar la gastronomia i gaudir d\'un dia inoblidable.';
$lang['TEXT_EXP_DESCR_171']= '<H1>Nit apartotel:</H1> Escapa\'t a la zona desitjada allotjant en un aparthotel amb totes les comoditats d\'un habitatge, podent visitar noves localitats, com a casa. Aquesta experiència et permetrà seguir amb les seves rutines dins d\'un entorn nou per descobrir en parella.';
$lang['TEXT_EXP_DESCR_271']= '<H1>Noche en Hotel Balneario:</H1> ¿Una llarga setmana de feina? Vols desconnectar del dia a dia en un hotel dedicat al benestar i la tranquil•litat? Amb aquesta experiència podràs allotjar-se en un hotel que et permetrà relaxar-te i dedicar el temps a fer aquelles activitats que vulgueu.';

//BASES
$lang['TEXT_BASES_HOGAR']= '
<p><h4>BASES LEGALS DE LA PROMOCIÓ</h4>
   <h3>"DISFRUTA UNA EXPERIENCIA ÚNICA"</h3>
</div>
<div class="element_to_pop_up_content2">
  <h2><strong> I.- ORGANITZADOR </strong></h2>
   <p><strong> La gestió de la promoció serà realitzada per SENSACIONS I EXPERIÈNCIES TPH SOCIETAT LIMITADA </strong>, proveïda i C.I.F. nombre <strong> B-55560718 </strong> amb domicili social a <strong> (43715) Valldossera (Querol, Tarragona) </strong>, d\'ara endavant denominada "TPH Màrqueting" o "TPH".
   <h2> <strong> II.- OBJECTE DE LA PROMOCIÓ </strong> </h2>
   S\'ofereix als participants majors de 18 anys residents legals a Espanya, una experiència per a dues persones. <br>
   Les bases de la present promoció les podran trobar al lloc web, www.disfrutaunaexperienciaunica.com a l\'apartat bases legals. <br>
   L\'àmbit territorial de la promoció serà Espanya, i per a clients / consumidors amb residència a Espanya. <br>
   <u> Atenció al client: </u> a
   Mitjançant l\'apartat de contacte de la web de la promoció. </p>
   
   
<h2><strong>III.- PERÍODE DE VIGÈNCIA</strong></h2>
  <p>  El període de vigència de la promoció començarà el15 d\'octubre de 2017 a 30 d\'abril de 2018, ambdós inclosos (en endavant, "El PERÍODE PROMOCIONAL"). <br>
   S\'han de tenir en compte a més, els següents terminis:
   <ul type = "disc">
     <li> <strong> Període d\'inscripció a la pàgina web www.disfrutaunaexperienciaunica.com: </strong> iniciarà l\'1 de febrer de 2018 a 30 d\'abril de 2018, ambdós inclosos. </li>
     <li> <strong> Període per gaudir de les experiències: </strong> es podran gaudir entre 1 de març de 2018 a 31 de desembre de 2018, ambdós inclosos. </li>
   </ul>
  </p>
  
<h2> <strong> IV.- DEFINICIÓ DEL PREMI I REQUISITS PER A PARTICIPAR EN LA PROMOCIÓ </strong> </h2>
  <p>  El "premi" es lliurarà als participants majors de 18 anys, residents legals a Espanya, que compleixin els requisits exigits, <strong> directament i sense sorteig </strong>. Aquells participants que renovin l\'assegurança de salut, obtindran un codi alfanumèric de conformitat amb aquestes Bases, durant el període de vigència de la promoció, que els donarà dret a obtenir <strong> una experiència per a dues persones</strong>.<br>
   El premi consisteix en un dels que es descriuen a continuació *:</p>
  
  
<ul type = "disc">
    <li> <strong> Nit d\'hotel: </strong> el regal consisteix en una nit d\'hotel per a dues persones per gastar a qualsevol ciutat d\'Espanya, de les següents categories: hotel rural, business, design, urbans, de platja, aparta-hotel, familiars, balnearis, hotels de 3,4 i 5 estrelles etc. En règim d\'allotjament. </li>
    <li> <strong> Bellesa i benestar: </strong> el regal consisteix en una experiència a escollir entre les que apareguin en el desplegable de la seva àrea personal. </li>
    <li> <strong> Restauració: </strong> el regal consisteix en un dinar o sopar a triar entre les categories que apareixeran en el desplegable de la seva àrea personal. </li>
    <li> <strong> Oci i cultura: </strong> el regal consisteix en una entrada a escollir entre les experiències d\'aquesta categoria que apareixeran en el desplegable de la seva àrea personal. </li>
    <li> <strong> Esport i aventura: </strong> el regal consisteix en el gaudi d\'una d\'experiències a triar entre les que apareixeran en el desplegable de la seva àrea personal. </li>
    <li> <strong> Cursos i tallers: </strong> el regal consisteix en el gaudi d\'una de les activitats a escollir entre les que apareguin en el desplegable de la seva àrea personal. </li>
  </ul>
  
<P> (*) Aquestes experiències estan subjectes a canvis en funció de la disponibilitat per a la realització de les mateixes segons la proximitat a les adreces facilitades pels participants a l\'hora de realitzar la seva inscripció. Els detalls de les mateixes i les seves limitacions estaran reflectits al web promocional. <br>
    El premi objecte de la present promoció queda subjecte a aquestes Bases i no serà possible substituir-lo per diners en metàl·lic. Si un participant rebutja el regal, no se li oferirà cap regal alternatiu. Els participants gaudiran de l\'experiència d\'acord amb les condicions establertes pel centre corresponent. Les experiències seran personals i intransferibles quedant terminantment prohibida la venda o comercialització d\'aquestes. <br>
    Limitacions a la participació: cada usuari tindrà dret a participar una sola vegada per cada codi. El premi és personal i intransferible. La identitat dels participants es comprovarà a través de documents oficials (DNI, NIE, etc.). <br>
    En particular, TPH es reserva el dret a desqualificar qualsevol participant que manipuli els procediments de registre i / o que violi qualsevol de les bases contingudes en el present document. Així mateix, TPH també es reserva el dret a verificar per qualsevol procediment que estimi apropiat que el participant compleix amb tots els requisits d\'aquest document i que les dades que proporcioni són exactes i verídiques. Entre altres qüestions, TPH podrá demanar documentació als participants perquè acreditin el compliment dels requisits per participar-hi. La no aportació d\'aquesta documentació podrà donar lloc a la desqualificació del participant requerit. <br>
    
    
TPH descartarà aquelles participacions que siguin abusives o fraudulentes. S\'entén, a títol enunciatiu però no limitador, que es produeix frau quan es detecta el suposat ús d\'aplicacions independents a la pàgina web www.disfrutaunaexperienciaunica.com; l\'abús de consultes al servidor i tots aquells comportaments que puguin resultar aparentment abusius i / o malintencionats. La constatació de qualsevol d\'aquestes circumstàncies durant la Promoció suposarà la desqualificació automàtica, així com la pèrdua del dret a gaudir de l\'activitat seleccionada si se li hagués atorgat. <br>
    TPH rebutjarà qualsevol sol·licitud de participació que no compleixi amb el procediment de registre o amb les condicions de participació establertes en les presents Bases. Els formularis incomplets, diferents del proporcionat a la pàgina de la promoció, incorrectament emplenats, enviats després de les dates límit, enviats a adreces errònies o que no incloguin els requisits exigits (per exemple, fotocòpia de documents oficials o menors d\'edat), es consideraran nuls i el participant perdrà el dret al regal d\'aquesta promoció. <br>
    No podran participar en la promoció les persones que hagin intervingut en la seva organització, ni qualsevol societat que hagi participat directament o inderectamente en l\'organització de la promoció, ni els seus familiars fins a tercer grau de consanguinitat, ascendents, descendents, cònjuges o parelles de fet. <br>
    TPH es reserva el dret d\'emprendre accions judicials contra aquelles persones que realitzin qualsevol tipus d\'acte susceptible de ser considerat manipulació o falsificació de la promoció. <br>
    <Strong> COSTOS NO INCLOSOS EN EL PREU: </strong> Seran per compte de la persona premiada totes les despeses de desplaçament i/o assegurances obligatòries de risc, així com qualsevol extra que es generi en ocasió de l\'assistència al lloc de l\'experiència. En el cas que es vulgui alguna millora o ampliació del premi atorgat, el participant s\'haurà de posar en contacte amb el proveïdor. <br>
 
  
<H2> <strong> V.- MECÀNICA DE LA PROMOCIÓ </strong> </h2>
  El participant haurà d\'entrar a la pàgina de la promoció www.disfrutaunaexperienciaunica.com i registrar les seves dades personals (acceptant les clàusules relatives als termes i condicions, i a protecció de dades). <br>
  Per triar de manera definitiva la seva experiència, el participant haurà de bescanviar un dels codis promocionals al Microsite. D\'aquesta manera el participant es dóna d\'alta. A més, haurà d\'emplenar el formulari de reserva amb les seves dades personals, amb tres preferències d\'experiències diferents entre si, i amb tres preferències de dates, per ordre de preferència (de més a menys) establint en cada preferència una data que di fi era, almenys , en 30 dies amb la resta de preferències. Sent la primera data acceptada en el termini de trenta dies a comptar de la data d\'inscripció en la pàgina web. <br>
  <Strong> Mètodes d\'enviament: </strong> a
  El participant s\'obliga a enviar la fotocòpia amb qualitat gràfica dels documents identificatius (DNI / NIE) ambdues cares, i del formulari contractual degudament signat a mà, adjuntant els arxius en els següents formats (.jpg .pdf .png) amb un pes total màxim de 4MB en la seva àrea personal del web de la promoció, en un termini màxim de 15 dies des de la inscripció en la promoció i / o inserció del codi. <br>
  
  
En el termini màxim de 15 dies des de la recepció de la documentació el participant rebrà per correu electrònic l\'experiència assignada, la qual haurà d\'acceptar o rebutjar en un termini màxim de 5 dies des de l\'enviament de l\'esmentat correu electrònic. Transcorregut el termini, si no es rep cap resposta per part del participant el regal quedarà desert. <br>
  
  
<H2> <strong> VI.- DISPOSICIONS GENERALS I LIMITACIÓ DE RESPONSABILITAT </strong> </h2>
  <ul type = "disc">
    <li> TPH es reserva el dret de variar algunes experiències i / o característiques de les mateixes, si cal, per causes de problemes en el subministrament, etc.. </li>
    <li> TPH no serà responsable i per això no està obligat a donar per vàlida una inscripció quan no s\'hagi finalitzat el procés de participació. </li>
    <li> Encara TPH farà tot el possible per evitar el subministrament de les dades o documents incorrectes o falses per part dels participants, no es farà responsable de la veracitat de les dades o documents que aquests facilitin. Per tant, si les dades o documents facilitats no fossin correctes o tinguessin errors, TPHno es farà responsable de no poder contactar amb els participants per comunicar-los l\'experiència que se\'ls ha estat atorgat, de no poder gestionar amb ells la reserva de l\'experiència i de totes les incidències que poguessin derivar-se de la incorrecció o falsedat de les dades o documents subministrats. </li>
    
    
<li> TPH no es responsabilitzarà en el cas que l\'experiència no pogués gaudir-se de forma satisfactòria o completa per qualsevol causa aliena al mateix o per omissió en la confirmació d\'horaris de l\'experiència. Així com tampoc es responsabilitzés pels possibles danys i perjudicis de tota naturalesa que puguin resultar de la manca de disponibilitat d\'horaris per gaudir de l\'experiència. </li>
    <li> TPH no es responsabilitzarà pels serveis que terceres empreses hagin de prestar amb motiu del premi de la present promoció. Així com tampoc ho farà per les incidències que puguin ocórrer durant el gaudi de l\'experiència, passin aquestes en el trajecte cap al lloc de l\'experiència, durant el gaudi de la mateixa, o posteriorment a la finalització d\'aquesta. </li>
    <li> El termini màxim establert per a qualsevol tipus de reclamació referida i / o relacionada amb la Promoció s\'extingeix al cap de dos mesos des de la participació en la Promoció. Després de finalitzar el termini, es perdrà tot dret a reclamació. </li>
  </ul>
  
  
<H2> <strong> VII.- ACCEPTACIÓ I DIPÒSIT DE LES BASES </strong> </h2>
  <P> La participació en aquesta promoció suposa l\'acceptació íntegra de les presents Bases. TPH es reserva el dret a variar aquestes bases si ho exigeixen les circumstàncies i dirimir tots els dubtes puguin sorgir en el no previst en les mateixes. <br>
    L\'Organitzador es reserva també el dret d\'anul·lar, prorrogar, ampliar, retallar o modificar la promoció si les circumstàncies ho obliguen, sense haver de justificar la decisió i sense que se li pugui reclamar cap tipus de responsabilitat com a conseqüència d\'això, sempre que no perjudiquin els drets adquirits pels participants. <br>
    TPH ha posat a disposició dels participants un Centre d\'atenció telefònica per atendre qualsevol dubte o consulta sobre el funcionament de la promoció. A aquests efectes, el participant podrà resoldre tots els seus dubtes en el compte de correu electrònic promo@disfrutaunaexperienciaunica.com per atendre a aquestes qüestions. El número de telèfon publicat a la web i l\'esmentat compte de correu electrònic estaran operatius fins a 2 mesos després de la finalització de la promoció. <br>
    Les bases estaran disponibles en tot moment i accessibles per a la consulta al microsite. <br>
  </P>
  
  
<H2> <strong> VIII.- PROTECCIÓ DE DADES </strong> </h2>
  Els participants coneixen i consenten que les seves dades personals (nom, cognoms, edat, adreça, residència, DNI, número de telèfon i correu electrònic) siguin incloses en un fitxer sota titularitat de <strong> SENSACIONS I EXPERIÈNCIES TPH SOCIETAT LIMITADA </strong> , amb CIF nombre B55560718, (des d\'ara, <strong> TPH MÀRQUETING </strong>) amb domicili social a (43715) Valldossera (Querol, Tarragona), qui farà servir i tractarà els mateixos, d\'acord amb la legislació vigent en matèria de protecció de dades de caràcter personal, amb la finalitat de gestionar aquesta promoció. <br>
  <Strong> TPH MÀRQUETING </strong> tenen implantades les mesures de seguretat d\'índole tècnica i organitzatives necessàries tendents a garantir la seguretat i integritat de les dades de caràcter personal facilitades pels participants. <br>
  En qualsevol moment, els participants podran exercir els seus drets d\'accés, rectificació, cancel·lació i oposició, enviant un correu electrònic a l\'adreça promo@disfrutaunaexperienciaunica.com, amb la indicació & ldquo; Promoció GAUDEIX UNA EXPERIÈNCIA & rdquo ;, consignant el seu nom i cognoms junt amb còpia del document que l\'identifiqui. <br>
  
  
<H2> <strong> IX.-  RÈGIM FISCAL </strong> </h2>
   Els premis objecte de la present promoció no es troben subjectes a retenció ni ingrés a compte de l\'Impost sobre la Renda de les persones físiques d\'acord amb l\'article 75.3.f) del Reial Decret 439/2007, pel qual s\'aprova el Reglament de l\'IRPF, per ser la seva base de retenció inferior a 300 € (tres-cents euros). <br>
  
<H2> <strong> X.- Llei aplicable i jurisdicció </strong> </h2>
   Les presents bases es regiran i interpretaran d\'acord amb el seu propi contingut i per les lleis d\'Espanya. En cas de divergència entre els participants en la Promoció i la interpretació de les presents bases per TPH, seran competents per conèixer dels litigis que es puguin plantejar els Jutjats i Tribunals de la Ciutat de Barcelona (Espanya), renunciant expressament els participants d\'aquesta Promoció al seu propi fur en cas que ho tenen. El període de reclamació de la present PROMOCIÓ finalitzarà trenta (30) dies hàbils després de la finalització, és a dir, el 31 de maig de 2018.
 <H2> <strong>ACCEPTACIÓ</strong> </h2> 

   Llegides i compreses, de forma individual i singular, totes i cadascuna de les condicions i bases de contractació específiques per a la participació en la present oferta que consten en aquest document, i en concret respecte a la política de protecció de dades de caràcter personal, àmbit temporal de l\'oferta, la seva mecànica, obligacions per a les parts, documents, exclusions, submissió i limitacions 
   <br><br>
   EL TEXT LEGAL DE REFERÈNCIA ÉS EL DE LA <a href="https://disfrutaunaexperienciaunica.com/index.php?lang=es">VERSIÓ EN CASTELLÀ</a></p>
';

//LOPD
$lang['TEXT_LOPD_POPUP'] = '

<h4>PROTECCIÓ DE DADES</h4>
<p>

D\'acord amb el que estableix la Llei orgànica 15/1999, de 13 de desembre, de protecció de dades de caràcter
personal, l\'informem que les seves dades formen part d\'un fitxer titularitat de SEGURCAIXA ADESLAS, S.A. DE
SEGUROS Y REASEGUROS, la sevafinalitat és la gestió de la relació amb els seus clients i la prestació dels serveis
contractats.</br></br>
En aquest sentit, la present promoció serà gestionada per TPH Marketing, en qualitat de prestador de serveis de
SegurCaixa Adeslas, actuant com encarregat de la tramitació, per això, TPH Marketing no tractarà ni conservarà les
seves dades per cap altra finalitat. Igualment l\'informem que TPH Marketing prendrà les mesures de seguretat,
tècniques i organitzatives, necessàries para garantir la cofidencialitat, integritat i seguretat de les dades personals.</br></br>

Li recordem que podrà exercir el seu dret d\'accés, rectficació, cancel·lació i oposició, en aquest cas, enviant un
correu electrònic a la direcció promo@disfrutaunaexperienciaunica.com, amb la indicació "Promoció Disfruta una
experiencia única", indicant el seu nom i cognoms, amb una còpia del seu DNI ambdues cares o document anàleg que l\'identfi-
qui.

</p>
';



?>