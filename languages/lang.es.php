<?php
/* 
-----------------
Language: Spanish
-----------------
*/

$lang = array();

$lang['PAGE_TITLE'] = 'Disfruta una experiencia única';

// Menu  

$lang['MENU_HOME'] = 'Inicio';
$lang['MENU_REGALO'] = 'Regalos';
$lang['MENU_COMO'] = 'Cómo solicitar el regalo';
$lang['MENU_FAQ'] = 'Preguntas frecuentes';
$lang['MENU_CONTACT'] = 'Contacto';
$lang['MENU_HOTEL'] = 'Hotel';
$lang['MENU_FUTBOL'] = 'Fútbol';
$lang['MENU_EXPE'] = 'Experiencias';



// Banner Home  

$lang['TEXT_BANNER'] = 'Vive una experiencia única al renovar tu póliza con SegurCaixa Adeslas.';
$lang['BOTON_BANNER'] = 'Descubre cómo';


// Slide Home  

$lang['TEXT_SLIDE1'] = 'Regalo';
$lang['SUBTEXT_SLIDE1'] = '<h2>FÚTBOL</h2>';
$lang['TEXT_SLIDEA'] = 'Regalo';
$lang['SUBTEXT_SLIDEA'] = '<h2>EXPERIENCIAS PARA DOS PERSONAS</h2>';
$lang['TEXT_SLIDEB'] = 'Regalo';
$lang['SUBTEXT_SLIDEB'] = '<h2>NOCHE DE HOTEL PARA DOS PERSONAS</h2>';
$lang['BOTON_SLIDE'] = 'LEER MÁS';
$lang['BANNER_RES'] = '<img class="img-responsive" src="images/slide_res_hogar.jpg" alt="SegurCaixa Adeslas">';
$lang['BANNER_PRIN'] = '<img class="img-responsive" src="images/slide_cast.jpg" alt="SegurCaixa Adeslas">';



// Panells Home  

$lang['TEXT_CUADRO1'] = '¿Cómo solicitar el regalo?';
$lang['BOTON_CUADRO1'] = 'VER';
$lang['TEXT_CUADRO2'] = 'Preguntas frecuentes';
$lang['BOTON_CUADRO2'] = 'LEER MÁS';
$lang['TEXT_CUADRO3'] = 'Contacta con nosotros';
$lang['BOTON_CUADRO3'] = 'Contacto';


// Hotel

$lang['TEXT_HOTEL1'] = 'La escapada perfecta para dos';
$lang['TEXT_HOTEL2'] = 'NOCHE DE HOTEL';
$lang['TEXT_HOTEL3'] = '<H2>CONSULTA LAS CATEGORÍAS</h2>
                       <P>Cuando dispongas de tu código regalo podrás disfrutar de una noche de hotel para dos personas. Al registrarte deberás indicarnos tres categorías de hotel en las que te gustaría alojarte, la provincia y tres fechas. Nosotros te asignaremos una de ellas en función de la disponibilidad.<br />
¿A qué esperas? Consulta las categorías que te ofrecemos.</P>';

$lang['TEXT_HOTEL4'] = 'Elige una provincia';
$lang['TEXT_HOTEL5'] = 'Elige una categoría';
$lang['TEXT_SELEC'] = 'Selecciona';


// Futbol

$lang['TEXT_FUTBOL1'] = 'Entradas de Fútbol';
$lang['TEXT_FUTBOL2'] = 'LTe regalamos una entrada de fútbol</br> de la Liga 2016-2017 para el equipo que elija.';
$lang['TEXT_FUTBOL3'] = '<H2>CONSULTE LOS EQUIPOS</h2>
                       <P>Para consultar los equipos elija primera o segunda división. La entrada se asignará de manera aleatoria para partidos en casa del equipo elegido, dependiendo de la disponibilidad de entradas del club.</P>';

$lang['TEXT_FUTBOL4'] = 'Selecciona la División *';
$lang['TEXT_FUTBOL5'] = 'Selecciona el equipo *';
$lang['TEXT_FUTBOL6'] = 'Primera División';
$lang['TEXT_FUTBOL7'] = 'Segunda División';

$lang['TEXT_TUPARTIDO'] = 'PARTIDO ASIGNADO';
$lang['TEXT_FUTBOL_CAN_1'] = 'Un momento por favor';
$lang['TEXT_FUTBOL_CAN_2'] = 'ASIGNANDO PARTIDO';
$lang['TEXT_FUTBOL_CAN_3'] = '¡IMPORTANTE!';
$lang['TEXT_FUTBOL_CAN_4'] = 'SI RECHAZA ESTE PARTIDO NO TENDRÁ MÁS OPCIONES DE DISFRUTAR DEL REGALO.';
$lang['TEXT_FUTBOL_CAN_5'] = 'VOLVER';
$lang['TEXT_FUTBOL_CAN_6'] = 'RECHAZAR';
$lang['TEXT_FUTBOL_CAN_7'] = '¡ENHORABUENA!';
$lang['TEXT_FUTBOL_CAN_8'] = 'El partido del que disfrutará es el que corresponde a la jornada';
$lang['TEXT_FUTBOL_CAN_9'] = 'el día';
$lang['TEXT_FUTBOL_CAN_10'] = 'en el';
$lang['TEXT_FUTBOL_CAN_11'] = 'entre el';
$lang['TEXT_FUTBOL_CAN_12'] = 'y el';
$lang['TEXT_FUTBOL_CAN_13'] = 'ACEPTAR';
// Experiencias

$lang['TEXT_EXPE1'] = '<H2>CONSULTA LAS EXPERIENCIAS</h2>
                       <P>Cuando dispongas de tu código regalo podrás disfrutar de una experiencia para dos personas. Al registrarte deberás indicarnos tres categorías de experiencias que te gustaría disfrutar, la provincia y tres fechas. Nosotros te asignaremos una de ellas en función de la disponibilidad.<br />
¿A qué esperas? Consulta las experiencias que te ofrecemos.</P>';
$lang['TEXT_EXPE2'] = '';
$lang['TEXT_EXPE3'] = 'Elige una categoría';
$lang['TEXT_EXPE4'] = 'Elige una experiencia';
$lang['TEXT_EXPE5'] = 'BUSCAR';
$lang['TEXT_EXPE6'] = '<h2>PILATES</h2>
                    <p>Con esta sesión descubre los beneficios del pilates. Los ejercicios de pilates fortalecen la musculatura sin aumentar excesibamente el volumen muscular, en especial los músculos del abdomen y espalda. No solo fortalece el vientre muscular sino también los ligamentos y la flexibilidad de estos.
                Aumenta la flexibilidad articular lo que se traduce en una mejora de los movimientos y de las articulaciones, también repercute en la posibilidad de lesiones reduciéndolas drásticamente.</br>
                Corrige la postura, ayudando a la consabida higiene postural y esto a su vez reduce los dolores de cuello, espalda y lumbares, no solo por la tonificación muscular del abdomen, glúteos y espalda sino también por la concienciación en aspectos como las posturas, la respiración o el equilibrio.
                También te ayudará a establecer la fuerza central y estabilidad y aumentar la conciencia cuerpo-mente.
                     </p>';
$lang['TEXT_EXPE7'] = '<h2>PISCINA</h2>
                    <p>Sumérjate en un mar de placer, envuélvate de tranquilidad y recupera tus mejores sensaciones… así te sentirás tras pasar por una piscina climatizada. También, sea la época del año que sea, podráss practicar tu deporte favorito, entrenar o simplemente divertirse llevando a cabo todo tipo de juegos acuáticos.</br>
             La experiencia consiste en una, o dos entradas, a una piscina climatizada.</p>';
$lang['TEXT_EXPE8'] = '<h2>REFLEXOLOGÍA</h2>
                    <p>La reflexología permite que el cuerpo funcione correctamente mediante la estimulación de un sistema de puntos de presión específicos, ubicados en los pies o en las manos y en las que todas las terminaciones nerviosas del sistema nervioso central, incluyendo los órganos y las estructuras internas, son reflejadas en miniatura.</p>';
$lang['TEXT_CATAS1'] = 'Restauración';
$lang['TEXT_CATAS'] = 'CATAS Y CENAS PARA DOS PERSONAS';
$lang['TEXT_ESPEC1'] = 'Ocio y Cultura';
$lang['TEXT_ESPEC'] = 'ESPECTÁCULOS PARA DOS PERSONAS';
$lang['TEXT_SALUD1'] = 'Belleza y Bienestar';
$lang['TEXT_SALUD'] = 'EXPERIENCIA PARA DOS PERSONAS';
$lang['TEXT_AVENTURA1'] = 'Deporte y Aventura';
$lang['TEXT_AVENTURA'] = 'EXPERIENCIA PARA DOS PERSONAS';
$lang['TEXT_CURSOS1'] = 'Cursos y Talleres';
$lang['TEXT_CURSOS'] = 'EXPERIENCIA PARA DOS PERSONAS';


$lang['TEXT_BELLEZA_2'] = 'Belleza y bienestar';
$lang['TEXT_CURSOS_2'] = 'Cursos y talleres';
$lang['TEXT_OCIO_2'] = 'Ocio y cultura';
$lang['TEXT_RESTAURACION_2'] = 'Restauración';
$lang['TEXT_DEPORTES_2'] = 'Deportes y aventura';


////// contacto

$lang['FORM_CONTACTO1'] = 'Introduce tu nombre';
$lang['FORM_CONTACTO2'] = 'Introduce tus apellidos';
$lang['FORM_CONTACTO3'] = 'Introduce tu e-mail';
$lang['FORM_CONTACTO4'] = 'Introduce un e-mail correcto';
$lang['FORM_CONTACTO5'] = 'Introduce una consulta';
$lang['FORM_CONTACTO6'] = 'Debes aceptar la lopd.';

// Solicitar regalo paso 1 

$lang['MENU_TIEMPO'] = 'Bienvenido, has introducido tu código el ';
$lang['MENU_TIEMPO2'] = ' .Dispones de 15 días para terminar el registro y adjuntar la documentación solicitada.';

$lang['TEXT_MIDDLE'] = '
					 <H1>Ser cliente de <b>Adeslas</b> te permitirá disfrutar de un regalo a elegir entre una noche de hotel o una experiencia para dos personas.</H1>
					 <br />
                     <p><br />
<b style="font-size:16px;">1º.</b> Debes ser titular de una póliza de salud de Adeslas y haber recibido un sms indicando la póliza de salud que renovarás próximamente.</br>
<b style="font-size:16px;">2º.</b> A continuación puedes informarte de los regalos en esta página web. No tienes que hacer nada más, solo esperar a que llegue la renovación.</br>
<b style="font-size:16px;">3º.</b> Al mes siguiente de la renovación de la póliza (enero-febrero de 2018), siempre y cuando se haya renovado, recibirás otro sms con un código para solicitar el regalo deseado en esta web. En ese momento ya podrás solicitar tu regalo indicando el código regalo, código postal y número de póliza.
                     </p>';

$lang['BUBBLE_FORM1'] = '<img class="img-responsive" src="images/bubble_es.jpg" alt="SegurCaixa Adeslas">';
$lang['TEXT_FORM'] = ' <H1>
                    Solicita tu regalo
                    </H1>
                    <p>Elige qué regalo prefieres, una noche de hotel o una experiencia. Después, solicita tu regalo introduciendo tu código postal, código regalo y número de póliza.</br>
<b>Después de introducir el código regalo dispones de 15 días para terminar el registro y enviar la documentación solicitada.</b>					
					</p>';

$lang['GLOBO_TEXT'] = 'Si ya dispones de tu</br> código, solicíta el regalo aquí.';
$lang['LABEL_CP'] = 'Código postal*';
$lang['LABEL_REGALO'] = 'Código regalo*';
$lang['LABEL_NUM'] = 'Número de póliza*';
$lang['LABEL_LEGAL'] = 'He leido y acepto las';
$lang['LABEL_CAMPOS'] = '* Todos los campos son obligatorios';
$lang['BOTON_TEXT'] = 'COMPROBAR';
$lang['ACEP_TEXT'] = 'ACEPTAR';

$lang['FORM_1'] = 'Entrar código postal.';
$lang['FORM_1_1'] = 'El código postal debe contener 5 cifras.';
$lang['FORM_1_2'] = 'Introduce tu código postal';
$lang['FORM_2'] = 'Entrar código de regalo.';
$lang['FORM_2_1'] = 'Introduce tu código de regalo';
$lang['FORM_3'] = 'Entrar la póliza.';
$lang['FORM_3_1'] = 'Introduce tu póliza.';
$lang['FORM_3_caducado'] = 'El código ya ha sido utilizado, se introdujo el ';
$lang['FORM_4'] = 'Tienes que aceptar las bases legales.';
$lang['FORM_5_1'] = 'Elige el tipo de regalo';
$lang['FORM_5_2'] = 'Hotel';
$lang['FORM_5_3'] = 'Fútbol';
$lang['FORM_5_4'] = 'Experiencias';
$lang['BOTON_ACEPTAR'] = 'ACEPTAR';

$lang['VIDEO'] = 'El navegador no soporta el video.';
/*$lang['VIDEO_TXT'] = '<a class="bla-1" href="http://disfrutaunaexperienciaunica.com/video_hogar.mp4" target="_blank"><img src="images/video.png" alt="SegurCaixa Adeslas"></a>
                   </br>
                   VER VÍDEO EXPLICATIVO';*/
$lang['VIDEO_TXT'] = ' <a href="#" data-toggle="modal" data-target="#video-modal"><img src="images/video.png" alt="SegurCaixa Adeslas"></a>
                   </br>
                   VER VÍDEO EXPLICATIVO';		   
				   
				   
$lang['POPUP_FUTBOL'] = '<h4>"DISFRUTA UNA EXPERIENCIA ÚNICA"</h4>
             <h3>CALENDARIO DE LIGA NO DISPONIBLE</h3>
<p class="red">Estimado cliente.</br>

El sorteo del calendario de liga de primera y segunda división de fútbol se realizará a mediados de julio de 2016, hasta el día 1 de Agosto no podrás proseguir con el registro.   </br>
</br>

Gracias.
</p>';


// Solicitar regalo paso 2 

$lang['TEXT_MIDDLE_PASO1'] = '<H1>
                    <span class="bold">ELIGE TRES EXPERIENCIAS</span>
                    </H1>
                    <p>Deberás elegir tres experiencias distintas por orden de preferencia.</p>';			
$lang['TEXT_MIDDLE_PASO2'] = '<H1>
                    <span class="bold">ELIGE TRES FECHAS</span>
                    </H1>
                    <p>Ahora elige tres fechas diferentes, al menos con un mes de diferencia entre sí. La primera fecha no podrás solicitarse en un plazo inferior a 30 días.</p>';						
$lang['TEXT_11'] = 'Categoria de la primera experiencia*.';
$lang['TEXT_12'] = 'Primera experiencia*.';
$lang['TEXT_13'] = 'Primera fecha (dd-mm-aaaa)*.';
$lang['TEXT_21'] = 'Categoria de la segunda experiencia*.';
$lang['TEXT_22'] = 'Segunda experiencia*.';
$lang['TEXT_23'] = 'Segunda fecha (dd-mm-aaaa)*.';
$lang['TEXT_31'] = 'Categoria de la tercera experiencia*.';
$lang['TEXT_32'] = 'Tercera experiencia*.';
$lang['TEXT_33'] = 'Tercera fecha (dd-mm-aaaa)*.';
$lang['CONFIRM'] = 'CONFIRMAR';
$lang['RECUERDE'] = 'Si vas a disfrutar del regalo acompañado, debes disponer de los datos personales de ambos en la solicitud (nombre, dirección, DNI, fecha de nacimiento, teléfono y correo electrónico).';
$lang['TEXT_REQ'] = 'Campo requerido';
$lang['TEXT_DATA1'] = 'Primera fecha (dd-mm-aaaa)*';
$lang['TEXT_DATA2'] = 'Segunda fecha (dd-mm-aaaa)*';
$lang['TEXT_DATA3'] = 'Tercera fecha (dd-mm-aaaa)*';
$lang['TEXT_IRSOLO'] = '<h3>¿ESTÁS SEGURO DE QUE DESEAS IR SÓLO?</h3>
                 <h4>¡RECUERDA! </h4>
                <p><strong>Si decides ir solo, no podrás incorporar a ningún acompañante</strong></p>
                <div>
                <p><strong><span class="orange"></span></strong></p>';


// Solicitar regalo paso 2 ( HOTEL )

$lang['TEXT_MIDDLE_PASO1_HOTEL'] = '<H1>
                    <span class="bold">ELIJE TRES OPCIONES</span>
                    </H1>
                    <p>Deberás elegir la provincia o provincias que desees. Después tres categorías distintas por orden de preferencia.</p>';
$lang['TEXT_11_HOTEL'] = 'Elige una provincia*';
$lang['TEXT_12_HOTEL'] = 'Elige una categoría*';
$lang['TEXT_21_HOTEL'] = 'Elige una provincia*';
$lang['TEXT_22_HOTEL'] = 'Elige una categoría*';
$lang['TEXT_31_HOTEL'] = 'Elige una provincia*';
$lang['TEXT_32_HOTEL'] = 'Elige una categoría*';

// Solicitar regalo paso 2 ( FUTBOL )

$lang['TEXT_MIDDLE_PASO1_FUTBOL'] = '<H1>
                    <span class="bold">ELIGE UNA CATEGORÍA Y UN EQUIPO</span>
                    </H1>
                    <p>La entrada se asignará de manera aleatoria para partidos en casa del equipo elegido, dependiendo de la disponibilidad de entradas del club.</p>';
$lang['RECUERDE_FUTBOL'] = '';
$lang['TEXT_FUTBOL_LIGA'] = 'Elija una categoría*';
$lang['TEXT_FUTBOL_EQUIPO'] = 'Elija un equipo*';




// Solicitar regalo paso 3 

$lang['TEXT3_1'] = '<h1>FORMULARIO DE REGISTRO</h1>
                  <p>Sigue con el proceso de canjeo introduciendo los datos, de la persona que quiera disfrutar de la experiencia, en el formulario. </p>';
$lang['TEXT3_2'] = '';
$lang['TEXT3_3'] = 'Nombre*';
$lang['TEXT3_4'] = 'Apellidos*';
$lang['TEXT3_5'] = 'Fecha de nacimiento (dd-mm-aaaa)*';
$lang['TEXT3_6'] = 'DNI/NIE*';
$lang['TEXT3_7'] = 'Correo electrónico*';
$lang['TEXT3_8'] = 'Teléfono*';
$lang['TEXT3_9'] = 'Dirección*';
$lang['TEXT3_10'] = 'Población*';
$lang['TEXT3_11'] = 'Código postal*';
$lang['TEXT3_12'] = 'Provincia*';
$lang['TEXT3_13'] = '* Todos los campos son obligatorios';
$lang['TEXT3_14'] = 'IR SOLO';
$lang['TEXT3_15'] = 'IR ACOMPAÑADO';
$lang['TEXT3_16'] = 'Si vas a disfrutar del regalo acompañado, debes disponer de los datos personales de ambos en la solicitud (nombre, dirección, DNI, fecha de nacimiento, teléfono y correo electrónico)';
$lang['TEXT3_17'] = 'Campo nombre requerido';
$lang['TEXT3_18'] = 'Campo apellidos requerido';
$lang['TEXT3_19'] = 'Campo DNI requerido';
$lang['TEXT3_20'] = 'Campo correo electrónico requerido';
$lang['TEXT3_21'] = 'Campo teléfono requerido';
$lang['TEXT3_22'] = 'Campo dirección postal requerido';
$lang['TEXT3_23'] = 'Campo código postal requerido';
$lang['TEXT3_24'] = 'Sexo*';
$lang['TEXT3_25'] = 'Hombre';
$lang['TEXT3_26'] = 'Mujer';

/////PASO3////     
$lang['TEXT_FORM3_1'] = 'El DNI/NIE';     
$lang['TEXT_FORM3_2'] = 'ya ha participado en esta promoción el día';     
$lang['TEXT_FORM3_3'] = 'Sólamente se permite una participación por persona.';  
$lang['TEXT_FORM3_4'] = 'Introduce tu nombre';  
$lang['TEXT_FORM3_5'] = 'Introduce tus apellidos';  
$lang['TEXT_FORM3_6'] = 'Introduce tu sexo';  
$lang['TEXT_FORM3_7'] = 'Introduce tu fecha de nacimiento';  
$lang['TEXT_FORM3_7_menor'] = 'El participante ha de ser mayor de edad';
$lang['TEXT_FORM3_8'] = 'Introduce tu DNI';  
$lang['TEXT_FORM3_9'] = 'Introduce tu e-mail';  
$lang['TEXT_FORM3_10'] = 'Introduce tu teléfono de contacto';  
$lang['TEXT_FORM3_10_format'] = 'Introduce un teléfono correcto (9 dígitos, sin espacios)';  
$lang['TEXT_FORM3_11'] = 'Introduce tu dirección'; 
$lang['TEXT_FORM3_12'] = 'Introduce tu municipio'; 
$lang['TEXT_FORM3_13'] = 'Introduce tu provincia'; 
$lang['TEXT_FORM3_14'] = 'Introduce tu código postal'; 
//ACOMPAÑANTE///
$lang['TEXT_FORM3_15'] = 'Introduce el nombre de su acompañante'; 
$lang['TEXT_FORM3_16'] = 'Introduce los apellidos de su acompañante'; 
$lang['TEXT_FORM3_17'] = 'Selecciona el sexo de su acompañante'; 
$lang['TEXT_FORM3_18'] = 'Introduce la fecha de nacimiento de su acompañante'; 
$lang['TEXT_FORM3_19'] = 'Selecciona si su acompañante es menor de edad'; 
$lang['TEXT_FORM3_20'] = 'Introduce el DNI de su acompañante'; 
$lang['TEXT_FORM3_21'] = 'Introduce el e-mail de su acompañante, si no tiene, introduzca el suyo'; 
$lang['TEXT_FORM3_22'] = 'Introduce el teléfono de su acompañante'; 
$lang['TEXT_FORM3_23'] = 'Introduce la dirección de su acompañante'; 
$lang['TEXT_FORM3_24'] = 'Introduce la provincia de su acompañante'; 
$lang['TEXT_FORM3_25'] = 'Introduce la población de su acompañante'; 
$lang['TEXT_FORM3_26'] = 'Introduce el código postal de su acompañante'; 
$lang['TEXT_FORM3_27'] = 'DNI Incorrecto'; 
$lang['TEXT_FORM3_28'] = 'Introduce tu consulta'; 

$lang['TEXT_MAIL_USER_22_TIT'] = 'disfrutaunaexperienciaunica.com';
$lang['TEXT_MAIL_USER_22_CONT'] = '
<p>Hemos recibido tu documentaci&oacute;n y la hemos enviado a nuestro Departamento de validaci&oacute;n.</p>
<p>Si cumples con los requisitos de la promoci&oacute;n, el Departamento de reservas contactar&aacute; contigo para la asignaci&oacute;n del regalo deseado.</p>
<p>Gracias por participar en &quot;DISFRUTA UNA EXPERIENCIA &Uacute;NICA&quot;.</p>';

$lang['TEXT_MAIL_USER_1_TIT'] = 'Registro en disfrutaunaexperienciaunica.com';
$lang['TEXT_MAIL_USER_1'] = 'Gracias por registrarte en la promoci&oacute;n &quot;Disfruta una experiencia &uacute;nica&quot;. <br />
<br />
Ahora debes enviar tu formulario contractual de participaci&oacute;n que puede encontrar '; 
$lang['TEXT_MAIL_USER_2'] = ' firmado a mano por los participantes junto con una fotocopia con calidad gr&aacute;fica de los documentos identificativos (DNI/NIE) ambas caras, del titular y acompa&ntilde;ante, en un plazo m&aacute;ximo de 15 d&iacute;as desde el registro en la promoci&oacute;n. <br />
'; 
$lang['TEXT_MAIL_USER_3'] = '
<br />
Adjuntando un archivo en los siguientes formatos (.jpg .pdf .png .rar   .zip) con un peso total m&aacute;ximo de 4mb siguiendo el siguiente enlace:
<br />
'; 
$lang['TEXT_MAIL_USER_4'] = '
<br />
Te recordamos que el formulario debe estar firmado por el participante o los participantes, en caso de ir acompa&ntilde;ado, para que sea v&aacute;lido. <br />
<br />
Gracias por tu participaci&oacute;n. <br />
<br />
Un saludo.
'; 


// Sube archivos
$lang['TEXTARCH_1'] = 'Archivo';
$lang['TEXTARCH_2'] = 'subido correctamente.';
$lang['TEXTARCH_3'] = 'Hemos recibido correctamente tu documentación, si la documentación es correcta, nos pondremos en contacto contigo en un plazo máximo de 15 días mediante correo electrónico para informarte de la experiencia confirmada.';
//errores Sube archivos
$lang['TEXTARCH_4'] = 'Error al subir el archivo';
$lang['TEXTARCH_5'] = 'Error tipo de archivo';
$lang['TEXTARCH_6'] = 'Ningun archivo seleccionado.';
$lang['TEXTARCH_7'] = 'Error al subir los archivos.';


// Solicitar regalo paso 3 Acompañante

$lang['TEXT_ACO1'] = '<h1>DATOS DEL ACOMPAÑANTE</h1><p>Sigue con el proceso de canjeo introduciendo los datos del acompañante en el formulario.</p>';
$lang['TEXT_ACO2'] = '¿Es menor de edad?*';
$lang['TEXT_ACO3'] = 'Descarga la autorización de menores';
$lang['TEXT_ACO4'] = 'Descargar';
$lang['TEXT_ACO5'] = 'Los siguientes datos pueden ser los mismos que los del participante';
$lang['TEXT_ACO6'] = '&nbsp;Los mismos que el participante';
$lang['TEXT_ACO7'] = 'FINALIZAR';
$lang['TEXT3_27'] = 'Sí con DNI';
$lang['TEXT3_28'] = 'Sí sin DNI';
$lang['TEXT3_29'] = 'No';


// Solicitar regalo paso 4 (La majoria de camps son els del paso 3) 

$lang['TEXT3_41'] = '<h1>FORMULARIO DE CONTACTO</h1>
                  <p>Si tienes alguna duda puedes rellenar este formulario y mandarnos tu consulta.</p>';
$lang['TEXT3_42'] = 'FINALIZAR';


// Solicitar regalo paso 4 (La majoria de camps son els del paso 3) 

$lang['TEXT4_1'] = '<H1>Ahora completa los datos de su acompañante</h1>
                   <P>Estos datos son necesarios para seguir con el proceso.</P>';
$lang['TEXT4_2'] = 'FINALIZAR';


// FAQ 

$lang['TEXT_FAQ'] = '
<H2>PREGUNTAS FRECUENTES</H2>
       <div class="container">
          <div class="accordion">
            <dl>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger">1. ¿En qué consiste exactamente el regalo?</a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                <p>El regalo consiste en una experiencia a elegir entre las siguientes categorías: Experiencias para dos personas (belleza y bienestar, cursos y talleres, deporte y aventura, restauración y ocio y cultura) y Noche de Hotel para dos personas.
Cuando dispongas de tu código regalo deberás indicarnos qué categoría prefieres, la provincia y tres fechas. Nosotros te asignaremos una de ellas en función de la disponibilidad. 

				</p>
              </dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger">2. ¿Qué tengo que hacer para conseguir el regalo?</a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                <p>Una vez la renovación sea efectiva, recibirás otro sms (enero-febrero 2018) con un código para solicitar gratuitamente la experiencia deseada en esta web.
</p>
              </dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger">3. ¿Quién puede disfrutar del regalo?</a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                <p>El regalo va dirigido al titular de la póliza, pero puede ser disfrutado por cualquier persona que éste eligiese. Se deberá registrar la persona que disfrutará del regalo. Se deberá facilitar mediante documento oficial la identidad de los participantes y siempre que (los acompañantes) no sean mayores de edad bajo autorización de su tutor.</p>
              </dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger">4. ¿Puedo cambiar el regalo elegido una vez confirmado?</a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                <p>Una vez confirmado el regalo solicitado no será posible hacer ningún tipo de modificación.</p>
              </dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger">5. ¿Qué documentación debo enviar para formalizar la entrega del regalo?</a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                <p>El cliente debe enviar fotocopia con calidad gráfica de los documentos identificativos (DNI/NIE) ambas caras, de él y del acompañante (si la experiencia elegida es para dos personas) junto con el formulario contractual de participación firmado a mano.</p>
              </dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger">6. ¿Cuánto tiempo tengo para enviar la documentación?</a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                <p>El plazo para enviar la documentación y formulario es de 15 días desde la fecha de la solicitud.</p>
              </dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger">7. Si ya he enviado mi documentación, ¿qué debo hacer ahora?</a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                <p>Una vez recibimos tu documentación, el departamento de reservas, en otro plazo máximo de 15 días, contactará con el cliente mediante correo electrónico para informarle del regalo asignado. A continuación, el cliente tendrá un plazo de 5 días para responder, aceptando o rechazando el regalo.</p>
              </dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger">8. ¿Cómo y cuándo recibiré el bono para disfrutar del regalo?</a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                <p>5 días laborables antes de la fecha de disfrute del regalo confirmado se contactará con el cliente para hacerle entrega de la documentación necesaria.</p>
              </dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger">9. ¿Qué debo entregar al establecimiento para disfrutar del regalo?</a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                <p>El mismo día de la experiencia, se deberá entregar al establecimiento la documentación recibida (BONO) y presentar su DNI.</p>
              </dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger">10. ¿Tengo que abonar algo?</a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                <p>La experiencia es totalmente gratuita, aunque serán por cuenta de la persona premiada todos los gastos de desplazamiento, así como cualquier extra que se genere con ocasión de la asistencia al lugar de la experiencia y del alojamiento en el hotel.</p>
              </dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger">11. ¿Qué plazo tengo para solicitar el regalo?</a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                <p>El plazo de solicitud del regalo es hasta el 30 de abril de 2018.</p>
              </dd>
              <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger">12. ¿Qué plazo tengo para disfrutar del regalo?</a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                <p>El regalo podrá disfrutarse entre el 1 de marzo del 2018 y el 31 de diciembre de 2018, ambos inclusive.
</p>
              </dd>
			  <dt>
                <a href="#accordion1" aria-expanded="false" aria-controls="accordion1" class="accordion-title accordionTitle js-accordionTrigger">13. ¿Si tengo alguna duda/consulta qué debo hacer?</a>
              </dt>
              <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">
                <p>Tienes a tu disposición el formulario de contacto en el apartado de contacto.</p>
              </dd>
            </dl>
          </div>
        </div>';


// Contacto (La majoria de camps son els del paso 3) 

$lang['CONT_1'] = '<h2>FORMULARIO DE CONTACTO</h2>
                  <p>Si tienes alguna duda puedes rellenar este formulario y mandarnos tu consulta.</p>';
$lang['CONT_2'] = '';
$lang['CONT_3'] = 'ENVIAR';
$lang['CONT_4'] = '<h1>SU CONSULTA HA SIDO ENVIADA CORRECTAMENTE</h1>
<p>Con la mayor brevedad posible te daremos una respuesta.</p>';
$lang['CONT_5'] = 'VOLVER';
$lang['CONT_6'] = 'Comentarios';
$lang['CONT_7'] = 'He leido y acepto las ';
$lang['TEXT_LOPD'] = 'LOPD';


// Footer  

$lang['TEXT_BASES'] = 'Bases legales';
$lang['TEXT_POL_COOK'] = 'Política de cookies';
$lang['TEXT_POL_PRIV'] = 'Política de privacidad';


// Tu experiencia  

$lang['TEXT_TUEXPE1'] = '<h1>FORMULARIO FINAL</h1>
                                <p>Ahora debes enviar este formulario contractual de participación firmado a mano y escaneado, junto con una fotocopia con calidad gráfica de los documentos identificativos (DNI/NIE) ambas caras, en un plazo máximo de 15 días desde la inscripción en la promoción.</p>';
$lang['TEXT_TUEXPE2'] = 'EXPERIENCIAS';
$lang['TEXT_TUEXPE3'] = 'Primera experiencia';
$lang['TEXT_TUEXPE4'] = 'Primera fecha';
$lang['TEXT_TUEXPE5'] = 'Segunda experiencia';
$lang['TEXT_TUEXPE6'] = 'Segunda fecha';
$lang['TEXT_TUEXPE7'] = 'Tercera experiencia';
$lang['TEXT_TUEXPE8'] = 'Tercera fecha';
$lang['TEXT_TUEXPE9'] = 'DATOS DEL PARTICIPANTE';
$lang['TEXT_TUEXPE10'] = 'Nombre';
$lang['TEXT_TUEXPE11'] = 'Apellidos';
$lang['TEXT_TUEXPE12'] = 'Fecha de nacimiento';
$lang['TEXT_TUEXPE13'] = 'DNI/NIE';
$lang['TEXT_TUEXPE14'] = 'Correo electrónico';
$lang['TEXT_TUEXPE15'] = 'Teléfono';
$lang['TEXT_TUEXPE16'] = 'Dirección postal';
$lang['TEXT_TUEXPE17'] = 'Población';
$lang['TEXT_TUEXPE18'] = 'Código postal';
$lang['TEXT_TUEXPE19'] = 'Provincia';
$lang['TEXT_TUEXPE20'] = 'DATOS DEL ACOMPAÑANTE';
$lang['TEXT_TUEXPE21'] = '<h2>ENVIAR DOCUMENTACIÓN</h2>';
$lang['TEXT_EXPE23'] = 'Adjuntar DNI';
$lang['TEXT_EXPE24'] = 'Adjuntar formulario Firmado';
$lang['TEXT_EXPE25'] = 'Adjuntar Autoritzación de menores'; 
$lang['TEXT_EXPE26'] = 'Adjuntar DNI del acompañante';   
$lang['TEXT_TUEXPE27'] = '<strong>CORREO POSTAL ORDINARIO</strong></br>
								Promoción “Disfruta una experiencia única” </br>
								Apartado de correos nº 91 </br>
								CP 08720 </br>
								Vilafranca del Penedès. </br>
								(Barcelona) </br>';    
$lang['TEXT_TUEXPE28'] = '';    
$lang['TEXT_TUEXPE29'] = 'Guardar en PDF';  

/////////////////////////ERRORES FORM//////////////////////
/////PASO1////
$lang['TEXT_FORM1_1'] = 'El código';      
$lang['TEXT_FORM1_2'] = 'ya ha sido usado'; 
$lang['TEXT_FORM1_2_2'] = 'es erróneo'; 
  
$lang['TEXT_FORM1_3'] = 'El número de póliza';  
$lang['TEXT_FORM1_4'] = 'es erroneo, revisa tu número de póliza'; 
 
$lang['TEXT_FORM1_5'] = 'La póliza';  
$lang['TEXT_FORM1_6'] = 'ya ha sido usada';  

$lang['TEXT_FORM1_7'] = 'Introduce un código postal'; 
$lang['TEXT_FORM1_8'] = 'Introduce un código postal correcto'; 

$lang['TEXT_FORM1_9'] = 'Selecciona el tipo de regalo'; 
$lang['TEXT_FORM1_10'] = 'Introduce tu número de póliza';

/////PASO2////   
$lang['TEXT_FORM2_1'] = 'Selecciona una categoría';   
$lang['TEXT_FORM2_1_1'] = 'Selecciona una provincia';  
$lang['TEXT_FORM2_2'] = 'Selecciona una experiencia';     
$lang['TEXT_FORM2_2_2'] = 'Selecciona una categoría';  
$lang['TEXT_FORM2_3'] = 'Selecciona una fecha';  

$lang['TEXT_FORM2_1_exp'] = 'Introduce 3 experiencias distintas'; 

$lang['TEXT_FORM2_3_comp'] = 'Selecciona una fecha con mínimo 30 días después de la segunda fecha elegida'; 
$lang['TEXT_FORM2_2_comp'] = 'Selecciona una fecha con mínimo 30 días después de la primera fecha elegida'; 
$lang['TEXT_FORM2_1_comp'] = 'Selecciona una fecha con mínimo 30 días después de hoy';   

/////PASO3////     
$lang['TEXT_FORM3_1'] = 'El DNI/NIE';     
$lang['TEXT_FORM3_2'] = 'ya ha participado en esta promoción el día';     
$lang['TEXT_FORM3_3'] = 'Sólamente se permite una participación por persona.';  
$lang['TEXT_FORM3_4'] = 'Introduce tu nombre';  
$lang['TEXT_FORM3_5'] = 'Introduce tus apellidos';  
$lang['TEXT_FORM3_6'] = 'Introduce tu sexo';  
$lang['TEXT_FORM3_7'] = 'Introduce tu fecha de nacimiento';  
$lang['TEXT_FORM3_7_menor'] = 'El participante ha de ser mayor de edad';
$lang['TEXT_FORM3_8'] = 'Introduce tu DNI';  
$lang['TEXT_FORM3_9'] = 'Introduce tu e-mail';  
$lang['TEXT_FORM3_9_format'] = 'Introduce un e-mail correcto';
$lang['TEXT_FORM3_10'] = 'Introduce tu teléfono de contacto';  
$lang['TEXT_FORM3_10_format'] = 'Introduce un teléfono correcto (9 dígitos, sin espacios)';  
$lang['TEXT_FORM3_11'] = 'Introduce tu dirección'; 
$lang['TEXT_FORM3_12'] = 'Introduce tu municipio'; 
$lang['TEXT_FORM3_13'] = 'Introduce tu provincia'; 
$lang['TEXT_FORM3_14'] = 'Introduce tu código postal'; 
//ACOMPAÑANTE///
$lang['TEXT_FORM3_15'] = 'Introduce el nombre de su acompañante'; 
$lang['TEXT_FORM3_16'] = 'Introduce los apellidos de su acompañante'; 
$lang['TEXT_FORM3_17'] = 'Selecciona el sexo de su acompañante'; 
$lang['TEXT_FORM3_18'] = 'Introduce la fecha de nacimiento de su acompañante'; 
$lang['TEXT_FORM3_18_menor'] = 'Acompañante menor de edad, la fecha de nacimiento es incorrecta'; 
$lang['TEXT_FORM3_18_mayor'] = 'Acompañante mayor de edad, la fecha de nacimiento es incorrecta'; 
$lang['TEXT_FORM3_19'] = 'Selecciona si su acompañante es menor de edad'; 
$lang['TEXT_FORM3_20'] = 'Introduce el DNI de su acompañante'; 
$lang['TEXT_FORM3_20_format'] = 'DNI incorrecto'; 
$lang['TEXT_FORM3_21'] = 'Introduce el e-mail de su acompañante, si no tiene, introduzca el suyo'; 
$lang['TEXT_FORM3_22'] = 'Introduce el teléfono de su acompañante'; 
$lang['TEXT_FORM3_23'] = 'Introduce la dirección de su acompañante'; 
$lang['TEXT_FORM3_24'] = 'Introduce la provincia de su acompañante'; 
$lang['TEXT_FORM3_25'] = 'Introduce la población de su acompañante'; 
$lang['TEXT_FORM3_26'] = 'Introduce el código postal de su acompañante'; 
$lang['TEXT_FORM3_27'] = 'DNI Incorrecto'; 


// experiencia selecciona hotel mail recordatorio

$lang['MAIL_RECORDATOTIO_1'] = '<p>Le recordamos su elección:</p>'; 
$lang['MAIL_RECORDATOTIO_2'] = '<p>Para la fecha:</p>'; 
$lang['MAIL_RECORDATOTIO_3'] = '<p>Cinco días antes de la fecha, el departamento de reservas contactará contigo para que nos confirme la asistencia y te enviarán su número de reserva y/o localizador, que deberás indicar en la recepción del hotel a su llegada.</p>'; 
$lang['MAIL_RECORDATOTIO_3_ocio'] = '<p>Cinco días antes de la fecha, el departamento de reservas contactará contigo para que nos confirmes tu asistencia y te enviarán tu número de reserva y/o localizador, que deberás indicar en el establecimiento a su llegada.</p>'; 
$lang['MAIL_RECORDATOTIO_4'] = '<p>Para más información consulta las bases de la promoción.</p>'; 
$lang['MAIL_RECORDATOTIO_5'] = '<p>Gracias por su participación.</p>'; 

//Experiences description - the number is the crm id for this experience.

//Salud y Bienestar.
$lang['TEXT_EXP_DESCR_80'] = '<H1>Mesoterapia Facial y vitaminas:</H1> La mesoterapia es una técnica de aplicación de medicamentos a través de microagujas en forma local, es decir, sobre el área a tratar. En el caso del envejecimiento facial se aplican, precisamente, en el rostro.';
$lang['TEXT_EXP_DESCR_4'] = '<H1>Spinning:</H1> El spinning es un ejercicio aeróbico y de piernas principalmente, donde el monitor o profesor puede mediante el cambio de la frecuencia de pedaleo y de la resistencia al movimiento, realizar todo tipo de intensidades. Es una gimnasia muy adaptable al nivel del alumno, pudiendo ser tan sencillas como un paseo tranquilo o agotador hasta para un ciclista profesional.';
$lang['TEXT_EXP_DESCR_86'] = '<H1>Tratamiento Anti-Age:</H1> Se realiza una limpieza de la piel profunda con el peeling, especialmente en las zonas de las arrugas. Luego se continúa con un pequeño drenaje facial para favorecer el trabajo en las zonas mas problemáticas. Se aplica una máscara especial sobre la superficie de la cara para realizar la fotoporación y electroporación y favorecer la penetración de los principios activos a la piel. ';
$lang['TEXT_EXP_DESCR_1'] = '<H1>Masaje:</H1> El masaje es una forma de manipulación de las capas superficiales y profundas de los músculos del cuerpo utilizando varias técnicas, para mejorar sus funciones, ayudar en procesos de curación, disminuir la actividad refleja de los músculos, inhibir la excitabilidad motoneuronal, promover la relajación y el bienestar y como actividad recreativa.';
$lang['TEXT_EXP_DESCR_3'] = '<H1>Reflexología:</H1> La reflexología es la disciplina que promueve el tratamiento de diversas afecciones a través de masajes en las manos o en los pies. De acuerdo a esta doctrina, los masajes que se aplican en ciertos puntos del cuerpo provocan un reflejo en otras regiones corporales, permitiendo el alivio de un malestar.';
$lang['TEXT_EXP_DESCR_5'] = '<H1>Aeróbic:</H1> Conocida como una de las disciplinas deportivas más populares y divertidas de la actualidad, el aerobic es una actividad física que implica una alta dosis de gasto de energía a través del baile o del seguimiento de consignas y rutinas bailables que ponen en movimiento al organismo y permiten gastar muchas calorías.';
$lang['TEXT_EXP_DESCR_176'] = '<H1>Pilates:</H1> Pilates es un método de ejercicio y movimiento físico diseñado para estirar, fortalecer y equilibrar el cuerpo.';
$lang['TEXT_EXP_DESCR_177'] = '<H1>Yoga:</H1> Yoga es un término sánscrito que puede traducirse como “esfuerzo” o “unión”. El concepto tiene dos grandes aplicaciones: por un lado, se trata del conjunto de disciplinas físicas y mentales que se originaron en la India y que buscan alcanzar la perfección espiritual y la unión con lo absoluto; por otra parte, el yoga está formado por las prácticas modernas que derivan de la mencionada tradición hindú y que promueven el dominio del cuerpo y una mayor capacidad de concentración.';
$lang['TEXT_EXP_DESCR_23'] = '<H1>Reiki:</H1> El reiki puede considerarse como una disciplina pseudo-científica, una técnica o una modalidad de medicina alternativa que busca la sanación del paciente mediante la imposición de manos. Se basa en la canalización de lo que sus practicantes denominan energía vital universal para lograr el equilibrio.';
$lang['TEXT_EXP_DESCR_6'] = '<H1>Zumba:</H1> La zumba es un tipo de actividad física (fitness) basada en ritmos y músicas lationamericanas. Su origen es Colombia y está extendida por todo el mundo. Las coreografías de zumba incluyen ritmos como la samba, la salsa, el reggaeton, la cumbia, el merengue y el mambo.';
$lang['TEXT_EXP_DESCR_7'] = '<H1>Natación:</H1> Seguro que has oído miles de veces que la natación es ideal para la salud: buena para la espalda, poco agresiva con las articulaciones, una forma de entrenar todo el cuerpo, apto para todo tipo de personas... Relájese y disfrute de una sesión de entrenamiento.';
$lang['TEXT_EXP_DESCR_8'] = '<H1>Buceo:</H1> El buceo es una actividad subacuática que puede realizarse con fines recreativos, investigativos o deportivos. Consiste en ingresar al agua y sumergir la totalidad del cuerpo, por lo que, en general, se desarrolla con la ayuda de algún tipo de equipamiento que permite al buzo no tener que salir a la superficie a respirar.';
$lang['TEXT_EXP_DESCR_9'] = '<H1>Urban dance:</H1> La expresión “baile urbano” o “danza urbana”, se refiere a ciertos eventos, performances y creaciones coreográficas, donde los bailarines bailan en pleno espacio público, como lo puede ser, en la calle, en una plaza, centro comercial, etc etc';
$lang['TEXT_EXP_DESCR_11'] = '<H1>Ballet:</H1> La danza clásica, también conocida como ballet, es un tipo de danza que cuenta con distintas técnicas y movimientos específicos. Ballet es, además, el nombre que permite hacer referencia a la pieza musical compuesta para ser interpretada a través de la danza.';
$lang['TEXT_EXP_DESCR_12'] = '<H1>Flamenco:</H1> El flamenco es un estilo de música y danza propio de Andalucía, Extremadura y Murcia. Sus principales facetas son el cante, el toque y el baile, contando también con sus propias tradiciones y normas';
$lang['TEXT_EXP_DESCR_15'] = '<H1>Manicura:</H1> Unas manos bonitas, suaves y bien cuidadas son sin duda uno  de los símbolos más representativos de la feminidad.  A veces no les prestamos mucha atención, pero para tener y mantener unas manos bonitas es imprescindible cuidarlas, y por lo tanto realizar una buena manicura.';
$lang['TEXT_EXP_DESCR_14'] = '<H1>Pedicura:</H1> Unos pies bonitos, suaves y bien cuidados son sin duda uno  de los símbolos más representativos de la feminidad.  A veces no les prestamos mucha atención, pero para tener y mantener unos pies bonitos es imprescindible cuidarlos, y por lo tanto realizar una buena pedicura.';
$lang['TEXT_EXP_DESCR_16'] = '<H1>Cambio de  look:</H1> No hace falta variar de color de pelo y de peinado todos los meses. La suma de pequeñas modificaciones en nuestra apariencia puede ayudarnos a lograr un verdadero cambio de imagen, que muchas buscamos pero no nos animamos a hacer. Un experto te ayudará y te asesorará para que tu cambio sea personalizado.';
$lang['TEXT_EXP_DESCR_2'] = '<H1>Spa:</H1> Permítete evadirte de la vida cotidiana y adéntrate en un exquisito santuario de la relajación. Una terapia con agua con piscinas, jacuzzis, hidromasajes, chorros, sauna, etc etc';
$lang['TEXT_EXP_DESCR_17'] = '<H1>Fishing pedicure:</H1> ¡Siente tus pies frescos y sanos con la pedicura más natural! Sumerge tus dos pies en agua tibia y... ¡cientos de peces empezarán a trabajar limpiando tus impurezas! Un trabajo exfoliante que te hará sentir un pequeño cosquilleo al principio y un placentero masaje final.
Gracias a los peces Garra rufa, originarios de Turquía, verás cómo las impurezas y restos de piel muerta desaparecen de tus pies. Su boca en forma de ventosa y sin dientes realiza un tratamiento exfoliante a la vez que masajean, mejorando la circulación, ayudándote a relajarte e hidratar la piel.';
$lang['TEXT_EXP_DESCR_18'] = '<H1>Maquillaje profesional:</H1> Descubre todo lo que puede llegar a hacer con ayuda de un profesional, aprenderás técnicas más avanzadas y te dejarán como una autentica profesional.';
$lang['TEXT_EXP_DESCR_202'] = '<H1>Tinte y permanente de pestañas:</H1> Atraer todas las miradas esta temporada, consigue embellecer tu  mirada más aún dándole un toque de sensualidad, desenfado y juventud, para que en todo momento estés perfecta de día y de noche, vaya donde vaya.';

//Espectáculos.
$lang['TEXT_EXP_DESCR_63'] = '<H1>Cine:</H1> Vaya al cine y disfruta del estreno que elija. Visualice una película en una gran pantalla y disfrute de su sonido envolvente, solo tendráss que pensar si las palomitas las quiere dulces o saladas. La duración será, la de la película escogida.';
$lang['TEXT_EXP_DESCR_144'] = '<H1>Musical:</H1> Disfruta de un musical en el que la acción se desenvuelve con la secciones cantadas y bailadas. Una forma de teatro que combina música, canción, diálogo y baile, y que se representa en grandes escenarios.';

$lang['TEXT_EXP_DESCR_142'] = '<H1>Conciertos:</H1> Disfruta de un concierto, una magnifica actuación en vivo y en directo. Tararee sus canciones al ritmo que se mueve escuchando la música.';
$lang['TEXT_EXP_DESCR_141'] = '<H1>Circo:</H1> Disfruta de un espectáculo artístico, normalmente itinerante, que se puede incluir acróbatas, payasos, magos, tragafuegos y muchos más artistas. Incluso en ocasiones con animales.';
$lang['TEXT_EXP_DESCR_145'] = '<H1>Teatro:</H1> Diviértate con el arte escénico que relaciona la actuación y que representa historias frente a una audiencia usando una combinación de discurso, gesto, escenografía, música, sonido y espectáculo.';

$lang['TEXT_EXP_DESCR_265'] = '<H1>Monologo:</H1> El monólogo cómico es una técnica teatral interpretada siempre por una sola persona normalmente de pie y sin ningún tipo de decoración o vestuario especial. Normalmente el intérprete o monologuista expone un tema o situación de la que va haciendo diversas observaciones siempre desde un punto de vista cómico con la intención de provocar la risa';
$lang['TEXT_EXP_DESCR_266'] = '<H1>Mágia:</H1> Déjate sorprender con grandes trucos de magia y descubra un espectáculo que le dejará sin palabras. Un show que presenta a los mejores magos del panorama nacional, que le harán pasar una noche inolvidable.';
$lang['TEXT_EXP_DESCR_267'] = '<H1>Infantil:</H1> Un espectáculo infantil donde podrás disfrutar de una obra de teatro, títeres o marionetas donde representaran un cuento, esta divertida y original propuesta de animación para niños recupera el encanto de lo tradicional. Una buena manera de inculcar a los más pequeños de la manera más lúdica y entretenida el amor por la lectura y la fantasía.';

//Aventura.
$lang['TEXT_EXP_DESCR_29'] = '<H1>RUTA BTT:</H1> Encuentra las mejores rutas por su provincia, disfruta del campo encima de una bicicleta. Esta experiencia incluye el alquiler de una bicicleta.';
$lang['TEXT_EXP_DESCR_30'] = '<H1>TREKING:</H1> Durante la ruta o excursión, un monitor hará de guía y te explicará todo lo que vaya encontrando en el camino.';
$lang['TEXT_EXP_DESCR_31'] = '<H1>KAYAK:</H1> Paseo o descenso en una kayak. Esta experiencia incluye: ruta en kayak y el material técnico para realizar la actividad. ';
$lang['TEXT_EXP_DESCR_32'] = '<H1>SENDERISMO:</H1> Una experiencia reconfortante. Aléjese de los ruidos de la ciudad. No olvide ropa cómoda y calzado deportivo.';
$lang['TEXT_EXP_DESCR_37'] = '<H1>RAFTING:</H1> Un punto de encuentro para todos los aventureros y una experiencia que no olvidará en mucho tiempo. Uno de los deportes de aventura de más de moda para todos aquellos que les gusta la aventura por el agua.';
$lang['TEXT_EXP_DESCR_38'] = '<H1>CIRCUITO MULTIAVENTURA:</H1> ¡Sin duda una jornada ideal! Incluye: un circuito completo de retos en un parque de aventura.';
$lang['TEXT_EXP_DESCR_40'] = '<H1>RUTA A CABALLO:</H1> Disfruta de una ruta a caballo supervisada por un monitor especializado, por caminos especialmente elegidos para que el premiado pueda disfrutar de paisajes espectaculares.';
$lang['TEXT_EXP_DESCR_41'] = '<H1>TIRO CON ARCO:</H1> Un monitor te enseñará todo lo que debe saber para tensar el arco, colocar la flecha y afinar el tiro para que sea certero. También repasará las posturas y las técnicas básicas de este divertido deporte.';
$lang['TEXT_EXP_DESCR_42'] = '<H1>QUAD:</H1> La actividad consiste en la realización de una ruta en quad por un entorno natural perfecto. La duración puede variar según proveedor.';

$lang['TEXT_EXP_DESCR_43'] = '<H1>ESQUÍ O SNOWBOARD:</H1> Disfruta de un día en la nieve con una clase de esquí o snowboard.';
$lang['TEXT_EXP_DESCR_44'] = '<H1>ARBORISMO:</H1> Realización de recorridos de equilibrio y habilidad entre los árboles en un circuito acondicionado.';

$lang['TEXT_EXP_DESCR_45'] = '<H1>ESPELEOLOGÍA:</H1> Vive la naturaleza desde su interior. Iniciase en la aventura de la espeleología, una experiencia de lo más diferente.';
$lang['TEXT_EXP_DESCR_24'] = '<H1>PITCH & PUTT:</H1> Descubre todos los secretos del Pitch & Putt rodeado de un entorno natural fantástico.';
$lang['TEXT_EXP_DESCR_10'] = '<H1>GIMNASIO:</H1> Vuélvate a poner en forma de la manera más sana. Una entrada gratuita para un día a un gimnasio para una persona.';
$lang['TEXT_EXP_DESCR_21'] = '<H1>AQUAGYM:</H1> Aprende a moverte dentro del agua con ejercicios aeróbicos de lo más sencillos.';
//$lang['TEXT_EXP_DESCR_4'] = '<H1>SPINNING:</H1> Si le gusta la bicicleta, la música y el deporte, esta es su experiencia! Una clase de Spinning conseguirá aumentar su adrenalina y liberar el estrés acumulado durante el día.';
//$lang['TEXT_EXP_DESCR_5'] = '<H1>AERÓBIC:</H1> Baile, salte, y muévase al ritmo de la música! Una clase de Aeróbic conseguirá mover todos los músculos de su cuerpo disfrutando de un momento divertido en pareja.';
//$lang['TEXT_EXP_DESCR_176'] = '<H1>PILATES O YOGA:</H1> Si desea desconectar de un día ajetreado, con una sesión de Pilates o Yoga alcanzará la plena armonía gracias a un buen ambiente, desconectando del día a día y centrando toda la atención al propio cuerpo y mente.';
//$lang['TEXT_EXP_DESCR_23'] = '<H1>REIKI:</H1> La energía desbloquea las zonas del receptor en las que el Ki ha quedado enquistado y ha contribuido a crearle un mal anímico o de salud. Con esta experiencia gozará de un  espacio de tiempo dedicado únicamente a usted reestableciendo el equilibrio de su cuerpo con la ayuda de las manos de un experto.';
//$lang['TEXT_EXP_DESCR_6'] = '<H1>ZUMBA:</H1> Si desea tonificar su cuerpo, quemar calorías, elevar las pulsaciones  al ritmo de la música, con esta experiencia lo conseguirá con disfrutando de un momento lleno de diversión!';
$lang['TEXT_EXP_DESCR_33'] = '<H1>ESCALADA:</H1> Esta experiencia incluye una sesión de escalada, la supervisión de un monitor y el material técnico.';
$lang['TEXT_EXP_DESCR_34'] = '<H1>PIRAGÜISMO:</H1> Disfruta de una experiencia entre los paisajes inolvidables de la zona. No es necesario que haya practicado este deporte con anterioridad, puesto que las rutas están al alcance de todos los públicos. ¡Sienta la libertad de navegar sobre las aguas!';
$lang['TEXT_EXP_DESCR_35'] = '<H1>PUENTING:</H1> Un monitor te guiará en esta alucinante experiencia. ¿Qué incluye? un salto al vacío, la supervisión de un monitor, material técnico, seguro de responsabilidad civil y asistencia médica privada.';
$lang['TEXT_EXP_DESCR_36'] = '<H1>ALPINISMO:</H1> Esta experiencia incluye una sesión de alpinismo o escalada para dos personas. Si es un amante de los deportes de riesgo, disfrutará de un día lleno de emociones adaptado a su nivel.';

$lang['TEXT_EXP_DESCR_54'] = '<H1>KARTS:</H1> ¡Ponte el casco y vaya al circuito! Una experiencia única para los amantes del motor de competición en la que podrás demostrar que, cuando se trata de karts, no hay quien le gane. Sentirá el calor del asfalto y la fuerza de las curvas, pero nada podrás detenerte hasta llegar a la línea de meta.';
$lang['TEXT_EXP_DESCR_55'] = '<H1>MINIGOLF:</H1> El golf en miniatura es una práctica derivada del golf convencional; la gran diferencia que éste posee es que su práctica se hace en una escala mucho menor, tanto en área como en campo activo de golf. Por lo general, las reglas de éste son las misma, solo que el minigolf posee ciertas diferencias al momento de ejecutarlo y no hace falta tener conocimientos de golf ni un buen estado de forma para lograrlo.';

$lang['TEXT_EXP_DESCR_114'] = '<H1>ROCÓDROMO:</H1> ¿LTe gusta la escalada y no tiene tiempo? Tenemos la solución. Pasa un buen rato en un rocódromo preparado específicamente para practicar la escalada sin necesidad de desplazarte a la montaña...';

//CATAS
$lang['TEXT_EXP_DESCR_220'] = '<H1>CATA DE VINOS:</H1> Disfruta de una experiencia llena de sensaciones, en la que podráss probar diferentes tipos de vinos de la zona, tales como de las variedades macabeu, xarl•lo, parellada, rioja, verdejo, albariño, Cabernet-Sauvignon, Garnacha, Pinot Noir,…';
$lang['TEXT_EXP_DESCR_221'] = '<H1>CATA DE CAVAS:</H1> Eligiendo esta cata descubrirás el gran mundo del cava, probando las exquisiteces de la zona y de sus diferentes vinos espumosos rosados, blancos, dulces, secos,… Enseñe a su paladar a diferenciar los diferentes sabores que esconde cada copa.';
$lang['TEXT_EXP_DESCR_222'] = '<H1>CATA DE CERVEZAS:</H1> La cerveza está llena de sabores y secretos de elaboración, los cuales podrás descubrir adentrándose en cada uno de ellos mediante la cata de las cervezas típicas de la zona. Te enamorará.';
$lang['TEXT_EXP_DESCR_223'] = '<H1>CATA DE ACEITES:</H1> Si eres un amante del buen aliño, no dudes en elegir esta experiencia, en la que podrás gozar de una experiencia de cata de los diferentes aceites según la variedad de los  aceituna que lo compone. Descubrirá que la base de un buen plato es el aceite.';
$lang['TEXT_EXP_DESCR_224'] = '<H1>CATA DE CAFÉS:</H1> Si tu pasión es el café, no se pierda esta experiencia. Podrás probar diferentes variedades que esconde esta semilla y los rincones a los que le puede llevar.';
$lang['TEXT_EXP_DESCR_225'] = '<H1>CATA DE QUESOS:</H1> Disfruta de una experiencia en la que podráss saborear diferentes variedades de quesos. Podrás educar a su paladar y a la vez aprender un poco más del mundo de la elaboración de los quesos según la zona y la procedencia. Si eres un amante de los quesos con esta cata gozará de una explosión de sabores disfrutando de un buen momento en pareja.';

//Cena para dos.
$lang['TEXT_EXP_DESCR_212'] = '<H1>Cocina mediterránea:</H1> Si algo caracteriza a la dieta mediterránea es la intensidad de sus sabores proveniente de sus tierras, deléitese con una cena para dos personas y disfruta de la experiencia.';
$lang['TEXT_EXP_DESCR_214'] = '<H1>Hamburguesería:</H1> Saboread el intenso sabor de la carne de hamburguesa hechas a la parrilla o a la plancha  acompañándolo con la guarnición que más guste.';
$lang['TEXT_EXP_DESCR_215'] = '<H1>Vegetariano:</H1> Experimentad con una cena para dos, los sabores más exquisitos y saludables que proporciona la cocina vegetariana compuesta por vegetales cocinados de lo más creativos.';
$lang['TEXT_EXP_DESCR_216'] = '<H1>Pizzería:</H1> Disfrutad de una fantástica cena para 2 al más puro estilo italiano, con un menú pizza acompañado de los ingredientes que más desean. Margarita, cuatro quesos, campestre, etc…? Solo te tendrás que preocupar de cual escoger.';
$lang['TEXT_EXP_DESCR_219'] = '<H1>Cocina Marroquí:</H1> Deléitese con una cena para dos, de la diversidad y riqueza que componen los platos de la cocina marroquí y su variedad de sabores que la caracterizan, hummus, tabules, cuscús, etc';
$lang['TEXT_EXP_DESCR_205'] = '<H1>Braseria:</H1> La cocina tradicional  especializada en carnes a la brasa. Disfruta de un buen trozo de carne con la guarnición que más te guste.';
$lang['TEXT_EXP_DESCR_206'] = '<H1>Bar de tapas:</H1> Disfruta del tapeo! A quien no le gusta sentarse en una terraza y degustar las tapas más típicas de nuestro país, patatas bravas, chocos, pulpo gallego, tortilla de patata, etc solo tendrás de preocuparse que te apetece de beber.';
$lang['TEXT_EXP_DESCR_209'] = '<H1>Restaurante chino:</H1> Podrás disfrutar de un menú para dos personas donde podrás degustar sus platos más típicos.';
$lang['TEXT_EXP_DESCR_217'] = '<H1>Restaurante japonés:</H1> Disfruta de una fantástica experiencia para dos personas original, con recetas que proyectan los sabores y el carácter japonés a su mesa.
        Podrás degustar los platos más sabrosos de sushi o percibir los aromas de los condimentos en su paladar.';
$lang['TEXT_EXP_DESCR_211'] = '<H1>Marisquería:</H1> Experiencia a disfrutar para dos personas dónde podrás saborear los pescados y mariscos más frescos del mercado .';

//ENTRADA DE FÚTBOL
$lang['TEXT_EXP_DESCR_79'] = '<H1>ENTRADA DE FÚTBOL:</H1> Si deseas vivir un momento único junto con su equipo preferido, con esta experiencia gozará de una entrada para un partido de fútbol de la Liga BBVA de 1ª o 2ª división. ¡Viva la afición de este deporte! Podrás elegir equipo y aleatoriamente se le asignará un partido.';

//NOCHE DE HOTEL
$lang['TEXT_EXP_DESCR_161'] = '<H1>Noche en Hotel Rural:</H1> Disfruta de una escapada para dos personas al campo, gozando de la tranquilidad del paisaje y el entorno rural de la zona. Podrás dar largos paseos con el único ruido de la naturaleza.';
$lang['TEXT_EXP_DESCR_268'] = '<H1>Noche en Hotel Design:</H1> ¿Eres un amante del arte y el lujo? Escápese una noche a un hotel de diseño disfrutando de las comodidades que éste le ofrece dentro de la zona deseada.';
$lang['TEXT_EXP_DESCR_269'] = '<H1>Noche en Hotel Urbano:</H1> Si deseas realizar largos recorridos por la ciudad descubriendo sus secretos y calles más emblemáticas, elija una noche en Hotel Urbano, experiencia que le permitirá disfrutar de un día por la metrópoli teniendo al alcance una gran variedad de bares, restaurantes, tiendas, parques y todo tipo de actividades.';
$lang['TEXT_EXP_DESCR_270'] = '<H1>Noche en Hotel de playa:</H1> Disfruta de una noche junto al mar y largos paseos por la orilla. Con esta experiencia disfrutará de una noche en un hotel de la costa deseada, dónde podrás degustar la gastronomía y disfrutar de un día inolvidable.';
$lang['TEXT_EXP_DESCR_171'] = '<H1>Noche en Aparta-hotel:</H1> Escápate a la zona deseada alojándose en un Aparta-hotel con todas las comodidades de una vivienda, pudiendo visitar nuevas localidades como en casa. Esta experiencia le permitirá seguir con sus rutinas dentro de un entorno nuevo por descubrir en pareja.';
$lang['TEXT_EXP_DESCR_271'] = '<H1>Noche en Hotel Balneario:</H1> ¿Una larga semana de trabajo? ¿Desea desconectar del día a día en un hotel dedicado al bienestar y la tranquilidad? Con esta experiencia podrás alojarse en un hotel que te permitirá relajarte y dedicar el tiempo a realizar aquellas actividades que deseas.';

//BASES
$lang['TEXT_BASES_HOGAR']= '


            
             <h4>BASES LEGALES DE LA PROMOCIÓN</h4>
             <h3>"DISFRUTA UNA EXPERIENCIA ÚNICA"</h3>        
          </div>
           <div class="element_to_pop_up_content2">
<!-- Bases legales --> 
 <H2><strong>I.- ORGANIZADOR</strong></H2>
  <strong>La gestión de la promoción será realizada por SENSACIONES  Y EXPERIENCIAS TPH SOCIEDAD LIMITADA</strong>, provista e C.I.F. número&nbsp;<strong>B-55560718</strong>&nbsp;con  domicilio social en&nbsp;<strong>(43715) Valldossera (Querol, Tarragona)</strong>, en lo  sucesivo denominada &ldquo;TPH Marketing&rdquo; o &ldquo;TPH&rdquo;.<br>

<H2><strong>II.- OBJETO DE LA PROMOCIÓN</strong></H2>

  Se ofrece a los participantes mayores de 18 años residentes  legales en España, una experiencia para dos personas.<strong>&nbsp;</strong><br>
  Las bases de la presente promoción las podrán encontrar en el sitio web,  www.disfrutaunaexperienciaunica.com en el apartado bases legales.<br>
  El ámbito territorial de la promoción será España, y para clientes/consumidores  con residencia en España.<br>
  <u>Atención al cliente:</u><br>
  Mediante el apartado de contacto de la web de la promoción.<br>

<H2><strong>III.- PERIODO DE VIGENCIA</strong></H2>

  El periodo de vigencia de la promoción comenzará el<strong>&nbsp;</strong>15  de octubre de 2017 a 30 de abril de 2018, ambos inclusive (en adelante, &ldquo;El  PERIODO PROMOCIONAL&rdquo;).<br>
  Deben tenerse en cuenta además, los siguientes plazos:
<ul type="disc">
  <li><strong>Periodo       de inscripción en la página web www.disfrutaunaexperienciaunica.com:&nbsp;</strong>iniciará       el 1 de febrero de 2018 a 30 de abril de 2018, ambos inclusive.</li>
  <li><strong>Periodo       para disfrutar de las experiencias:</strong>&nbsp;se podrán disfrutar entre 1       de marzo de 2018 a 31 de diciembre de 2018, ambos inclusive.</li>
</ul>


<H2><strong>IV.- DEFINICIÓN DEL PREMIO Y REQUISITOS PARA PARTICIPAR  EN LA PROMOCIÓN</strong></H2>


  El &ldquo;premio&rdquo; se entregará a los participantes mayores de 18  años, residentes legales en España, que cumplan los requisitos exigidos,&nbsp;<strong>directamente  y sin sorteo</strong>. Aquellos participantes que renueven el seguro de salud,  obtendrán un código alfanumérico de conformidad con estas Bases, durante el  periodo de vigencia de la promoción, que les dará derecho a obtener&nbsp;<strong>una  experiencia para dos personas</strong><br>
  El premio consiste en uno de los que se describen a continuación*:
<ul type="disc">
  <li><strong>Noche       de hotel:</strong>&nbsp;el regalo consiste en una noche de hotel para dos       personas para canjear en cualquier ciudad de España, de las siguientes       categorías: hotel rural, business, design, urbanos, de playa, aparta-hotel,       familiares, balnearios, hoteles de 3,4 y 5 estrellas etc. En régimen de       alojamiento.</li>
  <li><strong>Belleza       y bienestar:</strong>&nbsp;el regalo consiste en una experiencia a escoger       entre las que aparezcan en el desplegable de su área personal. </li>
  <li><strong>Restauración:</strong> el regalo consiste en una comida o cena a elegir entre las categorías que       aparecerán en el desplegable de su área personal.</li>
  <li><strong>Ocio       y cultura:</strong>&nbsp;el regalo consiste en una entrada a escoger  entre las experiencias de esta categoría       que aparecerán en el desplegable de su área personal. </li>
  <li><strong>Deporte       y aventura:</strong>&nbsp;el regalo consiste en el disfrute de una de experiencias       a elegir entre las que aparecerán en el desplegable de su área personal.</li>
  <li><strong>Cursos       y talleres:</strong>&nbsp;el regalo consiste en el disfrute de una de las       actividades a escoger entre las que aparezcan en el desplegable de su área       personal.</li>
</ul>
<p>(*) Estas experiencias estarán sujetas a cambios en función  de la disponibilidad para la realización de las mismas según la cercanía a las  direcciones facilitadas por los participantes a la hora de realizar su  inscripción. Los detalles de las mismas y sus limitaciones estarán reflejados  en la web promocional.&nbsp;&nbsp;<br>
  El premio objeto de la presente promoción queda sujeto a estas Bases y no será  posible sustituirlo por dinero en metálico. Si un participante rechaza el  regalo, no se le ofrecerá ningún regalo alternativo. Los participantes  disfrutarán de la experiencia de acuerdo con las condiciones establecidas por  el centro correspondiente. Las experiencias serán personales e intransferibles  quedando terminantemente prohibida la venta o comercialización de éstas.<br>
  Limitaciones a la participación: cada usuario tendrá derecho a participar una  sola vez por cada código. El premio es personal e intransferible. La identidad  de los participantes se comprobará a través de documentos oﬁciales (DNI, NIE).<br>
  En particular, TPH<strong>&nbsp;</strong>&nbsp;se reserva el derecho a descalificar a  cualquier participante que manipule los procedimientos de registro y/o que  viole cualquiera de las bases contenidas en el presente documento. Asimismo,  TPH<strong>&nbsp;</strong>&nbsp;también se reserva el derecho a verificar por cualquier  procedimiento que estime apropiado que el participante cumple con todos los  requisitos de este documento y que los datos que proporcione al<strong>&nbsp;</strong>promotor<strong>&nbsp;</strong>son  exactos y verídicos. Entre otras cuestiones, TPH<strong>&nbsp;</strong>podrá pedir  documentación a los participantes para que acrediten el cumplimiento de los  requisitos para participar. La no aportación de esta documentación podrá dar  lugar a la descalificación del participante requerido.<br>
  TPH<strong>&nbsp;</strong>descartará aquellas participaciones que sean abusivas o  fraudulentas. Se entenderá, a título enunciativo pero no limitativo, que se  produce fraude cuando se detecta el supuesto uso de aplicaciones independientes  a la página web www.disfrutaunaexperienciaunica.com; el abuso de consultas al  servidor y todos aquellos comportamientos que puedan resultar aparentemente  abusivos y/o malintencionados. La constatación de cualquiera de estas  circunstancias durante la Promoción supondrá la descalificación automática, así  como la pérdida del derecho a disfrutar de la actividad seleccionada si se le  hubiere otorgado.<br>
  TPH rechazará cualquier solicitud de participación que no cumpla con el  procedimiento de registro o con las condiciones de participación establecidas  en las presentes Bases. Los formularios incompletos, distintos del  proporcionado en la página de la promoción, incorrectamente cumplimentados,  enviados después de las fechas límite, enviados a direcciones erróneas o que no  incluyan los requisitos exigidos (por ejemplo, fotocópia de documentos  oficiales o menores de edad), se considerarán nulos y el participante perderá  el derecho al regalo de esta promoción.<br>
  No podrán participar en la promoción las personas que hayan intervenido en su  organización, ni cualquier sociedad que haya participado directa o  inderectamente en la organización de la promoción, ni sus familiares hasta  tercer grado de consanguineidad, ascendientes, descendientes, cónyuges o  parejas de hecho.<br>
  TPH<strong>&nbsp;</strong>&nbsp;se reserva el derecho de emprender acciones judiciales contra  aquellas personas que realicen cualquier tipo de acto susceptible de ser  considerado manipulación o falsificación de la Promoción.<br>
  <strong>COSTES NO INCLUIDOS EN EL PRECIO:</strong>&nbsp;Serán por cuenta de la persona  premiada todos los gastos de desplazamientoy/o seguros obligatorios de riesgo, así como cualquier extra que se  genere con ocasión de la asistencia al lugar de la experiencia. En el caso de  que se desee alguna mejora o ampliación del premio otorgado, el participante  deberá ponerse en contacto con el proveedor.<br>
  
  <H2><strong>V.- MECÁNICA DE LA PROMOCIÓN</strong></H2>

  El participante deberá entrar en la página de la promoción  www.disfrutaunaexperienciaunica.com y registrar sus datos personales (aceptando  las cláusulas relativas a los términos y condiciones, y a protección de datos).<br>
  Para elegir de manera deﬁnitiva su experiencia, el participante deberá canjear  uno de los códigos promocionales en el Microsite. De este modo el participante  se da de alta. Además, deberá cumplimentar el formulario de reserva con sus  datos personales, con tres preferencias de experiencias diferentes entre sí, y  con tres preferencias de fechas, por orden de preferencia (de mayor a menor)  estableciendo en cada preferencia una fecha que diﬁera, al menos, en 30 días  con el resto de preferencias. Siendo la primera fecha aceptada en el plazo de  treinta días a contar desde la fecha de inscripción en la página web.<br>
  <strong>Métodos de envío:</strong><br>
  El participante se obliga a enviar la fotocopia con calidad gráfica de los  documentos identificativos (DNI/NIE) ambas caras y del formulario contractual debidamente  firmado a mano, adjuntando los&nbsp; archivos en los siguientes formatos (.jpg  .pdf .png) con un peso total máximo de 4mb en su área personal de la web de la  promoción, en un plazo máximo de 15 días desde la inscripción en la promoción y/o inserción del código.<br>
  En el plazo máximo de 15 días desde la recepción de la documentación el  participante recibirá mediante correo electrónico la experiencia asignada, la  cual deberá aceptar o rechazar en un plazo máximo de 5 días desde el envío del  mencionado e-mail. Transcurrido el plazo, si no se recibe respuesta alguna por  parte del participante el regalo quedará desierto.<br>
  
  
    <H2><strong>VI.- DISPOSICIONES GENERALES Y LIMITACION DE  RESPONSABILIDAD</strong></H2>

<ul type="disc">
  <li>TPH<strong>&nbsp;</strong>&nbsp;se       reserva el derecho de variar algunas experiencias y/o características de       las mismas, si fuera necesario, por causas de problemas en el suministro,       etc.</li>
  <li>TPH<strong>&nbsp;</strong>&nbsp;no       será responsable y por ello no está obligado a dar por válida una       inscripción cuando no se haya finalizado el proceso de participación.</li>
  <li>Aunque       TPH<strong>&nbsp;</strong>&nbsp;hará todo lo posible por evitar el suministro de los       datos o documentos incorrectos o falsos por parte de los participantes, no       se hará responsable de la veracidad de los datos o documentos que éstos       faciliten. Por consiguiente, si los datos o documentos facilitados no       fueran correctos o tuvieran errores, TPH<strong>&nbsp;</strong>no se hará       responsable de no poder contactar con los participantes para comunicarles       la experiencia que se les ha sido otorgado, de no poder gestionar con       ellos la reserva de la experiencia y de todas las incidencias que pudieran       derivarse de la incorrección o falsedad de los datos o documentos       suministrados.</li>
</ul>
<ul type="disc">
  <li>TPH<strong>&nbsp;</strong>&nbsp;no       se responsabilizará en el caso de que la experiencia no pudiera       disfrutarse de forma satisfactoria o completa por cualquier causa ajena al       mismo o por omisión en la confirmación de horarios de la experiencia. Así       como tampoco se responsabilizara por los posibles daños y perjuicios de       toda naturaleza que puedan resultar de la falta de disponibilidad de       horarios para disfrutar de la experiencia.&nbsp;</li>
</ul>
<ul type="disc">
  <li>TPH<strong>&nbsp;</strong>&nbsp;no       se responsabilizará por los servicios que terceras empresas deban prestar       con motivo del premio de la presente promoción. Así como tampoco lo hará       por las incidencias que puedan ocurrir durante el disfrute de la       experiencia, ocurran éstas en el trayecto hacia el lugar de la       experiencia, durante el disfrute de la misma, o posteriormente a la       finalización de esta.</li>
</ul>
<ul type="disc">
  <li>El       plazo máximo establecido para cualquier tipo de reclamación referida y/o       relacionada con la Promoción se extingue a los dos meses desde la       participación en la Promoción. Tras finalizar dicho plazo, se perderá todo       derecho a reclamación.</li>
</ul>

    <H2><strong>VII.- ACEPTACIÓN Y DEPÓSITO DE LAS BASES</strong></H2>


<p>La participación en la presente Promoción supone la  aceptación íntegra de las presentes Bases. TPH<strong>&nbsp;</strong>&nbsp;se reserva el  derecho a variar estas bases si así lo exigieran las circunstancias y dirimir  cuantas dudas pudieran surgir en lo no previsto en las mismas.<br>
  El Organizador se reserva también el derecho de anular, prorrogar, ampliar,  recortar o modificar la promoción si las circunstancias lo obligan, sin tener  que justificar la decisión y sin que se le pueda reclamar ningún tipo de  responsabilidad como consecuencia de ello, siempre que no perjudiquen los  derechos adquiridos por los participantes.<br>
  TPH&nbsp; ha puesto a disposición de los participantes un Call center para  atender cualquier duda o consulta sobre el funcionamiento de la Promoción. A  tales efectos, el participante podrá resolver todas sus dudas en la cuenta de  correo electrónico promo@disfrutaunaexperienciaunica.com para atender a dichas  cuestiones. El número de teléfono publicado en la web y la mencionada cuenta de  correo electrónico estarán operativos hasta 2 meses tras la finalización de la  Promoción.<br>
  Las bases estarán disponibles en todo momento y accesibles para su consulta en  el microsite.<br>
  
  <H2><strong>VIII.- PROTECCIÓN DE DATOS</strong></H2>
  
  Los participantes conocen y consienten que sus datos  personales (nombre, apellidos, edad, dirección, residencia, DNI, número de  teléfono y correo electrónico) sean incluidos en un fichero bajo titularidad  de&nbsp;<strong>SENSACIONES Y EXPERIENCIAS TPH SOCIEDAD LIMITADA</strong>, provista de  C.I.F. número B55560718, (en adelante,&nbsp;<strong>TPH MARKETING</strong>) con domicilio  social en (43715) Valldossera (Querol, Tarragona), quién usará y tratará los  mismos, conforme a la legislación vigente en materia de protección de datos de  carácter personal, con la finalidad de gestionar la presente Promoción.<br>
  <strong>TPH MARKETING</strong>&nbsp;tienen implantadas las medidas de seguridad de índole  técnica y organizativas necesarias tendentes a garantizar la seguridad e  integridad de los datos de carácter personal facilitados por los participantes.<br>
  En cualquier momento, los participantes podrán ejercer sus derechos de acceso,  rectificación, cancelación y oposición, remitiendo un correo electrónico a la  dirección promo@disfrutaunaexperienciaunica.com, con la indicación &ldquo;Promoción  DISFRUTA UNA EXPERIENCIA&rdquo;, consignando su nombre y apellidos junto con copia  del documento que le identifique.<br>
  
    <H2><strong>IX.- REGIMEN FISCAL</strong></H2>
  
  Los premios objeto de la presente promoción no se encuentran  sujetos a retención ni a ingreso a cuenta del Impuesto sobre la Renta de las  Personas Físicas de acuerdo con el artículo 75.3.f) del Real Decreto 439/2007,  por el que se aprueba el Reglamento del IRPF, por ser su base de retención  inferior a 300 € (trescientos euros).<br>
  
  <H2><strong>X.- LEY APLICABLE Y JURISDICCIÓN</strong></H2>
  
  Las presentes bases se regirán e interpretarán de acuerdo  con su propio contenido y por las leyes de España. En caso de divergencia entre  los participantes en la Promoción y la interpretación de las presentes bases  por TPH, serán competentes para conocer de los litigios que&nbsp; puedan  plantearse los Juzgados y Tribunales de la Ciudad de Barcelona (España),  renunciando expresamente los participantes de esta Promoción a su propio fuero  en caso de que lo tuvieren. El período de reclamación de la presente PROMOCIÓN  finalizará treinta (30) días hábiles después de su finalización, es decir, el  31 de mayo de 2018.<br>
  <strong>ACEPTACIÓN</strong><br>
  Leídas y comprendidas, de forma individual y singular, todas  y cada una de las condiciones y bases de contratación específicas para la  participación en la presente oferta que constan en este documento, y en  concreto respecto a la política de Protección de Datos de carácter personal,  ámbito temporal de la oferta, su mecánica, obligaciones para las partes,  documentos, exclusiones, sumisión y limitaciones</p>
';

//LOPD
$lang['TEXT_LOPD_POPUP'] = '

<h4>PROTECCIÓN DE DATOS</h4>
              <p><p>En cumplimiento de lo establecido en la Ley Orgánica  15/1999, de 13 de diciembre, de protección de datos de carácter personal, le  informamos que sus datos forman parte de un fichero titularidad de SEGURCAIXA  ADESLAS, S.A. DE SEGUROS Y REASEGUROS, cuya finalidad es la gestión de la  relación con sus clientes y la prestación de los servicios contratados.<br>
  <br>
  En este sentido, la presente promoción será gestionada por TPH Marketing, en su  calidad de prestador de servicios de SegurCaixa Adeslas, actuando como  encargado del tratamiento, por lo que, TPH Marketing no tratará ni conservará  sus datos con ninguna otra finalidad. Igualmente le informamos que TPH  Marketing adoptará las medidas de seguridad, técnicas y organizativas,  necesarias para garantizar la confidencialidad, integridad y seguridad de los  datos personales.<br>
  <br>
  Le recordamos que podrá ejercer sus derechos de acceso, rectificación,  cancelación y oposición, en este caso, remitiendo un correo electrónico a la  dirección&nbsp;<a href="mailto:promo@disfrutaunaexperienciaunica.com">promo@disfrutaunaexperienciaunica.com</a>,  con la indicación &ldquo;Promoción Disfruta una experiencia única&rdquo;, consignando su  nombre y apellidos junto con copia de DNI ambas caras o documento análogo que le  identifique.</p>
</p>      

';




?>