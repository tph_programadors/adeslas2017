<?php include_once 'common.php';?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    
    <title><?php echo $lang['PAGE_TITLE']; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="assets/css/modern-business.css" rel="stylesheet">
    <link href="assets/css/cookies.css" rel="stylesheet" type="text/css">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="assets/js/jquery.js"></script>
    
    <!-- Script Show Hide Categories & Experiences -->
    <script type="text/javascript">
	$(document).ready(function(){
    $("#categorias").change(function () {
           $("#categorias option:selected").each(function () {
            elegido=$(this).val();
            $.post("./assets/includes/forms/categorias-select-futbol-main.php", { elegido: elegido }, function(data){
            $("#equipos").html(data);
            });            
        });
   })
});
</script>
    <script type="text/javascript">
   	 $(function(){
        $("#buscaequipo").on("submit", function(e){
            e.preventDefault();
            var f = $(this);
            var formData = new FormData(document.getElementById("buscaequipo"));
            formData.append("dato", "valor");
            //formData.append(f.attr("name"), $(this)[0].files[0]);
            $.ajax({
                url: "./assets/includes/forms/categorias-busca-futbol.php",
                type: "post",
                dataType: "html",
                data: formData,
                cache: false,
                contentType: false,
	     processData: false
            })
                .done(function(res){
                    $("#respuestaequipo").html(res);
                });
        });
    });
    </script>
	<?php include("assets/includes/analytics.php"); ?>
</head>

<body>


    <?php include("assets/includes/top.php"); ?>
    
    <?php include("assets/includes/menu.php"); ?>
 
    
<div class="back_color">
<!-- Image Header -->
    <!-- Page Content -->

        <div class="container">
            <!-- Banner Carousel -->
              <div class="image_top_futbol">
              <h1><?php echo $lang['TEXT_FUTBOL1']; ?>
              </h1>
              <p> <?php echo $lang['TEXT_FUTBOL2']; ?></p>
              </div>
            </div>
            <!-- /.container -->

        </div>
    <!-- /.back color -->
    
    <!-- Page Content -->
    <div class="container">



<div class="panel_middle">
        <div class="row">
            <div class="col-lg-12">
            
           <?php echo $lang['TEXT_FUTBOL3']; ?>
                    <div class="box"><?php echo $lang['TEXT_EXPE2']; ?></div>
                      <div class="form">
                        <form name="buscaequipo" id="buscaequipo" novalidate>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <label><?php echo $lang['TEXT_FUTBOL4']; ?></label>                                 
                            <select class="form-control form-select" id="categorias" name="categorias">
                                <option value='0'><?php echo $lang['TEXT_SELEC']; ?></option>
                                <option value='1'><?php echo $lang['TEXT_FUTBOL6']; ?></option>
                                <option value='2'><?php echo $lang['TEXT_FUTBOL7']; ?></option>
                            </select>
                            
                            
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <label><?php echo $lang['TEXT_FUTBOL5']; ?></label> 
                            <select class="form-control form-select" id="equipos" name="equipos">
                                <option value='0'><?php echo $lang['TEXT_SELEC']; ?></option>
                            </select>
                            
                                </div>
                            </div>
                            <!-- For success/fail messages -->
                           <button type="submit" class="btn_new_short" id="getexper"><img src="images/enviar.png" width="18" height="20"><span class="text"><?php echo $lang['TEXT_EXPE5']; ?></span></button>            
                        </form>
                        
                        </div><!-- fi form-->
                     </div> <!-- /.fi col -->
    
            </div> <!-- /.fi row -->
        </div> <!-- /.fi panell -->




    </div>
    <!-- /.container -->
<div id="respuestaequipo"></div>
<!--<div class="back_color">
    <!-- Page Content -->
  <!--  <div class="container">

        <div class="row">
            <div class="col-md-12">
                    <div class="panel_bottom">                 
                    <div id="expe" class="expe1cat1 caja">expe1cat1<?php echo $lang['TEXT_EXPE6']; ?></div>
                    <div id="expe" class="expe2cat1 caja">expe2cat1</div>
                    <div id="expe" class="expe1cat2 caja">expe1cat2<?php echo $lang['TEXT_EXPE7']; ?></div>  
                    <div id="expe" class="expe2cat2 caja">expe2cat2</div>
                    <div id="expe" class="expe1cat3 caja">expe1cat3<?php echo $lang['TEXT_EXPE8']; ?></div>
                    <div id="expe" class="expe2cat3 caja">expe2cat3</div>

                </div> <!-- /.panel bottom -->
          <!--  </div><!-- /.col 12 -->

      <!--  </div><!-- /.row -->
   <!-- </div><!-- /.container -->

  <!--  </div><!-- /.back color -->
    
                       <!--//BLOQUE COOKIES-->
            <div id="barraaceptacion">
                <div class="block_cookie">
                    Utilizamos cookies de terceros para mejorar su accesibilidad, personalizar y analizar sus hábitos de navegación. Si continúa navegando, consideramos que acepta su uso. Puede cambiar la configuración u obtener más información en Política de Cookies.
                    <a href="javascript:void(0);" onclick="PonerCookie();"><b>OK</b></a> | 
                    <a href="#" data-toggle="modal" data-target="#politica">M&aacute;s informaci&oacute;n</a>
                </div>
            </div>
    <?php include("assets/includes/popups/cookies.php"); ?>   
    <?php include("assets/includes/footer.php"); ?>

    </body>
</html> 

        <script>
            function getCookie(c_name){
                var c_value = document.cookie;
                var c_start = c_value.indexOf(" " + c_name + "=");
                if (c_start == -1){
                    c_start = c_value.indexOf(c_name + "=");
                }
                if (c_start == -1){
                    c_value = null;
                }else{
                    c_start = c_value.indexOf("=", c_start) + 1;
                    var c_end = c_value.indexOf(";", c_start);
                    if (c_end == -1){
                        c_end = c_value.length;
                    }
                    c_value = unescape(c_value.substring(c_start,c_end));
                }
                return c_value;
            }
            
            function setCookie(c_name,value,exdays){
                var exdate=new Date();
                exdate.setDate(exdate.getDate() + exdays);
                var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
                document.cookie=c_name + "=" + c_value;
            }
            
            if(getCookie('tiendaaviso')!="1"){
                document.getElementById("barraaceptacion").style.display="block";
            }
            function PonerCookie(){
                setCookie('tiendaaviso','1',365);
                document.getElementById("barraaceptacion").style.display="none";
            }
            </script>
        <!--//FIN BLOQUE COOKIES-->

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Vertical tabs -->
    <script src="assets/js/tabs.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/popup.js"></script>
    
    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>