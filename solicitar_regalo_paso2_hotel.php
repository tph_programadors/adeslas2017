<?php
session_start();
include_once 'common.php';
if($lang_sql=="cat"){
    $lengbusc="_cat";
}else{
    $lengbusc="";
}
$fecha_actual = date('Y-m-d H:i:s');
$ipvisita = $_SERVER['REMOTE_ADDR'];
if (!isset($_SESSION["adeslas2hogar2016_codigo"])) {//comprueba que la sesion existe.
session_unset();
session_destroy();
header ("Location: http://disfrutaunaexperienciaunica.com/");
}
if (isset($_SESSION["adeslas2hogar2016_paso2"])) {//comprueba que la sesion paso 2existe.
    $paso2sesion = $_SESSION["adeslas2hogar2016_paso2"];
    if ($paso2sesion != "1") {
        session_unset();
        session_destroy();
        echo"<script type='text/javascript'>
            window.location='http://disfrutaunaexperienciaunica.com/';
            </script>";
    }
} else {
    session_unset();
    session_destroy();
    echo"<script type='text/javascript'>
        window.location='http://disfrutaunaexperienciaunica.com/';
        </script>";
}


include "./assets/connect/conexion.php";
include "./assets/php/categoryServices.php";

$tipocampana = $_SESSION["adeslas2hogar2016_tipocampana"];

$table_prefix = "adeslas2hogar2016";

$CategoryServiceArray = getCategoryWithActiveServices($table_prefix, $link, "");
$sqlcompruebacodigo = "SELECT * FROM `".$table_prefix."__cod_promo` WHERE `CODIGO` = '".$_SESSION["adeslas2hogar2016_codigo"]."'";
$rscompruebacodigo = mysqli_query($link, $sqlcompruebacodigo);
$num_total_codigo = mysqli_num_rows($rscompruebacodigo);
while ($row = mysqli_fetch_array($rscompruebacodigo)) {
	$n_codigo = $row["CODIGO"];
	$n_promo = $row["PROMO"];
	$n_fechareg = $row["F_REGISTRO"];
	$n_experiencia = $row["ESPECIAL"];
	$n_provincia = $row["CP"];
}
?>
<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <title><?php echo $lang['PAGE_TITLE']; ?></title>

        <!-- Bootstrap Core CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="assets/css/modern-business.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- Data Picker -->
        <link rel="stylesheet" href="assets/css/jquery-ui.css">
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/jquery-ui.js"></script>
        <script type="text/javascript">
            $(function () {

                $("#selectExperienceForm").submit(function () {
                    var url = "assets/includes/forms/canjeacodigo-2.php"; // El script a donde se realizara la peticion.
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: $("#selectExperienceForm").serialize(), // Adjuntar los campos del formulario enviado.
                        success: function (data)
                        {
                            $("#respuesta2").html(data);
                        }
                    });
                    return false; // Evitar ejecutar el submit del formulario.
                });

                $("#datepicker1").datepicker({
                    changeMonth: true,
                    changeYear: true,
					<?php
			  if($lang_sql=="cat"){
				  echo'monthNamesShort: ["Gen", "Feb", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dec"],
				  dayNamesMin: ["Dg","Dl","Dt","Dc","Dj","Dv","Ds"],';
			  }
			  ?>
                    firstDay: 1
                });

                $("#datepicker2").datepicker({
                    changeMonth: true,
                    changeYear: true,
					<?php
			  if($lang_sql=="cat"){
				  echo'monthNamesShort: ["Gen", "Feb", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dec"],
				  dayNamesMin: ["Dg","Dl","Dt","Dc","Dj","Dv","Ds"],';
			  }
			  ?>
                    firstDay: 1
                });

                $("#datepicker3").datepicker({
                    changeMonth: true,
                    changeYear: true,
					<?php
			  if($lang_sql=="cat"){
				  echo'monthNamesShort: ["Gen", "Feb", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dec"],
				  dayNamesMin: ["Dg","Dl","Dt","Dc","Dj","Dv","Ds"],';
			  }
			  ?>
                    firstDay: 1
                });
            });

        </script>
    </head>
    <body>



        <?php include("assets/includes/top_hidden.php"); ?>

        <?php include("assets/includes/analytics.php"); ?>

        <form name="selectExperienceForm" id="selectExperienceForm">
            <div class="back_color">
                <!-- Image Header -->  
                
            <div class="container">
            <!-- Banner Carousel -->
              <div class="image_top_hotel">
              <h1><?php echo $lang['TEXT_HOTEL1']; ?>
              </h1>
              <p> <?php echo $lang['TEXT_HOTEL2']; ?></p>
              </div>
            </div>
            <!-- /.container -->
            </div>
            <!-- /.back color -->

            <!-- Page Content -->
            <div class="container" style="padding-left:0px; padding-right:0px;">
                <!-- Banner Carousel -->
                <div class="col-md-12">

                    <!-- OPCIONS PER SELECCIONAR HOTEL -->
                    <div class="panel_middle_paso1">
                        <?php echo $lang['TEXT_MIDDLE_PASO1_HOTEL']; ?>
                        </br>
                        <!--<form name="selectExperienceForm" id="selectExperienceForm">-->
                        <div class="col-md-4">
                            <label for="categorySelect1"><?php echo $lang['TEXT_11_HOTEL']; ?></label>
                            <select class="form-control form-select categorySelect" id="categorySelect1" name="province1" for="serviceSelect1">
                                <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                                <?php
                                    $sqlprovincias = "SELECT * FROM `crm_provincias` ORDER BY crm_provincia ASC";
                                    $rsprovincias = mysqli_query($link,$sqlprovincias);
                                    while ( $row = mysqli_fetch_array($rsprovincias) ) {
                                        $id_provincia = $row["crm_id_provincia"];
                                        $nombre_provincia2 = $row["crm_provincia"];
                                        $nombre_provincia = utf8_encode($nombre_provincia2);
                                        echo"<option value='$id_provincia'>$nombre_provincia</option>";
                                    }
                                ?>
                            </select>
                            <label for=""><?php echo $lang['TEXT_12_HOTEL']; ?></label>
                            <select class="form-control form-select" id="serviceSelect1" name="experiencia1">
                                <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                                <?php
								$sqlcompruebahoteles = "SELECT * FROM `servicios` WHERE `id_servicio` = '161' OR `id_servicio` = '171' OR `id_servicio` = '268' OR `id_servicio` = '269' OR `id_servicio` = '270' OR `id_servicio` = '271'";
								$rscompruebahoteles = mysqli_query($link, $sqlcompruebahoteles);
								$num_total_compruehoteles = mysqli_num_rows($rscompruebahoteles);
								while ($row = mysqli_fetch_array($rscompruebahoteles)) {
								$s_desc_id=$row["id_servicio"];
								$s_desc_esp = utf8_encode($row["descripcion_es"]);
								$s_desc_cat = utf8_encode($row["descripcion_cat"]);
								$s_tit_esp = utf8_encode($row["nombre_servicio"]);
								$s_tit_cat = utf8_encode($row["nombre_servicio_cat"]);
								if($lang_sql=='cat'){
									$descripcionhotel=$s_desc_cat;
									$titulohotel=$s_tit_cat;
								}else{
									$descripcionhotel=$s_desc_esp;
									$titulohotel=$s_tit_esp;
								}
								echo"<option value='$s_desc_id'>$titulohotel</option>";
								}
								?>
                            </select>

                        </div>
                        <div class="col-md-4">
                            <label class="primera" for="categorySelect2"><?php echo $lang['TEXT_21_HOTEL']; ?></label>
                            <select class="form-control form-select categorySelect" id="categorySelect2" name="province2" for="serviceSelect2">
                                <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                                <?php
                                    $sqlprovincias = "SELECT * FROM `crm_provincias` ORDER BY crm_provincia ASC";
                                    $rsprovincias = mysqli_query($link,$sqlprovincias);
                                    while ( $row = mysqli_fetch_array($rsprovincias) ) {
                                        $id_provincia = $row["crm_id_provincia"];
                                        $nombre_provincia2 = $row["crm_provincia"];
                                        $nombre_provincia = utf8_encode($nombre_provincia2);
                                        echo"<option value='$id_provincia'>$nombre_provincia</option>";
                                    }
                                ?>
                            </select>
                            <label for=""><?php echo $lang['TEXT_22_HOTEL']; ?></label>
                            <select class="form-control form-select" id="serviceSelect2" name="experiencia2">
                                <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                                <?php
								$sqlcompruebahoteles2 = "SELECT * FROM `servicios` WHERE `id_servicio` = '161' OR `id_servicio` = '171' OR `id_servicio` = '268' OR `id_servicio` = '269' OR `id_servicio` = '270' OR `id_servicio` = '271'";
								$rscompruebahoteles2 = mysqli_query($link, $sqlcompruebahoteles2);
								$num_total_compruehoteles2 = mysqli_num_rows($rscompruebahoteles2);
								while ($row = mysqli_fetch_array($rscompruebahoteles2)) {
								$s_desc_id2=$row["id_servicio"];
								$s_desc_esp2 = utf8_encode($row["descripcion_es"]);
								$s_desc_cat2 = utf8_encode($row["descripcion_cat"]);
								$s_tit_esp2 = utf8_encode($row["nombre_servicio"]);
								$s_tit_cat2 = utf8_encode($row["nombre_servicio_cat"]);
								if($lang_sql=='cat'){
									$descripcionhotel2=$s_desc_cat2;
									$titulohotel2=$s_tit_cat2;
								}else{
									$descripcionhotel2=$s_desc_esp2;
									$titulohotel2=$s_tit_esp2;
								}
								echo"<option value='$s_desc_id2'>$titulohotel2</option>";
								}
								?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="primera" for="categorySelect3"><?php echo $lang['TEXT_31_HOTEL']; ?></label>
                            <select class="form-control form-select categorySelect" id="categorySelect3" name="province3" for="serviceSelect3">
                                <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                                <?php
                                    $sqlprovincias = "SELECT * FROM `crm_provincias` ORDER BY crm_provincia ASC";
                                    $rsprovincias = mysqli_query($link,$sqlprovincias);
                                    while ( $row = mysqli_fetch_array($rsprovincias) ) {
                                        $id_provincia = $row["crm_id_provincia"];
                                        $nombre_provincia2 = $row["crm_provincia"];
                                        $nombre_provincia = utf8_encode($nombre_provincia2);
                                        echo"<option value='$id_provincia'>$nombre_provincia</option>";
                                    }
                                ?>
                            </select>
                            <label for=""><?php echo $lang['TEXT_32_HOTEL']; ?></label>
                            <select class="form-control form-select" id="serviceSelect3" name="experiencia3">
                                <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                                <?php
								$sqlcompruebahoteles3 = "SELECT * FROM `servicios` WHERE `id_servicio` = '161' OR `id_servicio` = '171' OR `id_servicio` = '268' OR `id_servicio` = '269' OR `id_servicio` = '270' OR `id_servicio` = '271'";
								$rscompruebahoteles3 = mysqli_query($link, $sqlcompruebahoteles3);
								$num_total_compruehoteles3 = mysqli_num_rows($rscompruebahoteles3);
								while ($row = mysqli_fetch_array($rscompruebahoteles3)) {
								$s_desc_id3=$row["id_servicio"];
								$s_desc_esp3 = utf8_encode($row["descripcion_es"]);
								$s_desc_cat3 = utf8_encode($row["descripcion_cat"]);
								$s_tit_esp3 = utf8_encode($row["nombre_servicio"]);
								$s_tit_cat3 = utf8_encode($row["nombre_servicio_cat"]);
								if($lang_sql=='cat'){
									$descripcionhotel3=$s_desc_cat3;
									$titulohotel3=$s_tit_cat3;
								}else{
									$descripcionhotel3=$s_desc_esp3;
									$titulohotel3=$s_tit_esp3;
								}
								echo"<option value='$s_desc_id3'>$titulohotel3</option>";
								}
								?>
                            </select>
                        </div>
                        <p>&nbsp;</p>
                    </div>    <!-- /.fi panell middle  paso 1 -->
                    <!-- FI OPCIONS PER SELECCIONAR HOTEL -->



                </div>
            </div>
            <!-- /.container -->


            <div class="back_color">
                <!-- Page Content -->
                <div class="container" style="padding-left:0px; padding-right:0px;">
                    <!-- Banner Carousel -->
                    <div class="col-md-12">

                        <div class="panel_bottom">
                            <?php echo $lang['TEXT_MIDDLE_PASO2']; ?>
                            </br>
                            <div class="col-md-4"> 
                                <label for=""><?php echo $lang['TEXT_13']; ?></label>
                                </br>
                                <input type="text" id="datepicker1" name="fecha1" class="form-control" placeholder="<?php echo $lang['TEXT_DATA1']; ?>">
                            </div>
                            <div class="col-md-4">
                                <label for=""><?php echo $lang['TEXT_23']; ?></label>
                                </br>
                                <input type="text" id="datepicker2" name="fecha2" class="form-control" placeholder="<?php echo $lang['TEXT_DATA2']; ?>">
                            </div>
                            <div class="col-md-4">
                                <label for=""><?php echo $lang['TEXT_33']; ?></label>
                                </br>
                                <input type="text" id="datepicker3" name="fecha3" class="form-control" placeholder="<?php echo $lang['TEXT_DATA3']; ?>">
                            </div>
                            <p>&nbsp;</p>
                            <p class="recuerde"><?php echo $lang['LABEL_CAMPOS']; ?></p>
                            <button type="submit" class="btn_new1"><span class="icon1"><i class="fa fa-check fa-2x"></i></span><span class="text"><?php echo $lang['CONFIRM']; ?></span></button> 
                            <!--</form>-->
                            <div id="respuesta2"></div>
                            <p class="recuerde"><?php echo $lang['RECUERDE']; ?>
                            </p>

                        </div>    <!-- /.fi panel_bottom -->
                    </div>    <!-- /.fi col 12 -->

                </div> <!-- /.fi back container -->
            </div>  <!-- /.fi back color -->
        </form>
        <?php include("assets/includes/footer.php"); ?>

        <!-- Bootstrap Core JavaScript -->
        <script src="assets/js/bootstrap.min.js"></script>

        <!-- Vertical tabs -->
        <script src="assets/js/tabs.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="assets/js/popup.js"></script>

        <!-- Script to Activate the Carousel -->
        <script>
            $('.carousel').carousel({
                interval: 5000 //changes the speed
            })
        </script>
    </body>
</html>