
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    
    <title><?php echo $lang['PAGE_TITLE']; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="assets/css/modern-business.css" rel="stylesheet">
    <link href="assets/css/cookies.css" rel="stylesheet" type="text/css">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
  
    
    

<?php include("assets/includes/analytics.php"); ?>
</head>

<body>
	
	<?php
    include_once 'common.php';
    ?>

    <?php include("assets/includes/top.php"); ?>
    
    <?php include("assets/includes/menu.php"); ?>
     

<div class="back_color">
<!-- Image Header -->
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <div class="banner_home">
                 <!-- <div class="text_caption"><?php echo $lang['TEXT_BANNER']; ?></div>-->
                 <div class="label_caption"><a href="solicitar_regalo_paso1.php"><?php echo $lang['BOTON_BANNER']; ?></a></div>
                <!--<img class="img-responsive" src="http://placehold.it/1200x300" alt="">-->
                <a href="solicitar_regalo_paso1.php"><?php echo $lang['BANNER_PRIN']; ?></a>
                </div><!-- /.banner home -->
                <div class="banner_home_res">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                              <a href="solicitar_regalo_paso1.php">
                              <?php echo $lang['BANNER_RES']; ?>
                              </a>
                           </div>
                         </div>
                      </div>
                </div><!-- /.banner home -->
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
        </div>
    <!-- /.back color -->
    
    
    
        <!-- Page Content -->
   <div class="container">
    <div class="panel_middle">
        <div class="row">
            <div class="col-lg-12">
                            <?php echo $lang['CONT_1']; ?>
                    <div class="box1"><?php echo $lang['CONT_2']; ?></div>
                      <div class="form">
                        <form action="#" method="POST" name="formulariocontacto" id="formulariocontacto">
                            <div class="control-group form-group">
                                <div class="controls">
                                    <label><?php echo $lang['TEXT3_3']; ?></label>
                                    <input type="text" class="form-control" name="nombreform" id="nombreform">
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <label><?php echo $lang['TEXT3_4']; ?></label>
                                    <input type="text" class="form-control" name="surnameform" id="surnameform">
                                </div>
                            </div>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <label><?php echo $lang['TEXT3_7']; ?></label>
                                    <input type="text" class="form-control"  name="emailform" id="emailform">
                                </div>
                            </div>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <label><?php echo $lang['CONT_6']; ?></label>
                                    <textarea class="form-control" name="consultaform" id="consultaform"></textarea>
                                </div>
                            </div>
                            <div class="control-group form-group">
                                <div class="controls">
                                   <p>
                                    <input type="checkbox" id="lopd" name="lopd" value="1">

                                   <?php echo $lang['CONT_7']; ?> <a href="#" data-toggle="modal" data-target="#lopd-modal"><?php echo $lang['TEXT_LOPD']; ?></a></p>
                                    <p><span class="orange"><?php echo $lang['LABEL_CAMPOS']; ?></span></p>
                                </div>
                            </div>
                            <div id="success"></div>
                            <!-- For success/fail messages -->
                           <!--<a href="#" id="submitModal" data-toggle="modal" data-target="#contact-modal">-->
                               <button type="submit" class="btn_new_short" id="enviaform">
                                   <img src="images/enviar.png" width="18" height="20">
                                   <span class="text"><?php echo $lang['CONT_3']; ?></span>
                               </button>
                           <!--</a>-->
                        </form>
                        
                        </div><!-- fi form-->
                     </div> <!-- /.fi col -->
    
            </div> <!-- /.fi row -->
        </div> <!-- /.fi panell -->
      </div> <!-- /.fi container -->
          
          
           <!--//BLOQUE COOKIES-->
            <div id="barraaceptacion">
                <div class="block_cookie">
                    Utilizamos cookies de terceros para mejorar tu accesibilidad, personalizar y analizar tus hábitos de navegación. Si continuas navegando, consideramos que aceptas su uso. Puedes cambiar la configuración u obtener más información en Política de Cookies.
                    <a href="javascript:void(0);" onclick="PonerCookie();"><b>OK</b></a> | 
                    <a href="#" data-toggle="modal" data-target="#politica">M&aacute;s informaci&oacute;n</a>
                </div>
            </div>
    <?php include("assets/includes/popups/cookies.php"); ?>
    
	<?php include("assets/includes/popups/enviado.php"); ?>
    
    <?php include("assets/includes/popups/lopd.php"); ?>
    
    <?php include("assets/includes/footer.php"); ?>
    


        

    <!-- Bootstrap Core JavaScript -->

        <script src="assets/js/jquery.js"></script>


    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Vertical tabs -->
    <script src="assets/js/tabs.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/popup.js"></script>

        <script>  
    $(function(){
        //Tigger Bind form submit from legal bases accept button.
        $( "#enviaform" ).click(function() {
            var url = "assets/includes/mail/form_contacto.php"; // El script a dónde se realizará la petición.
            $.ajax({
                type: "POST",
                url: url,
                data: $("#formulariocontacto").serialize(), // Adjuntar los campos del formulario enviado.
                success: function(data)
            {
                $("#success").html(data);
            }
            });
        return false; // Evitar ejecutar el submit del formulario.
        });
    });
    </script>

    </body>
</html> 