<?php 
session_start();
include_once 'common.php';

$c_aco_partner = 'false';

$table_prefix = "adeslas2hogar2016";

if (isset($_SESSION["adeslas2hogar2016_codigo"])){//comprueba que la sesion existe.
	$sesion_codigo=$_SESSION["adeslas2hogar2016_codigo"];
        $sesion_id_peticion=$_SESSION["adeslas2hogar2016_id_peticion"];
}else{
	header("Location: http://disfrutaunaexperienciaunica.com/");
}

include "./assets/connect/conexion.php";
require "./assets/php/conviertefecha.php";


if($lang_sql=="cat"){
	$lengbusc="_cat";
	$n_fechasindia = "traducefechasindiacat";
	$n_fechacondia = "traducefechacat";
}else{
	$lengbusc="";
	$n_fechasindia = "traducefechasindiaes";
	$n_fechacondia = "traducefechaes";
}     
$sqlcompruebacorreo = "
	SELECT * 
	FROM `".$table_prefix."__peticion`  LEFT JOIN `crm_municipios` ON  crm_municipios.crm_id_municipio =  ".$table_prefix."__peticion.POBLACION LEFT JOIN `crm_provincias` ON  crm_provincias.crm_id_provincia =  ".$table_prefix."__peticion.PROVINCIA 
	WHERE `N_PET` = '$sesion_id_peticion'
	"; 

mysqli_query($link,$sqlcompruebacorreo);
$rscompruebacorreo= mysqli_query($link,$sqlcompruebacorreo);
$num_total_registros = mysqli_num_rows($rscompruebacorreo);
while ($row=mysqli_fetch_array($rscompruebacorreo)) { 
	$c_contacto_idcontador=$row["N_PET"];
	$c_contacto_nombre=$row["NOMBRE"];
	$c_contacto_nombre =utf8_encode($c_contacto_nombre);
	$c_contacto_apellidos=$row["APELLIDOS"];
	$c_contacto_apellidos =utf8_encode($c_contacto_apellidos);
	$c_contacto_nif=$row["NIF"];
	$c_contacto_nif =utf8_encode($c_contacto_nif);
	$c_contacto_direccion=stripslashes($row["DIRECCION"]);
	$c_contacto_direccion =utf8_encode($c_contacto_direccion);
	$c_contacto_poblacion =$row["crm_municipio"];
	$c_contacto_poblacion =utf8_encode($c_contacto_poblacion);
	$c_contacto_cp =$row["CP"];
	$c_contacto_cp =utf8_encode($c_contacto_cp);
	$c_contacto_provincia =$row["crm_provincia"];
	$c_contacto_provincia =utf8_encode($c_contacto_provincia);
	$c_contacto_mail =$row["MAIL"];
	$c_contacto_mail =utf8_encode($c_contacto_mail);
	$c_contacto_telf =$row["TELEFONO"];
	$c_contacto_telf =utf8_encode($c_contacto_telf);
	$c_contacto_codigo =$row["CODIGO"];
	$c_contacto_poliza =$row["POLIZA"];
	$c_contacto_dest1 =$row["DEST1"];
	$c_contacto_dest2 =$row["DEST2"];
	$c_contacto_dest3 =$row["DEST3"];
	$c_contacto_ida11 =$row["F_IDA_1"];
	$c_contacto_ida21 =$row["F_IDA_2"];
	$c_contacto_ida31 =$row["F_IDA_3"];
        $c_contacto_fechanac2=$row["fechanacimiento"];
	$tipocampana = $row["TIPOCAMPANA"];
}

if($num_total_registros=='0'){
	header("Location: http://disfrutaunaexperienciaunica.com/");
}

$c_contacto_fechanacx =$n_fechasindia($c_contacto_fechanac2);
$c_contacto_fechanac = utf8_decode($c_contacto_fechanacx);

$c_contacto_ida1x =$n_fechasindia($c_contacto_ida11);
$c_contacto_ida2x =$n_fechasindia($c_contacto_ida21);
$c_contacto_ida3x =$n_fechasindia($c_contacto_ida31);
$c_contacto_ida1 = $c_contacto_ida1x;
$c_contacto_ida2 = $c_contacto_ida2x;
$c_contacto_ida3 = $c_contacto_ida3x;

if($tipocampana==1){
	$simbolo_dest = "_"; 
	$posiciondest1 = strpos ($c_contacto_dest1, $simbolo_dest); 
	$c_contacto_dest_prov1 = explode('_', $c_contacto_dest1);
	$c_contacto_dest_prov1=$c_contacto_dest_prov1[0];
	$c_contacto_dest1 = substr ($c_contacto_dest1, ($posiciondest1+1)); 
	$sqlprov1= "SELECT * FROM crm_provincias WHERE crm_id_provincia = '$c_contacto_dest_prov1'";
	$rsprov1 = mysqli_query($link, $sqlprov1);
    while ($row=mysqli_fetch_array($rsprov1)) {
                $s_nombre_prov1=utf8_encode($row["crm_provincia"]);
	}
	
	$posiciondest2 = strpos ($c_contacto_dest2, $simbolo_dest); 
	$c_contacto_dest_prov2 = explode('_', $c_contacto_dest2);
	$c_contacto_dest_prov2=$c_contacto_dest_prov2[0];
	$c_contacto_dest2 = substr ($c_contacto_dest2, ($posiciondest2+1)); 
	$sqlprov2= "SELECT * FROM crm_provincias WHERE crm_id_provincia = '$c_contacto_dest_prov2'";
	$rsprov2 = mysqli_query($link, $sqlprov2);
    while ($row=mysqli_fetch_array($rsprov2)) {
                $s_nombre_prov2=utf8_encode($row["crm_provincia"]);
	}
	
	$posiciondest3 = strpos ($c_contacto_dest3, $simbolo_dest); 
	$c_contacto_dest_prov3 = explode('_', $c_contacto_dest3);
	$c_contacto_dest_prov3=$c_contacto_dest_prov3[0];
	$c_contacto_dest3 = substr ($c_contacto_dest3, ($posiciondest3+1));
	$sqlprov3= "SELECT * FROM crm_provincias WHERE crm_id_provincia = '$c_contacto_dest_prov3'";
	$rsprov3 = mysqli_query($link, $sqlprov3);
    while ($row=mysqli_fetch_array($rsprov3)) {
                $s_nombre_prov3=utf8_encode($row["crm_provincia"]);
	}
	
}

$sqlcompruebaexp1 = "SELECT * FROM `servicios` WHERE `id_servicio` = '$c_contacto_dest1'"; 
mysqli_query($link,$sqlcompruebaexp1);
$rscompruebaexp1= mysqli_query($link,$sqlcompruebaexp1);
while ($row=mysqli_fetch_array($rscompruebaexp1)) { 
		$exp1=utf8_encode($row["nombre_servicio".$lengbusc.""]);
}
$sqlcompruebaexp2 = "SELECT * FROM `servicios` WHERE `id_servicio` = '$c_contacto_dest2'"; 
mysqli_query($link,$sqlcompruebaexp2);
$rscompruebaexp2= mysqli_query($link,$sqlcompruebaexp2);
while ($row=mysqli_fetch_array($rscompruebaexp2)) { 
		$exp2=utf8_encode($row["nombre_servicio".$lengbusc.""]);
}
$sqlcompruebaexp3 = "SELECT * FROM `servicios` WHERE `id_servicio` = '$c_contacto_dest3'"; 
mysqli_query($link,$sqlcompruebaexp3);
$rscompruebaexp3= mysqli_query($link,$sqlcompruebaexp3);
while ($row=mysqli_fetch_array($rscompruebaexp3)) { 
		$exp3=utf8_encode($row["nombre_servicio".$lengbusc.""]);
}

if($tipocampana==1){
$exp1="$exp1 <br /> $s_nombre_prov1";
$exp2="$exp2 <br /> $s_nombre_prov2";
$exp3="$exp3 <br /> $s_nombre_prov3";
}
if($tipocampana==2){ 
	$sqlcompruebapartidos = "SELECT * FROM ".$table_prefix."__partidos WHERE id_partido = '$c_contacto_dest1'";
	$rscompruebapartidos= mysqli_query($link,$sqlcompruebapartidos);
	$num_total_PARTIDOS = mysqli_num_rows($rscompruebapartidos);
	while ($row=mysqli_fetch_array($rscompruebapartidos)) { 
	$p_nombre_equipocasa2=$row["nombre_equipo"];
	$p_nombre_equipocasa=utf8_encode($p_nombre_equipocasa2);
	$p_id_partido=$row["id_partido"];
	$p_jornada_partido=$row["jornada_partido"];
	$p_fecha_partido2=$row["fecha_partido"];
	$p_fecha_partido=utf8_decode($n_fechasindia($p_fecha_partido2));
	$p_equipo_local=$row["equipo_a"];
	$p_equipo_visitante=$row["equipo_b"];
	}
	$sqlequipolocal="SELECT * FROM ".$table_prefix."__equipos WHERE id_equipo='$p_equipo_local'";
	$rsequipolocal= mysqli_query($link,$sqlequipolocal);
	while ($row=mysqli_fetch_array($rsequipolocal)) { 
	$s_nombre_local2=$row["nombre_equipo"];
	$s_nombre_local=utf8_encode($s_nombre_local2);
	$s_camiseta_local=$row["camiseta_equipo"];
	$s_estadio_local2=$row["estadio_equipo"];
	$s_estadio_local=utf8_encode($s_estadio_local2);
	}
	$sqlequipovisitante="SELECT * FROM ".$table_prefix."__equipos WHERE id_equipo='$p_equipo_visitante'";
	$rsequipovisitante= mysqli_query($link,$sqlequipovisitante);
	while ($row=mysqli_fetch_array($rsequipovisitante)) { 
	$s_nombre_visitante2=$row["nombre_equipo"];
	$s_nombre_visitante=utf8_encode($s_nombre_visitante2);
	$s_camiseta_visitante=$row["camiseta_equipo"];
	}
$exp1="<b>$s_nombre_local</b><br /> vs <br /><b>$s_nombre_visitante</b>";
$exp2="<b>$s_estadio_local</b>";
$exp3="Jornada <b>$p_jornada_partido</b> <br /><b>$p_fecha_partido</b>";

$c_contacto_ida1x="";
$c_contacto_ida2x="";
$c_contacto_ida3x="";

}

//Partner data

//      $sqlcheckpartner = "SELECT * FROM `".$table_prefix."__peticion`             LEFT JOIN `crm_municipios` ON  crm_municipios.crm_id_municipio =  ".$table_prefix."__peticion.POBLACION                             LEFT JOIN `crm_provincias` ON  crm_provincias.crm_id_provincia =  ".$table_prefix."__peticion.PROVINCIA                             WHERE `N_PET` = '$sesion_id_peticion'"; 

        $sqlcheckpartner = "SELECT * FROM `".$table_prefix."__peticion_acompanante` LEFT JOIN `crm_municipios` ON  crm_municipios.crm_id_municipio =  ".$table_prefix."__peticion_acompanante.municipio_pet_acompanante LEFT JOIN `crm_provincias` ON  crm_provincias.crm_id_provincia =  ".$table_prefix."__peticion_acompanante.provincia_pet_acompanante WHERE `rel_pet_acompanante` = '$sesion_id_peticion'";
    //    $sqlcheckpartner .= " AND `desactivado` != '1'";

        /* change character set to utf8 */

	
	$partnerrs= mysqli_query($link,$sqlcheckpartner);
	if (mysqli_num_rows($partnerrs) == 1) {
        $params["get_ready"]=true;
        
            //Populate array of values
            while ($row=mysqli_fetch_array($partnerrs)) {
                    $c_aco_nombre_d=$row["nombre_pet_acompanante"];
                    $c_aco_nombre = utf8_encode($c_aco_nombre_d);

                    $c_aco_apellidos_d=$row["apellidos_pet_acompanante"];
                    $c_aco_apellidos = utf8_encode($c_aco_apellidos_d);

                    $c_aco_sexo = $row["sexo"];

                    $c_aco_fechanac2 =$row["fechanac_pet_acompanante"];
                    $c_aco_nif_d=$row["dni_pet_acompanante"];
                    $c_aco_nif = utf8_encode($c_aco_nif_d);
                    $c_aco_mail_d =$row["email_pet_acompanante"];
                    $c_aco_mail = utf8_encode($c_aco_mail_d);
                    $c_aco_telf =$row["telf_pet_acompanante"];
                    $c_aco_provincia_d =$row["crm_provincia"];
                    $c_aco_provincia = utf8_encode($c_aco_provincia_d);

                    $c_aco_poblacion_d =$row["crm_municipio"];
                    $c_aco_poblacion = utf8_encode($c_aco_poblacion_d);
                    $c_aco_cp =$row["cp_pet_acompanante"];

                    $c_aco_direccion_d=stripslashes($row["direccion_pet_acompanante"]);
                    $c_aco_direccion = utf8_encode($c_aco_direccion_d);
                    
                    $c_aco_menor = $row["menor_pet_acompanante"];
                    $c_aco_partner = 'true';
            }
			
            $_SESSION["adeslas2hogar2016_partner"]=$formValuesPartner["partner"];
            
            //set partner birth date string format
            $c_aco_fechanac =$n_fechasindia($c_aco_fechanac2);
			$c_aco_fechanac =utf8_decode($c_aco_fechanac);
            
        }else{
            
	}

$estilobg="form";
if ($c_aco_partner == 'true'){
    $estilobg .= 'Partner_';
}else{
    $estilobg .= 'Alone_';
}

switch ($tipocampana) {
    //Hotel
    case 1:
        $estilobg .= "Hotel".$lengbusc.".jpg";
        break;
    //Futbol
    case 2:
        $estilobg .= "Football".$lengbusc.".jpg";
        break;
    //Experiencias
    case 3:
        $estilobg .= "Experience".$lengbusc.".jpg";
        break;
    //Otros
    default:
        $estilobg .= "Experience".$lengbusc.".jpg";
}

$linkExperiencia="http://disfrutaunaexperienciaunica.com/tuexperiencia.php?x=".$c_contacto_nif."&y=".$c_contacto_idcontador."&z=".$c_contacto_codigo;


$html="
			<html>
	<head>
		
		<style type='text/css'>
			body {
				font-family: 'helvetica';
				font-weight: bold;	
				margin:0px;
				padding:0px;
				padding-left:10px;
				font-size:10px;
				background-image:url($estilobg);
				width:800px;
			}
			.myPrintArea {
				width:800px;
				height:900px;
				position:relative;
			}
			#poliza{
				position:absolute;
				height:25px;
				width:100px;
				float:left;
				margin-left:430px;
				padding-top:191px;
			}
			#codigo{
				position:absolute;
				height:25px;
				width:100px;
				float:left;
				padding-top:190px;	
				margin-left:80px;
	
			}
			.clearleft{
				clear:left;
			}
			#exp1{
				position:absolute;
				height:25px;
				width:230px;
				float:left;
				padding-top:68px;	
				padding-left20px;	
			}
			#exp2{
				position:absolute;
				height:25px;
				width:230px;
				float:left;
				margin-left:16px;
				padding-top:68px;	
			}
			#exp3{
				position:absolute;
				height:25px;
				width:190px;
				float:left;
				margin-left:20px;
				padding-top:68px;
			}";
			if($tipocampana==2){ 
				$html .="#fecha1{
					position:absolute;
					height:25px;
					width:230px;
					float:left;
					padding-top:23px;	
					padding-left20px;	
				}
				#fecha2{
					position:absolute;
					height:25px;
					width:230px;
					float:left;
					margin-left:16px;
					padding-top:23px;	
				}
				#fecha3{
					position:absolute;
					height:25px;
					width:190px;
					float:left;
					margin-left:20px;
					padding-top:23px;
				}";
			}else{
				$html .="#fecha1{
					position:absolute;
					height:25px;
					width:230px;
					float:left;
					padding-top:33px;	
					padding-left20px;	
				}
				#fecha2{
					position:absolute;
					height:25px;
					width:230px;
					float:left;
					margin-left:16px;
					padding-top:33px;	
				}
				#fecha3{
					position:absolute;
					height:25px;
					width:190px;
					float:left;
					margin-left:20px;
					padding-top:33px;
				}";
			}
			$html .="#nombre{
				position:absolute;
				height:25px;
				width:230px;
				float:left;
				padding-top:53px;	
			}
			#apellidos{
				position:absolute;
				height:25px;
				width:230px;
				float:left;
				margin-left:16px;
				padding-top:53px;	
			}
			#dni{
				position:absolute;
				height:25px;
				width:190px;
				float:left;
				margin-left:20px;
				padding-top:53px;
			}
			#nacimiento{
				position:absolute;
				height:25px;
				width:230px;
				float:left;
				padding-top:17px;	
			}
			#mail{
				position:absolute;
				height:25px;
				width:230px;
				float:left;
				margin-left:16px;
				padding-top:17px;	
			}
			#telf{
				position:absolute;
				height:25px;
				width:190px;
				float:left;
				margin-left:20px;
				padding-top:17px;
			}
			#direccion{
				position:absolute;
				height:25px;
				width:230px;
				float:left;
				padding-top:20px;	
			}
			#cp{
				position:absolute;
				height:25px;
				width:230px;
				float:left;
				margin-left:16px;
				padding-top:22px;	
			}
			#poblacion{
				position:absolute;
				height:25px;
				width:190px;
				float:left;
				margin-left:20px;
				padding-top:22px;
			}
			#provincia{
				position:absolute;
				height:25px;
				width:230px;
				float:left;
				padding-top:20px;	
			}
			#ac_nombre{
				position:absolute;
				height:25px;
				width:230px;
				float:left;
				padding-top:79px;	
			}
			#ac_apellidos{
				position:absolute;
				height:25px;
				width:230px;
				float:left;
				margin-left:16px;
				padding-top:79px;	
			}
			#ac_dni{
				position:absolute;
				height:25px;
				width:190px;
				float:left;
				margin-left:20px;
				padding-top:79px;
			}
			#ac_nacimiento{
				position:absolute;
				height:25px;
				width:230px;
				float:left;
				padding-top:19px;	
			}
			#ac_mail{
				position:absolute;
				height:25px;
				width:230px;
				float:left;
				margin-left:16px;
				padding-top:19px;	
			}
			#ac_telf{
				position:absolute;
				height:25px;
				width:190px;
				float:left;
				margin-left:20px;
				padding-top:19px;
			}
			#ac_direccion{
				position:absolute;
				height:25px;
				width:230px;
				float:left;
				padding-top:18px;	
			}
			#ac_cp{
				position:absolute;
				height:25px;
				width:230px;
				float:left;
				margin-left:16px;
				padding-top:18px;	
			}
			#ac_poblacion{
				position:absolute;
				height:25px;
				width:190px;
				float:left;
				margin-left:16px;
				padding-top:18px;
			}
			#ac_provincia{
				position:absolute;
				height:25px;
				width:230px;
				float:left;
				padding-top:18px;	
			}
			#enlace{
				position:absolute;
				height:25px;
				width:700px;
				float:left;";
				 if ($c_aco_partner == 'true'){
					 $html .="padding-top:134px;";
				 }else{
					 $html .="padding-top:368px;";
				 }
				$html .="
				color:#e67201;
				text-transform: lowercase;	
			}
		</style>
	</head>
	<body>
			<div id='myPrintArea' style=''>
			
		          <div id='poliza' style=''>
		           $c_contacto_poliza
				  </div>
				  
				  <div id='codigo' style=''>
                    $sesion_codigo
				  </div>
				  
				  <div class='clearleft'></div>
				  
				  <div id='exp1'>
				  $exp1
				  </div>
				  <div id='exp2'>
				  $exp2
				  </div>
				  <div id='exp3'>
				  $exp3
				  </div>
				  
				  <div class='clearleft'></div>
				  
				  <div id='fecha1'>
				$c_contacto_ida1x
				  </div>
				  <div id='fecha2'>
				$c_contacto_ida2x
				  </div>
				  <div id='fecha3'>
				$c_contacto_ida3x
				  </div>
				  
				  <div class='clearleft'></div>
				  
				  <div id='nombre'>
				  $c_contacto_nombre
				  </div>
				  <div id='apellidos'>
				  $c_contacto_apellidos
				  </div>
				  <div id='dni'>
				  $c_contacto_nif
				  </div>
				  
				  <div class='clearleft'></div>
				  
				  <div id='nacimiento'>
				  $c_contacto_fechanac
				  </div>
				  <div id='mail'>
				  $c_contacto_mail
				  </div>
				  <div id='telf'>
				  $c_contacto_telf
				  </div>
				  
				  <div class='clearleft'></div>
				  
				  <div id='direccion'>
				  $c_contacto_direccion
				  </div>
				  <div id='cp'>
				  $c_contacto_cp
				  </div>
				  <div id='poblacion'>
				  $c_contacto_poblacion
				  </div>
				  
				  <div class='clearleft'></div>
				  
				  <div id='provincia'>
				  $c_contacto_provincia
				  </div>
				  
				  <div class='clearleft'></div>";
				   if ($c_aco_partner == 'true'){
					   $html .="<div id='ac_nombre'>
					  $c_aco_nombre
					  </div>
					  <div id='ac_apellidos'>
					  $c_aco_apellidos
					  </div>
					  <div id='ac_dni'>
					  $c_aco_nif
					  </div>
					  
					  <div class='clearleft'></div>
				  
					  <div id='ac_nacimiento'>
					  $c_aco_fechanac
					  </div>
					  <div id='ac_mail'>
					  $c_aco_mail
					  </div>
					  <div id='ac_telf'>
					  $c_aco_telf
					  </div>
					  
					  <div class='clearleft'></div>
					  
					  <div id='ac_direccion'>
					  $c_aco_direccion
					  </div>
					  <div id='ac_cp'>
					  $c_aco_cp
					  </div>
					  <div id='ac_poblacion'>
					  $c_aco_poblacion
					  </div>
					  
					  <div class='clearleft'></div>
				  
					  <div id='ac_provincia'>
					  $c_aco_provincia
					  </div>
					  
					  <div class='clearleft'></div>";
					  
				   }
					   
				

			$html .="
			
						<div id='enlace'>
					  	$linkExperiencia
					  	</div>
					</div>
				</body>
</html>";


	
include("mpdf/mpdf.php");
$mpdf=new mPDF('c'); 

$mpdf->WriteHTML($html);
$mpdf->Output($filename, array("Attachment" => 0));
exit;
?>