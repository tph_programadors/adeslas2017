<?php
include "./assets/connect/conexion.php";
include "./assets/includes/funciones.php";
$table_prefix = "adeslas2hogar2016";
$response_prefix="";
$response_post="";
$response_style="";
$statusmensajearchivos1vez=0;
$status = "";
$fecha_actual=date('Y-m-d H:i:s');

function limpiar_caracteres_especiales_ok($s) {
	$s = preg_replace("/[áàâãª]/","a",$s);
	$s = preg_replace("/[Á]/","A",$s);
	$s = preg_replace("/[éèê]/","e",$s);
	$s = preg_replace("/[ÉÈÊ]/","E",$s);
	$s = preg_replace("/[íìî]/","i",$s);
	$s = preg_replace("/[ÍÌÎ]/","I",$s);
	$s = preg_replace("/[óòôõº]/","o",$s);
	$s = preg_replace("/[ÓÒÔÕ]/","O",$s);
	$s = preg_replace("/[úùûü]/","u",$s);
	$s = preg_replace("/[ÚÙÛ]/","U",$s);
	$s = preg_replace("/[ ]/","-",$s);
	$s = preg_replace("/[ñ]/","n",$s);
	$s = preg_replace("/[Ñ]/","N",$s);
	$s = preg_replace("/[ç]/","c",$s);
	$s = preg_replace("/[,]/","",$s);
	$s = preg_replace("/[¡]/","",$s);
	$s = preg_replace("/[!]/","",$s);
	$s = preg_replace("/[¿]/","",$s);
	$s = preg_replace("/[?]/","",$s);
	$s = preg_replace("/[']/","-",$s);
	$s = preg_replace("/[(]/","",$s);
	$s = preg_replace("/[)]/","",$s);
	return $s;
}

if( isset($_POST["action"]) ){
    if ($_POST["action"] == "upload") {
        $filesOK=0;
        foreach ($_FILES as $idArchivo => $archivoArray) {

            // obtenemos los datos del archivo
            $tamano = $archivoArray['size'];
            $tipo = $archivoArray['type'];
            $archivo = limpiar_caracteres_especiales_ok($archivoArray['name']);
			$archivosql = utf8_encode(limpiar_caracteres_especiales_ok($archivoArray['name']));
            //Add timestamp and random to filename to view storage moment and avoid rewrite some files with same name.
			$archivotipo=$_POST['archivotipo'];
            $prefijo = "_".substr(md5(uniqid(rand())),0,6);
            $id_pet = $_POST["peti"];
            if (!is_dir("archivos/$id_pet/")) {
                mkdir("archivos/$id_pet/");
            }
            if ($archivo != "") {
                if (
                           $tipo== "image/png" 
                        OR $tipo== "image/jpeg" 
                        OR $tipo== "image/pjpeg" 
                        OR $tipo== "image/JPEG" 
                        OR $tipo== "image/PJPEG" 
                        OR $tipo== "image/PNG" 
                        OR $tipo== "image/GIF" 
                        OR $tipo== "application/zip" 
                        OR $tipo== "application/x-zip" 
                        OR $tipo== "application/x-zip-compressed" 
                        OR $tipo== "application/octet-stream" 
                        OR $tipo== "application/x-compress" 
                        OR $tipo== "application/x-compressed" 
                        OR $tipo== "multipart/x-zip"
                        OR $tipo== "application/x-rar-compressed"
                        OR $tipo== "application/pdf"
                ){
                    // guardamos el archivo a la carpeta files
//                    $destino =  "archivos/$id_pet/".$prefijo."_".$archivo;
                    $destino =  "archivos/".$id_pet."/".$archivotipo."".$prefijo."_".$archivo;
                     if (copy($archivoArray['tmp_name'],$destino)) {
							if($statusmensajearchivos1vez==0){
							$status2ok = "<span style='color:green;'>Archivos enviados</span></br>";
							echo $status2ok;
											$sqlcompruebapet = "SELECT * FROM `".$table_prefix."__peticion` WHERE `N_PET` = '$id_pet'"; 
											$rscompruebapet = mysqli_query($link, $sqlcompruebapet);
											while ($row = mysqli_fetch_array($rscompruebapet)) {
											$c_contacto_nombre_d=$row["NOMBRE"];
											$c_contacto_nombre = $c_contacto_nombre_d;
											$c_contacto_mail_d =$row["MAIL"];
											$c_contacto_mail = $c_contacto_mail_d;
											}
											
							}
							$statusmensajearchivos1vez=1;

                        echo"
						<script type='text/javascript'>
							location.reload();
						</script>
						";
                        
                        $sqlquery1 = "UPDATE `".$table_prefix."__peticion` SET `ENVIODOC` ='1', `ESTADO` ='1' WHERE `N_PET` = '$id_pet'";
						
											

                        /* change character set to utf8 */
                        if (!$link->set_charset("utf8")) {
                            printf("Error loading character set utf8: %s\n", $link->error);
                        }
                        $result = $link->query($sqlquery1);
                        
			$sql="INSERT INTO ".$table_prefix."__peticion_archivos (`nombre_archivo_peticion`, `rel_archivo_peticion`, `tipo_archivo_peticion`,`subidopor_archivo_peticion`,`fecha_archivo_peticion`,`estado_archivo_peticion`)
			 VALUES ('".$archivotipo."".$prefijo."_".$archivosql."','$id_pet','$archivotipo','Usuario','$fecha_actual','0')";

                        $link->query($sql);
                        
                        $filesOK++;
						
						
							
                    } else {
                        $status = "<span style='color:red;'>Error al subir el archivo</span></br>";
                        echo $status;
                    }
                }else{
                    $status = "<span style='color:red;'>Error tipo de archivo</span></br>";
                    echo $status;
                }
            }
        }
        if ($filesOK==0) {
            $status = "<span style='color:red;'>Ningún archivo seleccionado</span></br>";
            echo $status;            
        }
    }
}else{
    $status = "<span style='color:red;'>Error al subir los archivos</span></br>";
    echo $status;
}
?>