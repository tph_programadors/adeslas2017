<?php
    include_once 'common.php';
    ?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <title><?php echo $lang['PAGE_TITLE']; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="assets/css/modern-business.css" rel="stylesheet">
    <link href="assets/css/cookies.css" rel="stylesheet" type="text/css">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="assets/js/jquery.js"></script>
    <script type="text/javascript" src="assets/js/jquery.flexisel.js"></script>
    <?php include("assets/includes/analytics.php"); ?>
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>

    <?php include("assets/includes/top.php"); ?>
    <?php include("assets/includes/menu.php"); ?>
 
<div class="back_color">
<!-- Image Header -->
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <div class="banner_home">
                <!-- <div class="text_caption"><?php echo $lang['TEXT_BANNER']; ?></div>-->
                 <div class="label_caption"><a href="solicitar_regalo_paso1.php"><?php echo $lang['BOTON_BANNER']; ?></a></div>
                 <a href="solicitar_regalo_paso1.php"><?php echo $lang['BANNER_PRIN']; ?></a>
                </div><!-- /.banner home -->
            </div>
             <div class="banner_home_res">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                              <a href="solicitar_regalo_paso1.php">
                               <?php echo $lang['BANNER_RES']; ?>
                              </a>
                           </div>
                         </div>
                      </div>
                </div><!-- /.banner home -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
        </div>
    <!-- /.back color -->
    
    
     <!-- Page Content -->
    <div class="container" style="padding-right:10px; padding-left:10px;">   
            <ul id="flexiselDemo3">
                <li>
                        <header id="myCarousel1" class="carousel slide">
            <div class="carousel-inner" style="border: 5px solid #FFFFFF;">
                <div class="item active">
                <a href="experiencias.php">
                    <!--<div class="fill" style="background-image:url('images/carroa.jpg');"></div>-->
                    <div class="fill" style="background-image:url('images/carro_experiencias.jpg');"></div>
                    <div class="carousel-caption">
                        <h3><a href="experiencias.php"><?php echo $lang['TEXT_SLIDEA']; ?></h3>
      <h2><?php echo $lang['SUBTEXT_SLIDEA']; ?></a></h2>
                        <a href="experiencias.php" class="btn_slide"><span class="icon"><i class="fa fa-search fa-lg "></i></span><span class="text"><?php echo $lang['BOTON_SLIDE']; ?></span></a>
                    </div>
                </a>
                </div>
            </div>
        </header>
                </li>
                <li>
                
                        <header id="myCarousel2" class="carousel slide">
            <div class="carousel-inner" style="border: 5px solid #FFFFFF;">
                <div class="item active">
                <a href="hotel.php">
                    <!--<div class="fill" style="background-image:url('images/carroa.jpg');"></div>-->
                    <div class="fill" style="background-image:url('images/carro_hotel.jpg');"></div>
                    <div class="carousel-caption">
                        <h3><a href="hotel.php"><?php echo $lang['TEXT_SLIDEB']; ?></h3>
                        <!-- <h3><a href="http://tphmarketing.com/" target="_blank"><?php //echo $lang['TEXT_SLIDEB']; ?></h3> -->
      <h2><?php echo $lang['SUBTEXT_SLIDEB']; ?></a></h2>
                        <a href="hotel.php" class="btn_slide"><span class="icon"><i class="fa fa-search fa-lg "></i></span><span class="text"><?php echo $lang['BOTON_SLIDE']; ?></span></a>
                    </div>
                </a>
                </div>
            </div>
        </header>
                
                </li>                                  
            </ul> 
     </div>
    <!-- /.container -->
        
    <div class="back_color">
    <!-- Page Content -->
    <div class="container">

        <!-- Marketing Icons Section -->
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body-expe1">
                        <p><?php echo $lang['TEXT_CUADRO1']; ?></p>
                        <a href="solicitar_regalo_paso1.php" class="btn_new"><span class="icon"><i class="fa fa-play fa-lg "></i>
</span><span class="text"><?php echo $lang['BOTON_CUADRO1']; ?></span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body-expe2">
                        <p><?php echo $lang['TEXT_CUADRO2']; ?></p>
                        <a href="faq.php" class="btn_new"><span class="icon"><i class="fa fa-search fa-lg "></i>
</span><span class="text"><?php echo $lang['BOTON_CUADRO2']; ?></span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body-expe3">
                        <p><?php echo $lang['TEXT_CUADRO3']; ?></p>
                        <a href="contacto.php" class="btn_new"><span class="icon"><i class="fa fa-phone fa-lg "></i>
</span><span class="text"><?php echo $lang['BOTON_CUADRO3']; ?></span></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->

    </div>
    <!-- /.back color -->
                   <!--//BLOQUE COOKIES-->
            <div id="barraaceptacion">
                <div class="block_cookie">
                    Utilizamos cookies de terceros para mejorar tu accesibilidad, personalizar y analizar tus hábitos de navegación. Si continuas navegando, consideramos que aceptas su uso. Puedes cambiar la configuración u obtener más información en Política de Cookies.
                    <a href="javascript:void(0);" onclick="PonerCookie();"><b>OK</b></a> | 
                    <a href="#" data-toggle="modal" data-target="#politica">M&aacute;s informaci&oacute;n</a>
                </div>
            </div>
    	<div id="hover"></div>


    
    <?php include("assets/includes/popups/cookies.php"); ?>
         
    <?php include("assets/includes/popups/enviado.php"); ?>
    
    <?php include("assets/includes/popups/lopd.php"); ?>
    
    <?php include("assets/includes/footer.php"); ?>


    </body>
   </html> 

    
    
      <script type="text/javascript">

			$(window).load(function() {
			
				$("#flexiselDemo3").flexisel({
					visibleItems: 2,
					animationSpeed: 1000,
					autoPlay: false,
					autoPlaySpeed: 3000,            
					pauseOnHover: true,
					enableResponsiveBreakpoints: true,
					responsiveBreakpoints: { 
						portrait: { 
							changePoint:480,
							visibleItems: 1
						}, 
						landscape: { 
							changePoint:640,
							visibleItems: 2
						},
						tablet: { 
							changePoint:768,
							visibleItems: 2
						}
					}
				});
				
			});
			</script>
    
                <script>
            function getCookie(c_name){
                var c_value = document.cookie;
                var c_start = c_value.indexOf(" " + c_name + "=");
                if (c_start == -1){
                    c_start = c_value.indexOf(c_name + "=");
                }
                if (c_start == -1){
                    c_value = null;
                }else{
                    c_start = c_value.indexOf("=", c_start) + 1;
                    var c_end = c_value.indexOf(";", c_start);
                    if (c_end == -1){
                        c_end = c_value.length;
                    }
                    c_value = unescape(c_value.substring(c_start,c_end));
                }
                return c_value;
            }
            
            function setCookie(c_name,value,exdays){
                var exdate=new Date();
                exdate.setDate(exdate.getDate() + exdays);
                var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
                document.cookie=c_name + "=" + c_value;
            }
            
            if(getCookie('tiendaaviso')!="1"){
                document.getElementById("barraaceptacion").style.display="block";
            }
            function PonerCookie(){
                setCookie('tiendaaviso','1',365);
                document.getElementById("barraaceptacion").style.display="none";
            }
            </script>
        <!--//FIN BLOQUE COOKIES-->

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Vertical tabs -->
    <script src="assets/js/tabs.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/popup.js"></script>
    
    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>
