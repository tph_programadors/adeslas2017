<?php
session_start();
header('Cache-control: private'); // IE 6 FIX

if(isSet($_GET['lang']))
{
$lang = $_GET['lang'];

// register the session and set the cookie
$_SESSION['lang'] = $lang;

setcookie("lang", $lang, time() + (3600 * 24 * 30));
}
else if(isSet($_SESSION['lang']))
{
$lang = $_SESSION['lang'];
}
else if(isSet($_COOKIE['lang']))
{
$lang = $_COOKIE['lang'];
}
else
{
$lang = 'es';
}

switch ($lang) {
  case 'cat':
  $lang_file = 'lang.cat.php';
  $lang_sql = 'cat';
  $lang_gionarchivo = '_cat';
  $lang_insert="CAT";
  break;
  
  case 'es':
  $lang_file = 'lang.es.php';
  $lang_sql = 'es';
  $lang_gionarchivo = '';
  $lang_insert="";
  break;

  default:
  $lang_file = 'lang.es.php';
  $lang_sql = 'es';
  $lang_gionarchivo = '';
  $lang_insert="";

}

include_once 'languages/'.$lang_file;
?>