<?php
    include_once 'common.php';
    ?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <title><?php echo $lang['PAGE_TITLE']; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../assets/css/modern-business.css" rel="stylesheet">
    <link href="../assets/css/cookies.css" rel="stylesheet" type="text/css">

    <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- se inicializa el modo "no-conflicto" -->
<script>var $j = jQuery.noConflict();</script>
    
    <!-- Carrousel js & css -->
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    
     <!-- jQuery -->
    <script src="../assets/js/jquery.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.flexisel.js"></script>
    <?php include("assets/includes/analytics.php"); ?>
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

</head>

<body>
	
	

    <?php include("../assets/includes/top.php"); ?>
    
    
    

    
    

        
    <div class="back_color">
    <!-- Page Content -->
    <div class="container">

    </div>
    <!-- /.container -->

    </div>
    <!-- /.back color -->
    
    <div class="container">

        <H2>ENLACE CADUCADO</H2><br>
               <p>Hola <?php echo"$c_contacto_nombre";?>.</p>
                <p>El enlace ya ha caducado.</p><br>
                <p>Disponias de 7 días desde el <?php echo"$fecha_caducidad_bonook";?> para ultilzarlo.</p><br>
				<p>Gracias.</p>
    </div>
    
    
    
                   <!--//BLOQUE COOKIES-->
            <div id="barraaceptacion">
                <div class="block_cookie">
                    Utilizamos cookies de terceros para mejorar su accesibilidad, personalizar y analizar sus hábitos de navegación. Si continúa navegando, consideramos que acepta su uso. Puede cambiar la configuración u obtener más información en Política de Cookies.
                    <a href="javascript:void(0);" onclick="PonerCookie();"><b>OK</b></a> | 
                    <a href="#" data-toggle="modal" data-target="#politica">M&aacute;s informaci&oacute;n</a>
                </div>
            </div>
    
    
    <?php include("../assets/includes/popups/cookies.php"); ?>
         
    <?php include("../assets/includes/popups/enviado.php"); ?>
    
    <?php include("../assets/includes/popups/lopd.php"); ?>
    
    <?php include("../assets/includes/footer.php"); ?>
    
    </body>
   </html> 

    
    
      <script type="text/javascript">

			$(window).load(function() {
			
				$("#flexiselDemo3").flexisel({
					visibleItems: 2,
					animationSpeed: 1000,
					autoPlay: true,
					autoPlaySpeed: 3000,            
					pauseOnHover: true,
					enableResponsiveBreakpoints: true,
					responsiveBreakpoints: { 
						portrait: { 
							changePoint:480,
							visibleItems: 1
						}, 
						landscape: { 
							changePoint:640,
							visibleItems: 2
						},
						tablet: { 
							changePoint:768,
							visibleItems: 2
						}
					}
				});
				
			});
			</script>
    
                <script>
            function getCookie(c_name){
                var c_value = document.cookie;
                var c_start = c_value.indexOf(" " + c_name + "=");
                if (c_start == -1){
                    c_start = c_value.indexOf(c_name + "=");
                }
                if (c_start == -1){
                    c_value = null;
                }else{
                    c_start = c_value.indexOf("=", c_start) + 1;
                    var c_end = c_value.indexOf(";", c_start);
                    if (c_end == -1){
                        c_end = c_value.length;
                    }
                    c_value = unescape(c_value.substring(c_start,c_end));
                }
                return c_value;
            }
            
            function setCookie(c_name,value,exdays){
                var exdate=new Date();
                exdate.setDate(exdate.getDate() + exdays);
                var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
                document.cookie=c_name + "=" + c_value;
            }
            
            if(getCookie('tiendaaviso')!="1"){
                document.getElementById("barraaceptacion").style.display="block";
            }
            function PonerCookie(){
                setCookie('tiendaaviso','1',365);
                document.getElementById("barraaceptacion").style.display="none";
            }
            </script>
        <!--//FIN BLOQUE COOKIES-->

    <!-- Bootstrap Core JavaScript -->
    <script src="../assets/js/bootstrap.min.js"></script>

    <!-- Vertical tabs -->
    <script src="../assets/js/tabs.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../assets/js/popup.js"></script>
    
    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>