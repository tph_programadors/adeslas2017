<?php
    include_once 'common.php';
    ?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <title><?php echo $lang['PAGE_TITLE']; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="assets/css/modern-business.css" rel="stylesheet">
    <link href="assets/css/cookies.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="assets/css/jquery.fancybox2.css" media="screen" />
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- se inicializa el modo "no-conflicto" -->
<script>var $j = jQuery.noConflict();</script>
    
    <!-- Carrousel js & css -->
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    
     <!-- jQuery -->
    <script src="assets/js/jquery.js"></script>
    <script type="text/javascript" src="assets/js/jquery.flexisel.js"></script>
    <?php include("assets/includes/analytics.php"); ?>
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
        <script src="assets/js/jquery.fancybox.js"></script>
<script type="text/javascript">
		$(document).ready(function() {
			$('.fancybox').fancybox();
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});
			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});
			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});
			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});
			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});
			$("#fancybox-manual-a").click(function() {
				$.fancybox.open('1_b.jpg');
			});

			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'iframe.html',
					type : 'iframe',
					padding : 5
				});
			});

			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});


		});
	</script>

</head>

<body>
	
	

    <?php include("assets/includes/top_sinidiomas.php"); ?>
    
    
    

    
    

        
    <div class="back_color">
    <!-- Page Content -->
    <div class="container">

    </div>
    <!-- /.container -->

    </div>
    <!-- /.back color -->
    
    <div class="container" style="text-align:center;">
    	<img class="img-responsive" src="images/slide_cast.jpg" alt="SegurCaixa Adeslas">
		<br>
        <br>
        <br>
		
              <H2>EXPERIENCIA ASIGNADA</H2><br>
               <p>Hola <?php echo"$c_contacto_nombre";?>.</p>
                <p>Desde TPH Marketing y gracias a Segurcaixa Adeslas nos complace poderle ofrecer la experiencia deseada. <br>
                Su elección <b><?php echo"$p_nombre_servicio2";?></b>, ha sido la asignada!!! <br>
                El centro donde irá a disfrutar de su experiencia es: </p>
				<p><br></p>
				<p><b><?php echo"$p_nombre_proveedor2";?></b></p>
                <p><b><?php echo"$p_direccion_proveedor2, $p_cp_proveedor2 - $p_municipio_proveedor2 ($p_provincia_proveedor2)";?></b></p>
                <p><b><?php echo"$expotorgadafechaok";?></b></p>
				<p><br></p>
                <p>Deberá confirmar y concretar hora con el proveedor llamando al teléfono indicado en el BONO.</p>
                <p>Solo falta que nos confirme la experiencia y prodrá descargarse el BONO. </p> 
                <p>Un saludo. </p>
                     <form id="formulariocontactoform" action="" method="post" class="formulariocontactox">
                     <a href="bono/peticionaceptaexperiencia.php?x=<?php echo"$codigoget";?>&y=<?php echo"$idget";?>" ><button type="button" class="btn_new1" id="formaceptaexp" name="formaceptaexp"><span class="icon1"><i class="fa fa-check fa-2x"></i></span><span class="text">ACEPTAR</span></button></a>
                     <a class="fancybox" href="#segurodeniega"><button type="button" class="btn_new1" id="formaceptaexp" name="formaceptaexp"><span class="icon1"><i class="fa fa-check fa-2x"></i></span><span class="text">RECHAZAR</span></button></a>
                    <br />
                    <br />
					</form>
                    </div>
                    <div id="respuestacontacto"></div>
        <br>
        <br>
    </div>
    
    


<div id="segurodeniega" style="display:none;">
			<div class="contform">
               <H2>¡Aviso!</H2>
               <hr>
               <p>¿Está seguro de que desea denegar la experiencia otorgada?</p>
                <p>Una vez denegada, su participación se considerará nula y perderá el derecho al regalo.<br>
                <hr>
                <form>
                <a href="bono/peticiondeniegaexperiencia.php?x=<?php echo"$codigoget";?>&y=<?php echo"$idget";?>"><button type="button" class="btn_new1" id="formaceptaexp" name="formaceptaexp"><span class="icon1"><i class="fa fa-check fa-2x"></i></span><span class="text">ESTOY SEGURO</span></button></a>
                </form>
            </div>
</div>
    
                   <!--//BLOQUE COOKIES-->
            <div id="barraaceptacion">
                <div class="block_cookie">
                    Utilizamos cookies de terceros para mejorar su accesibilidad, personalizar y analizar sus hábitos de navegación. Si continúa navegando, consideramos que acepta su uso. Puede cambiar la configuración u obtener más información en Política de Cookies.
                    <a href="javascript:void(0);" onclick="PonerCookie();"><b>OK</b></a> | 
                    <a href="#" data-toggle="modal" data-target="#politica">M&aacute;s informaci&oacute;n</a>
                </div>
            </div>
    
    
    <?php include("assets/includes/popups/cookies.php"); ?>
         
    <?php include("assets/includes/popups/enviado.php"); ?>
    
    <?php include("assets/includes/popups/lopd.php"); ?>
    
    <?php include("assets/includes/footer.php"); ?>
    
    </body>
   </html> 

    
    
      <script type="text/javascript">

			$(window).load(function() {
			
				$("#flexiselDemo3").flexisel({
					visibleItems: 2,
					animationSpeed: 1000,
					autoPlay: true,
					autoPlaySpeed: 3000,            
					pauseOnHover: true,
					enableResponsiveBreakpoints: true,
					responsiveBreakpoints: { 
						portrait: { 
							changePoint:480,
							visibleItems: 1
						}, 
						landscape: { 
							changePoint:640,
							visibleItems: 2
						},
						tablet: { 
							changePoint:768,
							visibleItems: 2
						}
					}
				});
				
			});
			</script>
    
                <script>
            function getCookie(c_name){
                var c_value = document.cookie;
                var c_start = c_value.indexOf(" " + c_name + "=");
                if (c_start == -1){
                    c_start = c_value.indexOf(c_name + "=");
                }
                if (c_start == -1){
                    c_value = null;
                }else{
                    c_start = c_value.indexOf("=", c_start) + 1;
                    var c_end = c_value.indexOf(";", c_start);
                    if (c_end == -1){
                        c_end = c_value.length;
                    }
                    c_value = unescape(c_value.substring(c_start,c_end));
                }
                return c_value;
            }
            
            function setCookie(c_name,value,exdays){
                var exdate=new Date();
                exdate.setDate(exdate.getDate() + exdays);
                var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
                document.cookie=c_name + "=" + c_value;
            }
            
            if(getCookie('tiendaaviso')!="1"){
                document.getElementById("barraaceptacion").style.display="block";
            }
            function PonerCookie(){
                setCookie('tiendaaviso','1',365);
                document.getElementById("barraaceptacion").style.display="none";
            }
            </script>
        <!--//FIN BLOQUE COOKIES-->

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Vertical tabs -->
    <script src="assets/js/tabs.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/popup.js"></script>
    
    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>