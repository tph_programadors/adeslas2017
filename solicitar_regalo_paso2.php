<?php
session_start();
include_once 'common.php';
if($lang_sql=="cat"){
	$lengbusc="_cat";
}else{
	$lengbusc="";
}
$fecha_actual = date('Y-m-d H:i:s');
$ipvisita = $_SERVER['REMOTE_ADDR'];
if (!isset($_SESSION["adeslas2hogar2016_codigo"])) {//comprueba que la sesion existe.
    session_unset();
    session_destroy();
    header ("Location: http://disfrutaunaexperienciaunica.com");
}
if (isset($_SESSION["adeslas2hogar2016_paso2"])) {//comprueba que la sesion paso 2existe.
    $paso2sesion = $_SESSION["adeslas2hogar2016_paso2"];
    if ($paso2sesion != "1") {
        session_unset();
        session_destroy();
        echo"<script type='text/javascript'>
            window.location='http://disfrutaunaexperienciaunica.com';
            </script>";
    }
} else {
    session_unset();
    session_destroy();
    echo"<script type='text/javascript'>
        window.location='http://disfrutaunaexperienciaunica.com';
        </script>";
}

$provincia=1;

include "./assets/connect/conexion.php";
include "./assets/php/categoryServices.php";

$tipocampana = $_SESSION["adeslas2hogar2016_tipocampana"];

$table_prefix = "adeslas2hogar2016";

$CategoryServiceArray = getCategoryWithActiveServices($table_prefix, $link, "10,12");

$sqlcompruebacodigo = "SELECT * FROM `".$table_prefix."__cod_promo` WHERE `CODIGO` = '".$_SESSION["adeslas2hogar2016_codigo"]."'";
$rscompruebacodigo = mysqli_query($link, $sqlcompruebacodigo);
$num_total_codigo = mysqli_num_rows($rscompruebacodigo);
while ($row = mysqli_fetch_array($rscompruebacodigo)) {
	$n_codigo = $row["CODIGO"];
	$n_promo = $row["PROMO"];
	$n_fechareg = $row["F_REGISTRO"];
	$n_experiencia = $row["ESPECIAL"];
	$n_provincia = $row["CP"];
}
?>
<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <title><?php echo $lang['PAGE_TITLE']; ?></title>

        <!-- Bootstrap Core CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="assets/css/modern-business.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- Data Picker -->
        <link rel="stylesheet" href="assets/css/jquery-ui.css">
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/jquery-ui.js"></script>
        <script type="text/javascript">
            $(function () {

                $("#selectExperienceForm").submit(function () {
                    var url = "assets/includes/forms/canjeacodigo-2.php"; // El script a donde se realizara la peticion.
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: $("#selectExperienceForm").serialize(), // Adjuntar los campos del formulario enviado.
                        success: function (data)
                        {
                            $("#respuesta2").html(data);
                        }
                    });
                    return false; // Evitar ejecutar el submit del formulario.
                });

                $("#datepicker1").datepicker({
                    changeMonth: true,
                    changeYear: true,
					<?php
					  if($lang_sql=="cat"){
						  echo'monthNamesShort: ["Gen", "Feb", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dec"],
						  dayNamesMin: ["Dg","Dl","Dt","Dc","Dj","Dv","Ds"],';
					  }
					  ?>
                    firstDay: 1
                });

                $("#datepicker2").datepicker({
                    changeMonth: true,
                    changeYear: true,
					<?php
					  if($lang_sql=="cat"){
						  echo'monthNamesShort: ["Gen", "Feb", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dec"],
						  dayNamesMin: ["Dg","Dl","Dt","Dc","Dj","Dv","Ds"],';
					  }
					  ?>
                    firstDay: 1
                });

                $("#datepicker3").datepicker({
                    changeMonth: true,
                    changeYear: true,
					<?php
					  if($lang_sql=="cat"){
						  echo'monthNamesShort: ["Gen", "Feb", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dec"],
						  dayNamesMin: ["Dg","Dl","Dt","Dc","Dj","Dv","Ds"],';
					  }
					  ?>
                    firstDay: 1
                });
            });

        </script>
        <script type="text/javascript">
		$(document).ready(function(){
			$("#categorySelect1").change(function () {
				   $("#categorySelect1 option:selected").each(function () {
					elegido=$(this).val();
					$.post("./assets/includes/forms/categorias-select-experiencias.php", { elegido: elegido }, function(data){
					$("#serviceSelect1").html(data);
					});            
				});
		   })
		});
		</script>
        <script type="text/javascript">
		$(document).ready(function(){
			$("#categorySelect2").change(function () {
				   $("#categorySelect2 option:selected").each(function () {
					elegido=$(this).val();
					$.post("./assets/includes/forms/categorias-select-experiencias.php", { elegido: elegido }, function(data){
					$("#serviceSelect2").html(data);
					});            
				});
		   })
		});
		</script>
        <script type="text/javascript">
		$(document).ready(function(){
			$("#categorySelect3").change(function () {
				   $("#categorySelect3 option:selected").each(function () {
					elegido=$(this).val();
					$.post("./assets/includes/forms/categorias-select-experiencias.php", { elegido: elegido }, function(data){
					$("#serviceSelect3").html(data);
					});            
				});
		   })
		});
		</script>


    <style>
	 .carousel-control .icon-prev{left:50%;margin-left:-120px}
	 .carousel-control .icon-next{right:50%;margin-right:-120px}
	 
	</style>
    </head>
    <body>



        <?php include("assets/includes/top_hidden.php"); ?>

        <?php include("assets/includes/analytics.php"); ?>

        <form name="selectExperienceForm" id="selectExperienceForm">
            <div class="back_color">
                <!-- Image Header -->
                <!-- Page Content -->
                
                
                <div class="container">
             <!-- Banner Carousel -->
            <header id="myCarousel" class="carousel slide">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                    <li data-target="#myCarousel" data-slide-to="4"></li>
                </ol>
        
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="fill" style="background-image:url('images/catas.jpg');"></div>
                        <div class="carousel-caption">
                           <h3><?php echo $lang['TEXT_CATAS1']; ?></h3>
                           <h2><?php echo $lang['TEXT_CATAS']; ?></h2>
                            
                        </div>
                    </div>
                    <div class="item">
                        <div class="fill" style="background-image:url('images/curos.jpg');"></div>
                        <div class="carousel-caption">
                           <h3><?php echo $lang['TEXT_CURSOS1']; ?></h3>
                           <h2><?php echo $lang['TEXT_CURSOS']; ?></h2>
                            
                        </div>
                    </div>
                    <div class="item">
                        <div class="fill" style="background-image:url('images/espectaculos.jpg');"></div>
                        <div class="carousel-caption">
                           <h3><?php echo $lang['TEXT_ESPEC1']; ?></h3>
                           <h2><?php echo $lang['TEXT_ESPEC']; ?></h2>
                            
                        </div>
                    </div>
                    <div class="item">
                        <div class="fill" style="background-image:url('images/salud.jpg');"></div>
                        <div class="carousel-caption">
                           <h3><?php echo $lang['TEXT_SALUD1']; ?></h3>
                           <h2><?php echo $lang['TEXT_SALUD']; ?></h2>
                            
                        </div>
                    </div>
                    <div class="item">
                        <div class="fill" style="background-image:url('images/aventura.jpg');"></div>
                        <div class="carousel-caption">
                           <h3><?php echo $lang['TEXT_AVENTURA1']; ?></h3>
                           <h2><?php echo $lang['TEXT_AVENTURA']; ?></h2>
                            
                        </div>
                    </div>
                    
                    
                </div>
        
                <!-- Controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="icon-prev"><i class="fa fa-angle-left fa-2x "></i>
        </span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="icon-next"><i class="fa fa-angle-right fa-2x "></i>
        </span>
                </a>
            </header>
            </div>
                
                
                
                
                <!-- /.container -->
            </div>
            <!-- /.back color -->

            <!-- Page Content -->
            <div class="container" style="padding-left:0px; padding-right:0px;">
                <!-- Banner Carousel -->
                <div class="col-md-12">




                    <!-- OPCIONS PER SELECCIONAR EXPERIENCIES -->
                    <div class="panel_middle_paso1">
                        <?php echo $lang['TEXT_MIDDLE_PASO1']; ?>
                        </br>
                        <!--<form name="selectExperienceForm" id="selectExperienceForm">-->
                        <div class="col-md-4"> 
                            <label for="categorySelect1"><?php echo $lang['TEXT_11']; ?></label>
                            <select class="form-control form-select categorySelect" id="categorySelect1" name="categorySelect1" for="serviceSelect1">
                                <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                                <option value="1"><?php echo $lang['TEXT_BELLEZA_2'];?></option>
                                <option value="2"><?php echo $lang['TEXT_CURSOS_2'];?></option>
                                <option value="3"><?php echo $lang['TEXT_DEPORTES_2'];?></option>
                                <option value="4"><?php echo $lang['TEXT_RESTAURACION_2'];?></option>
                                <option value="5"><?php echo $lang['TEXT_OCIO_2'];?></option>  
                            </select>
                            <label for=""><?php echo $lang['TEXT_12']; ?></label>
                            <select class="form-control form-select" id="serviceSelect1" name="experiencia1">
                                <option value="" selected="selected"></option>
                            </select>

                        </div>
                        <div class="col-md-4">
                            <label class="primera" for="categorySelect2"><?php echo $lang['TEXT_21']; ?></label>
                            <select class="form-control form-select categorySelect" id="categorySelect2" name="categorySelect2" for="serviceSelect2">
                                <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                                <option value="1"><?php echo $lang['TEXT_BELLEZA_2'];?></option>
                                <option value="2"><?php echo $lang['TEXT_CURSOS_2'];?></option>
                                <option value="3"><?php echo $lang['TEXT_DEPORTES_2'];?></option>
                                <option value="4"><?php echo $lang['TEXT_RESTAURACION_2'];?></option>
                                <option value="5"><?php echo $lang['TEXT_OCIO_2'];?></option>                            </select>
                            <label for=""><?php echo $lang['TEXT_22']; ?></label>
                            <select class="form-control form-select" id="serviceSelect2" name="experiencia2" >
                                <option value="" selected="selected"></option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="primera" for="categorySelect3"><?php echo $lang['TEXT_31']; ?></label>
                            <select class="form-control form-select categorySelect" id="categorySelect3" name="categorySelect3" for="serviceSelect3">
                                <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                                <option value="1"><?php echo $lang['TEXT_BELLEZA_2'];?></option>
                                <option value="2"><?php echo $lang['TEXT_CURSOS_2'];?></option>
                                <option value="3"><?php echo $lang['TEXT_DEPORTES_2'];?></option>
                                <option value="4"><?php echo $lang['TEXT_RESTAURACION_2'];?></option>
                                <option value="5"><?php echo $lang['TEXT_OCIO_2'];?></option>
                            </select>
                            <label for=""><?php echo $lang['TEXT_32']; ?></label>
                            <select class="form-control form-select" id="serviceSelect3" name="experiencia3" >
                                <option value="" selected="selected"></option>
                            </select>
                        </div>
                        <p>&nbsp;</p>
                    </div>    <!-- /.fi panell middle  paso 1 -->
                    <!-- FI OPCIONS PER SELECCIONAR EXPERIENCIES -->

                    

                    <!-- OPCIONS PER SELECCIONAR EXPERIENCIES ok-->
                    <!--<div class="panel_middle_paso1">
                        <?php echo $lang['TEXT_MIDDLE_PASO1']; ?>
                        </br>
                        <div class="col-md-4"> 
                            <label for=""><?php echo $lang['TEXT_12']; ?></label>
                            <select class="form-control form-select" id="serviceSelect1" name="experiencia1">
                                <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                                 <?php
                                    $sqlservicios2 = "SELECT * FROM campanas_experiencias WHERE id_rel_campana = '17' "; 
                                    $rsservicios2 = mysqli_query($link, $sqlservicios2);
                                        while ($row=mysqli_fetch_array($rsservicios2)) { 
                                            $id_servicio=utf8_encode($row["id_rel_servicio"]);
                                            echo"";

                                            $sqlprovin2= "SELECT * FROM proveedores_servicios
                                            LEFT JOIN proveedores_servicios_provincias ON proveedores_servicios_provincias.rel_serv_provincia = proveedores_servicios.id_servicio_ofrece 
                                            LEFT JOIN servicios ON servicios.id_servicio = proveedores_servicios.rel_servicio
                                            LEFT JOIN proveedores ON proveedores.id_prov = proveedores_servicios.rel_proveedor_servicio 
                                            WHERE ((rel_prov_provincia = '$provincia')or(proveedores.provincia_prov = '$provincia')) AND id_servicio='$id_servicio' AND precio_servicio_ofrece < '12.00' AND servicio_activo = '1' AND rel_categoria !='12' GROUP BY nombre_servicio "; 
                                            $rsprovin2= mysqli_query($link, $sqlprovin2);
                                            $num_total_servicios = mysqli_num_rows($rsprovin2);
                                            if($num_total_servicios==0){

                                            }else{
                                                while ($row=mysqli_fetch_array($rsprovin2)) { 
                                                    $nombreprov=utf8_encode($row["nombre_prov"]);
                                                    $nombre_servicio=utf8_encode($row["nombre_servicio"]);
                                                    $id_servicio_ofrece=utf8_encode($row["id_servicio_ofrece"]);
                                                    $rel_servicio_ofrece=utf8_encode($row["rel_servicio"]);
                                                    echo"<option value='$rel_servicio_ofrece'> $nombre_servicio </option>";
                                                }
                                            }
                                        }
                                ?>
                            </select>

                        </div>
                        <div class="col-md-4">
                            <label for=""><?php echo $lang['TEXT_22']; ?></label>
                            <select class="form-control form-select" id="serviceSelect2" name="experiencia2" >
                                <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                                   <?php
                                    $sqlservicios2 = "SELECT * FROM campanas_experiencias WHERE id_rel_campana = '17' "; 
                                    $rsservicios2 = mysqli_query($link, $sqlservicios2);
                                        while ($row=mysqli_fetch_array($rsservicios2)) { 
                                            $id_servicio=utf8_encode($row["id_rel_servicio"]);
                                            echo"";

                                            $sqlprovin2= "SELECT * FROM proveedores_servicios
                                            LEFT JOIN proveedores_servicios_provincias ON proveedores_servicios_provincias.rel_serv_provincia = proveedores_servicios.id_servicio_ofrece 
                                            LEFT JOIN servicios ON servicios.id_servicio = proveedores_servicios.rel_servicio
                                            LEFT JOIN proveedores ON proveedores.id_prov = proveedores_servicios.rel_proveedor_servicio 
                                            WHERE ((rel_prov_provincia = '$provincia')or(proveedores.provincia_prov = '$provincia')) AND id_servicio='$id_servicio' AND precio_servicio_ofrece < '12.00' AND servicio_activo = '1' AND rel_categoria !='12' GROUP BY nombre_servicio "; 
                                            $rsprovin2= mysqli_query($link, $sqlprovin2);
                                            $num_total_servicios = mysqli_num_rows($rsprovin2);
                                            if($num_total_servicios==0){

                                            }else{
                                                while ($row=mysqli_fetch_array($rsprovin2)) { 
                                                    $nombreprov=utf8_encode($row["nombre_prov"]);
                                                    $nombre_servicio=utf8_encode($row["nombre_servicio"]);
                                                    $id_servicio_ofrece=utf8_encode($row["id_servicio_ofrece"]);
                                                    $rel_servicio_ofrece=utf8_encode($row["rel_servicio"]);
                                                    echo"<option value='$rel_servicio_ofrece'> $nombre_servicio </option>";
                                                }
                                            }
                                        }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for=""><?php echo $lang['TEXT_32']; ?></label>
                            <select class="form-control form-select" id="serviceSelect3" name="experiencia3" >
                                <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                                 <?php
                                    $sqlservicios2 = "SELECT * FROM campanas_experiencias WHERE id_rel_campana = '17' "; 
                                    $rsservicios2 = mysqli_query($link, $sqlservicios2);
                                        while ($row=mysqli_fetch_array($rsservicios2)) { 
                                            $id_servicio=utf8_encode($row["id_rel_servicio"]);
                                            echo"";

                                            $sqlprovin2= "SELECT * FROM proveedores_servicios
                                            LEFT JOIN proveedores_servicios_provincias ON proveedores_servicios_provincias.rel_serv_provincia = proveedores_servicios.id_servicio_ofrece 
                                            LEFT JOIN servicios ON servicios.id_servicio = proveedores_servicios.rel_servicio
                                            LEFT JOIN proveedores ON proveedores.id_prov = proveedores_servicios.rel_proveedor_servicio 
                                            WHERE ((rel_prov_provincia = '$provincia')or(proveedores.provincia_prov = '$provincia')) AND id_servicio='$id_servicio' AND precio_servicio_ofrece < '12.00' AND servicio_activo = '1' AND rel_categoria !='12' GROUP BY nombre_servicio "; 
                                            $rsprovin2= mysqli_query($link, $sqlprovin2);
                                            $num_total_servicios = mysqli_num_rows($rsprovin2);
                                            if($num_total_servicios==0){

                                            }else{
                                                while ($row=mysqli_fetch_array($rsprovin2)) { 
                                                    $nombreprov=utf8_encode($row["nombre_prov"]);
                                                    $nombre_servicio=utf8_encode($row["nombre_servicio"]);
                                                    $id_servicio_ofrece=utf8_encode($row["id_servicio_ofrece"]);
                                                    $rel_servicio_ofrece=utf8_encode($row["rel_servicio"]);
                                                    echo"<option value='$rel_servicio_ofrece'> $nombre_servicio </option>";
                                                }
                                            }
                                        }
                                ?>
                            </select>
                        </div>
                        <p>&nbsp;</p>
                    </div> -->
                       <!-- /.fi panell middle  paso 1 -->
                    <!-- FI OPCIONS PER SELECCIONAR EXPERIENCIES ok-->
                    
                    
                </div>
            </div>
            <!-- /.container -->


            <div class="back_color">
                <!-- Page Content -->
                <div class="container" style="padding-left:0px; padding-right:0px;">
                    <!-- Banner Carousel -->
                    <div class="col-md-12">

                        <div class="panel_bottom">
                            <?php echo $lang['TEXT_MIDDLE_PASO2']; ?>
                            </br>
                            <div class="col-md-4"> 
                                <label for=""><?php echo $lang['TEXT_13']; ?></label>
                                </br>
                                <input type="text" id="datepicker1" name="fecha1" class="form-control"  placeholder="<?php echo $lang['TEXT_DATA1']; ?>">
                            </div>
                            <div class="col-md-4">
                                <label for=""><?php echo $lang['TEXT_23']; ?></label>
                                </br>
                                <input type="text" id="datepicker2" name="fecha2" class="form-control"  placeholder="<?php echo $lang['TEXT_DATA2']; ?>">
                            </div>
                            <div class="col-md-4">
                                <label for=""><?php echo $lang['TEXT_33']; ?></label>
                                </br>
                                <input type="text" id="datepicker3" name="fecha3" class="form-control"  placeholder="<?php echo $lang['TEXT_DATA3']; ?>">
                            </div>
                            <p>&nbsp;</p>
                            <p class="recuerde"><?php echo $lang['LABEL_CAMPOS']; ?></p>
                            <button type="submit" class="btn_new1"><span class="icon1"><i class="fa fa-check fa-2x"></i></span><span class="text"><?php echo $lang['CONFIRM']; ?></span></button> 
                            <!--</form>-->
                            <div id="respuesta2"></div>
                            <p class="recuerde"><?php echo $lang['RECUERDE']; ?>
                            </p>

                        </div>    <!-- /.fi panel_bottom -->
                    </div>    <!-- /.fi col 12 -->

                </div> <!-- /.fi back container -->
            </div>  <!-- /.fi back color -->
        </form>
        <?php include("assets/includes/footer.php"); ?>

        <!-- Bootstrap Core JavaScript -->
        <script src="assets/js/bootstrap.min.js"></script>

        <!-- Vertical tabs -->
        <script src="assets/js/tabs.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="assets/js/popup.js"></script>

        <!-- Script to Activate the Carousel -->
        <script>
            $('.carousel').carousel({
                interval: 5000 //changes the speed
            })
        </script>
    </body>
</html>