<?php
session_start();
$tipocampana = $_SESSION["adeslas2hogar2016_tipocampana"];
$table_prefix = "adeslas2hogar2016";
include_once 'common.php';
if($lang_sql=="cat"){
	$lengbusc="_cat";
}else{
	$lengbusc="";
}
include "./assets/connect/conexion.php";

function getUrlParams($params) {
    filter_input(INPUT_GET, 'y', FILTER_SANITIZE_NUMBER_INT);
    filter_input(INPUT_GET, 'z', FILTER_SANITIZE_STRING);

    $params["get_npet"]        = isset($_GET["y"])    ? filter_input(INPUT_GET, 'y', FILTER_SANITIZE_NUMBER_INT) : $params["get_npet"];
    $params["get_contcodigo"]  = isset($_GET["z"])    ? filter_input(INPUT_GET, 'z', FILTER_SANITIZE_STRING)     : $params["get_contcodigo"];
    
    if ((isset($_GET["y"]))&&(isset($_GET["z"]))){
        $params["userDataReset"]=true;
    }
    
    return $params;
}

//default params init
$params = array();
$params["get_npet"] = '';
$params["get_contcodigo"] = '';
$params["get_ready"]=false;
$params["userDataReset"]=false;

$formValues = array();
$formValues["nombreformcanjea"] = '';
$formValues["apellidosformcanjea"] = '';
$formValues["sexoformcanjea"] = '';
$formValues["nacimformcanjea"] = date('d-m-Y');
$formValues["dniformcanjea"] = '';
$formValues["mailformcanjea"] = '';
$formValues["telfformcanjea"] = '';
$formValues["provinciaformcanjea"] = '';
$formValues["poblacionformcanjea"] = '';
$formValues["cpformcanjea"] = '';
$formValues["direccionformcanjea"] = '';


$formValuesPartner = array();
$formValuesPartner["nombreformcanjea"] = '';
$formValuesPartner["apellidosformcanjea"] = '';
$formValuesPartner["sexoformcanjea"] = '';
$formValuesPartner["nacimformcanjea"] = date('d-m-Y');
$formValuesPartner["dniformcanjea"] = '';
$formValuesPartner["mailformcanjea"] = '';
$formValuesPartner["telfformcanjea"] = '';
$formValuesPartner["provinciaformcanjea"] = '';
$formValuesPartner["poblacionformcanjea"] = '';
$formValuesPartner["cpformcanjea"] = '';
$formValuesPartner["direccionformcanjea"] = '';

//true if partner, false if not.
$formValuesPartner["partner"] = 'false';

$params=getUrlParams($params);

$fecha_actual=date('Y-m-d H:i:s');
$ipvisita=$_SERVER['REMOTE_ADDR'];

//If we have all the GET params
if (($params["get_npet"] != '')&&($params["get_contcodigo"] != '')){
    $sqlcompruebaexiste = "SELECT * FROM `".$table_prefix."__peticion` WHERE `N_PET` = '".$params["get_npet"]."' AND CODIGO = '".$params["get_contcodigo"]."'";
    //if comming from edit url ( with params ) check if can be edited.
    if ($params["userDataReset"] == true){
        $sqlcompruebaexiste .= " AND NOEDITABLE ='0'";
    }

    /* change character set to utf8 */
    if (!$link->set_charset("utf8")) {
        printf("Error loading character set utf8: %s\n", $link->error);
    }
	
    $result= mysqli_query($link,$sqlcompruebaexiste);
    if (mysqli_num_rows($result) == 1) {
        $params["get_ready"]=true;
        
        //Populate array of values
        while ($row=mysqli_fetch_array($result)) {
                $formValues["nombreformcanjea"] 	= $row["NOMBRE"];
                $formValues["apellidosformcanjea"]	= $row["APELLIDOS"];
                $formValues["sexoformcanjea"]           = $row["SEXO"];
                $formValues["nacimformcanjea"]		= $row["fechanacimiento"];
                $formValues["dniformcanjea"]		= $row["NIF"];
                $formValues["mailformcanjea"]		= $row["MAIL"];
                $formValues["telfformcanjea"]		= $row["TELEFONO"];
                $formValues["provinciaformcanjea"]	= $row["PROVINCIA"];
                $formValues["poblacionformcanjea"]	= $row["POBLACION"];
                $formValues["cpformcanjea"]		= $row["CP"];
                $formValues["direccionformcanjea"]	= $row["DIRECCION"];
        }
	
        
        $sqlcheckpartner = "SELECT * FROM `".$table_prefix."__peticion_acompanante` WHERE `rel_pet_acompanante` = '".$params["get_npet"]."'";
    //    $sqlcheckpartner .= " AND `desactivado` != '1'";

        /* change character set to utf8 */
        if (!$link->set_charset("utf8")) {
            printf("Error loading character set utf8: %s\n", $link->error);
        }
	
	$partnerrs= mysqli_query($link,$sqlcheckpartner);
	if (mysqli_num_rows($partnerrs) == 1) {
        $params["get_ready"]=true;
        
            //Populate array of values
            while ($row=mysqli_fetch_array($partnerrs)) {
                    $formValuesPartner["nombreformcanjea"]      = $row["nombre_pet_acompanante"];
                    $formValuesPartner["apellidosformcanjea"]   = $row["apellidos_pet_acompanante"];
                    $formValuesPartner["sexoformcanjea"]        = $row["sexo"];
                    $formValuesPartner["nacimformcanjea"]       = $row["fechanac_pet_acompanante"];
                    $formValuesPartner["dniformcanjea"]         = $row["dni_pet_acompanante"];
                    $formValuesPartner["mailformcanjea"]        = $row["email_pet_acompanante"];
                    $formValuesPartner["telfformcanjea"]        = $row["telf_pet_acompanante"];
                    $formValuesPartner["provinciaformcanjea"]   = $row["provincia_pet_acompanante"];
                    $formValuesPartner["poblacionformcanjea"]   = $row["municipio_pet_acompanante"];
                    $formValuesPartner["cpformcanjea"]          = $row["cp_pet_acompanante"];
                    $formValuesPartner["direccionformcanjea"]   = $row["direccion_pet_acompanante"];

                    $formValuesPartner["menorformcanjea"] 		= $row["menor_pet_acompanante"];
                    $formValuesPartner["partner"] = 'true';
            }

            $_SESSION["adeslas2hogar2016_partner"]=$formValuesPartner["partner"];
        }else{
            
	}
	
        //set session params for later register steps.
        $_SESSION["adeslas2hogar2016_codigo"]=$params["get_contcodigo"];
        $_SESSION["adeslas2hogar2016_paso2"]=2;
        $_SESSION["adeslas2hogar2016_id_peticion"]=$params["get_npet"];
        $_SESSION["adeslas2hogar2016_userDataReset"]=$params["userDataReset"];
        
    }else{
        session_unset();
        session_destroy();
        echo"<script type='text/javascript'>
            window.location='http://disfrutaunaexperienciaunica.com';
            </script>";
	}
}

if ($params["get_ready"]==false){
    if (!isset($_SESSION["adeslas2hogar2016_codigo"])){//comprueba que la sesion existe.
        session_unset();
        session_destroy();
        echo"<script type='text/javascript'>
            window.location='http://disfrutaunaexperienciaunica.com';
            </script>";
    }
    if (isset($_SESSION["adeslas2hogar2016_paso2"])){//comprueba que la sesion paso 2existe.
        $paso2sesion=$_SESSION["adeslas2hogar2016_paso2"];
        if($paso2sesion != "2"){
            session_unset();
            session_destroy();
            echo"<script type='text/javascript'>
                window.location='http://disfrutaunaexperienciaunica.com';
                </script>";
        }
    }else{
        session_unset();
        session_destroy();
        echo"<script type='text/javascript'>
            window.location='http://disfrutaunaexperienciaunica.com';
            </script>";
    }
}
$sqlcompruebacodigo = "SELECT * FROM `".$table_prefix."__cod_promo` WHERE `CODIGO` = '".$_SESSION["adeslas2hogar2016_codigo"]."'";
$rscompruebacodigo = mysqli_query($link, $sqlcompruebacodigo);
$num_total_codigo = mysqli_num_rows($rscompruebacodigo);
while ($row = mysqli_fetch_array($rscompruebacodigo)) {
	$n_codigo = $row["CODIGO"];
	$n_promo = $row["PROMO"];
	$n_fechareg = $row["F_REGISTRO"];
	$n_experiencia = $row["ESPECIAL"];
	$n_provincia = $row["CP"];
}
?>

<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    
    <title><?php echo $lang['PAGE_TITLE']; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="assets/css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <!-- Data Picker -->
    <link rel="stylesheet" href="assets/css/jquery-ui.css">
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#nacimformcanjea" ).datepicker({
              changeMonth: true,
              changeYear: true,
			  <?php
			  if($lang_sql=="cat"){
				  echo'monthNamesShort: ["Gen", "Feb", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dec"],
				  dayNamesMin: ["Dg","Dl","Dt","Dc","Dj","Dv","Ds"],';
			  }
			  ?>
              firstDay: 1
            });
            
            $( "#nacim_aco_formcanjea" ).datepicker({
              changeMonth: true,
              changeYear: true,
			  <?php
			  if($lang_sql=="cat"){
				  echo'monthNamesShort: ["Gen", "Feb", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dec"],
				  dayNamesMin: ["Dg","Dl","Dt","Dc","Dj","Dv","Ds"],';
			  }
			  ?>
              firstDay: 1
            });
            
            $("#registro").submit(function () {
                var url = "./assets/includes/forms/canjeacodigo-3.php"; // El script a donde se realizara la peticion.

                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#registro").serialize(), // Adjuntar los campos del formulario enviado.
                    success: function (data)
                    {
                        $("#respuesta3").html(data);
                    }
                });

                return false; // Evitar ejecutar el submit del formulario.
            });
            
            $("#registroAco").submit(function () {
                var url = "./assets/includes/forms/canjeacodigo-3.php?aco=1"; // El script a donde se realizara la peticion.
                var serialData = $("#registro").serialize() + "&" + $("#registroAco").serialize();
                $.ajax({
                    type: "POST",
                    url: url,
                    data: serialData,
                    success: function (data)
                    {
                        $("#respuesta3").html(data);
                    }
                });

                return false; // Evitar ejecutar el submit del formulario.
            });
            
            // Select provincies & municipios
            $("#provinciaformcanjea").change(function () {
                    $("#provinciaformcanjea option:selected").each(function () {
                        field_province = $(this).val();
                        $.post(
                            "./assets/php/municipios.php", {
                                elegido: field_province,
                                selected: <?php echo "\"".$formValues["poblacionformcanjea"]."\"";  ?>
                            }, 
                            function (data) {
                                $("#poblacionformcanjea").html(data);
                            }
                        );
                });
            });
            
            //trigger change to fullfill town field when paramters ( data preload )
            $( "#provinciaformcanjea" ).trigger( "change" );
            
            
            // Select provincies & municipios
            $("#provincia_aco_formcanjea").change(function () {
                    $("#provincia_aco_formcanjea option:selected").each(function () {
                        field_province = $(this).val();
                        //set sync post
                        $.ajaxSetup({async: false});
                        $.post(
                            "./assets/php/municipios.php", {
                                elegido: field_province,
                                selected: <?php echo "\"".$formValuesPartner["poblacionformcanjea"]."\"";  ?>
                            }, 
                            function (data) {
                                $("#poblacion_aco_formcanjea").html(data);
                            }
                        );
                        //set againdasync post
                        $.ajaxSetup({async: true});
                });
            });
            
            //trigger change to fullfill town field when paramters ( data preload )
            $( "#provincia_aco_formcanjea" ).trigger( "change" );
            
            
            
            // Select if ID document is select and enable/disable form authorization download.
            $("#menor_aco_formcanjea").change(function () {
                    $("#menor_aco_formcanjea option:selected").each(function () {
                        var option = $(this).val();
                        var authFormDownloadButton = $("#authFormDownloadButton");
                        switch(option) {
                            case "0":
                                //0 : No under age
                                authFormDownloadButton.addClass( "disabled" );
                                break;
                            case "1":
                                //1 : Yes under age with ID document
                                authFormDownloadButton.removeClass( "disabled" );
                                break;
                            case "2":
                                //2 : Yes under age without ID document
                                authFormDownloadButton.removeClass( "disabled" );
                                break;
                            default:
                                authFormDownloadButton.removeClass( "disabled" );
                        }
                });
            });
            
            //trigger change to fullfill town field when paramters ( data preload )
            $( "#menor_aco_formcanjea" ).trigger( "change" );
            
            //bind copyAddress click event to null?
            $("#copyAddress").click(function() {null;});
            
            //
			$("#botonirsolo").click(function() {
				$('#irsolo-aceptar-modal').modal('hide');
			});
			$(document).ready(function() {
                //Togle visibility of partner data input div ( and focus )
                $('#botoniracompanado').click(function(){
					$('#irsolo-aceptar-modal').modal('hide');
                    if ( $('#content_two').is(":visible") == false ){
                        $('#content_two').show(200);
						$('html,body').animate({
							scrollTop: $("#divcontacopm").offset().top
						}, 1000);
                    }else{
                        $('#content_two').hide(200);
                    }
                });
                    
            });
			
             $("#copyAddress").change(function() {
                if (this.checked){
                    $("#direccion_aco_formcanjea").val($("#direccionformcanjea").val());
                    $("#provincia_aco_formcanjea").val($("#provinciaformcanjea").val());
                    $("#cp_aco_formcanjea").val($("#cpformcanjea").val());
					var select1 = document.getElementById("poblacionformcanjea");
					var select2 = document.getElementById("poblacion_aco_formcanjea");
					var $options = $("#poblacionformcanjea > option").clone();
					$('#poblacion_aco_formcanjea').empty();
					$('#poblacion_aco_formcanjea').append($options);
					$('#poblacion_aco_formcanjea').val($('#poblacionformcanjea').val());

						
                }
            });
            
            //trigger change to fullfill town field when paramters ( data preload )
            $( "#provincia_aco_formcanjea" ).trigger( "change" );
			
            $(document).ready(function() {
                //Togle visibility of partner data input div ( and focus )
                $('#b1').click(function(){
                    if ( $('#content_two').is(":visible") == false ){
                        $('#content_two').show(200);
                        $('html,body').animate({
							scrollTop: $("#divcontacopm").offset().top
						}, 1000);
                    }else{
                        $('#content_two').hide(200);
                    }
                });     
            });
			$("#menor_aco_formcanjea").change(function(){
				if(this.value == '1'||this.value == '2'){
					$("#textomenor").show(200);
				}else{
					$("#textomenor").hide(200);
				}
				if(this.value == '2'){
					$("#acomp_dni").hide(200);
				}else{
					$("#acomp_dni").show(200);
				}
				
			});
        });
    </script>
</head>

<body>

    <?php include("assets/includes/top_hidden.php"); ?>
    
    <?php include("assets/includes/analytics.php"); ?>
        
<div class="back_color">
    <!-- Page Content -->
    <div class="container">
        <!-- Marketing Icons Section -->
        <div class="row">
            <div class="col-md-12">
                 <div class="panel_bottom">
                 <?php echo $lang['TEXT3_1']; ?>
                  
                <div class="form">
                <form name="enviarRegistro" id="registro" >
                    <div class="control-group form-group">
                        <div class="controls">
                            <label><?php echo $lang['TEXT3_3']; ?></label>
                            <input type="text" class="form-control" id="nombreformcanjea" name="nombreformcanjea">
                            <p class="help-block"></p>
                        </div>
                    </div>
                    
                    <div class="control-group form-group">
                        <div class="controls">
                            <label><?php echo $lang['TEXT3_4']; ?></label>
                            <input type="text" class="form-control" id="apellidosformcanjea" name="apellidosformcanjea" >
                            <p class="help-block"></p>
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label for="sexoformcanjea"><?php echo $lang['TEXT3_24']; ?></label>
                            <select class="form-control form-select" id="sexoformcanjea" name="sexoformcanjea" class="form-control">
                                <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                                <option value="0" <?php echo ($formValues["sexoformcanjea"]=="0")? "selected": null ?> ><?php echo $lang['TEXT3_25']; ?></option>
                                <option value="1" <?php echo ($formValues["sexoformcanjea"]=="1")? "selected": null ?> ><?php echo $lang['TEXT3_26']; ?></option>
                            </select>
                        </div>
                    </div>
                    

              <div class="form-group">
                    <label><?php echo $lang['TEXT3_5']; ?></label>
                    </br>
                    <input type="text" id="nacimformcanjea" name="nacimformcanjea" class="form-control">
                    </div>
                    
                    <div class="control-group form-group">
                        <div class="controls">
                            <label><?php echo $lang['TEXT3_6']; ?></label>
                            <input type="text" class="form-control" id="dniformcanjea" name="dniformcanjea">
                            <p class="help-block"></p>
                        </div>
                    </div>
                    
                    <div class="control-group form-group">
                        <div class="controls">
                            <label><?php echo $lang['TEXT3_7']; ?></label>
                            <input type="email" class="form-control" id="mailformcanjea" name="mailformcanjea">
                        </div>
                    </div>
                    
                    
                    <div class="control-group form-group">
                        <div class="controls">
                            <label><?php echo $lang['TEXT3_8']; ?></label>
                            <input type="text" class="form-control" id="telfformcanjea" name="telfformcanjea">
                        </div>
                    </div>
                    
                    
                    <div class="control-group form-group">
                        <div class="controls">
                            <label><?php echo $lang['TEXT3_9']; ?></label>
                            <input type="text" class="form-control" id="direccionformcanjea" name="direccionformcanjea">
                            <p class="help-block"></p>
                        </div>
                    </div>
                    
                    <div class="form-group">
                      <label for="provinciaformcanjea"><?php echo $lang['TEXT3_12']; ?></label>
                    <select class="form-control form-select" id="provinciaformcanjea" name="provinciaformcanjea">
                     <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                        <?php
                            $sqlprovincias = "SELECT * FROM `crm_provincias` ORDER BY crm_provincia ASC";
                            $rsprovincias = mysqli_query($link,$sqlprovincias);
                            while ( $row = mysqli_fetch_array($rsprovincias) ) {
                                $selected="";
                                $id_provincia = $row["crm_id_provincia"];
                                $nombre_provincia2 = $row["crm_provincia"];
                                $nombre_provincia = utf8_encode($nombre_provincia2);
                                if ($formValues["provinciaformcanjea"] == $id_provincia){
                                        $selected="selected";
                                }
                                echo"<option value='$id_provincia' $selected>$nombre_provincia</option>";
                            }
                        ?>
                    </select>
                    </div>
                    
                    <div class="form-group">
                      <label for="poblacionformcanjea"><?php echo $lang['TEXT3_10']; ?></label>
                    <select class="form-control form-select" id="poblacionformcanjea" name="poblacionformcanjea">
                        <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                        <option value="" >Opción</option>
                    </select>
                    </div>

                    
                    <div class="control-group form-group">
                        <div class="controls">
                            <label><?php echo $lang['TEXT3_11']; ?></label>
                            <input type="text" class="form-control" id="cpformcanjea" name="cpformcanjea">
                            <p class="help-block"></p>
                        </div>
                    </div>
                    
                    

                    <p><?php echo $lang['TEXT3_13']; ?></p>

                    <!-- For success/fail messages -->
                    <div class="col-md-6">
                    <?php include("assets/includes/popups/irsolo_aceptar.php"); ?>
                    <?php if($tipocampana=="2"){?>
                    <button type="submit" class="btn_new" id="botonirsolo"><span class="icon"><i class="fa fa-step-forward fa-2x"></i></span><span class="text"><?php echo $lang['TEXT_ACO7']; ?></span></button>
                    <?php }else{?>
                    <a href="#" data-toggle="modal" data-target="#irsolo-aceptar-modal"> <button type="button" class="btn_new"><span class="icon"><i class="fa fa-male fa-2x"></i></span><span class="text"><?php echo $lang['TEXT3_14']; ?></span></button>   </a>
                     <?php } ?>
                    </div>
                    <?php if($tipocampana!="2"){?>
                    <div class="col-md-6">
                        <button type="button" id="b1" class="btn_new2"><span class="icon"><i class="fa fa-male fa-2x"></i><i class="fa fa-male fa-2x"></i></span><span class="text"><?php echo $lang['TEXT3_15']; ?></span></button>
                        </br>
                    </div>
                    <?php } ?>
                    <div id="respuesta3"></div>
                </form>
                </div><!-- fi form-->
                
                <p class="recuerde_full"><?php echo $lang['TEXT3_16']; ?></p>
                </div><!-- Fi panel bottom -->
             </div><!-- Fi col 12 -->
        </div><!-- /.row -->
      </div><!-- /. container -->
    </div><!-- /.back color -->


    
    
    
     <!------------------------------------ Acompañante Formulario ------------------------------------>
    

               
<div id="content_two" <?php echo ($formValuesPartner["partner"] == 'true')? '': 'style="display: none;"'; ?> >
  <div class="back_color_white">
    <!-- Page Content -->
    <div class="container">
        <!-- Marketing Icons Section -->
         <div class="row">
             <div class="col-md-12">
                 <div class="panel_bottom_grey" id="divcontacopm">
                 <?php echo $lang['TEXT_ACO1']; ?>

                <div class="form">
                <form name="enviarRegistroAco" id="registroAco" >
                    <div class="control-group form-group">
                        <div class="controls">
                            <label><?php echo $lang['TEXT3_3']; ?></label>
                            <input type="text" class="form-control" id="nombre_aco_formcanjea" name="nombre_aco_formcanjea">
                            <p class="help-block"></p>
                        </div>
                    </div>
                    
                    <div class="control-group form-group">
                        <div class="controls">
                            <label><?php echo $lang['TEXT3_4']; ?></label>
                            <input type="text" class="form-control" id="apellidos_aco_formcanjea" name="apellidos_aco_formcanjea">
                            <p class="help-block"></p>
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label for="sexo_aco_formcanjea"><?php echo $lang['TEXT3_24']; ?></label>
                            <select class="form-control form-select" id="sexo_aco_formcanjea" name="sexo_aco_formcanjea" class="form-control">
                                <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                                <option value="0" <?php echo ($formValuesPartner["sexoformcanjea"]=="0")? "selected": null ?> ><?php echo $lang['TEXT3_25']; ?></option>
                                <option value="1" <?php echo ($formValuesPartner["sexoformcanjea"]=="1")? "selected": null ?> ><?php echo $lang['TEXT3_26']; ?></option>
                            </select>
                        </div>
                    </div>
                    

              <div class="form-group">
                    <label><?php echo $lang['TEXT3_5']; ?></label>
                    </br>
                    <input type="text" id="nacim_aco_formcanjea" name="nacim_aco_formcanjea" class="form-control">
                    </div>
                   
                    
                    <div class="control-group form-group">
                        <div class="controls">
                            <label for="menor_aco_formcanjea"><?php echo $lang['TEXT_ACO2']; ?></label>
                            <select class="form-control form-select" id="menor_aco_formcanjea" name="menor_aco_formcanjea" class="form-control">
                                <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                                <option value="1" <?php echo ($formValuesPartner["menorformcanjea"]=="1")? "selected": null ?> ><?php echo $lang['TEXT3_27']; ?></option>
                                <option value="2" <?php echo ($formValuesPartner["menorformcanjea"]=="2")? "selected": null ?> ><?php echo $lang['TEXT3_28']; ?></option>
                                <option value="0" <?php echo ($formValuesPartner["menorformcanjea"]=="0")? "selected": null ?> ><?php echo $lang['TEXT3_29']; ?></option>
                            </select>
                        </div>
                        <p id="textomenor" style="display:none;"><?php echo $lang['TEXT_ACO3']; ?> 
                        </br> <button type="button" id="authFormDownloadButton" 
                                      onclick="if(!($('#authFormDownloadButton').hasClass('disabled'))){window.open('./docs/AUTORIZACION_MENORES<?php echo"$lengbusc";?>.pdf','_blank');}"
                                      class="btn btn_new"><span class="icon1"><i class="fa fa-download fa-2x"></i></span><span class="text"><?php echo $lang['TEXT_ACO4']; ?></span></button></p>
                    </div>
                    
                    <div class="control-group form-group" id="acomp_dni">
                        <div class="controls">
                            <label><?php echo $lang['TEXT3_6']; ?></label>
                            <input type="text" class="form-control" id="dni_aco_formcanjea" name="dni_aco_formcanjea">
                            <p class="help-block"></p>
                        </div>
                    </div>
                    
                    <div class="control-group form-group">
                        <div class="controls">
                            <label><?php echo $lang['TEXT3_7']; ?></label>
                            <input type="email" class="form-control" id="mail_aco_formcanjea" name="mail_aco_formcanjea" >
                            <p class="help-block"></p>
                        </div>
                    </div>
                    
                      
                    <div class="control-group form-group">
                        <div class="controls">
                            <label><?php echo $lang['TEXT3_8']; ?></label>
                            <input type="text" class="form-control" id="telf_aco_formcanjea" name="telf_aco_formcanjea" >
                            <p class="help-block"></p>
                        </div>
                    </div>
                    
                    <p><input type="checkbox" id="copyAddress"><?php echo $lang['TEXT_ACO6']; ?></p>
                    
                    <div class="control-group form-group">
                        <div class="controls">
                            <label><?php echo $lang['TEXT3_9']; ?></label>
                            <input type="text" class="form-control" id="direccion_aco_formcanjea" name="direccion_aco_formcanjea">
                            <p class="help-block"></p>
                        </div>
                    </div>
                    
                    <div class="form-group">
                      <label for="provincia_aco_formcanjea"><?php echo $lang['TEXT3_12']; ?></label>
                    <select class="form-control form-select" id="provincia_aco_formcanjea"  name="provincia_aco_formcanjea">
                     <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                        <?php
                            $sqlprovincias = "SELECT * FROM `crm_provincias` ORDER BY crm_provincia ASC";
                            $rsprovincias = mysqli_query($link,$sqlprovincias);
                            while ( $row = mysqli_fetch_array($rsprovincias) ) {
                                $selected="";
                                $id_provincia = $row["crm_id_provincia"];
                                $nombre_provincia2 = $row["crm_provincia"];
                                $nombre_provincia = utf8_encode($nombre_provincia2);
                                if ($formValuesPartner["provinciaformcanjea"] == $id_provincia){
                                        $selected="selected";
                                }
                                echo"<option value='$id_provincia' $selected>$nombre_provincia</option>";
                            }
                        ?>
                    </select>
                    </div>
                                        
                    <div class="form-group">
                      <label for="poblacion_aco_formcanjea"><?php echo $lang['TEXT3_10']; ?></label>
                    <select class="form-control form-select" id="poblacion_aco_formcanjea" name="poblacion_aco_formcanjea">
                        <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                        <option value="" >Opción</option>
                    </select>
                    </div>

                    
                    <div class="control-group form-group">
                        <div class="controls">
                            <label><?php echo $lang['TEXT3_11']; ?></label>
                            <input type="text" class="form-control" id="cp_aco_formcanjea" name="cp_aco_formcanjea" >
                            <p class="help-block"></p>
                        </div>
                    </div>
                    
                    

                    <p><?php echo $lang['TEXT3_13']; ?></p>

                    <!-- For success/fail messages -->
                    <div class="col-md-6">
                        <button type="submit" class="btn_new"><span class="icon"><i class="fa fa-step-forward fa-2x"></i></span><span class="text"><?php echo $lang['TEXT_ACO7']; ?></span></button>
                    </div>
                    <div id="respuesta3"></div>
                </form>
                </div><!-- fi form-->
                
                <p class="recuerde_full"><?php echo $lang['TEXT3_16']; ?></p>
                </div><!-- Fi panel bottom -->
             </div><!-- Fi col 12 -->
        </div><!-- /.row -->
       </div><!-- /. container -->
    </div><!-- /. back_color_white -->
  </div><!-- /.  content_two-->
  
               
               
               
               
               
    
    <?php include("assets/includes/footer.php"); ?>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Vertical tabs -->
    <script src="assets/js/tabs.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/popup.js"></script>