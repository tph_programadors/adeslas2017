<?php
session_start();
include_once '../../../common.php';
$table_prefix = "adeslas2hogar2016";

$fecha_actual = date('Y-m-d H:i:s');
$fecha_actual_dia = date('Y-m-d');

function CalculaDias($fecha_i,$fecha_f){
	$dias	= (strtotime($fecha_i)-strtotime($fecha_f))/86400;
	$dias 	= abs($dias); $dias = floor($dias);		
	return $dias;
}
$user_agent = $_SERVER['HTTP_USER_AGENT'];
 
function getBrowser($user_agent){

if(strpos($user_agent, 'MSIE') !== FALSE)
   return 'Internet explorer';
elseif(strpos($user_agent, 'Trident') !== FALSE) //IE 11
    return 'Internet explorer';
elseif(strpos($user_agent, 'Firefox') !== FALSE)
   return 'Mozilla Firefox';
elseif(strpos($user_agent, 'Chrome') !== FALSE)
   return 'Google Chrome';
elseif(strpos($user_agent, 'Opera Mini') !== FALSE)
   return "Opera Mini";
elseif(strpos($user_agent, 'Opera') !== FALSE)
   return "Opera";
elseif(strpos($user_agent, 'Safari') !== FALSE)
   return "Safari";
else
   return 'No hemos podido detectar su navegador';
}
function getPlatform($user_agent) {
   $plataformas = array(
           'Windows 10' => 'Windows NT 10.0+',
      'Windows 8' => 'Windows NT 6.3+',
      'Windows 8' => 'Windows NT 6.2+',
      'Windows 7' => 'Windows NT 6.1+',
      'Windows Vista' => 'Windows NT 6.0+',
      'Windows XP' => 'Windows NT 5.1+',
      'Windows 2003' => 'Windows NT 5.2+',
      'Windows otros' => 'Windows',
      'iPhone' => 'iPhone',
      'iPad' => 'iPad',
      'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
      'Mac otros' => 'Macintosh',
      'Android' => 'Android',
      'BlackBerry' => 'BlackBerry',
      'Linux' => 'Linux',
   );
   foreach($plataformas as $plataforma=>$pattern){
      if (eregi($pattern, $user_agent))
         return $plataforma;
   }
   return 'Otras';
}
  
$navegador = getBrowser($user_agent);
$SO = getPlatform($user_agent);
$ipvisita=$_SERVER['REMOTE_ADDR'];

if (isset($_SESSION["adeslas2hogar2016_codigo"])) {//comprueba que la sesion existe.

    if (isset($_SESSION["adeslas2hogar2016_paso2"])) {//comprueba que la sesion paso 2existe.
	
        $paso2sesion = $_SESSION["adeslas2hogar2016_paso2"];
        if ($paso2sesion != "1") {
            session_unset();
            session_destroy();
            echo"<script type='text/javascript'>
                    window.location='https://disfrutaunaexperienciaunica.com';
                </script>";
        } else {
			
			
			$sesion_codigo = $_SESSION["adeslas2hogar2016_codigo"]; //definimos variable sesion.
			$tipocampana = $_SESSION["adeslas2hogar2016_tipocampana"];
			$poliza = $_SESSION["adeslas2hogar2016_poliza"];
		
			$experiencia1 = $_POST["experiencia1"];
			$experiencia1ok = utf8_decode($experiencia1);
			
			$experiencia2 = $_POST["experiencia2"];
			$experiencia2ok = utf8_decode($experiencia2);
			
			$experiencia3 = $_POST["experiencia3"];
			$experiencia3ok = utf8_decode($experiencia3);
			
			if ($tipocampana == "1"){
			$Categoria1 = $_POST["province1"];
			$Categoria2 = $_POST["province2"];
			$Categoria3 = $_POST["province3"];
			}else{
			$Categoria1 = $_POST["categorySelect1"];
			$Categoria2 = $_POST["categorySelect2"];
			$Categoria3 = $_POST["categorySelect3"];
			}
		
			$fecha1_1 = $_POST["fecha1"];
			$fecha1_12 = new DateTime($fecha1_1);
			$fecha1 = $fecha1_12->format('Y-m-d');
		
			$fecha2_1 = $_POST["fecha2"];
			$fecha2_12 = new DateTime($fecha2_1);
			$fecha2 = $fecha2_12->format('Y-m-d');
		
			$fecha3_1 = $_POST["fecha3"];
			$fecha3_12 = new DateTime($fecha3_1);
			$fecha3 = $fecha3_12->format('Y-m-d');
		
			include "../../connect/conexion.php";
			
			//if tipo campana ==1 HOtel save at database DEST1..DEST3 as province_experience
			//eg: 12_34
			if ($tipocampana == "1"){
				$experiencia1ok = utf8_decode($_POST["province1"]) ."_". $experiencia1ok;
				$experiencia2ok = utf8_decode($_POST["province2"]) ."_". $experiencia2ok;
				$experiencia3ok = utf8_decode($_POST["province3"]) ."_". $experiencia3ok;
			}
			
						////////////////////comprueba errores/////////////////////////
			if($tipocampana==1){
						$errorcat1_hotel="TEXT_FORM2_1_1";
						$errorcat2_hotel="TEXT_FORM2_2_2";
					}else{
						$errorcat1_hotel="TEXT_FORM2_1";
						$errorcat2_hotel="TEXT_FORM2_2";
					}
			if(($Categoria1=="")or($Categoria1==NULL)){
					$num_total_Categoria1 = 1;	
					$errorText_Categoria1="$('#categorySelect1').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang[$errorcat1_hotel]."</div>');";
			}else{
					$errorText_Categoria1="";
			}
			
			if(($experiencia1=="")or($experiencia1==NULL)){
					$num_total_experiencia1 = 1;
					$errorText_experiencia1="$('#serviceSelect1').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang[$errorcat2_hotel]."</div>');";
			}else{
					$errorText_experiencia1="";
			}
			
			
			if(($Categoria2=="")or($Categoria2==NULL)){
					$num_total_Categoria2 = 1;
					$errorText_Categoria2="$('#categorySelect2').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang[$errorcat1_hotel]."</div>');";
			}else{
					$errorText_Categoria2="";
			}
			
			if(($experiencia2=="")or($experiencia2==NULL)){
					$num_total_experiencia2 = 1;
					$errorText_experiencia2="$('#serviceSelect2').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang[$errorcat2_hotel]."</div>');";
			}else{
					$errorText_experiencia2="";
			}
			
			
			if(($Categoria3=="")or($Categoria3==NULL)){
					$num_total_Categoria3 = 1;
					$errorText_Categoria3="$('#categorySelect3').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang[$errorcat1_hotel]."</div>');";
			}else{
					$errorText_Categoria3="";
			}
			
			if(($experiencia3=="")or($experiencia3==NULL)){
					$num_total_experiencia3 = 1;
					$errorText_experiencia3="$('#serviceSelect3').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang[$errorcat2_hotel]."</div>');";
			}else{
					$errorText_experiencia3="";
			}
			
			
			$num_total_exp_iguales==0;
			if((($experiencia1==$experiencia2)or($experiencia1==$experiencia3)or($experiencia2==$experiencia3))&&((($experiencia1!="")or($experiencia1!=NULL))&&(($experiencia2!="")or($experiencia2!=NULL))&&(($experiencia3!="")or($experiencia3!=NULL)))){
				$num_total_exp_iguales = 1;
					$errorText_exp_iguales="$('#serviceSelect1').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM2_1_exp']."</div>');";
			}else{
					$errorText_exp_iguales="";
			}
			
			
			
			if(($fecha1_1=="")or($fecha1_1==NULL)){
					$num_total_fecha1_1 = 1;
					$errorText_fecha1_1="$('#datepicker1').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM2_3']."</div>');";
			}else{
					$errorText_fecha1_1="";
					if($fecha1<$fecha_actual_dia){
						$num_total_fecha1_1_comp = 1;
						$errorText_fecha1_1_comp="$('#datepicker1').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM2_1_comp']."</div>');";
					}else{
						if( CalculaDias($fecha_actual_dia,$fecha1)<30){
								$num_total_fecha1_1_comp = 1;
								$errorText_fecha1_1_comp="$('#datepicker1').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM2_1_comp']."</div>');";
						}else{
								$errorText_fecha1_1_comp="";
						}
					}
			}
			
			
			
			
			if(($fecha2_1=="")or($fecha2_1==NULL)){
					$num_total_fecha2_1 = 1;
					$errorText_fecha2_1="$('#datepicker2').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM2_3']."</div>');";
			}else{
					$errorText_fecha2_1="";
					if($fecha2<$fecha1){
						$num_total_fecha2_1_comp = 1;
						$errorText_fecha2_1_comp="$('#datepicker2').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM2_2_comp']."</div>');";
					}else{
						if( CalculaDias($fecha1,$fecha2)<30){
								$num_total_fecha2_1_comp = 1;
								$errorText_fecha2_1_comp="$('#datepicker2').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM2_2_comp']."</div>');";
						}else{
								$errorText_fecha2_1_comp="";
						}
					}
			}
			
			
			if(($fecha3_1=="")or($fecha3_1==NULL)){
					$num_total_fecha3_1 = 1;
					$errorText_fecha3_1="$('#datepicker3').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM2_3']."</div>');";
			}else{
					$errorText_fecha3_1="";
					if($fecha3<$fecha2){
						$num_total_fecha3_1_comp = 1;
						$errorText_fecha3_1_comp="$('#datepicker3').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM2_3_comp']."</div>');";
					}else{
						if( CalculaDias($fecha2,$fecha3)<30){
								$num_total_fecha3_1_comp = 1;
								$errorText_fecha3_1_comp="$('#datepicker3').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM2_3_comp']."</div>');";
						}else{
								$errorText_fecha3_1_comp="";
						}
					}
			}

			

			////////////////////fin comprueba errores/////////////////////////
			
				if(($num_total_experiencia1==1)or($num_total_experiencia2==1)or($num_total_experiencia3==1)or($num_total_fecha1_1==1)or($num_total_fecha1_1_comp==1)or($num_total_fecha2_1==1)or($num_total_fecha2_1_comp==1)or($num_total_fecha3_1==1)or($num_total_fecha3_1_comp==1)or($num_total_exp_iguales==1)){
					echo"<script type='text/javascript'>  
							$(document).ready(function(){
								$('.DivError').remove();
								$errorText_Categoria1 
								$errorText_experiencia1
								$errorText_Categoria2 
								$errorText_experiencia2
								$errorText_Categoria3 
								$errorText_experiencia3
								$errorText_fecha3_1
								$errorText_fecha3_1_comp
								$errorText_fecha2_1
								$errorText_fecha2_1_comp
								$errorText_fecha1_1
								$errorText_fecha1_1_comp
								$errorText_exp_iguales
								$('#avisoPop').show();
								return false;  
							});
						</script>";
				}else{
					include "../../connect/conexion.php";

					$sql = "INSERT INTO " . $table_prefix . "__peticion ( `ESTADO`, `F_REGISTRO`, `F_REGISTROIP`, `CODIGO`, `IDIOMA`,`CP`, `POLIZA`,`SO`,`NAV`,`DEST1`,`DEST2`,`DEST3`,`F_IDA_1`,`F_IDA_2`,`F_IDA_3`,`TIPOCAMPANA`) VALUES ('1', '$fecha_actual','$ipvisita','$sesion_codigo', '$lang_insert', '$cpinsertado', '$poliza','$SO','$navegador','$experiencia1ok','$experiencia2ok','$experiencia3ok','$fecha1','$fecha2','$fecha3','$tipocampana')";
			mysqli_query($link, $sql);
	
			$sesion_id_peticion = mysqli_insert_id($link);
			$_SESSION["adeslas2hogar2016_id_peticion"] = $sesion_id_peticion;
			
			$sqlpromo = "UPDATE `".$table_prefix."__cod_promo` SET `PROMO` = '$sesion_id_peticion' WHERE CODIGO = '$sesion_codigo'";
			mysqli_query($link,$sqlpromo);
					
					$_SESSION["adeslas2hogar2016_paso2"] = "2";
					
					echo"<script type='text/javascript'>
					window.location='./solicitar_regalo_paso3.php';
					</script>";
				}
       
	    }
    } else {
        session_unset();
        session_destroy();
        echo"<script type='text/javascript'>
            window.location='http://disfrutaunaexperienciaunica.com';
        </script>";
    }

} else {//si la sesión no existe, redirecciona
    echo"<script type='text/javascript'>
    window.location='http://disfrutaunaexperienciaunica.com';
    </script>";
}
			
	
?>