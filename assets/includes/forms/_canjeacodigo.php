<?php
session_start();
include_once '../../../common.php';
$table_prefix = "adeslas2hogar2016";

$codigoinsertado = $_POST["insertacodigo"];
$cpinsertado = $_POST["cp"];
$poliza = $_POST["poliza"];
$tipocampana = $_POST["subcampaign"];

$fecha_actual = date('Y-m-d H:i:s');

$user_agent = $_SERVER['HTTP_USER_AGENT'];
 
function getBrowser($user_agent){
 
if(strpos($user_agent, 'MSIE') !== FALSE)
   return 'Internet explorer';
elseif(strpos($user_agent, 'Trident') !== FALSE) //IE 11
    return 'Internet explorer';
elseif(strpos($user_agent, 'Firefox') !== FALSE)
   return 'Mozilla Firefox';
elseif(strpos($user_agent, 'Chrome') !== FALSE)
   return 'Google Chrome';
elseif(strpos($user_agent, 'Opera Mini') !== FALSE)
   return "Opera Mini";
elseif(strpos($user_agent, 'Opera') !== FALSE)
   return "Opera";
elseif(strpos($user_agent, 'Safari') !== FALSE)
   return "Safari";
else
   return 'No hemos podido detectar su navegador';
 
 
}
 
function getPlatform($user_agent){
	if(strpos($user_agent, 'Windows NT 10.0') !== FALSE)
		return "Windows 10";
	elseif(strpos($user_agent, 'Windows NT 6.3') !== FALSE)
		return "Windows 8.1";
	elseif(strpos($user_agent, 'Windows NT 6.2') !== FALSE)
		return "Windows 8";
	elseif(strpos($user_agent, 'Windows NT 6.1') !== FALSE)
		return "Windows 7";
	elseif(strpos($user_agent, 'Windows NT 6.0') !== FALSE)
		return "Windows Vista";
	elseif(strpos($user_agent, 'Windows NT 5.1') !== FALSE)
		return "Windows XP";
	elseif(strpos($user_agent, 'Windows NT 5.2') !== FALSE)
		return 'Windows 2003';
	elseif(strpos($user_agent, 'Windows NT 5.0') !== FALSE)
		return 'Windows 2000';
	elseif(strpos($user_agent, 'Windows ME') !== FALSE)
		return 'Windows ME';
	elseif(strpos($user_agent, 'Win98') !== FALSE)
		return 'Windows 98';
	elseif(strpos($user_agent, 'Win95') !== FALSE)
		return 'Windows 95';
	elseif(strpos($user_agent, 'WinNT4.0') !== FALSE)
		return 'Windows NT 4.0';
	elseif(strpos($user_agent, 'Windows Phone') !== FALSE)
		return 'Windows Phone';
	elseif(strpos($user_agent, 'Windows') !== FALSE)
		return 'Windows';
	elseif(strpos($user_agent, 'iPhone') !== FALSE)
		return 'iPhone';
	elseif(strpos($user_agent, 'iPad') !== FALSE)
		return 'iPad';
	elseif(strpos($user_agent, 'Debian') !== FALSE)
		return 'Debian';
	elseif(strpos($user_agent, 'Ubuntu') !== FALSE)
		return 'Ubuntu';
	elseif(strpos($user_agent, 'Slackware') !== FALSE)
		return 'Slackware';
	elseif(strpos($user_agent, 'Linux Mint') !== FALSE)
		return 'Linux Mint';
	elseif(strpos($user_agent, 'Gentoo') !== FALSE)
		return 'Gentoo';
	elseif(strpos($user_agent, 'Elementary OS') !== FALSE)
		return 'ELementary OS';
	elseif(strpos($user_agent, 'Fedora') !== FALSE)
		return 'Fedora';
	elseif(strpos($user_agent, 'Kubuntu') !== FALSE)
		return 'Kubuntu';
	elseif(strpos($user_agent, 'Linux') !== FALSE)
		return 'Linux';
	elseif(strpos($user_agent, 'FreeBSD') !== FALSE)
		return 'FreeBSD';
	elseif(strpos($user_agent, 'OpenBSD') !== FALSE)
		return 'OpenBSD';
	elseif(strpos($user_agent, 'NetBSD') !== FALSE)
		return 'NetBSD';
	elseif(strpos($user_agent, 'SunOS') !== FALSE)
		return 'Solaris';
	elseif(strpos($user_agent, 'BlackBerry') !== FALSE)
		return 'BlackBerry';
	elseif(strpos($user_agent, 'Android') !== FALSE)
		return 'Android';
	elseif(strpos($user_agent, 'Mobile') !== FALSE)
		return 'Firefox OS';
	elseif(strpos($user_agent, 'Mac OS X+') || strpos($user_agent, 'CFNetwork+') !== FALSE)
		return 'Mac OS X';
	elseif(strpos($user_agent, 'Macintosh') !== FALSE)
		return 'Mac OS Classic';
	elseif(strpos($user_agent, 'OS/2') !== FALSE)
		return 'OS/2';
	elseif(strpos($user_agent, 'BeOS') !== FALSE)
		return 'BeOS';
	elseif(strpos($user_agent, 'Nintendo') !== FALSE)
		return 'Nintendo';
	else
		return 'Unknown Platform';
}
 
 
 
$navegador = getBrowser($user_agent);
$SO = getPlatform($user_agent);
$ipvisita=$_SERVER['REMOTE_ADDR'];

include "../../connect/conexion.php";


//comprueba cp insertado...
		if(($cpinsertado=="")or($cpinsertado==NULL)){
			$num_total_cp = 1;
		}else{
			if(preg_match('/^[0-9]{5}$/i', $cpinsertado)) {
			$num_total_cp = 0;
			}else{
			$num_total_cp = 2;
			}
		
		}
//comprueba tipo regalo insertado...		
		if(($tipocampana=="")or($tipocampana==NULL)){
			$num_total_tipocampana = 1;
		}else{
			$num_total_tipocampana = 0;	
		}

$sqlcompruebacodigo = "SELECT * FROM `" . $table_prefix . "__cod_promo` WHERE `CODIGO` = '$codigoinsertado'";
$rscompruebacodigo = mysqli_query($link, $sqlcompruebacodigo);
$num_total_codigo = mysqli_num_rows($rscompruebacodigo);
if ($num_total_codigo == NULL) {
    $num_total_codigo = 0;
}

		/*if(preg_match('#^90000.*#s', trim($poliza))){
			$num_poliza_existe = 0; 
        }else{
			$sqlcompruebapolizaexiste = "SELECT * FROM `" . $table_prefix . "__peticion` WHERE `POLIZA` = '$poliza'";
			$rscompruebapolizaexiste = mysqli_query($link, $sqlcompruebapolizaexiste);
			$num_poliza_existe = mysqli_num_rows($rscompruebapolizaexiste);
			if ($num_poliza_existe == NULL) {
				$num_poliza_existe = 0;
			}
 
        }*/
		


$sqlcompruebapoliza = "SELECT * FROM `" . $table_prefix . "__peticion_polizas` WHERE `POLIZA` = '$poliza'";
$rscompruebapoliza = mysqli_query($link, $sqlcompruebapoliza);
$num_total_poliza = mysqli_num_rows($rscompruebapoliza);
if ($num_total_poliza == NULL) {
    $num_total_poliza = 0;
}

$sqlcompruebapolizaycodigo = "SELECT * FROM `" . $table_prefix . "__peticion_polizas` WHERE `POLIZA` = '$poliza' AND `CODIGO` = '$codigoinsertado'";
$rscompruebapolizaycodigo = mysqli_query($link, $sqlcompruebapolizaycodigo);
$num_total_polizaycodigo = mysqli_num_rows($rscompruebapolizaycodigo);
if(($num_total_polizaycodigo!=0)or($num_total_polizaycodigo!=NULL)){
	while ($row=mysqli_fetch_array($rscompruebapolizaycodigo)) { 
	$x_tirada22=$row["TIRADA"]; 
	}
}
/*if(($x_tirada22==1)or($x_tirada22==2)or($x_tirada22==3)or($x_tirada22==4)or($x_tirada22==5)or($x_tirada22==6)or($x_tirada22==7)or($x_tirada22==8)or($x_tirada22==9)){
	$error_tirada_polizaycodigo = 1;
}else{*/
	$error_tirada_polizaycodigo = 0;
/*}
*/
if ($num_total_polizaycodigo == NULL) {
    $num_total_polizaycodigo = 0;
}

//comprobamos si el codigo existe
if (($num_total_codigo != "0") && ($num_total_poliza != "0")&&/*($num_poliza_existe=="0")&&*/($num_total_cp=="0")&&($num_total_tipocampana=="0")&&($num_total_polizaycodigo=="1")&&($error_tirada_polizaycodigo=="0")) {
    while ($row = mysqli_fetch_array($rscompruebacodigo)) {
        $n_codigo = $row["CODIGO"];
        $n_promo = $row["PROMO"];
        $n_fechareg = $row["F_REGISTRO"];
    }
	$fechacaducidad = strtotime ('+15 day' , strtotime ($n_fechareg)) ;
	$nuevafecha = date ( 'Y-m-d H:i:s' , $fechacaducidad);
	$segundos = strtotime($nuevafecha) - strtotime($fecha_actual); 
	if($segundos>0){
		$tiempo_agotado="0";
	}else{
		$tiempo_agotado="1";
	}
	$error_null=0;
	$error_usado=0;
	$error_paso2=0;
	$error_paso3=0;

    //si el código introducido no esta usado...
    if ($n_promo == NULL) {

			session_unset();
			session_destroy();
	
			session_start();
	
			
			$_SESSION["adeslas2hogar2016_tipocampana"] = $tipocampana;
			$_SESSION["adeslas2hogar2016_cp"] = $cpinsertado;
			$_SESSION["adeslas2hogar2016_codigo"] = $n_codigo;
			$_SESSION["adeslas2hogar2016_poliza"] = $poliza;
			$_SESSION["adeslas2hogar2016_paso2"] = "1";
			

			$redirect="#bases-aceptar-modal";
			
			switch ($tipocampana) {
				//Hotel
				case 1:
					$redirect="#bases-aceptar-modal-hotel";
					break;
				//Futbol
				case 2:
					$redirect="#bases-aceptar-modal-futbol";
					break;
				//Experiencias
				case 3:
					$redirect="#bases-aceptar-modal";
					break;
				//Otros
				default:
					$redirect="#bases-aceptar-modal";
			}
			echo"<script type='text/javascript'>
					$('$redirect').modal('show');
				</script>";
	}else{
		$error_null=1;
	}
	
	//////fin promo null
	////// comprobamos si el codigo esta usado -1 y la fecha esta ok.
	if(($n_promo=="-1")&&($tiempo_agotado=="0")){
		$_SESSION["adeslas2hogar2016_codigo"] = $n_codigo;
		$_SESSION["adeslas2hogar2016_cp"] = $cpinsertado;
		$_SESSION["adeslas2hogar2016_poliza"] = $poliza;
		$_SESSION["adeslas2hogar2016_fechareg"] = $n_fechareg;
		$_SESSION["adeslas2hogar2016_tipocampana"] = $tipocampana;
        $_SESSION["adeslas2hogar2016_paso2"] = "1";
		switch ($tipocampana) {
				//Hotel
				case 1:
					$redirect2="solicitar_regalo_paso2_hotel.php";
					break;
				//Futbol
				case 2:
					$redirect2="solicitar_regalo_paso2_futbol.php";
					break;
				//Experiencias
				case 3:
					$redirect2="solicitar_regalo_paso2.php";
					break;
				//Otros
				default:
					$redirect2="solicitar_regalo_paso2.php";
			}
		echo"<script type='text/javascript'>
		window.location='https://disfrutaunaexperienciaunica.com/$redirect2';
		</script>";
	}else{
		$error_usado=1;
	}
	////// comprobamos si existe peticion y en que paso está....
	$sqlcompruebapeticion = "SELECT * FROM `".$table_prefix ."__peticion` WHERE `CODIGO` = '$codigoinsertado' AND N_PET = '$n_promo'"; 
	$rscompruebapeticion= mysqli_query($link,$sqlcompruebapeticion);
	$num_total_peticion = mysqli_num_rows($rscompruebapeticion);
	while ($row=mysqli_fetch_array($rscompruebapeticion)) { 
		$c_contacto_idcontador=$row["N_PET"];
		$c_contacto_nombre=$row["NOMBRE"];
		$c_contacto_dest1=$row["DEST1"];
		$c_contacto_estado=$row["ESTADO"];
		$expotorgadafecha2=$row['FECHAEXP2'];
		$c_contacto_nif=$row['NIF'];	
		$c_contacto_campana=$row['TIPOCAMPANA'];
	}
	if(($tiempo_agotado==0)&&($c_contacto_nombre=="")&&($num_total_peticion!=0)&&($n_promo!="-1")){
		$_SESSION["adeslas2hogar2016_cp"] = $cpinsertado;
		$_SESSION["adeslas2hogar2016_codigo"] = $n_codigo;
		$_SESSION["adeslas2hogar2016_poliza"] = $poliza;
		$_SESSION["adeslas2hogar2016_fechareg"] = $n_fechareg;
		$_SESSION["adeslas2hogar2016_paso2"] = "2";
		$_SESSION["adeslas2hogar2016_id_peticion"] = $c_contacto_idcontador;
		echo"<script type='text/javascript'>
			window.location='https://disfrutaunaexperienciaunica.com/solicitar_regalo_paso3.php';
		</script>";	
	}else{
		$error_paso2=1;
	}
	if(($c_contacto_nombre!=NULL)&&($c_contacto_estado==1)&&($tiempo_agotado==0)){
		$_SESSION["adeslas2hogar2016_cp"] = $cpinsertado;
		$_SESSION["adeslas2hogar2016_codigo"] = $n_codigo;
		$_SESSION["adeslas2hogar2016_poliza"] = $poliza;
		$_SESSION["adeslas2hogar2016_fechareg"] = $n_fechareg;
		$_SESSION["adeslas2hogar2016_id_peticion"] = $c_contacto_idcontador;
		$_SESSION["adeslas2hogar2016_paso2"] = "3";
		echo"<script type='text/javascript'>
			window.location='https://disfrutaunaexperienciaunica.com/tuexperiencia.php?x=" . $c_contacto_nif . "&y=" . $c_contacto_idcontador . "&z=" . $n_codigo . "';
		</script>";
	}else{
		$error_paso3=1;
	}
	
	if(($error_null==1)&&($error_usado==1)&&($error_paso2==1)&&($error_paso3==1)){
		$errorTextcaducado="$('#insertacodigo').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['FORM_3_caducado']." $n_fechareg</div>');";
		echo"<script type='text/javascript'>  
		$(document).ready(function(){
		$('.DivError').remove();
		$errorTextcaducado
		$('#avisoPop').show();
		return false;  
		});
		</script>";
	}

//si no existe el codigo...
} else {
    
    
    $errorTextCodigo="";
    $errorTextPoliza="";
	$errorTextCPExiste="";
	$errorTextpolizaycodigo="";
	$error_sql=0;
    
    if ($num_total_codigo != "1") {
        $errorTextCodigo="$('#insertacodigo').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM1_1']." $codigoinsertado ".$lang['TEXT_FORM1_2_2']."</div>');";
		$error_sql=0;
    }
    
    if ($num_total_poliza != "1") {
        $errorTextPoliza="$('#poliza').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM1_3']." $poliza ".$lang['TEXT_FORM1_4']."</div>');";
		$error_sql=5;
    }
    
    /*if ($num_poliza_existe != "0") {
        $errorTextPolizaExiste="$('#poliza').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM1_5']." $poliza ".$lang['TEXT_FORM1_6']."</div>');";
    }*/
	if ($num_total_cp == "1") {
        $errorTextCPExiste="$('#cp').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM1_7']."</div>');";
    }
	if ($num_total_cp == "2") {
        $errorTextCPExiste="$('#cp').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM1_8']."</div>');";
    }
	if ($num_total_tipocampana == "1") {
        $errorTextcampanaExiste="$('#subcampaign').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM1_9']."</div>');";
    }
	if ($error_tirada_polizaycodigo==1){
		$errortiradapolizaycodigo="$('#insertacodigo').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">código caducado</div>');";
	}
	
	$sql = "INSERT INTO " . $table_prefix . "__cod_promo_error (codigo_insertado,ip,fecha,existe,cp, `POLIZA`) VALUES ('$codigoinsertado', '$ipvisita', '$fecha_actual', '$error_sql','$cpinsertado', $poliza)";
    mysqli_query($link, $sql);
    mysqli_close($link);

    
    echo"<script type='text/javascript'>  
            $(document).ready(function(){
                $('.DivError').remove();
				$errorTextcampanaExiste
				$errorTextCPExiste
                $errorTextCodigo
                $errorTextPoliza
                $errorTextPolizaExiste
				$errorTextpolizaycodigo
				$errortiradapolizaycodigo
                $('#avisoPop').show();
                return false;  
            });
        </script>";
}
?>