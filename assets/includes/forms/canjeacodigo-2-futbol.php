<?php

session_start();
include_once '../../../common.php';
$table_prefix = "adeslas2hogar2016";

if($lang_sql=="cat"){
	$lengbusc="_cat";
	$n_fechasindia = "traducefechasindiacat";
	$n_fechacondia = "traducefechacat";
}else{
	$lengbusc="";
	$n_fechasindia = "traducefechasindiaes";
	$n_fechacondia = "traducefechaes";
}     

$fecha_actual = date('Y-m-d H:i:s');
$fecha_actual_dia = date('Y-m-d');

$user_agent = $_SERVER['HTTP_USER_AGENT'];
 
function getBrowser($user_agent){

if(strpos($user_agent, 'MSIE') !== FALSE)
   return 'Internet explorer';
elseif(strpos($user_agent, 'Trident') !== FALSE) //IE 11
    return 'Internet explorer';
elseif(strpos($user_agent, 'Firefox') !== FALSE)
   return 'Mozilla Firefox';
elseif(strpos($user_agent, 'Chrome') !== FALSE)
   return 'Google Chrome';
elseif(strpos($user_agent, 'Opera Mini') !== FALSE)
   return "Opera Mini";
elseif(strpos($user_agent, 'Opera') !== FALSE)
   return "Opera";
elseif(strpos($user_agent, 'Safari') !== FALSE)
   return "Safari";
else
   return 'No hemos podido detectar su navegador';
}
function getPlatform($user_agent) {
   $plataformas = array(
           'Windows 10' => 'Windows NT 10.0+',
      'Windows 8' => 'Windows NT 6.3+',
      'Windows 8' => 'Windows NT 6.2+',
      'Windows 7' => 'Windows NT 6.1+',
      'Windows Vista' => 'Windows NT 6.0+',
      'Windows XP' => 'Windows NT 5.1+',
      'Windows 2003' => 'Windows NT 5.2+',
      'Windows otros' => 'Windows',
      'iPhone' => 'iPhone',
      'iPad' => 'iPad',
      'Mac OS X' => '(Mac OS X+)|(CFNetwork+)',
      'Mac otros' => 'Macintosh',
      'Android' => 'Android',
      'BlackBerry' => 'BlackBerry',
      'Linux' => 'Linux',
   );
   foreach($plataformas as $plataforma=>$pattern){
      if (eregi($pattern, $user_agent))
         return $plataforma;
   }
   return 'Otras';
}
  
$navegador = getBrowser($user_agent);
$SO = getPlatform($user_agent);
$ipvisita=$_SERVER['REMOTE_ADDR'];


if (isset($_SESSION["adeslas2hogar2016_codigo"])) {//comprueba que la sesion existe.

    if (isset($_SESSION["adeslas2hogar2016_paso2"])) {//comprueba que la sesion paso 2existe.
	
        $paso2sesion = $_SESSION["adeslas2hogar2016_paso2"];
        if ($paso2sesion != "1") {
            session_unset();
            session_destroy();
            echo"<script type='text/javascript'>
                    window.location='http://disfrutaunaexperienciaunica.com';
                </script>";
        } else {
			
			
			$sesion_codigo = $_SESSION["adeslas2hogar2016_codigo"]; //definimos variable sesion.
			$tipocampana = $_SESSION["adeslas2hogar2016_tipocampana"];
			$poliza = $_SESSION["adeslas2hogar2016_poliza"];
		
			$p_categoria_equipo = $_POST["categorias"];
			$p_equipo = $_POST["equipos"];

		
			include "../../connect/conexion.php";
			require "../../php/conviertefecha.php";

			
						////////////////////comprueba errores/////////////////////////
			if(($p_categoria_equipo=="")or($p_categoria_equipo==NULL)or($p_categoria_equipo==0)){
					$num_total_Categoria = 1;	
					$errorText_Categoria="$('#categorias').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FUTBOL4']."</div>');";
			}else{
					$errorText_Categoria="";
			}
			
			if(($p_equipo=="")or($p_equipo==NULL)or($p_equipo==0)){
					$num_total_equipo = 1;
					$errorText_equipo="$('#equipos').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FUTBOL5']."</div>');";
			}else{
					$errorText_equipo="";
			}
			
			
			

			////////////////////fin comprueba errores/////////////////////////
			
				if(($num_total_Categoria==1)or($num_total_equipo==1)){
					
					echo"<script type='text/javascript'>  
							$(document).ready(function(){
								$('.DivError').remove();
								$errorText_Categoria 
								$errorText_equipo
								$('#avisoPop').show();
								return false;  
							});
						</script>";
				}else{
					include "../../connect/conexion.php";
				
					/* $sql = "INSERT INTO " . $table_prefix . "__peticion ( `ESTADO`, `F_REGISTRO`, `F_REGISTROIP`, `CODIGO`, `CP`, `POLIZA`,`SO`,`NAV`,`DEST1`,`DEST2`,`DEST3`,`F_IDA_1`,`F_IDA_2`,`F_IDA_3`,`TIPOCAMPANA`) VALUES ('1', '$fecha_actual','$ipvisita','$sesion_codigo', '$cpinsertado', '$poliza','$SO','$navegador','$experiencia1ok','$experiencia2ok','$experiencia3ok','$fecha1','$fecha2','$fecha3','$tipocampana')";
			mysqli_query($link, $sql);
	
			$sesion_id_peticion = mysqli_insert_id($link);
			$_SESSION["adeslas2hogar2016_id_peticion"] = $sesion_id_peticion;
					
					$_SESSION["adeslas2hogar2016_paso2"] = "2";
					
					echo"<script type='text/javascript'>
					window.location='./solicitar_regalo_paso3.php';
					</script>"; */
					$fecha = date('Y-m-d');
					$nuevafecha = strtotime ( '+30 day' , strtotime ( $fecha ) ) ;
					$nuevafechaok = date ( 'Y-m-d' , $nuevafecha );

					$sqlcompruebapartidos = "SELECT * FROM ".$table_prefix."__partidos WHERE estado_partido = '1' AND peso_partido >= '3' AND equipo_a = '$p_equipo' AND fecha_partido >= '$nuevafechaok' ORDER BY RAND() LIMIT 1";
					$rscompruebapartidos= mysqli_query($link,$sqlcompruebapartidos);
					$num_total_PARTIDOS = mysqli_num_rows($rscompruebapartidos);
					while ($row=mysqli_fetch_array($rscompruebapartidos)) { 
					$p_nombre_equipocasa2=$row["nombre_equipo"];
					$p_nombre_equipocasa=utf8_encode($p_nombre_equipocasa2);
					$p_id_partido=$row["id_partido"];
					$p_jornada_partido=$row["jornada_partido"];
					$p_fecha_partido2=$row["fecha_partido"];
					$p_fecha_partido=utf8_decode($n_fechasindia($p_fecha_partido2)); 
					$p_equipo_local=$row["equipo_a"];
					$p_equipo_visitante=$row["equipo_b"];
					}
					
					$sqlequipolocal="SELECT * FROM ".$table_prefix."__equipos WHERE id_equipo='$p_equipo_local'";
					$rsequipolocal= mysqli_query($link,$sqlequipolocal);
					while ($row=mysqli_fetch_array($rsequipolocal)) { 
					$s_nombre_local2=$row["nombre_equipo"];
					$s_nombre_local=utf8_encode($s_nombre_local2);
					$s_camiseta_local=$row["camiseta_equipo"];
					$s_estadio_local2=$row["estadio_equipo"];
					$s_estadio_local=utf8_encode($s_estadio_local2);
					}
					$sqlequipovisitante="SELECT * FROM ".$table_prefix."__equipos WHERE id_equipo='$p_equipo_visitante'";
					$rsequipovisitante= mysqli_query($link,$sqlequipovisitante);
					while ($row=mysqli_fetch_array($rsequipovisitante)) { 
					$s_nombre_visitante2=$row["nombre_equipo"];
					$s_nombre_visitante=utf8_encode($s_nombre_visitante2);
					$s_camiseta_visitante=$row["camiseta_equipo"];
					}
					
					$_SESSION["adeslas2hogar2016_partidoasignado"]="$p_id_partido";
					
					echo"<script type='text/javascript'>
					$(document).ready(function() {
								$('#contform').hide(); 
								$('#pelotamove').animate({rotate: '+=10deg'}, 0);
								$('#contpelota').animate({'top': '-700px'}, 6000);
					});
						</script>
						<div id='tiempo' style='text-align:center;'>
						".$lang['TEXT_FUTBOL_CAN_1']."...<br>
						<h1><span class='bold'>".$lang['TEXT_FUTBOL_CAN_2']."...</span></h1><br>
						<img alt='' src='images/crono.gif'/><br />
						</div>
						<script type='text/javascript'>
						$(document).ready(function() {
							setTimeout(function() {
								$('#tiempo').fadeOut(1000);
								$('#partido').fadeIn(1000);
								$('#contpelota').hide();
							},5000);
						});
						</script> 
						";
					
					echo"
					<script type=\"text/javascript\">  
					$(function(){
					$('#linkrec').bind('click', function(e) {
					 var url = \"rechazaunavez.php\"; // El script a dónde se realizará la petición.
						$.ajax({
							   type: \"POST\",
							   url: url,
							   data: $(\"#rechazaunavezform\").serialize(), // Adjuntar los campos del formulario enviado.
							   success: function(data)
							   {
							   }
							 });
					 
						return false; // Evitar ejecutar el submit del formulario.
					 });
					});
					</script>
					<div class='modal fade' id='modal-rechaza' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true' style='display: none;'>
						<div class='modal-dialog'>
							<div class='modal-content'>
							<div class='element_to_pop_up_content1'>
								<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><img src='images/close.png' width='38' height='39' /></button>    
						  	</div>
							</div>
						   <div class='element_to_pop_up_content2' style='height:auto;overflow:hidden;text-align:center'>
						    <h1 style='color:#b42c33;'>".$lang['TEXT_FUTBOL_CAN_3']."</h1>
							<br />
							<br />
							<h4 style='color:#b42c33;'>".$lang['TEXT_FUTBOL_CAN_4']."</h4>
							<br />
							<br />
							<button id='acepta' name='acepta' type='button' class='btn_new1' data-dismiss='modal'><span class='icon1'><i class='fa fa-mail-reply fa-2x'></i></span><span class='text'>".$lang['TEXT_FUTBOL_CAN_5']."</span></button>
							<button id='linkrechaza' name='rechaza' type='button' class='btn_new1' onclick=\"window.location.href='assets/includes/forms/canjeacodigo-2-futbol-rechaza.php'\"><span class='icon1'><i class='fa fa-remove fa-2x'></i></span><span class='text'>".$lang['TEXT_FUTBOL_CAN_6']."</span></button>
							<br />
							<br />
						   </div>
						</div>
					</div>
						   
		   <div id='partido' style='display:none;'>
					<div id='textosaludo'>
					<h1><span class='bold'>".$lang['TEXT_FUTBOL_CAN_7']."</span></h1><br>
					<span class='saludoval5'>".$lang['TEXT_FUTBOL_CAN_8']." <b style='color:#b42c33;'>$p_jornada_partido</b>, ".$lang['TEXT_FUTBOL_CAN_9']." <b style='color:#b42c33;'>$p_fecha_partido</b> ".$lang['TEXT_FUTBOL_CAN_10']." <b style='color:#b42c33;'>$s_estadio_local</b> entre el <b style='color:#b42c33;'>$s_nombre_local</b> ".$lang['TEXT_FUTBOL_CAN_12']." <b style='color:#b42c33;'>$s_nombre_visitante</b><br></span><br><br />
					</div>
					<div style='max-width:500px;margin: 0 auto;'>
							<div style='width:48%;float:left;text-align:center;'>
							<img src='images/equipaciones/$s_camiseta_local' style='width:100%;'/>
							<br />
							<h1><span class='bold'>$s_nombre_local</span></h1>
							<br />
							<button id='acepta' name='acepta' type='button' class='btn_new1'  onclick=\"window.location.href='assets/includes/forms/canjeacodigo-2-futbol-acepta.php'\"><span class='icon1'><i class='fa fa-check fa-2x'></i></span><span class='text'>".$lang['TEXT_FUTBOL_CAN_13']."</span></button>
							</div>
							<div style='width:48%;float:right;text-align:center;'>
							<img src='images/equipaciones/$s_camiseta_visitante' style='width:100%;'/>
							<br />
							<h1><span class='bold'>$s_nombre_visitante</span></h1>
							<br />
							<a href='#' data-toggle='modal' data-target='#modal-rechaza'><button id='linkrechaza' name='rechaza' type='button' class='btn_new1'><span class='icon1'><i class='fa fa-remove fa-2x'></i></span><span class='text'>".$lang['TEXT_FUTBOL_CAN_6']."</span></button></a>
							</div>
					<div style='clear:left;clear:right;'></div>
					</div>";
					echo"</div>";
					$sqlquery1 = "UPDATE `" . $table_prefix . "__cod_promo` SET `PROMO` ='-1', `F_REGISTRO` = '$fecha_actual', `CP` = '$cpinsertado' WHERE `CODIGO` = '$sesion_codigo'";
					mysqli_query($link, $sqlquery1);

				}
       
	    }
    } else {
        session_unset();
        session_destroy();
        echo"<script type='text/javascript'>
            window.location='http://disfrutaunaexperienciaunica.com';
        </script>";
    }

} else {//si la sesión no existe, redirecciona
    echo"<script type='text/javascript'>
    window.location='http://disfrutaunaexperienciaunica.com';
    </script>";
}
			
	
?>