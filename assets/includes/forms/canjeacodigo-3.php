<?php
session_start();
include_once '../../../common.php';
$table_prefix = "adeslas2hogar2016";

$fecha_actual = date('Y-m-d H:i:s');
$ipvisita = $_SERVER['REMOTE_ADDR'];

if (isset($_SESSION["adeslas2hogar2016_codigo"])) {//comprueba que la sesion existe.
    if (isset($_SESSION["adeslas2hogar2016_paso2"])) {//comprueba que la sesion paso 2existe.
        $paso2sesion = $_SESSION["adeslas2hogar2016_paso2"];
        if ($paso2sesion != "2") {
            session_unset();
            session_destroy();
            echo"<script type='text/javascript'>
                    window.location='http://disfrutaunaexperienciaunica.com';
                </script>";
        } else {
			
			$sesion_codigo = $_SESSION["adeslas2hogar2016_codigo"]; //definimos variable sesion.
    //definimos variables de POST.
    $post_nombre2 = $_POST["nombreformcanjea"];
    $post_nombre = utf8_decode($post_nombre2);
    $post_apellidos2 = $_POST["apellidosformcanjea"];
    $post_apellidos = utf8_decode($post_apellidos2);
    $post_sexo2 = $_POST["sexoformcanjea"];
    $post_sexo = utf8_decode($post_sexo2);
    $post_fechanac2 = $_POST["nacimformcanjea"];
    $post_fechanac = date("Y-m-d", strtotime($post_fechanac2));
    $post_dni2 = $_POST["dniformcanjea"];
    $post_dni = str_replace("-", "", $post_dni2);
    $post_mail2 = $_POST["mailformcanjea"];
    $post_mail = utf8_decode($post_mail2);
    $post_telf = $_POST["telfformcanjea"];
    $post_direccion2 =  addslashes($_POST["direccionformcanjea"]);
    $post_direccion = utf8_decode($post_direccion2);
	$post_direccion = str_replace("'","´",$post_direccion);
    $post_poblacion2 = $_POST["poblacionformcanjea"];
    $post_poblacion = utf8_decode($post_poblacion2);
    $post_cp = $_POST["cpformcanjea"];
    $post_provincia2 = $_POST["provinciaformcanjea"];
    $post_provincia = utf8_decode($post_provincia2);
	
	function CalculaEdad($fecha) {
    list($Y,$m,$d) = explode("-",$fecha);
    return( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
	}
	$expresiontelf = '/^[9|6|7][0-9]{8}$/'; 
    
	if(isset($_GET["aco"])){
		$con_acompanante=1;
	}else{
		$con_acompanante=0;
	}
	
    //detect if partner request is comming, and get data if exist
    if($con_acompanante==1){
    
        //define partner post vars
        $post_aco_nombre2 = $_POST["nombre_aco_formcanjea"];
        $post_aco_nombre = utf8_decode($post_aco_nombre2);
        $post_aco_apellidos2 = $_POST["apellidos_aco_formcanjea"];
        $post_aco_apellidos = utf8_decode($post_aco_apellidos2);
        $post_aco_sexo2 = $_POST["sexo_aco_formcanjea"];
        $post_aco_sexo = utf8_decode($post_aco_sexo2);
        $post_aco_fechanac2 = $_POST["nacim_aco_formcanjea"];
        $post_aco_fechanac = date("Y-m-d", strtotime($post_aco_fechanac2));
        $post_aco_dni2 = $_POST["dni_aco_formcanjea"];
        $post_aco_dni = str_replace("-", "", $post_aco_dni2);
        $post_aco_mail2 = $_POST["mail_aco_formcanjea"];
        $post_aco_mail = utf8_decode($post_aco_mail2);
        $post_aco_telf = $_POST["telf_aco_formcanjea"];
        $post_aco_direccion2 = addslashes($_POST["direccion_aco_formcanjea"]);
        $post_aco_direccion = utf8_decode($post_aco_direccion2);
        $post_aco_poblacion2 = $_POST["poblacion_aco_formcanjea"];
        $post_aco_poblacion = utf8_decode($post_aco_poblacion2);
        $post_aco_cp = $_POST["cp_aco_formcanjea"];
        $post_aco_provincia2 = $_POST["provincia_aco_formcanjea"];
        $post_aco_provincia = utf8_decode($post_aco_provincia2);
        $post_aco_menor2 = $_POST["menor_aco_formcanjea"];
        $post_aco_menor = utf8_decode($post_aco_menor2);
    }
    
    include "../../connect/conexion.php";


    require "../../php/validadni.php";
    $cif2 = isValidIdNumber($post_dni);
    if ($post_dni != "91") {
        $sqlcompruebaDNI = "SELECT * FROM `" . $table_prefix . "__peticion` WHERE `NIF` = '$post_dni'"; 
        $rscompruebaDNI = mysqli_query($link, $sqlcompruebaDNI);
		$num_total_DNI = mysqli_num_rows($rscompruebaDNI);
		if($num_total_DNI>='1'){
            $sql = "INSERT INTO " . $table_prefix . "__cod_promo_error (codigo_insertado,ip,fecha,existe,dni_repe,dni_insertado) VALUES ('$sesion_codigo', '$ipvisita', '$fecha_actual', '1','1','$post_dni')";
            mysqli_query($link, $sql);
            $sqlquery1 = "UPDATE `" . $table_prefix . "__cod_promo` SET `PROMO` ='-1', `DNI_REPE` = '1' WHERE `CODIGO` = '$sesion_codigo'";


            mysqli_query($link, $sqlquery1);
            while ($row = mysqli_fetch_array($rscompruebaDNI)) {
                $n_fechareg = $row["F_REGISTRO"];
            }
            require "../../php/conviertefecha.php";
			if($lang_sql=="cat"){
			$n_fechareg2 = traducefechacat($n_fechareg);
			}else{
			$n_fechareg2 = traducefechaes($n_fechareg);
			}

            mysqli_close($link);
            session_unset();
            session_destroy();
            echo"
			<div class='modal fade' id='dni_repetido' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true' style='display: none;'>
    	<div class='modal-dialog' style='max-width:500px;height:350px;'>
			<div class='modal-content'>
                <div class='element_to_pop_up_content1'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true' id='dni_repetido_button'><img src='images/close.png' width='38' height='39' /></button>
                 </div>    
                <div class='element_to_pop_up_content2' style='overflow-y:inherit;overflow-x:inherit;height:auto;margin-top:30px;'>
                <h3>".$lang['TEXT_FORM3_1']." <b>$post_dni</b> ".$lang['TEXT_FORM3_2']." $n_fechareg2. 
                    <br />
                    <br />
                    ".$lang['TEXT_FORM3_3']."</span> 
                    <br />
                    <br /></h3>
					<button type='submit' class='btn_new' id='dni_repetido_button2'><span class='icon'><i class='fa fa-step-forward fa-2x'></i></span><span class='text'>".$lang['TEXT_ACO7']."</span></button>
                <div>
                <p><strong></strong></p>
        		</div>
        </div><!-- Fi Scroll -->
			</div>
		</div>
	</div>
			<script type='text/javascript'> 
			$(document).ready(function() {
					$('#dni_repetido').modal('show'); 
					$('#divformenvio').hide();
            });
			$('#dni_repetido').click(function(){
				window.location='http://disfrutaunaexperienciaunica.com';
			});  
			$('#dni_repetido_button').click(function(){
				window.location='http://disfrutaunaexperienciaunica.com';
			}); 
			$('#dni_repetido_button2').click(function(){
				window.location='http://disfrutaunaexperienciaunica.com';
			}); 
            </script>";
        } else {
			
				////////////////////////validaciones sin acomp//////////////////
				if(($post_nombre2=="")or($post_nombre2==NULL)){
						$num_total_error_nombre = 1;
						$errorText_nombre="$('#nombreformcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_4']."</div>');";
				}else{
						$errorText_nombre="";
				}
				if(($post_apellidos2=="")or($post_apellidos2==NULL)){
						$num_total_error_apellidos = 1;
						$errorText_apellidos="$('#apellidosformcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_5']."</div>');";
				}else{
						$errorText_apellidos="";
				}
				if(($post_sexo2=="")or($post_sexo2==NULL)){
						$num_total_error_sexo = 1;
						$errorText_sexo="$('#sexoformcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_6']."</div>');";
				}else{
						$errorText_sexo="";
				}
				if(($post_fechanac2=="")or($post_fechanac2==NULL)){
						$num_total_error_fechanac = 1;
						$errorText_fechanac="$('#nacimformcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_7']."</div>');";
				}else{
						$errorText_fechanac="";
				}
				if(CalculaEdad($post_fechanac)<18){
						$num_total_error_fechanacedad = 1;
						$errorText_fechanacedad="$('#nacimformcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_7_menor']."</div>');";
				}else{
						$errorText_fechanacedad="";
				}
				if(($post_dni2=="")or($post_dni2==NULL)){
						$num_total_error_dni = 1;
						$errorText_dni="$('#dniformcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_8']."</div>');";
				}else{
						$errorText_dni="";
				}
				if(($post_mail2=="")or($post_mail2==NULL)){
						$num_total_error_mail = 1;
						$errorText_mail="$('#mailformcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_9']."</div>');";
				}else{
						$errorText_mail="";
				}
				if (!filter_var($post_mail2, FILTER_VALIDATE_EMAIL)) {
						$num_total_error_mailformat = 1;
						$errorText_mailformat="$('#mailformcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_9_format']."</div>');";
				}else{
						$errorText_mailformat="";
				}
					
				if(($post_telf=="")or($post_telf==NULL)){
						$num_total_error_telf = 1;
						$errorText_telf="$('#telfformcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_10']."</div>');";
				}else{
						$errorText_telf="";
				}
				if(!preg_match($expresiontelf, $post_telf)){
					$num_total_error_telfformat = 1;
						$errorText_telfformat="$('#telfformcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_10_format']."</div>');";
				}else{
						$errorText_telfformat="";
				}
				if(($post_direccion2=="")or($post_direccion2==NULL)){
						$num_total_error_direccion = 1;
						$errorText_direccion="$('#direccionformcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_11']."</div>');";
				}else{
						$errorText_direccion="";
				}
				if(($post_poblacion2=="")or($post_poblacion2==NULL)){
						$num_total_error_poblacion = 1;
						$errorText_poblacion="$('#poblacionformcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_12']."</div>');";
				}else{
						$errorText_poblacion="";
				}
				if(($post_provincia2=="")or($post_provincia2==NULL)){
						$num_total_error_provincia = 1;
						$errorText_provincia="$('#provinciaformcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_13']."</div>');";
				}else{
						$errorText_provincia="";
				}
				if(($post_cp=="")or($post_cp==NULL)){
						$num_total_error_cp = 1;
						$errorText_cp="$('#cpformcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_14']."</div>');";
				}else{
						$errorText_cp="";
				}
				////////////////////////fin validaciones sin acomp//////////////////
				////////////////////////validaciones acomp//////////////////
				if($con_acompanante==1){
					
					if(($post_aco_nombre2=="")or($post_aco_nombre2==NULL)){
						$num_total_error_aco_nombre = 1;
						$errorText_aco_nombre="$('#nombre_aco_formcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_15']."</div>');";
					}else{
						$errorText_aco_nombre="";
					}
					if(($post_aco_apellidos2=="")or($post_aco_apellidos2==NULL)){
						$num_total_error_aco_apellidos = 1;
						$errorText_aco_apellidos="$('#apellidos_aco_formcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_16']."</div>');";
					}else{
						$errorText_aco_apellidos="";
					}
					if(($post_aco_sexo2=="")or($post_aco_sexo2==NULL)){
						$num_total_error_aco_sexo = 1;
						$errorText_aco_sexo="$('#sexo_aco_formcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_17']."</div>');";
					}else{
						$errorText_aco_sexo="";
					}
					if(($post_aco_fechanac2=="")or($post_aco_fechanac2==NULL)){
						$num_total_error_aco_fechanac = 1;
						$errorText_aco_fechanac="$('#nacim_aco_formcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_18']."</div>');";
					}else{
						$errorText_aco_fechanac="";
					}
					
					if(($post_aco_menor2=="")or($post_aco_menor2==NULL)){
						$num_total_error_aco_menor = 1;
						$errorText_aco_menor="$('#menor_aco_formcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_19']."</div>');";
					}else{
						if($post_aco_menor2!=2){
							if(($post_aco_dni2=="")or($post_aco_dni2==NULL)){
								$num_total_error_aco_dni = 1;
								$errorText_aco_dni="$('#dni_aco_formcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_20']."</div>');";
							}else{
								$errorText_aco_dni="";
							}
							
							if($post_aco_dni2==""){
								$num_total_error_aco_dni_format = 1;
								$errorText_aco_dni_format="$('#dni_aco_formcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_20_format']."</div>');";
							}else{
								$errorText_aco_dni_format="";
							}
						}else{
							$errorText_aco_menor="";
						}	
					}
					if(($post_aco_menor2=="1")or($post_aco_menor2=="2")){
							if(CalculaEdad($post_aco_fechanac)>18){
								$num_total_error_aco_fechanac_edad = 1;
								$errorText_aco_fechanac_edad="$('#nacim_aco_formcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_18_menor']."</div>');";
							}else{
								$errorText_aco_fechanac_edad="";
							}
					}
					if($post_aco_menor2=="0"){
							if(CalculaEdad($post_aco_fechanac)<18){
								$num_total_error_aco_fechanac_edad = 1;
								$errorText_aco_fechanac_edad="$('#nacim_aco_formcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_18_mayor']."</div>');";
							}else{
								$errorText_aco_fechanac_edad="";
							}
					}
					
					if(($post_aco_mail2=="")or($post_aco_mail2==NULL)){
						$num_total_error_aco_mail = 1;
						$errorText_aco_mail="$('#mail_aco_formcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_21']."</div>');";
					}else{
						$errorText_aco_mail="";
					}
					if(($post_aco_telf=="")or($post_aco_telf==NULL)){
						$num_total_error_aco_telf = 1;
						$errorText_aco_telf="$('#telf_aco_formcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_22']."</div>');";
					}else{
						$errorText_aco_telf="";
					}
					if(!preg_match($expresiontelf, $post_aco_telf)){
						$num_total_error_aco_telfformat = 1;
							$errorText_aco_telfformat="$('#telf_aco_formcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_10_format']."</div>');";
					}else{
							$errorText_aco_telfformat="";
					}
					if(($post_aco_direccion2=="")or($post_aco_direccion2==NULL)){
						$num_total_error_aco_direccion = 1;
						$errorText_aco_direccion="$('#direccion_aco_formcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_23']."</div>');";
					}else{
						$errorText_aco_direccion="";
					}
					if(($post_aco_provincia2=="")or($post_aco_provincia2==NULL)){
						$num_total_error_aco_provincia = 1;
						$errorText_aco_provincia="$('#provincia_aco_formcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_24']."</div>');";
					}else{
						$errorText_aco_provincia="";
					}
					if(($post_aco_poblacion2=="")or($post_aco_poblacion2==NULL)){
						$num_total_error_aco_poblacion = 1;
						$errorText_aco_poblacion="$('#poblacion_aco_formcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_25']."</div>');";
					}else{
						$errorText_aco_poblacion="";
					}
					if(($post_aco_cp=="")or($post_aco_cp==NULL)){
						$num_total_error_aco_cp = 1;
						$errorText_aco_cp="$('#cp_aco_formcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_26']."</div>');";
					}else{
						$errorText_aco_cp="";
					}
					
				////////////////////////fin validaciones acomp//////////////////	
				}
				
				if(($num_total_error_nombre==1)or($num_total_error_apellidos==1)or($num_total_error_sexo==1)or($num_total_error_fechanac==1)or($num_total_error_fechanacedad==1)or($num_total_error_dni==1)or($num_total_error_mail==1)or($num_total_error_mailformat==1)or($num_total_error_telf==1)or($num_total_error_telfformat==1)or($num_total_error_direccion==1)or($num_total_error_poblacion==1)or($num_total_error_provincia==1)or($num_total_error_cp==1)){
					
					echo"<script type='text/javascript'>  
							$(document).ready(function(){
								$('.DivError').remove();
								$errorText_nombre
								$errorText_apellidos
								$errorText_sexo
								$errorText_fechanac
								$errorText_fechanacedad
								$errorText_dni
								$errorText_mail
								$errorText_mailformat
								$errorText_telf
								$errorText_telfformat
								$errorText_direccion
								$errorText_poblacion
								$errorText_provincia
								$errorText_cp
								return false;
							});
						</script>";
						$errores_peticion=1;
						
				}else{
					$errores_peticion=0;
				if($con_acompanante==1){
					if(($num_total_error_aco_nombre==1)or($num_total_error_aco_apellidos==1)or($num_total_error_aco_sexo==1)or($num_total_error_aco_fechanac==1)or($num_total_error_aco_fechanac_edad==1)or($num_total_error_aco_menor==1)or($num_total_error_aco_dni==1)or($num_total_error_aco_dni_format==1)or($num_total_error_aco_mail==1)or($num_total_error_aco_telf==1)or($num_total_error_aco_telfformat==1)or($num_total_error_aco_direccion==1)or($num_total_error_aco_provincia==1)or($num_total_error_aco_poblacion==1)or($num_total_error_aco_cp==1)){
						echo"<script type='text/javascript'>  
							$(document).ready(function(){
								$('.DivError').remove();
								$errorText_aco_nombre
								$errorText_aco_apellidos
								$errorText_aco_sexo
								$errorText_aco_fechanac
								$errorText_aco_fechanac_edad
								$errorText_aco_menor
								$errorText_aco_dni
								$errorText_aco_dni_format
								$errorText_aco_mail
								$errorText_aco_telf
								$errorText_aco_telfformat
								$errorText_aco_direccion
								$errorText_aco_provincia
								$errorText_aco_poblacion
								$errorText_aco_cp
								return false;  
							});
						</script>";
						$errores_acompanante=1;
					}else{
						$errores_acompanante=0;
					}
				}
				///////////////////////////////////////////todo correcto///////////////////////////////////////////////////////
				if((($con_acompanante==0)&&($errores_peticion==0))or(($con_acompanante==1)&&($errores_acompanante==0)&&($errores_peticion==0))){
				$sql = "UPDATE " . $table_prefix . "__peticion SET NOMBRE = '$post_nombre', APELLIDOS= '$post_apellidos', SEXO = '$post_sexo2', NIF = '$post_dni', DIRECCION = '$post_direccion', POBLACION = '$post_poblacion', CP = '$post_cp', PROVINCIA = '$post_provincia', MAIL = '$post_mail', TELEFONO = '$post_telf', fechanacimiento = '$post_fechanac', ESTADO = '1', F_REGISTRO='$fecha_actual', F_REGISTROIP='$ipvisita' ".$sqlNoEditable." WHERE `CODIGO` = '$sesion_codigo'";
				mysqli_query($link, $sql);
	
				//Get last updated ID ( N_PET )
				$sql = "SELECT * FROM `" . $table_prefix . "__peticion` WHERE `CODIGO` = '$sesion_codigo'";
				$result= mysqli_query($link,$sql);
				//Populate array of values
				while ($row=mysqli_fetch_array($result)) {
					$id_peticion  = $row["N_PET"];
					$_SESSION["adeslas2hogar2016_id_peticion"] = $id_peticion;
				}
				
				
				if($con_acompanante==1){
					 $sqlPartner = "INSERT INTO `".$table_prefix."__peticion_acompanante`(
												`rel_pet_acompanante`, 
												`nombre_pet_acompanante`, 
												`apellidos_pet_acompanante`, 
												`fechanac_pet_acompanante`, 
												`sexo`, 
												`menor_pet_acompanante`, 
												`dni_pet_acompanante`, 
												`email_pet_acompanante`, 
												`telf_pet_acompanante`, 
												`direccion_pet_acompanante`, 
												`provincia_pet_acompanante`, 
												`municipio_pet_acompanante`, 
												`cp_pet_acompanante`
										)
										VALUES 
										(
												'".$id_peticion."',
												'".$post_aco_nombre."',
												'".$post_aco_apellidos."',
												'".$post_aco_fechanac."',
												'".$post_aco_sexo."',
												'".$post_aco_menor."',
												'".$post_aco_dni."',
												'".$post_aco_mail."',
												'".$post_aco_telf."',
												'".$post_aco_direccion."',
												'".$post_aco_provincia."',
												'".$post_aco_poblacion."',
												'".$post_aco_cp."'
										)";            
				mysqli_query($link, $sqlPartner);
				}
				$_SESSION["adeslas2hogar2016_id_peticion"] = $id_peticion;
				$sqlcompruebacorreo = "SELECT * FROM `" . $table_prefix . "__peticion` LEFT JOIN `crm_municipios` ON  crm_municipios.crm_id_municipio =  " . $table_prefix . "__peticion.POBLACION LEFT JOIN `crm_provincias` ON  crm_provincias.crm_id_provincia =  " . $table_prefix . "__peticion.PROVINCIA WHERE `CODIGO` = '$sesion_codigo'";
	
				mysqli_query($link, $sqlcompruebacorreo);
				$rscompruebacorreo = mysqli_query($link, $sqlcompruebacorreo);
				while ($row = mysqli_fetch_array($rscompruebacorreo)) {
					$c_contacto_idcontador = $row["N_PET"];
					$c_contacto_nombre_d = $row["NOMBRE"];
					$c_contacto_nombre = utf8_encode($c_contacto_nombre_d);
					$c_contacto_apellidos_d = $row["APELLIDOS"];
					$c_contacto_apellidos = utf8_encode($c_contacto_apellidos_d);
					$c_contacto_nif_d = $row["NIF"];
					$c_contacto_nif = utf8_encode($c_contacto_nif_d);
					$c_contacto_direccion_d = $row["DIRECCION"];
					$c_contacto_direccion = utf8_encode($c_contacto_direccion_d);
					$c_contacto_poblacion_d = $row["crm_municipio"];
					$c_contacto_poblacion = utf8_encode($c_contacto_poblacion_d);
					$c_contacto_cp = $row["CP"];
					$c_contacto_provincia_d = $row["crm_provincia"];
					$c_contacto_provincia = utf8_encode($c_contacto_provincia_d);
					$c_contacto_mail_d = $row["MAIL"];
					$c_contacto_mail = utf8_encode($c_contacto_mail_d);
					$c_contacto_telf = $row["TELEFONO"];
					$c_contacto_fechareg = $row["F_REGISTRO"];
					$c_contacto_codigo = $row["CODIGO"];
					$c_contacto_dest1 = $row["DEST1"];
					$c_contacto_dest2 = $row["DEST2"];
					$c_contacto_dest3 = $row["DEST3"];
					$c_contacto_ida11 = $row["F_IDA_1"];
					$c_contacto_ida21 = $row["F_IDA_2"];
					$c_contacto_ida31 = $row["F_IDA_3"];
				}
	
				
				//Get event name from servicios table
				$arrayExperiencias = array();
				$sqlExperiencias = 'SELECT `nombre_servicio` FROM `servicios` where `id_servicio` = "' . $c_contacto_dest1 . '"';
				$rsExperiencias = $link->query($sqlExperiencias);
				while ($row = $rsExperiencias->fetch_assoc()) {
					$arrayExperiencias[0] = $row["nombre_servicio"];
				}
	
				$sqlExperiencias = 'SELECT `nombre_servicio` FROM `servicios` where `id_servicio` = "' . $c_contacto_dest2 . '"';
				$rsExperiencias = $link->query($sqlExperiencias);
				while ($row = $rsExperiencias->fetch_assoc()) {
					$arrayExperiencias[1] = $row["nombre_servicio"];
				}
	
				$sqlExperiencias = 'SELECT `nombre_servicio` FROM `servicios` where `id_servicio` = "' . $c_contacto_dest3 . '"';
				$rsExperiencias = $link->query($sqlExperiencias);
				while ($row = $rsExperiencias->fetch_assoc()) {
					$arrayExperiencias[2] = $row["nombre_servicio"];
				}
	
				require "../../php/conviertefecha.php";
	
				$c_contacto_ida1 = traducefecha($c_contacto_ida11);
				$c_contacto_ida1ok = utf8_decode($c_contacto_ida1);
				$c_contacto_ida2 = traducefecha($c_contacto_ida21);
				$c_contacto_ida2ok = utf8_decode($c_contacto_ida2);
				$c_contacto_ida3 = traducefecha($c_contacto_ida31);
				$c_contacto_ida3ok = utf8_decode($c_contacto_ida3);
	
				require "../mail/PHPMailerAutoload.php";


//    //To send succesful registration mail to user we will get the mail content from database (table `campanas_emails`.
	//    $bodyCorreo="";
	//    $sqlCorreo = "SELECT * FROM `campanas_emails` where `rel_campana_email`= (SELECT id FROM `campanas` where `prefijo_campana` like \"%".$table_prefix."__%\") AND `valor_campana_email` = ( SELECT `id_valor_email_campana` FROM `campana_emails_valores` WHERE `valor_email_campana` LIKE \"%Email de registro (INTERNO)%\" )"; 
	//    
	//    mysqli_query($link,$sqlCorreo);
	//    $rsCorreo= mysqli_query($link,$sqlCorreo);
	//    while ($row=mysqli_fetch_array($rsCorreo)) {
	//        $bodyCorreo = $row["texto_campana_email"];
	//    }
	
				$mail = new phpmailer();
				$mail->Mailer = "smtp";
				$mail->IsSendMail();
				$mail->Host = "smtp.1and1.es";
				$mail->SMTPAuth = true;
				$mail->Username = "promo@disfrutaunaexperienciaunica.com";
				$mail->Password = "887456xx_gsD";
				//Indicamos cual es nuestra direcci?n de correo y el nombre que 
				//queremos que vea el usuario que lee nuestro correo
				$mail->From = "promo@disfrutaunaexperienciaunica.com";
				$mail->FromName = "disfrutaunaexperienciaunica.com";
				$mail->Timeout = 30;
				//Indicamos cual es la direcci?n de destino del correo
				$mail->AddAddress("registro@disfrutaunaexperienciaunica.com");
	
				//Asignamos asunto y cuerpo del mensaje
				//El cuerpo del mensaje lo ponemos en formato html, haciendo 
				//que se vea en negrita
				$mail->Subject = "disfrutaunaexperienciaunica.com";
				$mail->IsHTML = true;
				if(isset($_POST["nombre_aco_formcanjea"])){
				$mail->Body = "
		<table width='600' border='0'>
		  <tr>
			<td height='56'><table width='600' border='0'>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td><span style='font-size:18px;font-weight:bold;'>Formulario de Reserva</span></td>
			  </tr>
			  <tr>
				<td><table width='600' border='0'>
				  <tr>
					<td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Primera Elecci&oacute;n</span><br />
					  <span style='font-size:14px;color:#333;'>$arrayExperiencias[0]<br />codigo: $c_contacto_dest1<br />$c_contacto_ida1ok</span></td>
					<td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Segunda Elecci&oacute;n</span><br />
					  <span style='font-size:14px;color:#333;'>$arrayExperiencias[1]<br />codigo: $c_contacto_dest2<br />$c_contacto_ida2ok</span></td>
					<td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Tercera Elecci&oacute;n</span><br />
					  <span style='font-size:14px;color:#333;'>$arrayExperiencias[2]<br />codigo: $c_contacto_dest3<br />$c_contacto_ida3ok</span></td>
				  </tr>
				  <tr>
					<td width='200'>&nbsp;</td>
					<td width='200'>&nbsp;</td>
					<td width='200'>&nbsp;</td>
				  </tr>
				</table></td>
			  </tr>
			</table>
			  <table width='600' border='0'>
				<tr>
				  <td><span style='font-size:18px;font-weight:bold;'>Datos del Participante</span></td>
				</tr>
				<tr>
				  <td><table width='600' border='0'>
					<tr>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Nombre</span><br />
						<span style='font-size:14px;color:#333;'>$c_contacto_nombre_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Poblaci&oacute;n</span><br />
						<span style='font-size:14px;color:#333;'>$c_contacto_poblacion_d</span></td>
					 <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Tel&eacute;fono</span><br />
					  <span style='font-size:14px;color:#333;'>$c_contacto_telf</span></td>
					</tr>
					<tr>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					</tr>
					<tr>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Apellidos</span><br />
		<span style='font-size:14px;color:#333;'>$c_contacto_apellidos_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>C&oacute;digo Postal</span><br />
		<span style='font-size:14px;color:#333;'>$c_contacto_cp</span></td>
					 <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>C&oacute;digo</span><br />
		<span style='font-size:14px;color:#333;'>$c_contacto_codigo</span></td>
					</tr>
					<tr>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					</tr>
					<tr>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>DNI/NIE</span><br />
		<span style='font-size:14px;color:#333;'>$c_contacto_nif_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Provincia</span><br />
		<span style='font-size:14px;color:#333;'>$c_contacto_provincia_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Formulario n&ordm;</span><br />
		<span style='font-size:14px;color:#333;'>$c_contacto_idcontador</span></td>
					</tr>
					<tr>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					</tr>
					<tr>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Direcci&oacute;n</span><br />
		<span style='font-size:14px;color:#333;'>$c_contacto_direccion_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>email</span><br />
		<span style='font-size:14px;color:#333;'>$c_contacto_mail_d</span></td>
					 <td width='200' valign='top'>&nbsp;</td>
					</tr>
				  </table></td>
				</tr>
			  </table>
			  <table width='600' border='0'>
				<tr>
				  <td><span style='font-size:18px;font-weight:bold;'>Datos del Acompańante</span></td>
				</tr>
				<tr>
				  <td><table width='600' border='0'>
					<tr>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Nombre</span><br />
						<span style='font-size:14px;color:#333;'>$c_aco_nombre_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Poblaci&oacute;n</span><br />
						<span style='font-size:14px;color:#333;'>$c_aco_poblacion_d</span></td>
					 <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Tel&eacute;fono</span><br />
					  <span style='font-size:14px;color:#333;'>$c_aco_telf</span></td>
					</tr>
					<tr>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					</tr>
					<tr>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Apellidos</span><br />
		<span style='font-size:14px;color:#333;'>$c_aco_apellidos_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>C&oacute;digo Postal</span><br />
		<span style='font-size:14px;color:#333;'>$c_aco_cp</span></td>
					 <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>C&oacute;digo</span><br />
		<span style='font-size:14px;color:#333;'>$c_aco_codigo</span></td>
					</tr>
					<tr>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					</tr>
					<tr>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>DNI/NIE</span><br />
		<span style='font-size:14px;color:#333;'>$c_aco_nif_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Provincia</span><br />
		<span style='font-size:14px;color:#333;'>$c_aco_provincia_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Formulario n&ordm;</span><br />
		<span style='font-size:14px;color:#333;'>$c_aco_idcontador</span></td>
					</tr>
					<tr>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					</tr>
					<tr>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Direcci&oacute;n</span><br />
		<span style='font-size:14px;color:#333;'>$c_aco_direccion_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>email</span><br />
		<span style='font-size:14px;color:#333;'>$c_aco_mail_d</span></td>
					 <td width='200' valign='top'>&nbsp;</td>
					</tr>
				  </table></td>
				</tr>
			  </table>
			  <table width='600' border='0'>
				<tr>
				  <td>&nbsp;</td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				</tr>
			</table></td>
		  </tr>
		</table>
				  ";
				}else{
					$mail->Body = "
					<table width='600' border='0'>
		  <tr>
			<td height='56'><table width='600' border='0'>
			  <tr>
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td><span style='font-size:18px;font-weight:bold;'>Formulario de Reserva</span></td>
			  </tr>
			  <tr>
				<td><table width='600' border='0'>
				  <tr>
					<td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Primera Elecci&oacute;n</span><br />
					  <span style='font-size:14px;color:#333;'>$arrayExperiencias[0]<br />codigo: $c_contacto_dest1<br />$c_contacto_ida1ok</span></td>
					<td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Segunda Elecci&oacute;n</span><br />
					  <span style='font-size:14px;color:#333;'>$arrayExperiencias[1]<br />codigo: $c_contacto_dest2<br />$c_contacto_ida2ok</span></td>
					<td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Tercera Elecci&oacute;n</span><br />
					  <span style='font-size:14px;color:#333;'>$arrayExperiencias[2]<br />codigo: $c_contacto_dest3<br />$c_contacto_ida3ok</span></td>
				  </tr>
				  <tr>
					<td width='200'>&nbsp;</td>
					<td width='200'>&nbsp;</td>
					<td width='200'>&nbsp;</td>
				  </tr>
				</table></td>
			  </tr>
			</table>
			  <table width='600' border='0'>
				<tr>
				  <td><span style='font-size:18px;font-weight:bold;'>Datos del Participante</span></td>
				</tr>
				<tr>
				  <td><table width='600' border='0'>
					<tr>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Nombre</span><br />
						<span style='font-size:14px;color:#333;'>$c_contacto_nombre_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Poblaci&oacute;n</span><br />
						<span style='font-size:14px;color:#333;'>$c_contacto_poblacion_d</span></td>
					 <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Tel&eacute;fono</span><br />
					  <span style='font-size:14px;color:#333;'>$c_contacto_telf</span></td>
					</tr>
					<tr>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					</tr>
					<tr>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Apellidos</span><br />
		<span style='font-size:14px;color:#333;'>$c_contacto_apellidos_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>C&oacute;digo Postal</span><br />
		<span style='font-size:14px;color:#333;'>$c_contacto_cp</span></td>
					 <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>C&oacute;digo</span><br />
		<span style='font-size:14px;color:#333;'>$c_contacto_codigo</span></td>
					</tr>
					<tr>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					</tr>
					<tr>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>DNI/NIE</span><br />
		<span style='font-size:14px;color:#333;'>$c_contacto_nif_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Provincia</span><br />
		<span style='font-size:14px;color:#333;'>$c_contacto_provincia_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Formulario n&ordm;</span><br />
		<span style='font-size:14px;color:#333;'>$c_contacto_idcontador</span></td>
					</tr>
					<tr>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					</tr>
					<tr>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Direcci&oacute;n</span><br />
		<span style='font-size:14px;color:#333;'>$c_contacto_direccion_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>email</span><br />
		<span style='font-size:14px;color:#333;'>$c_contacto_mail_d</span></td>
					 <td width='200' valign='top'>&nbsp;</td>
					</tr>
				  </table></td>
				</tr>
			  </table>
			  <table width='600' border='0'>
				<tr>
				  <td><span style='font-size:18px;font-weight:bold;'>Datos del Acompańante</span></td>
				</tr>
				<tr>
				  <td><table width='600' border='0'>
					<tr>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Nombre</span><br />
						<span style='font-size:14px;color:#333;'>$c_aco_nombre_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Poblaci&oacute;n</span><br />
						<span style='font-size:14px;color:#333;'>$c_aco_poblacion_d</span></td>
					 <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Tel&eacute;fono</span><br />
					  <span style='font-size:14px;color:#333;'>$c_aco_telf</span></td>
					</tr>
					<tr>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					</tr>
					<tr>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Apellidos</span><br />
		<span style='font-size:14px;color:#333;'>$c_aco_apellidos_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>C&oacute;digo Postal</span><br />
		<span style='font-size:14px;color:#333;'>$c_aco_cp</span></td>
					 <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>C&oacute;digo</span><br />
		<span style='font-size:14px;color:#333;'>$c_aco_codigo</span></td>
					</tr>
					<tr>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					</tr>
					<tr>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>DNI/NIE</span><br />
		<span style='font-size:14px;color:#333;'>$c_aco_nif_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Provincia</span><br />
		<span style='font-size:14px;color:#333;'>$c_aco_provincia_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Formulario n&ordm;</span><br />
		<span style='font-size:14px;color:#333;'>$c_aco_idcontador</span></td>
					</tr>
					<tr>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					  <td width='200'>&nbsp;</td>
					</tr>
					<tr>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>Direcci&oacute;n</span><br />
		<span style='font-size:14px;color:#333;'>$c_aco_direccion_d</span></td>
					  <td width='200' valign='top'><span style='font-size:14px;font-weight:bold;'>email</span><br />
		<span style='font-size:14px;color:#333;'>$c_aco_mail_d</span></td>
					 <td width='200' valign='top'>&nbsp;</td>
					</tr>
				  </table></td>
				</tr>
			  </table>
			  <table width='600' border='0'>
				<tr>
				  <td>&nbsp;</td>
				</tr>
				<tr>
				  <td>&nbsp;</td>
				</tr>
			</table></td>
		  </tr>
		</table>
				  ";
				}
	
				//Definimos AltBody por si el destinatario del correo no admite email con formato html 
				$mail->AltBody = " Hola " . $c_contacto_nombre . ". Hemos recibido tu inscripcci&oacute;n, a la mayor brevedad te daremos una respuesta. Recibe un cordial saludo.";
	
				//se envia el mensaje, si no ha habido problemas 
				//la variable $exito tendra el valor true
				$exito = $mail->Send();
	
				//Si el mensaje no ha podido ser enviado se realizaran 4 intentos mas como mucho 
				//para intentar enviar el mensaje, cada intento se hara 5 segundos despues 
				//del anterior, para ello se usa la funcion sleep	
				$intentos = 1;
				while ((!$exito) && ($intentos < 5)) {
					sleep(5);
					//echo $mail->ErrorInfo;
					$exito = $mail->Send();
					$intentos = $intentos + 1;
				}
	
	
	//    //To send succesful registration mail to user we will get the mail content from database (table `campanas_emails`.
	//    $bodyCorreo="";
	//    $sqlCorreo = "SELECT * FROM `campanas_emails` where `rel_campana_email`= (SELECT id FROM `campanas` where `prefijo_campana` like \"%".$table_prefix."__%\") AND `valor_campana_email` = ( SELECT `id_valor_email_campana` FROM `campana_emails_valores` WHERE `valor_email_campana` LIKE \"%Email de registro%\" )"; 
	//    
	//    mysqli_query($link,$sqlCorreo);
	//    $rsCorreo= mysqli_query($link,$sqlCorreo);
	//    while ($row=mysqli_fetch_array($rsCorreo)) {
	//        $bodyCorreo = $row["texto_campana_email"];
	//    }
				
				if($lang_sql=="cat"){
					$enlace1="<a href='http://disfrutaunaexperienciaunica.com/tuexperiencia.php?lang=cat&x=$c_contacto_nif&y=$c_contacto_idcontador&z=$c_contacto_codigo'>aqu&iacute;</a>";
					$enlace2="";
					$enlace3="<a href='http://disfrutaunaexperienciaunica.com/tuexperiencia.php?lang=cat&x=$c_contacto_nif&y=$c_contacto_idcontador&z=$c_contacto_codigo'>disfrutaunaexperienciaunica.com/tuexperiencia.php?lang=cat&x=$c_contacto_nif&y=$c_contacto_idcontador&z=$c_contacto_codigo</a>";
				}else{
					$enlace1="<a href='http://disfrutaunaexperienciaunica.com/tuexperiencia.php?x=$c_contacto_nif&y=$c_contacto_idcontador&z=$c_contacto_codigo'>aqu&iacute;</a>";
					$enlace2="";
					$enlace3="<a href='http://disfrutaunaexperienciaunica.com/tuexperiencia.php?x=$c_contacto_nif&y=$c_contacto_idcontador&z=$c_contacto_codigo'>disfrutaunaexperienciaunica.com/tuexperiencia.php?x=$c_contacto_nif&y=$c_contacto_idcontador&z=$c_contacto_codigo</a>";
				}
				$enlace1="<a href='http://disfrutaunaexperienciaunica.com/tuexperiencia.php?x=$c_contacto_nif&y=$c_contacto_idcontador&z=$c_contacto_codigo'>aqu&iacute;</a>";
				$enlace2="";
				$enlace3="<a href='http://disfrutaunaexperienciaunica.com/tuexperiencia.php?x=$c_contacto_nif&y=$c_contacto_idcontador&z=$c_contacto_codigo'>disfrutaunaexperienciaunica.com/tuexperiencia.php?x=$c_contacto_nif&y=$c_contacto_idcontador&z=$c_contacto_codigo</a>";
				$mail2 = new phpmailer();
				$mail2->Mailer = "smtp";
				$mail2->IsSendMail();
				$mail2->Host = "smtp.1and1.es";
				$mail2->SMTPAuth = true;
				$mail2->Username = "promo@disfrutaunaexperienciaunica.com";
				$mail2->Password = "887456xx_gsD";
				//Indicamos cual es nuestra direcci?n de correo y el nombre que 
				//queremos que vea el usuario que lee nuestro correo
				$mail2->From = "promo@disfrutaunaexperienciaunica.com";
				$mail2->FromName = "Disfruta una experiencia unica - Registro Completo";
				$mail2->Timeout = 30;
				//Indicamos cual es la direcci?n de destino del correo
				$mail2->AddAddress($c_contacto_mail_d);
				$mail2->AddBCC("registro@disfrutaunaexperienciaunica.com");
	
				//Asignamos asunto y cuerpo del mensaje
				//El cuerpo del mensaje lo ponemos en formato html, haciendo 
				//que se vea en negrita
				$mail2->Subject = $lang['TEXT_MAIL_USER_1_TIT'];
				$mail2->Body = "
				<table width='865' border='0'>
				  <tr>
					<td><img src='http://disfrutaunaexperienciaunica.com/images/topmail".$lang_gionarchivo.".jpg' width='865' height='290' /></td>
				  </tr>
				  <tr>
					<td>
					<br />
					".$lang['TEXT_MAIL_USER_1']."$enlace1".$lang['TEXT_MAIL_USER_2']."$enlace2".$lang['TEXT_MAIL_USER_3']."$enlace3".$lang['TEXT_MAIL_USER_4']."
					<br />
					</td>
				  </tr>
				  <tr>
					<td><img src='http://disfrutaunaexperienciaunica.com/images/footermail".$lang_gionarchivo.".jpg' width='865' height='169' /></td>
				  </tr>
				</table>";
	
				//Definimos AltBody por si el destinatario del correo no admite email con formato html 
				$mail2->AltBody = "Nombre : " . $c_contacto_nombre_d . " E-mail : " . $c_contacto_mail . "";
	
				//se envia el mensaje, si no ha habido problemas 
				//la variable $exito tendra el valor true
				$exito = $mail2->Send();
	
				//Si el mensaje no ha podido ser enviado se realizaran 4 intentos mas como mucho 
				//para intentar enviar el mensaje, cada intento se hara 5 segundos despues 
				//del anterior, para ello se usa la funcion sleep	
				$intentos = 1;
				while ((!$exito) && ($intentos < 5)) {
					sleep(5);
					//echo $mail->ErrorInfo;
					$exito = $mail2->Send();
					$intentos = $intentos + 1;
				}
	
				mysqli_close($link);
				$_SESSION["adeslas2hogar2016_paso2"] = "3";
				echo"<script type='text/javascript'>
					window.location='./tuexperiencia.php?x=" . $c_contacto_nif_d . "&y=" . $c_contacto_idcontador . "&z=" . $c_contacto_codigo . "';
				</script>";
				}

          		///////////////////////////////////////////fin todo correcto///////////////////////////////////////////////////////
				}
		}
		} else {
			echo"<script type='text/javascript'>
			$(document).ready(function(){
				$('.DivError').remove();
				$('#dniformcanjea').focus().after('<div class=\"alert alert-danger DivError\" id=\"avisoPop\">".$lang['TEXT_FORM3_27']."</div>');
				return false;
			});
			</script>";
		}

		
		
	
			
        }
    } else {
        session_unset();
        session_destroy();
        echo"<script type='text/javascript'>
                window.location='http://disfrutaunaexperienciaunica.com';
            </script>";
    }
    
    
} else {//si la sesi�n no existe, redirecciona
    session_unset();
    session_destroy();
    echo"<script type='text/javascript'>
            window.location='http://disfrutaunaexperienciaunica.com';
        </script>";
}
?>