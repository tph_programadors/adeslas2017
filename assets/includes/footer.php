
        <hr>
        <!-- Footer -->
        <footer>
            <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="left"><a href="#" data-toggle="modal" data-target="#login-modal"><?php echo $lang['TEXT_BASES']; ?></a> | <a href="#" data-toggle="modal" data-target="#politica"><?php echo $lang['TEXT_POL_COOK']; ?></a> | <a href="#" data-toggle="modal" data-target="#lopd-modal"><?php echo $lang['TEXT_POL_PRIV']; ?></a> </div>
                </div>
                <div class="col-lg-6">
                    <div class="right">Copyright &copy; TPH Marketing</div>
                </div>      
            </div>
                </div>
    <!-- /.container -->
        </footer>
        
        <?php include("popups/bases.php"); ?>
