

  <!-- Bar top logo -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
               <!-- <div class="top_logo">
               
                <img src="images/segur_logo.png"  alt="SegurCaixa Adeslas">
                </div>-->
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
    
    <!-- Bar top idiomes -->
                <div class="top_idiomes">
                     <div class="container">
                        <div class="row">
                        	<div style="float:right;">
                            	<?php if(($_SESSION["adeslas2hogar2016_paso2"]==1)or(($_SESSION["adeslas2hogar2016_paso2"]==2))){
									function traducefecha2sindiaes($fecha){ 
										$fecha= strtotime($fecha); // convierte la fecha de formato mm/dd/yyyy a marca de tiempo 
										$diasemana=date("w", $fecha);// optiene el número del dia de la semana. El 0 es domingo 
											switch ($diasemana) 
											{ 
											case "0": 
												$diasemana="Domingo"; 
												break; 
											case "1": 
												$diasemana="Lunes"; 
												break; 
											case "2": 
												$diasemana="Martes"; 
												break; 
											case "3": 
												$diasemana="Miércoles"; 
												break; 
											case "4": 
												$diasemana="Jueves"; 
												break; 
											case "5": 
												$diasemana="Viernes"; 
												break; 
											case "6": 
												$diasemana="Sábado"; 
												break; 
											} 
										$dia=date("d",$fecha); // día del mes en número 
										$mes=date("m",$fecha); // número del mes de 01 a 12 
											switch($mes) 
											{ 
											case "01": 
												$mes="Enero"; 
												break; 
											case "02": 
												$mes="Febrero"; 
												break; 
											case "03": 
												$mes="Marzo"; 
												break; 
											case "04": 
												$mes="Abril"; 
												break; 
											case "05": 
												$mes="Mayo"; 
												break; 
											case "06": 
												$mes="Junio"; 
												break; 
											case "07": 
												$mes="Julio"; 
												break; 
											case "08": 
												$mes="Agosto"; 
												break; 
											case "09": 
												$mes="Septiembre"; 
												break; 
											case "10": 
												$mes="Octubre"; 
												break; 
											case "11": 
												$mes="Noviembre"; 
												break; 
											case "12": 
												$mes="Diciembre"; 
												break; 
											} 
										$ano=date("Y",$fecha); // optenemos el ano en formato 4 digitos 
										$fecha= "".$dia." de ".$mes." de ".$ano; // unimos el resultado en una unica cadena 
										return $fecha; //enviamos la fecha al programa 
									} 
									
									function traducefecha2sindiacat($fecha){ 
										$fecha= strtotime($fecha); // convierte la fecha de formato mm/dd/yyyy a marca de tiempo 
										$diasemana=date("w", $fecha);// optiene el número del dia de la semana. El 0 es domingo 
											switch ($diasemana) 
											{ 
											case "0": 
												$diasemana="Diumenge"; 
												break; 
											case "1": 
												$diasemana="Dilluns"; 
												break; 
											case "2": 
												$diasemana="Dimarts"; 
												break; 
											case "3": 
												$diasemana="Dimecres"; 
												break; 
											case "4": 
												$diasemana="Dijous"; 
												break; 
											case "5": 
												$diasemana="Divendres"; 
												break; 
											case "6": 
												$diasemana="Dissabte"; 
												break; 
											} 
										$dia=date("d",$fecha); // día del mes en número 
										$mes=date("m",$fecha); // número del mes de 01 a 12 
											switch($mes) 
											{ 
											case "01": 
												$mes="Gener"; 
												break; 
											case "02": 
												$mes="Febrer"; 
												break; 
											case "03": 
												$mes="Març"; 
												break; 
											case "04": 
												$mes="Abril"; 
												break; 
											case "05": 
												$mes="Maig"; 
												break; 
											case "06": 
												$mes="Juny"; 
												break; 
											case "07": 
												$mes="Juliol"; 
												break; 
											case "08": 
												$mes="Agost"; 
												break; 
											case "09": 
												$mes="Septembre"; 
												break; 
											case "10": 
												$mes="Octubre"; 
												break; 
											case "11": 
												$mes="Novembre"; 
												break; 
											case "12": 
												$mes="Decembre"; 
												break; 
											} 
										$ano=date("Y",$fecha); // optenemos el ano en formato 4 digitos 
										$fecha= "".$dia." de ".$mes." de ".$ano; // unimos el resultado en una unica cadena 
										return $fecha; //enviamos la fecha al programa 
									} 
									
									if($lang_sql=="cat"){
										$n_fechasindia2 = "traducefecha2sindiacat";
									}else{
										$n_fechasindia2 = "traducefecha2sindiaes";
									}
                                ?>
								 <?php 
                                   $fechacaducidad = strtotime ('+15 day' , strtotime ($n_fechareg)) ;
                                   $nuevafecha = date ( 'Y-m-d H:i:s' , $fechacaducidad);
                                   $segundos = strtotime($nuevafecha) - strtotime($fecha_actual); 
                                   ?>
                                    <div class="clock" style="margin:2em;"></div>
                                    <div style="text-align:center;"><?PHP echo $lang['MENU_TIEMPO']; echo $n_fechasindia2($n_fechareg); echo $lang['MENU_TIEMPO2'];?>
                                    </div>
                                    <!-- <script src="assets/js/countdown.js"></script>
                                    <script type="application/javascript">
                                    var myCountdown1 = new Countdown({
                                        time: <?php echo"$segundos";?> * 1, // 86400 seconds = 1 day
                                        width:200, 
                                        height:60,  
                                        rangeHi:"hour",
                                        onComplete	: countdownComplete,
                                        style:"flip"	// <- no comma on last item!
                                    });
                                    function countdownComplete(){
                                        $('#divtiempo24').hide();
                                        $('#divtiempoagotado').show();
                                    }
                                    
                                    </script>-->
                                    <div class="" id="divtiempo24">
                                    </div>
                                    
                                    <div class="alert alert-dismissible alert-danger" id="divtiempoagotado" style="display:none;">
                                    </div> 
                                    <?php } ?>
                            </div>
                        </div>
                         <!-- /.row -->
                    </div>
                    <!-- /.container -->
                </div> <!-- /.top idiomes -->