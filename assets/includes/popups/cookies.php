
<div class="modal fade" id="politica" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    	<div class="modal-dialog">
			<div class="modal-content">
            <div class="element_to_pop_up_content1">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="images/close.png" width="38" height="39" /></button>
             <h4>POLÍTICA DE COOKIES</h4>
          </div>
           <div class="element_to_pop_up_content2">
  <p>El  acceso y navegación en el Sitio Web puede implicar la utilización de cookies.<br>
<strong>¿Qué  son las cookies?</strong> <br>
Las  cookies son pequeños archivos que se almacenan en el navegador utilizado por  cada Usuario. Éstas permiten, entre otras cosas, almacenar y recuperar  información sobre los hábitos de navegación del Usuario o de su equipo,  audiencia de nuestras páginas, adaptar nuestro contenido editorial y,  dependiendo de la información que contengan y de la forma en que utilice su  equipo, pueden utilizarse para reconocer al Usuario Registrado durante el  periodo de validez o de registro de la cookie.<br>
El uso  de las cookies nos ayuda a saber de sus usos, preferencias y por lo tanto a  mejorar nuestro servicio día tras día.<br>
Las  cookies tienen, generalmente, una duración limitada en el tiempo. Ninguna  cookie permite que pueda contactarse con el número de teléfono del Usuario, su  dirección de correo electrónico o con cualquier otro medio de contacto. Ninguna  cookie puede extraer información del disco duro de cualquier dispositivo del  Usuario o robar información personal.<br>
<strong>¿Qué  tipos de cookies utilizamos?</strong> <br>
Cookies  de análisis: Son aquellas que bien tratadas por nosotros o por terceros, nos  permiten cuantificar el número de usuarios y así realizar la medición y  análisis estadístico de la utilización que hacen los usuarios del servicio  ofertado. Para ello se analiza su navegación en nuestra página web con el fin  de mejorar la oferta de productos o servicios que le ofrecemos.<br>
Cookies  de sesión: Son aquellas diseñadas para permanecer activas mientras el usuario  accede a la página web o para la prestación del servicio.<br>
Cookies  de personalización: Son aquellas que permiten al Usuario especificar o  personalizar algunas características de las opciones generales de la página  web. Por ejemplo, definir el idioma, configuración regional o tipo de  navegador.<br>
A  continuación, se ofrece información acerca de las cookies empleadas en el sitio  web:</p>
<table border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="189"><br>
      <strong>COOKIES PROPIAS</strong></td>
    <td width="378" colspan="2"><p>&nbsp;</p></td>
  </tr>
  <tr>
    <td width="189"><p><a name="_Hlk478983037"><strong>ORIGEN</strong></a></p></td>
    <td width="189"><p><strong>FINALIDAD</strong></p></td>
    <td width="189"><p><strong>PERSISTENCIA</strong></p></td>
  </tr>
  <tr>
    <td width="189"><p>Cookie sesión usuario</p></td>
    <td width="189"><p>Informacion de la sesion    de usuario</p></td>
    <td width="189"><p>Ilimitada</p></td>
  </tr>
  <tr>
    <td width="189"><p>Cookie sesión idioma</p></td>
    <td width="189"><p>Información    del&nbsp;idioma utilizado por el usuario</p></td>
    <td width="189"><p>Ilimitada</p></td>
  </tr>
</table>
<table border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="189"><p><strong>COOKIES DE TERCEROS</strong></p></td>
    <td width="378" colspan="2"><p>&nbsp;</p></td>
  </tr>
  <tr>
    <td width="189"><p><strong>ORIGEN</strong></p></td>
    <td width="189"><p><strong>FINALIDAD</strong></p></td>
    <td width="189"><p><strong>PERSISTENCIA</strong></p></td>
  </tr>
  <tr>
    <td width="189"><p>Google Analytics<br>
      (__gat, __gat_global,_ )</p></td>
    <td width="189"><p>Análisis destinado a    saber cómo navegas por nuestra web con el fin de mejorar los servicios que te    podemos ofrecer. Esta información es anónima, y solo la necesitamos para    fines estadísticos.</p></td>
    <td width="189"><p>Si bien las cookies se    encuentran ubicadas en el sitio web, son consideradas de terceros, por cuanto    la información que permiten recabar es almacenada en los servidores de Google    Inc.Su carácter es persistente, por lo que no desaparece al cerrar la sesión    de navegación.</p></td>
  </tr>
</table>
<p><strong>¿Cómo  se usan las cookies?</strong> <br>
  El  Sitio Web &nbsp;utiliza Cookies de Google Analytics para para recopilar datos  estadísticos de la actividad de los usuarios en el sitio Web y, de este modo,  poder mejorar los servicios prestados a los usuarios. Este servicio es ofrecido  por Google y que se rige por las condiciones generales de Google&nbsp;<a href="https://www.google.com/intl/es/policies/">https://www.google.com/intl/es/policies/</a>&nbsp;y la política de cookies accesible en&nbsp;<a href="https://tools.google.com/dlpage/gaoptout" target="_blank">https://tools.google.com/dlpage/gaoptout</a> <br>
  <strong>Cómo  rechazar las cookies</strong> <br>
  El  Usuario puede configurar su navegador para aceptar, rechazar, bloquear o  eliminar las cookies que y evitar el almacenamiento de las mismas en su disco  duro. Puede encontrar las instrucciones en la configuración de seguridad en su  navegador Web. También puede hacer uso de la sección &ldquo;Ayuda&rdquo; que encontrará en  la barra de herramientas de la mayoría de navegadores para cambiar los ajustes  de su equipo. El hecho de aceptar o no las cookies es bajo su responsabilidad,  pudiendo verse afectada la navegación por la Web, en términos de usabilidad en  caso de su rechazo.<br>
  Si  quiere borrar las cookies registradas, siga las instrucciones para los  distintos navegadores:</p>
<ul type="disc">
  <li><strong>Microsoft Internet Explorer</strong>:&nbsp;<a href="https://support.microsoft.com/es-es/help/17442/windows-internet-explorer-delete-manage-cookies">https://support.microsoft.com/es-es/help/17442/windows-internet-explorer-delete-manage-cookies</a></li>
  <li><strong>Firefox:</strong>&nbsp;<a href="https://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-sitios-web-rastrear-preferencias?redirectlocale=es&redirectslug=habilitar-y-deshabilitar-cookies-que-los-sitios-we">https://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-sitios-web-rastrear-preferencias?redirectlocale=es&amp;redirectslug=habilitar-y-deshabilitar-cookies-que-los-sitios-we</a></li>
  <li><strong>Google Chrome:&nbsp;</strong><a href="https://support.google.com/chrome/answer/95647?hl=es">https://support.google.com/chrome/answer/95647?hl=es</a></li>
  <li><strong>Safari:</strong>&nbsp;<a href="https://support.apple.com/es-es/HT201265">https://support.apple.com/es-es/HT201265</a></li>
</ul>
<p><strong>Cambios  en la Política de Cookies</strong> <br>
  Es  posible que actualicemos la Política de Cookies de nuestro Sitio Web, por ello  le recomendamos revisar esta política cada vez que acceda a los mismos con el  objetivo de estar adecuadamente informado sobre cómo y para qué usamos las  cookies.</p>


        </div><!-- Fi Scroll -->
			</div>
		</div>
	</div>
