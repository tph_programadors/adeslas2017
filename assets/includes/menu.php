
   <!-- Menu navbar logo -->
    <div class="container">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index.php" class="first"><?php echo $lang['MENU_HOME']; ?></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $lang['MENU_REGALO']; ?></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="hotel.php"><?php echo $lang['MENU_HOTEL']; ?></a>
                            </li>
                            <li>
                                <a href="experiencias.php" style="border:none;"><?php echo $lang['MENU_EXPE']; ?></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="solicitar_regalo_paso1.php"><?php echo $lang['MENU_COMO']; ?></a>
                    </li>
                     <li>
                        <a href="faq.php"><?php echo $lang['MENU_FAQ']; ?></a>
                    </li>
                    <li>
                        <a href="contacto.php"><?php echo $lang['MENU_CONTACT']; ?></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
    </nav>
     </div>
    <!-- /.container -->
    