<?php
session_start();
   include_once '../../common.php'; 
    function getCategoryWithActiveServices($table_prefix, $link, $excluded_categories) {
        $outputArray = array();
        
        //For every category
        $sqlcategorias = "SELECT * FROM categorias ";

        //Get sql to eclude categories by Id
        if ($excluded_categories !="" ){
            $excluded_categories = explode(',', $excluded_categories);
            $sqlcategorias .= "WHERE '1'='1' ";
            foreach ($excluded_categories as $arrayId => $categoryId) {
                $sqlcategorias .= " AND id_categoria != ".$categoryId;
            }
        }
		if($lang_sql=="cat"){
			$lengbusc="_cat";
		}else{
			$lengbusc="";
		}
        
        $sqlcategorias .= " order by nombre_categoria";
        
        $rscategorias = mysqli_query($link, $sqlcategorias);
        while ($row=mysqli_fetch_array($rscategorias)) {
            $s_id_categoria=$row["id_categoria"];
            $s_nombre_categoria2=$row["nombre_categoria"];
            $s_nombre_categoria=utf8_encode($s_nombre_categoria2);

            $sqlservicios = "SELECT * FROM servicios WHERE rel_categoria = '$s_id_categoria' order by nombre_servicio";
            $rsservicios = mysqli_query($link, $sqlservicios);
            while ($row=mysqli_fetch_array($rsservicios)) {
                $s_id_servicio=$row["id_servicio"];
                $s_nombre_servicio2=$row["nombre_servicio".$lengbusc.""];
                $s_nombre_servicio=utf8_encode($s_nombre_servicio2);

                $sqlservicios2 = "
                    SELECT * FROM campanas_experiencias WHERE id_rel_servicio = '$s_id_servicio' AND id_rel_campana = (
                        SELECT 
                                id 
                        FROM `campanas` 
                        where `prefijo_campana` = \"adeslas2hogar2016__\"
                    )
                ";
                $rsservicios2 = mysqli_query($link, $sqlservicios2);

                $num_total_servicios2 = $rsservicios2->num_rows;

                if($num_total_servicios2!=0){
                    $outputArray[$s_id_categoria."|".$s_nombre_categoria][]=$s_id_servicio."|".$s_nombre_servicio;
                }
            }
        }

            return $outputArray;
    } 
       
    function generateCategoryOptions($CategoryServiceArray) {
        $text = "";
        foreach ($CategoryServiceArray as $key=>$value) {
            $keyArray=Array();
            $keyArray=explode("|", $key);
            $text .= '<option value="'.$key.'" >'.$keyArray[1].'</option></br>';
        }
        
        return $text;
    }
    
    function generateServiceOptions($CategoryServiceArray, $CategoryId) {
        $text = "<option value=''></option>";
        $serviceArray=Array();
        $serviceArray=$CategoryServiceArray[$CategoryId];

        foreach ($serviceArray as $key) {
            $keyArray=Array();
            $keyArray=explode("|", $key);
            $text .= '<option value="'.$keyArray[0].'" >'.$keyArray[1].'</option>';
        }
        
        return $text;
    }
    
?>