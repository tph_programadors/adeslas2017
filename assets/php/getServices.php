<?php

include "../connect/conexion.php";
include "./categoryServices.php";

$table_prefix = "adeslas2hogar2016";

function getUrlParams($params) {
    $params["elegido"]              = isset($_POST["elegido"])              ? $_POST["elegido"]             : $params["elegido"];
    $params["field_for"]            = isset($_POST["field_for"])            ? $_POST["field_for"]           : $params["field_for"];
    return $params;
}

//default params init
$params = array();
$params["elegido"] = '';
$params["field_for"] = '';

$outputArray = array();

$params = getUrlParams($params);

$outputArray["field_for"] = $params["field_for"];

if ($params["elegido"] == "") {
    $outputArray["options"] = "<option value=''></option>";
} else {
    $CategoryServiceArray=getCategoryWithActiveServices($table_prefix, $link);
   
    $outputArray["options"] = generateServiceOptions($CategoryServiceArray, $params["elegido"]);
}
    echo json_encode($outputArray);
?>