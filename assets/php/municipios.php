<?php

include "../connect/conexion.php";

function getUrlParams($params) {
    $params["selected"]        = isset($_POST["selected"])    ? $_POST["selected"] : $params["selected"];
    $params["elegido"]         = isset($_POST["elegido"])     ? $_POST["elegido"]  : $params["elegido"];
    
    return $params;
}

//default params init
$params = array();
$params["selected"] = '';
$params["elegido"] = '';

$params = getUrlParams($params);

if ($params["elegido"] == "") {
    echo"<option value=''>Selecciona</option>";
} else {
    echo"<option value=''>Selecciona</option>";
    $sqlmunicipios = "SELECT * FROM `crm_municipios` WHERE `crm_rel_provincia` = " . $_POST["elegido"] . "";
    $rsmunicipios = mysqli_query($link, $sqlmunicipios);
    while ($row = mysqli_fetch_array($rsmunicipios)) {
        $selected = "";
        $id_municipio = $row["crm_id_municipio"];
        $nombre_municipio2 = $row["crm_municipio"];
        $nombre_municipio = utf8_encode($nombre_municipio2);
        if ($params["selected"] == $id_municipio) {
            $selected = "selected";
        }

        echo"<option value='$id_municipio' $selected>$nombre_municipio</option>";
    }
}
?>