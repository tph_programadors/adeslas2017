$("#enviar-cupon").on("click", function(e){
   e.preventDefault();
   var f = $(this);
   var formData = new FormData(document.getElementById("cupon-upload"));
   formData.append("dato", "valor");
   $.ajax({
	   url: "recibearchivo4.php",
	   type: "post",
	   dataType: "html",
	   data: formData,
	   cache: false,
	   contentType: false,
	   processData: false
   })
   .done(function(res){
	   $("#msg-upload-cupon").html(res);
   });
});

$("#enviar-formulario").on("click", function(e){
   e.preventDefault();
   var f = $(this);
   var formData = new FormData(document.getElementById("formulario-upload"));
   formData.append("dato", "valor");
   $.ajax({
	   url: "recibearchivo4.php",
	   type: "post",
	   dataType: "html",
	   data: formData,
	   cache: false,
	   contentType: false,
	   processData: false
   })
   .done(function(res){
	   $("#msg-upload-formulario").html(res);
   });
});

$("#enviar-dni").on("click", function(e){
   e.preventDefault();
   var f = $(this);
   var formData = new FormData(document.getElementById("dni-upload"));
   formData.append("dato", "valor");
   $.ajax({
	   url: "recibearchivo4.php",
	   type: "post",
	   dataType: "html",
	   data: formData,
	   cache: false,
	   contentType: false,
	   processData: false
   })
   .done(function(res){
	   $("#msg-upload-DNI").html(res);
   });
});

$("#enviar-dniaco").on("click", function(e){
   e.preventDefault();
   var f = $(this);
   var formData = new FormData(document.getElementById("dniaco-upload"));
   formData.append("dato", "valor");
   $.ajax({
	   url: "recibearchivo4.php",
	   type: "post",
	   dataType: "html",
	   data: formData,
	   cache: false,
	   contentType: false,
	   processData: false
   })
   .done(function(res){
	   $("#msg-upload-DNIACO").html(res);
   });
});

$("#enviar-librofamilia").on("click", function(e){
   e.preventDefault();
   var f = $(this);
   var formData = new FormData(document.getElementById("librofamilia-upload"));
   formData.append("dato", "valor");
   $.ajax({
	   url: "recibearchivo4.php",
	   type: "post",
	   dataType: "html",
	   data: formData,
	   cache: false,
	   contentType: false,
	   processData: false
   })
   .done(function(res){
	   $("#msg-upload-LIBROFAMILIA").html(res);
   });
});

$("#enviar-autmenores").on("click", function(e){
   e.preventDefault();
   var f = $(this);
   var formData = new FormData(document.getElementById("autmenores-upload"));
   formData.append("dato", "valor");
   $.ajax({
	   url: "recibearchivo4.php",
	   type: "post",
	   dataType: "html",
	   data: formData,
	   cache: false,
	   contentType: false,
	   processData: false
   })
   .done(function(res){
	   $("#msg-upload-AUTMENORES").html(res);
   });
});

$("#enviar-ticket").on("click", function(e){
   e.preventDefault();
   var f = $(this);
   var formData = new FormData(document.getElementById("ticket-upload"));
   formData.append("dato", "valor");
   $.ajax({
	   url: "recibearchivo4.php",
	   type: "post",
	   dataType: "html",
	   data: formData,
	   cache: false,
	   contentType: false,
	   processData: false
   })
   .done(function(res){
	   $("#msg-upload-TICKET").html(res);
   });
});

