<?php include_once 'common.php';?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    
    <title><?php echo $lang['PAGE_TITLE']; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="assets/css/modern-business.css" rel="stylesheet">
    <link href="assets/css/cookies.css" rel="stylesheet" type="text/css">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<?php include("assets/includes/analytics.php"); ?>
</head>

<body>

    <?php include("assets/includes/top.php"); ?>
    
    <?php include("assets/includes/menu.php"); ?>
    
    
 
<div class="back_color">
<!-- Image Header -->
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <div class="banner_home">
                 <!-- <div class="text_caption"><?php echo $lang['TEXT_BANNER']; ?></div>-->
                 <div class="label_caption"><a href="solicitar_regalo_paso1.php"><?php echo $lang['BOTON_BANNER']; ?></a></div>
                 <a href="solicitar_regalo_paso1.php"><?php echo $lang['BANNER_PRIN']; ?></a>
                </div><!-- /.banner home -->
                <div class="banner_home_res">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                              <a href="solicitar_regalo_paso1.php">
                               <?php echo $lang['BANNER_RES']; ?>
                              </a>
                           </div>
                         </div>
                      </div>
                </div><!-- /.banner home -->
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
        </div>
    <!-- /.back color -->
    
    
    <div class="container">
            <!-- Banner Carousel -->
            <div class="panel_middle">
               <div class="row">
                    <div class="col-md-12">
                       <?php echo $lang['TEXT_FAQ']; ?>
                    </div><!-- /.fi col 12 -->
               </div><!-- /.row -->
            </div><!-- /.fi panell -->
    </div>
    

            <!--//BLOQUE COOKIES-->
            <div id="barraaceptacion">
                <div class="block_cookie">
                    Utilizamos cookies de terceros para mejorar tu accesibilidad, personalizar y analizar tus hábitos de navegación. Si continuas navegando, consideramos que aceptas su uso. Puedes cambiar la configuración u obtener más información en Política de Cookies.
                    <a href="javascript:void(0);" onclick="PonerCookie();"><b>OK</b></a> | 
                    <a href="#" data-toggle="modal" data-target="#politica">M&aacute;s informaci&oacute;n</a>
                </div>
            </div>

    <?php include("assets/includes/popups/cookies.php"); ?>    
    <?php include("assets/includes/footer.php"); ?>
    
        </body>
   </html> 
   
           <script>
            function getCookie(c_name){
                var c_value = document.cookie;
                var c_start = c_value.indexOf(" " + c_name + "=");
                if (c_start == -1){
                    c_start = c_value.indexOf(c_name + "=");
                }
                if (c_start == -1){
                    c_value = null;
                }else{
                    c_start = c_value.indexOf("=", c_start) + 1;
                    var c_end = c_value.indexOf(";", c_start);
                    if (c_end == -1){
                        c_end = c_value.length;
                    }
                    c_value = unescape(c_value.substring(c_start,c_end));
                }
                return c_value;
            }
            
            function setCookie(c_name,value,exdays){
                var exdate=new Date();
                exdate.setDate(exdate.getDate() + exdays);
                var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
                document.cookie=c_name + "=" + c_value;
            }
            
            if(getCookie('tiendaaviso')!="1"){
                document.getElementById("barraaceptacion").style.display="block";
            }
            function PonerCookie(){
                setCookie('tiendaaviso','1',365);
                document.getElementById("barraaceptacion").style.display="none";
            }
            </script>
        <!--//FIN BLOQUE COOKIES-->
   
        <!-- jQuery -->
    <script src="assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Vertical tabs -->
    <script src="assets/js/tabs.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/popup.js"></script>
    