<?php include_once 'common.php';?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    
    <title><?php echo $lang['PAGE_TITLE']; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="assets/css/modern-business.css" rel="stylesheet">
    <link href="assets/css/cookies.css" rel="stylesheet" type="text/css">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="assets/js/jquery.js"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <link rel="stylesheet" type="text/css" href="assets/css/YouTubePopUp.css">
	<script type="text/javascript" src="assets/js/YouTubePopUp.jquery.js"></script>
	<script type="text/javascript">
		jQuery(function(){
			jQuery("a.bla-1").YouTubePopUp();
		});
	</script>
	<?php include("assets/includes/analytics.php"); ?>
</head>

<body>


    <?php include("assets/includes/top.php"); ?>
    
    <?php include("assets/includes/menu.php"); ?>
    
    
     
    
<div class="back_color">
<!-- Image Header -->
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="video">
                   <p><?php echo $lang['VIDEO_TXT']; ?></p>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
        </div>
    <!-- /.back color -->
    
    <!-- Page Content -->
    <div class="container" style="padding-left:0px; padding-right:0px;">
    <!-- Banner Carousel -->
       <div class="col-md-12">
                <div class="panel_middle">
                      <?php echo $lang['TEXT_MIDDLE']; ?>
                    <p>&nbsp;</p>
                </div>
            </div>

    </div>
    <!-- /.container -->

<div class="back_color">
    <!-- Page Content -->
    <div class="container">

        <!-- Marketing Icons Section -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel_bottom">
                    <?php echo $lang['TEXT_FORM']; ?>
                    <div class="boxleft">
                    <?php echo $lang['GLOBO_TEXT']; ?></div>
                    <div class="boxleft_img"><?php echo $lang['BUBBLE_FORM1']; ?></div>
                
                <div class="formright">
                <form name="sentMessage" id="canjeacodigo" action="" method="POST">

                     <div class="control-group form-group">
                        <div class="controls">                       
                            <label for=""><?php echo $lang['FORM_5_1']; ?></label>
                            <select class="form-control form-select required" id="subcampaign" name="subcampaign" placeholder="<?php echo $lang['TEXT_DATA1']; ?>">
                                <option value="" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                                <option value="1"><?php echo $lang['FORM_5_2']; ?></option>
                                <option value="2"><?php echo $lang['FORM_5_3']; ?></option>
                                <option value="3"><?php echo $lang['FORM_5_4']; ?></option>
                            </select>
                            <p class="help-block"></p>
                        </div>
                    </div>

                    <div class="control-group form-group">
                        <div class="controls">
                            <label><?php echo $lang['LABEL_CP']; ?></label>
                            <input type="text" id="cp" name="cp"  class="form-control" required placeholder="<?php echo $lang['FORM_1_2']; ?>" type="text" pattern="((0[1-9]|5[0-2])|[1-4][0-9])[0-9]{3}" title="<?php echo $lang['FORM_1_1']; ?>" required data-validation-required-message="<?php echo $lang['FORM_1']; ?>"/>
                            
                            <p class="help-block"></p>
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label><?php echo $lang['LABEL_REGALO']; ?></label>                         
                            <input id="insertacodigo" name="insertacodigo" class="form-control" required placeholder="<?php echo $lang['FORM_2_1']; ?>" required data-validation-required-message="<?php echo $lang['FORM_2']; ?>" type="text" />
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label><?php echo $lang['LABEL_NUM']; ?></label>
                            <input id="poliza" name="poliza" type="text" class="form-control" required placeholder="<?php echo $lang['FORM_3_1']; ?>" required data-validation-required-message="<?php echo $lang['FORM_3']; ?>" pattern="^[0-9]{1,45}$">
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                                                    
                           <p> <!--<input type="checkbox" id="legal" required data-validation-required-message="<?//php echo $lang['FORM_4']; ?>">-->
                           <!-- <?//php echo $lang['LABEL_LEGAL']; ?> <a href="#" data-toggle="modal" data-target="#bases-aceptar-modal"><?//php echo $lang['TEXT_BASES']; ?></a>--></p>
                            <p><!--<span class="orange"><?//php echo $lang['LABEL_CAMPOS']; ?></span>--></p>
                        </div>
                    </div>
                    <div id="success"></div>
                    <!-- For success/fail messages -->
                    
                    
                   <button type="submit" class="btn_new" id="botoncomprueba"><span class="icon1"><i class="fa fa-check fa-2x "></i></span><span class="text"><?php echo $lang['BOTON_TEXT']; ?></span></button>
                                                 
                            <div id="respuesta1">
                                <span id="strCode"></span>
                            </div>
                </form> 

                
                </div><!-- fi form-->
                <p class="recuerde_full"><?php echo $lang['RECUERDE']; ?></p>
                </div> <!-- /.panel bottom -->
            </div><!-- /.col 12 -->

        </div><!-- /.row -->
    </div><!-- /.container -->

    </div><!-- /.back color -->
<script>  
  
    $(function(){
        //Tigger Bind form submit from legal bases accept button.
//        $( "#LegalAccept" ).click(function() {
        $( "#botoncomprueba" ).click(function() {
            debugger;
            var url = "assets/includes/forms/canjeacodigo_prueba.php"; // El script a dónde se realizará la petición.
            $.ajax({
                type: "POST",
                url: url,
                data: $("#canjeacodigo").serialize(), // Adjuntar los campos del formulario enviado.
                success: function(data)
                {
                    $("#respuesta1").html(data);
                }
            });
            return false; // Evitar ejecutar el submit del formulario.
        });
    });

  </script>
  
                     <!--//BLOQUE COOKIES-->
            <div id="barraaceptacion">
                <div class="block_cookie">
                    Utilizamos cookies de terceros para mejorar su accesibilidad, personalizar y analizar sus hábitos de navegación. Si continúa navegando, consideramos que acepta su uso. Puede cambiar la configuración u obtener más información en Política de Cookies.
                    <a href="javascript:void(0);" onclick="PonerCookie();"><b>OK</b></a> | 
                    <a href="#" data-toggle="modal" data-target="#politica">M&aacute;s informaci&oacute;n</a>
                </div>
            </div>
    <?php include("assets/includes/popups/cookies.php"); ?>  
    <?php include("assets/includes/popups/video.php"); ?>  
    <?php include("assets/includes/popups/video_cat.php"); ?>
    <?php include("assets/includes/popups/bases_aceptar.php"); ?>
    <?php include("assets/includes/popups/bases_aceptar_hotel.php"); ?>
    <?php include("assets/includes/popups/bases_aceptar_futbol.php"); ?>
    <?php include("assets/includes/popups/aviso_futbol.php"); ?>
    <?php include("assets/includes/footer.php"); ?>
    
    </body>
   </html> 
   
           <script>
            function getCookie(c_name){
                var c_value = document.cookie;
                var c_start = c_value.indexOf(" " + c_name + "=");
                if (c_start == -1){
                    c_start = c_value.indexOf(c_name + "=");
                }
                if (c_start == -1){
                    c_value = null;
                }else{
                    c_start = c_value.indexOf("=", c_start) + 1;
                    var c_end = c_value.indexOf(";", c_start);
                    if (c_end == -1){
                        c_end = c_value.length;
                    }
                    c_value = unescape(c_value.substring(c_start,c_end));
                }
                return c_value;
            }
            
            function setCookie(c_name,value,exdays){
                var exdate=new Date();
                exdate.setDate(exdate.getDate() + exdays);
                var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
                document.cookie=c_name + "=" + c_value;
            }
            
            if(getCookie('tiendaaviso')!="1"){
                document.getElementById("barraaceptacion").style.display="block";
            }
            function PonerCookie(){
                setCookie('tiendaaviso','1',365);
                document.getElementById("barraaceptacion").style.display="none";
            }
            </script>
        <!--//FIN BLOQUE COOKIES-->

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Vertical tabs -->
    <script src="assets/js/tabs.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/popup.js"></script>
    
    

    
    
    


