<?php
include_once 'common.php';
include "./assets/connect/conexion.php";
include "./assets/php/categoryServices.php";

$table_prefix = "adeslas2hogar2016";

$CategoryServiceArray = getCategoryWithActiveServices($table_prefix, $link, "10,12");
?>

<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    
    <title><?php echo $lang['PAGE_TITLE']; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="assets/css/modern-business.css" rel="stylesheet">
    <link href="assets/css/cookies.css" rel="stylesheet" type="text/css">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!--<script>var $j = jQuery.noConflict();</script>-->
    
    <!-- Carrousel js & css -->
    <!--<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>-->
    
    <!-- jQuery -->
    <script src="assets/js/jquery.js"></script>
    <!--<script type="text/javascript" src="assets/js/jquery.flexisel.js"></script>-->
    
    <!-- Script Show Hide Categories & Experiences -->
    <script src="assets/js/showcat.js"></script>
    
     <script type="text/javascript">
   	 $(function(){
        $("#buscaexp").on("submit", function(e){
            e.preventDefault();
            var f = $(this);
            var formData = new FormData(document.getElementById("buscaexp"));
            formData.append("dato", "valor");
            //formData.append(f.attr("name"), $(this)[0].files[0]);
            $.ajax({
                url: "./assets/includes/forms/categorias-busca-experiencias.php",
                type: "post",
                dataType: "html",
                data: formData,
                cache: false,
                contentType: false,
	     processData: false
            })
                .done(function(res){
                    $("#respuestaexp").html(res);
                });
        });
    });
    </script>
    
<script type="text/javascript">
	$(document).ready(function(){
    $("#categorias").change(function () {
           $("#categorias option:selected").each(function () {
            elegido=$(this).val();
            $.post("./assets/includes/forms/categorias-select-experiencias.php", { elegido: elegido }, function(data){
            $("#experiencias").html(data);
            });            
        });
   })
});
</script>
    
  <?php include("assets/includes/analytics.php"); ?>         
        
    
    <style>
	 .carousel-control .icon-prev{left:50%;margin-left:-120px}
	 .carousel-control .icon-next{right:50%;margin-right:-120px}
	 
	</style>

</head>

<body>

    <?php include("assets/includes/top.php"); ?>
    
    <?php include("assets/includes/menu.php"); ?>
    
 
    
    <div class="back_color">
        <!-- Page Content -->


<div class="container">
            <!-- Banner Carousel -->
            <header id="myCarousel" class="carousel slide">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                    <li data-target="#myCarousel" data-slide-to="4"></li>
                </ol>
        
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="fill" style="background-image:url('images/catas.jpg');"></div>
                        <div class="carousel-caption">
                           <h3><?php echo $lang['TEXT_CATAS1']; ?></h3>
                           <h2><?php echo $lang['TEXT_CATAS']; ?></h2>
                            
                        </div>
                    </div>
                    <div class="item">
                        <div class="fill" style="background-image:url('images/curos.jpg');"></div>
                        <div class="carousel-caption">
                           <h3><?php echo $lang['TEXT_CURSOS1']; ?></h3>
                           <h2><?php echo $lang['TEXT_CURSOS']; ?></h2>
                            
                        </div>
                    </div>
                    <div class="item">
                        <div class="fill" style="background-image:url('images/espectaculos.jpg');"></div>
                        <div class="carousel-caption">
                           <h3><?php echo $lang['TEXT_ESPEC1']; ?></h3>
                           <h2><?php echo $lang['TEXT_ESPEC']; ?></h2>
                            
                        </div>
                    </div>
                    <div class="item">
                        <div class="fill" style="background-image:url('images/salud.jpg');"></div>
                        <div class="carousel-caption">
                           <h3><?php echo $lang['TEXT_SALUD1']; ?></h3>
                           <h2><?php echo $lang['TEXT_SALUD']; ?></h2>
                            
                        </div>
                    </div>
                    <div class="item">
                        <div class="fill" style="background-image:url('images/aventura.jpg');"></div>
                        <div class="carousel-caption">
                           <h3><?php echo $lang['TEXT_AVENTURA1']; ?></h3>
                           <h2><?php echo $lang['TEXT_AVENTURA']; ?></h2>
                            
                        </div>
                    </div>
                    
                    
                </div>
        
                <!-- Controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="icon-prev"><i class="fa fa-angle-left fa-2x "></i>
        </span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="icon-next"><i class="fa fa-angle-right fa-2x "></i>
        </span>
                </a>
            </header>
            </div>



    
            </div>
    <!-- /.back color -->
    
    <!-- Page Content -->
    <div class="container">



<div class="panel_middle">
        <div class="row">
            <div class="col-lg-12">
            
           <?php echo $lang['TEXT_EXPE1']; ?>
                    <div class="box"><?php echo $lang['TEXT_EXPE2']; ?></div>
                      <div class="form">
                        <form id="buscaexp" name="buscaexp" method="post" novalidate>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <label><?php echo $lang['TEXT_EXPE3']; ?></label>                                 
                            <select class="form-control form-select required  categorySelect" id="categorias"  name="categorias" for="experiencia">
                                <!--<option value='--All--'>Todas las categorías</option>-->
                                    <option value="0" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                                    <option value="1"><?php echo $lang['TEXT_BELLEZA_2'];?></option>
                                    <option value="2"><?php echo $lang['TEXT_CURSOS_2'];?></option>
                                    <option value="3"><?php echo $lang['TEXT_DEPORTES_2'];?></option>
                                    <option value="4"><?php echo $lang['TEXT_RESTAURACION_2'];?></option>
                                    <option value="5"><?php echo $lang['TEXT_OCIO_2'];?></option>                                 
                                    
                            <select>
                            
                            
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="control-group form-group">
                                <div class="controls">
                                    <label><?php echo $lang['TEXT_EXPE4']; ?></label> 
                            <select class="form-control form-select required" id="experiencias" name="experiencias" >
                                    <option value="0" selected="selected"><?php echo $lang['TEXT_SELEC']; ?></option>
                            <select>
                            
                                </div>
                            </div>
                            <!-- For success/fail messages -->
                           <button type="submit" class="btn_new_short" id="getexper"><img src="images/enviar.png" width="18" height="20"><span class="text"><?php echo $lang['TEXT_EXPE5']; ?></span></button>            
                        </form>
                        
                        </div><!-- fi form-->
                     </div> <!-- /.fi col -->
    
            </div> <!-- /.fi row -->
        </div> <!-- /.fi panell -->




    </div>
    <!-- /.container -->
<div id="respuestaexp"></div>
    
                           <!--//BLOQUE COOKIES-->
            <div id="barraaceptacion">
                <div class="block_cookie">
                    Utilizamos cookies de terceros para mejorar tu accesibilidad, personalizar y analizar tus hábitos de navegación. Si continuas navegando, consideramos que aceptas su uso. Puedes cambiar la configuración u obtener más información en Política de Cookies.
                    <a href="javascript:void(0);" onclick="PonerCookie();"><b>OK</b></a> | 
                    <a href="#" data-toggle="modal" data-target="#politica">M&aacute;s informaci&oacute;n</a>
                </div>
            </div>
    <?php include("assets/includes/popups/cookies.php"); ?>    
    <?php include("assets/includes/footer.php"); ?>
    

    
    
        </body>
   </html> 
   
           <script>
            function getCookie(c_name){
                var c_value = document.cookie;
                var c_start = c_value.indexOf(" " + c_name + "=");
                if (c_start == -1){
                    c_start = c_value.indexOf(c_name + "=");
                }
                if (c_start == -1){
                    c_value = null;
                }else{
                    c_start = c_value.indexOf("=", c_start) + 1;
                    var c_end = c_value.indexOf(";", c_start);
                    if (c_end == -1){
                        c_end = c_value.length;
                    }
                    c_value = unescape(c_value.substring(c_start,c_end));
                }
                return c_value;
            }
            
            function setCookie(c_name,value,exdays){
                var exdate=new Date();
                exdate.setDate(exdate.getDate() + exdays);
                var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
                document.cookie=c_name + "=" + c_value;
            }
            
            if(getCookie('tiendaaviso')!="1"){
                document.getElementById("barraaceptacion").style.display="block";
            }
            function PonerCookie(){
                setCookie('tiendaaviso','1',365);
                document.getElementById("barraaceptacion").style.display="none";
            }
            </script>
        <!--//FIN BLOQUE COOKIES-->   

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Vertical tabs -->
    <script src="assets/js/tabs.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/popup.js"></script>
    
        <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>
