<?php
session_start();
include_once 'common.php';
$_SESSION["adeslas2hogar2016_paso2"] = "3";
$table_prefix = "adeslas2hogar2016";

$post_nif=$_GET["x"];
$post_npet=$_GET["y"];
$post_contcodigo=$_GET["z"];

$c_aco_partner = 'false';

//Conexion
include "./assets/connect/conexion.php";

// Check connection
if ($link->connect_error) {
    die("Connection failed: " . $link->connect_error);
}

$sqlcompruebaexiste = "SELECT * FROM `".$table_prefix."__peticion` WHERE `N_PET` = '$post_npet' AND NIF ='$post_nif' AND CODIGO = '$post_contcodigo'";


if (!$link->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $link->error);
}

$result = $link->query($sqlcompruebaexiste);

if ($result->num_rows == 0) {
   header("Location: http://disfrutaunaexperienciaunica.com");
}else{
    $_SESSION["adeslas2hogar2016_codigo"]=$post_contcodigo;
    $_SESSION["adeslas2hogar2016_id_peticion"]=$post_npet;
}

$sqlcompruebacorreo = "SELECT * FROM `".$table_prefix."__peticion` LEFT JOIN `crm_municipios` ON  crm_municipios.crm_id_municipio =  ".$table_prefix."__peticion.POBLACION LEFT JOIN `crm_provincias` ON  crm_provincias.crm_id_provincia =  ".$table_prefix."__peticion.PROVINCIA WHERE `N_PET` = '$post_npet'"; 


if (!$link->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $link->error);
}

$rscompruebacorreo = $link->query($sqlcompruebacorreo);

if($lang_sql=="cat"){
$n_fechasindia = "traducefechasindiacat";
$n_fechacondia = "traducefechacat";
}else{
$n_fechasindia = "traducefechasindiaes";
$n_fechacondia = "traducefechaes";
}

while ($row = $rscompruebacorreo->fetch_assoc()) {
    $c_contacto_idcontador=$row["N_PET"];
    $c_contacto_nombre_d=$row["NOMBRE"];
    $c_contacto_nombre = $c_contacto_nombre_d;
    $c_contacto_apellidos_d=$row["APELLIDOS"];
    $c_contacto_apellidos = $c_contacto_apellidos_d;
    $c_contacto_fechanac2 =$row["fechanacimiento"];
    $c_contacto_nif_d=$row["NIF"];
    $c_contacto_nif = $c_contacto_nif_d;
    $c_contacto_direccion_d=stripslashes($row["DIRECCION"]);
    $c_contacto_direccion = $c_contacto_direccion_d;
    $c_contacto_poblacion_d =$row["crm_municipio"];
    $c_contacto_poblacion = $c_contacto_poblacion_d;
    $c_contacto_cp =$row["CP"];
    $c_contacto_provincia_d =$row["crm_provincia"];
    $c_contacto_provincia = $c_contacto_provincia_d;
    $c_contacto_mail_d =$row["MAIL"];
    $c_contacto_mail = $c_contacto_mail_d;
    $c_contacto_telf =$row["TELEFONO"];
    $c_contacto_fechareg =$row["F_REGISTRO"];
    $c_contacto_codigo =$row["CODIGO"];
    $c_contacto_dest1x =$row["DEST1"];
    $c_contacto_dest2x =$row["DEST2"];
    $c_contacto_dest3x =$row["DEST3"];
    $c_contacto_dest1 = $c_contacto_dest1x;
    $c_contacto_dest2 = $c_contacto_dest2x;
    $c_contacto_dest3 = $c_contacto_dest3x;
    $c_contacto_ida11 =$row["F_IDA_1"];
    $c_contacto_ida21 =$row["F_IDA_2"];
    $c_contacto_ida31 =$row["F_IDA_3"];
    $c_contacto_vuelta11 =$row["F_VUELTA_1"];
    $c_contacto_vuelta21 =$row["F_VUELTA_2"];
    $c_contacto_vuelta31 =$row["F_VUELTA_3"];
	$tipocampana = $row["TIPOCAMPANA"];
}

if($tipocampana==1){
	$simbolo_dest = "_"; 
	$posiciondest1 = strpos ($c_contacto_dest1, $simbolo_dest); 
	$c_contacto_dest_prov1 = explode('_', $c_contacto_dest1);
	$c_contacto_dest_prov1=$c_contacto_dest_prov1[0];
	$c_contacto_dest1 = substr ($c_contacto_dest1, ($posiciondest1+1)); 
	$sqlprov1= "SELECT * FROM crm_provincias WHERE crm_id_provincia = '$c_contacto_dest_prov1'";
	$rsprov1 = mysqli_query($link, $sqlprov1);
    while ($row=mysqli_fetch_array($rsprov1)) {
                $s_nombre_prov1=$row["crm_provincia"];
	}
	
	$posiciondest2 = strpos ($c_contacto_dest2, $simbolo_dest); 
	$c_contacto_dest_prov2 = explode('_', $c_contacto_dest2);
	$c_contacto_dest_prov2=$c_contacto_dest_prov2[0];
	$c_contacto_dest2 = substr ($c_contacto_dest2, ($posiciondest2+1)); 
	$sqlprov2= "SELECT * FROM crm_provincias WHERE crm_id_provincia = '$c_contacto_dest_prov2'";
	$rsprov2 = mysqli_query($link, $sqlprov2);
    while ($row=mysqli_fetch_array($rsprov2)) {
                $s_nombre_prov2=$row["crm_provincia"];
	}
	
	$posiciondest3 = strpos ($c_contacto_dest3, $simbolo_dest); 
	$c_contacto_dest_prov3 = explode('_', $c_contacto_dest3);
	$c_contacto_dest_prov3=$c_contacto_dest_prov3[0];
	$c_contacto_dest3 = substr ($c_contacto_dest3, ($posiciondest3+1));
	$sqlprov3= "SELECT * FROM crm_provincias WHERE crm_id_provincia = '$c_contacto_dest_prov3'";
	$rsprov3 = mysqli_query($link, $sqlprov3);
    while ($row=mysqli_fetch_array($rsprov3)) {
                $s_nombre_prov3=$row["crm_provincia"];
	}
	
}
require "./assets/php/conviertefecha.php";
$c_contacto_fechanac =$n_fechacondia($c_contacto_fechanac2);

setlocale(LC_TIME, 'es_ES', 'Spanish_Spain', 'Spanish');
$date = new DateTime($c_contacto_ida11);
$c_contacto_ida11 = strftime("%d/%m/%Y", $date->getTimestamp());

setlocale(LC_TIME, 'es_ES', 'Spanish_Spain', 'Spanish');
$date = new DateTime($c_contacto_ida21);
$c_contacto_ida21 = strftime("%d/%m/%Y", $date->getTimestamp());

setlocale(LC_TIME, 'es_ES', 'Spanish_Spain', 'Spanish');
$date = new DateTime($c_contacto_ida31);
$c_contacto_ida31 = strftime("%d/%m/%Y", $date->getTimestamp());

/* change character set to utf8 */
if (!$link->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $link->error);
}

//Get event name from *__events table
if($lang_sql=="cat"){
	$lengbusc="_cat";
}else{
	$lengbusc="";
}
$arrayCarreras = array();
$sqlCarreras = 'SELECT `nombre_servicio'.$lengbusc.'` FROM `servicios` where `id_servicio` = "'.$c_contacto_dest1.'"';
$rsCarreras = $link->query($sqlCarreras);
while ($row = $rsCarreras->fetch_assoc()) {
    $arrayCarreras[0]=$row["nombre_servicio".$lengbusc.""];
}

$sqlCarreras = 'SELECT `nombre_servicio'.$lengbusc.'` FROM `servicios` where `id_servicio` = "'.$c_contacto_dest2.'"';
$rsCarreras = $link->query($sqlCarreras);
while ($row = $rsCarreras->fetch_assoc()) {
    $arrayCarreras[1]=$row["nombre_servicio".$lengbusc.""];
}

$sqlCarreras = 'SELECT `nombre_servicio'.$lengbusc.'` FROM `servicios` where `id_servicio` = "'.$c_contacto_dest3.'"';
$rsCarreras = $link->query($sqlCarreras);
while ($row = $rsCarreras->fetch_assoc()) {
    $arrayCarreras[2]=$row["nombre_servicio".$lengbusc.""];
}



//Partner data
        $sqlcheckpartner = "SELECT * FROM `".$table_prefix."__peticion_acompanante` LEFT JOIN `crm_municipios` ON  crm_municipios.crm_id_municipio =  ".$table_prefix."__peticion_acompanante.municipio_pet_acompanante LEFT JOIN `crm_provincias` ON  crm_provincias.crm_id_provincia =  ".$table_prefix."__peticion_acompanante.provincia_pet_acompanante WHERE `rel_pet_acompanante` = '".$post_npet."'";
    //    $sqlcheckpartner .= " AND `desactivado` != '1'";

        /* change character set to utf8 */
        if (!$link->set_charset("utf8")) {
            printf("Error loading character set utf8: %s\n", $link->error);
        }
	
	$partnerrs= mysqli_query($link,$sqlcheckpartner);
	if (mysqli_num_rows($partnerrs) == 1) {
        $params["get_ready"]=true;
        
            //Populate array of values
            while ($row=mysqli_fetch_array($partnerrs)) {
                    $c_aco_nombre_d=$row["nombre_pet_acompanante"];
                    $c_aco_nombre = $c_aco_nombre_d;

                    $c_aco_apellidos_d=$row["apellidos_pet_acompanante"];
                    $c_aco_apellidos = $c_aco_apellidos_d;

                    $c_aco_sexo = $row["sexo"];

                    $c_aco_fechanac2 =$row["fechanac_pet_acompanante"];
                    $c_aco_nif_d=$row["dni_pet_acompanante"];
                    $c_aco_nif = $c_aco_nif_d;
                    $c_aco_mail_d =$row["email_pet_acompanante"];
                    $c_aco_mail = $c_aco_mail_d;
                    $c_aco_telf =$row["telf_pet_acompanante"];
                    $c_aco_provincia_d =$row["crm_provincia"];
                    $c_aco_provincia = $c_aco_provincia_d;

                    $c_aco_poblacion_d =$row["crm_municipio"];
                    $c_aco_poblacion = $c_aco_poblacion_d;
                    $c_aco_cp =$row["cp_pet_acompanante"];

                    $c_aco_direccion_d=stripslashes($row["direccion_pet_acompanante"]);
                    $c_aco_direccion = $c_aco_direccion_d;
                    
                    $c_aco_menor = $row["menor_pet_acompanante"];
                    $c_aco_partner = 'true';
            }
			if(($c_aco_menor==2)or($c_aco_menor==1)){
				$stylebuttonupdateaut="file";
				$stylelabelupdateaut="";
			}else{
				$stylebuttonupdateaut="hidden";
	  			$stylelabelupdateaut="style='display:none;";
			}
			if(($c_aco_menor==0)or($c_aco_menor==1)){
				$stylebuttonupdatedni="file";
				$stylelabelupdatedni="";
			}else{
				$stylebuttonupdatedni="hidden"; 
	 			$stylelabelupdatedni="style='display:none;'"; 
			}
			
			
            $_SESSION["adeslas2hogar2016_partner"]=$formValuesPartner["partner"];
            
            //set partner birth date string format
            $c_aco_fechanac =$n_fechacondia($c_aco_fechanac2);
            
        }else{
      $stylebuttonupdatedni="hidden"; 
	  $stylelabelupdatedni="style='display:none;'"; 
	  $stylebuttonupdateaut="hidden";
	  $stylelabelupdateaut="style='display:none;";    
	}
 $sesion_idpet=$_SESSION["adeslas2hogar2016_id_peticion"];    
?>
<!DOCTYPE HTML>
<!--
	Retrospect by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html lang="en" class="no-js">
    <head>
        <title><?php echo $lang['PAGE_TITLE']; ?></title>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />



        <!-- Bootstrap Core CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="assets/css/modern-business.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        
        

    
    </head>
    <body>

    <?php include("assets/includes/top_hidden.php"); ?>
    
    <?php include("assets/includes/analytics.php"); ?>

<div class="back_color">
    <!-- Page Content -->
    <div class="container">

        <!-- Marketing Icons Section -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel_bottom">



        <div class="top"></div>
        <section id="contacto" class="wrapper style2 special">
            <div class="inner">
                <div class="form">
                    <div class="box_resumen">
                        <div id="myPrintArea">
                           <div class="container 75%">
                               <div class="row uniform 100%">
                               
                                  <div class="col3_box">
                                  <?php echo $lang['TEXT_TUEXPE1']; ?>
                                   </div><!-- fi col3_box -->
                                    
                            		<?php if($tipocampana==2){ 
									$sqlcompruebapartidos = "SELECT * FROM ".$table_prefix."__partidos WHERE id_partido = '$c_contacto_dest1x'";
									$rscompruebapartidos= mysqli_query($link,$sqlcompruebapartidos);
									$num_total_PARTIDOS = mysqli_num_rows($rscompruebapartidos);
									while ($row=mysqli_fetch_array($rscompruebapartidos)) { 
									$p_nombre_equipocasa2=$row["nombre_equipo"];
									$p_nombre_equipocasa=$p_nombre_equipocasa2;
									$p_id_partido=$row["id_partido"];
									$p_jornada_partido=$row["jornada_partido"];
									$p_fecha_partido2=$row["fecha_partido"];
									$p_fecha_partido=traducefecha($p_fecha_partido2); 
									$p_equipo_local=$row["equipo_a"];
									$p_equipo_visitante=$row["equipo_b"];
									}
									$sqlequipolocal="SELECT * FROM ".$table_prefix."__equipos WHERE id_equipo='$p_equipo_local'";
									$rsequipolocal= mysqli_query($link,$sqlequipolocal);
									while ($row=mysqli_fetch_array($rsequipolocal)) { 
									$s_nombre_local2=$row["nombre_equipo"];
									$s_nombre_local=$s_nombre_local2;
									$s_camiseta_local=$row["camiseta_equipo"];
									$s_estadio_local2=$row["estadio_equipo"];
									$s_estadio_local=$s_estadio_local2;
									}
									$sqlequipovisitante="SELECT * FROM ".$table_prefix."__equipos WHERE id_equipo='$p_equipo_visitante'";
									$rsequipovisitante= mysqli_query($link,$sqlequipovisitante);
									while ($row=mysqli_fetch_array($rsequipovisitante)) { 
									$s_nombre_visitante2=$row["nombre_equipo"];
									$s_nombre_visitante=$s_nombre_visitante2;
									$s_camiseta_visitante=$row["camiseta_equipo"];
									}
									?>
                                    <div class="col3_box">
                                    <div class="text_main"><h2><?php echo $lang['TEXT_TUPARTIDO']; ?></h2></div>
                                    <div class="col3">
                                        <p>
                                        <?php echo"<b>$s_nombre_local</b> vs <b>$s_nombre_visitante</b><br>
										Jornada <b>$p_jornada_partido</b>, <b>$s_estadio_local</b>";?></p>
                                    </div>
                                    <div class="col3">
                                        
                                    </div>
                                    <div class="col3">
                                        
                                    </div>
                                   </div><!-- fi col3_box -->
                                    
                                    <?php }else{ ?>
										
                                   <div class="col3_box">
                                    <div class="text_main"><h2><?php echo $lang['TEXT_TUEXPE2']; ?></h2></div>
                                    <div class="col3">
                                        <p><strong><?php echo $lang['TEXT_TUEXPE3']; ?></strong></br>
                                           <?php echo $arrayCarreras[0]; 
										   echo"<br>$s_nombre_prov1";?></p>
                                        <p><strong><?php echo $lang['TEXT_TUEXPE4']; ?></strong></br>
                                           <?php echo $c_contacto_ida11; ?></p>
                                    </div>
                                    <div class="col3">
                                        <p><strong><?php echo $lang['TEXT_TUEXPE5']; ?></strong></br>
                                           <?php echo $arrayCarreras[1]; 
										   echo"<br>$s_nombre_prov2";?></p>
                                        <p><strong><?php echo $lang['TEXT_TUEXPE6']; ?></strong></br>
                                           <?php echo $c_contacto_ida21; ?></p>
                                    </div>
                                    <div class="col3">
                                        <p><strong><?php echo $lang['TEXT_TUEXPE7']; ?></strong></br>
                                           <?php echo $arrayCarreras[2]; 
										   echo"<br>$s_nombre_prov3";?></p>
                                        <p><strong><?php echo $lang['TEXT_TUEXPE8']; ?></br></strong>
                                           <?php echo $c_contacto_ida31; ?></p>
                                    </div>
                                   </div><!-- fi col3_box -->
                                   <?php } ?>
                                    
                                    
                                    
                                   <div class="col3_box">
                                    
                                    <div class="text_main"><h2><?php echo $lang['TEXT_TUEXPE9']; ?></h2>
                                    </div>
                                    <div class="col3">
                                        <p><strong><?php echo $lang['TEXT_TUEXPE10']; ?></strong></br>
                                            <?php echo $c_contacto_nombre; ?></p>
                                        <p><strong><?php echo $lang['TEXT_TUEXPE11']; ?></strong></br>
                                            <?php echo $c_contacto_apellidos; ?></p> 
                                        <p><strong><?php echo $lang['TEXT_TUEXPE12']; ?></br></strong>
                                            <?php echo $c_contacto_fechanac; ?></p>
                                        <p><strong><?php echo $lang['TEXT_TUEXPE13']; ?></strong></br>
                                            <?php echo $c_contacto_nif; ?></p>
                                    </div>
                                    <div class="col3">
                                        <p><strong><?php echo $lang['TEXT_TUEXPE14']; ?></br></strong>
                                            <?php echo $c_contacto_mail; ?></p>
                                        <p><strong><?php echo $lang['TEXT_TUEXPE15']; ?></strong></br>
                                            <?php echo $c_contacto_telf; ?></p> 
                                        <p><strong><?php echo $lang['TEXT_TUEXPE16']; ?></strong></br>
                                            <?php echo $c_contacto_direccion; ?></p>
                                        <p><strong><?php echo $lang['TEXT_TUEXPE17']; ?></strong></br>
                                            <?php echo $c_contacto_poblacion; ?></p>
                                    </div>
                                    <div class="col3">
                                        <p><strong><?php echo $lang['TEXT_TUEXPE18']; ?></strong></br>
                                            <?php echo str_pad($c_contacto_cp, 5, '0', STR_PAD_LEFT); ?></p>


                                        <p><strong><?php echo $lang['TEXT_TUEXPE19']; ?></strong></br>
                                            <?php echo $c_contacto_provincia; ?></p>
                                    </div>
                                  </div><!-- fi col3_box -->
                                 
                                   
                                   <div class="col3_box">
                                    <span <?php echo ($c_aco_partner == 'true')? '': 'style="display: none;"'; ?> >
                                        <div class="text_main"><h2><?php echo $lang['TEXT_TUEXPE20']; ?></h2>
                                        </div>
                                        <div class="col3">
	                                        <p><strong><?php echo $lang['TEXT_TUEXPE10']; ?></strong></br>
	                                            <?php echo $c_aco_nombre; ?></p>
	                                        <p><strong><?php echo $lang['TEXT_TUEXPE11']; ?></strong></br>
	                                            <?php echo $c_aco_apellidos; ?></p> 
	                                        <p><strong><?php echo $lang['TEXT_TUEXPE12']; ?></strong></br>
	                                            <?php echo $c_aco_fechanac; ?></p>
	                                        <p><strong><?php echo $lang['TEXT_TUEXPE13']; ?></strong></br>
	                                            <?php echo $c_aco_nif; ?></p>
                                        </div>
                                        <div class="col3">
	                                        <p><strong><?php echo $lang['TEXT_TUEXPE14']; ?></strong></br>
	                                            <?php echo $c_aco_mail; ?></p>
	                                        <p><strong><?php echo $lang['TEXT_TUEXPE15']; ?></strong></br>
	                                            <?php echo $c_aco_telf; ?></p> 
	                                        <p><strong><?php echo $lang['TEXT_TUEXPE16']; ?></strong></br>
	                                            <?php echo $c_aco_direccion; ?></p>
	                                        <p><strong><?php echo $lang['TEXT_TUEXPE17']; ?></strong></br>
	                                            <?php echo $c_aco_poblacion; ?></p>
                                        </div>
                                        <div class="col3">
	                                        <p><strong><?php echo $lang['TEXT_TUEXPE18']; ?></strong></br>
	                                            <?php echo str_pad($c_aco_cp, 5, '0', STR_PAD_LEFT); ?></p>
	
	
	                                        <p><strong><?php echo $lang['TEXT_TUEXPE19']; ?></strong></br>
	                                            <?php echo $c_aco_provincia; ?></p>
                                        </div>
                                    </span>
                                    <a href="descargatuparticipacion.php?<?php echo time(); ?>" target="_blank"> <button type="button" id="b1" class="btn_new1"><span class="icon1"><i class="fa fa-file-pdf-o fa-2x"></i></span><span class="text1"><?php echo $lang['TEXT_TUEXPE29']; ?></span></button></a>
                                  </div><!-- fi col3_box -->

     
                                   <div class="col3_box">
                                   <div class="text_main"><?php echo $lang['TEXT_TUEXPE21']; ?>
                                    </div>
                                  </div>
                                  <div class="col3_box">
                                    
                                    <div class="col3_box_grey">
                                     <p><?php echo $lang['TEXT_TUEXPE22']; ?></p>

                                        <table class="table table-striped">
                                                <!-- 
                                                <?php 
                                                ///////cupon
                                                $sql_comprueba_archivo_cupon = "SELECT * FROM ".$table_prefix."__peticion_archivos
                                                WHERE rel_archivo_peticion = '$sesion_idpet' AND tipo_archivo_peticion = 'CUPON' ORDER BY id_archivo_peticion DESC";
                                                $rs_comprueba_archivo_cupon = mysqli_query($link, $sql_comprueba_archivo_cupon);
                                                $num_total_comprueba_archivo_cupon  = mysqli_num_rows($rs_comprueba_archivo_cupon);
                                                ?>
                                                <?php if($num_total_comprueba_archivo_cupon == 0){?>
                                                      <tr>
                                                            <td class="col-md-4">
                                                                <form class="fileUpload" enctype="multipart/form-data" id="cupon-upload" method="post">
                                                                    <input type="file" name="archivo" id="file-cupon" class="inputfile inputfile-99" data-multiple-caption="{count} files selected" multiple/>
                                                                    <label for="file-cupon" id="button1" class="btn_upload"> <span><i class="fa fa-upload" aria-hidden="true"></i>
 Adjuntar Cupón</span></label>
                                                                    <input type="hidden" name="archivotipo" value="CUPON"/>
                                                                    <input name="action" type="hidden" value="upload" />
                                                                    <input name="peti" type="hidden" value="<?php echo"$c_contacto_idcontador";?>" />
                                                                </form>  
                                                            </td>
                                                            <td class="col-md-2">
                                                                <button name="enviar" id="enviar-cupon" type="button" value="Upload File" class="btn btn-success" style="margin-top:0px;" />ENVIAR</button>
                                                            </td>
                                                            <td class="col-md-6">
                                                                <div id="msg-upload-cupon" class="col-md-12"></div>
                                                            </td>
                                                        
                                                     </tr>
                                                    
                                                <?php }else{ ?>
                                                    <tr>
                                                        <td>
                                                    <?php while ($row=mysqli_fetch_array($rs_comprueba_archivo_cupon)) { 
                                                            $archivo_cupon_id=$row["id_archivo_peticion"];
                                                            $archivo_cupon_rel=$row["rel_archivo_peticion"];
                                                            $archivo_cupon_nombre=$row["nombre_archivo_peticion"];
                                                            $archivo_cupon_estado=$row["estado_archivo_peticion"];
                                                            if($archivo_cupon_estado==0){
                                                                $archivo_cupon_estado_text="<span style='color:blue;'>Pendiente de revisión</span>";
                                                            }
                                                            if($archivo_cupon_estado==1){
                                                                $archivo_cupon_estado_text="<span style='color:red;'>Cupón erróneo</span>";
                                                            }
                                                            if($archivo_cupon_estado==2){
                                                                $archivo_cupon_estado_text="<span style='color:green;'>Cupón correcto</span>";
                                                            }
                                                            /* 0-revisar 1-erroneo 2-correcto*/
                                                ?>
                                                    
                                                            <b>Tarjeta:</b> <a target="_blank" href="archivos/<?php echo"$archivo_cupon_rel";?>/<?php echo"$archivo_cupon_nombre";?>">Ver Archivo</a><br>
                                                            <b>Estado:</b> <?php echo"$archivo_cupon_estado_text";?><br>
                                                       
                                                
                                                <?php   } ?> 
                                                    </td>
                                                    <td>
                                                        <?php if(($dias_desde_registro<16)&&(($archivo_cupon_estado==1)or($archivo_cupon_estado==0))&&(($c_contacto_estado==0)or($c_contacto_estado==1))){?>
                                                            
                                                                <table>
                                                                    <tr>
                                                                        <td class="col-md-4">
                                                                            <form class="fileUpload" enctype="multipart/form-data" id="cupon-upload" method="post">
                                                                                <input type="file" name="archivo" id="file-cupon" class="inputfile inputfile-99" data-multiple-caption="{count} files selected" multiple/>
                                                                                <label for="file-cupon" id="button1" class="btn_upload"> <span><i class="fa fa-upload" aria-hidden="true"></i> Adjuntar tarjeta</span></label>
                                                                                <input type="hidden" name="archivotipo" value="CUPON"/>
                                                                                <input name="action" type="hidden" value="upload" />
                                                                                <input name="peti" type="hidden" value="<?php echo"$c_contacto_idcontador";?>" />
                                                                            </form>
                                                                            
                                                                        </td>
                                                                        <td class="col-md-2">
                                                                            <button name="enviar" id="enviar-cupon" type="button" value="Upload File" class="btn btn-success" style="margin-top:0px;" />REENVIAR</button>
                                                                        </td>
                                                                        <td class="col-md-6">
                                                                            <div id="msg-upload-cupon" class="col-md-12"></div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            
                                                        <?php }?>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <?php }?>
                                                
                                                <?php 
                                            //////////ticket
                                                $sql_comprueba_archivo_TICKET = "SELECT * FROM ".$table_prefix."__peticion_archivos
                                                WHERE rel_archivo_peticion = '$sesion_idpet' AND tipo_archivo_peticion = 'TICKET' ORDER BY id_archivo_peticion DESC";
                                                $rs_comprueba_archivo_TICKET = mysqli_query($link, $sql_comprueba_archivo_TICKET);
                                                $num_total_comprueba_archivo_TICKET  = mysqli_num_rows($rs_comprueba_archivo_TICKET);
                                                ?>
                                                <?php if($num_total_comprueba_archivo_TICKET == 0){?>
                                                     <tr>
                                                        
                                                            <td class="col-md-4">
                                                                <form class="fileUpload" enctype="multipart/form-data" id="ticket-upload" method="post">
                                                                    <input type="file" name="archivo" id="file-ticket" class="inputfile inputfile-99" data-multiple-caption="{count} files selected" multiple/>
                                                                    <label for="file-ticket" id="button1" class="btn_upload">  <span><i class="fa fa-upload" aria-hidden="true"></i> Adjuntar Ticket</span></label>
                                                                    <input type="hidden" name="archivotipo" value="TICKET"/>
                                                                    <input name="action" type="hidden" value="upload" />
                                                                    <input name="peti" type="hidden" value="<?php echo"$c_contacto_idcontador";?>" />
                                                                </form>
                                                                
                                                            </td>
                                                            <td class="col-md-2">
                                                                <button name="enviar" id="enviar-ticket" type="button" value="Upload File" class="btn btn-success" style="margin-top:0px;" />ENVIAR</button>
                                                            </td>
                                                            <td class="col-md-6">
                                                                <div id="msg-upload-TICKET" class="col-md-12"></div>
                                                            </td>
                                                        
                                                    </tr>
                                                <?php }else{ ?>
                                                    <tr>
                                                        <td>
                                                    <?php while ($row=mysqli_fetch_array($rs_comprueba_archivo_TICKET)) { 
                                                            $archivo_TICKET_id=$row["id_archivo_peticion"];
                                                            $archivo_TICKET_rel=$row["rel_archivo_peticion"];
                                                            $archivo_TICKET_nombre=$row["nombre_archivo_peticion"];
                                                            $archivo_TICKET_estado=$row["estado_archivo_peticion"];
                                                            if($archivo_TICKET_estado==0){
                                                                $archivo_TICKET_estado_text="<span style='color:blue;'>Pendiente de revisión</span>";
                                                            }
                                                            if($archivo_TICKET_estado==1){
                                                                $archivo_TICKET_estado_text="<span style='color:red;'>Tique erróneo</span>";
                                                            }
                                                            if($archivo_TICKET_estado==2){
                                                                $archivo_TICKET_estado_text="<span style='color:green;'>Tique correcto</span>";
                                                            }
                                                            /* 0-revisar 1-erroneo 2-correcto*/
                                                ?>
                                                    
                                                            <b>Ticket:</b> <a target="_blank" href="archivos/<?php echo"$archivo_TICKET_rel";?>/<?php echo"$archivo_TICKET_nombre";?>">Ver Archivo</a><br>
                                                            <b>Estado:</b> <?php echo"$archivo_TICKET_estado_text";?><br>
                                                       
                                                
                                                <?php   } ?> 
                                                    </td>
                                                    <td>
                                                        <?php if(($dias_desde_registro<16)&&(($archivo_TICKET_estado==1)or($archivo_TICKET_estado==0))&&(($c_contacto_estado==0)or($c_contacto_estado==1))){?>
                                                            
                                                                <table>
                                                                    <tr>
                                                                        <td class="col-md-4">
                                                                            <form class="fileUpload" enctype="multipart/form-data" id="ticket-upload" method="post">
                                                                                <input type="file" name="archivo" id="file-ticket" class="inputfile inputfile-99" data-multiple-caption="{count} files selected" multiple/>
                                                                                <label for="file-ticket" id="button1" class="btn_upload">  <span><i class="fa fa-upload" aria-hidden="true"></i> Adjuntar Tique</span></label>
                                                                                <input type="hidden" name="archivotipo" value="TICKET"/>
                                                                                <input name="action" type="hidden" value="upload" />
                                                                                <input name="peti" type="hidden" value="<?php echo"$c_contacto_idcontador";?>" />
                                                                            </form>
                                                                        </td>
                                                                        <td class="col-md-2">
                                                                            <button name="enviar" id="enviar-ticket" type="button" value="Upload File" class="btn btn-success" style="margin-top:0px;" />REENVIAR</button>
                                                                        </td>
                                                                        <td class="col-md-6">
                                                                            <div id="msg-upload-TICKET" class="col-md-12"></div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            
                                                        <?php }?>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <?php }?>
                                                -->

                                                <?php 
                                                ///////formulario
                                                $sql_comprueba_archivo_formulario = "SELECT * FROM ".$table_prefix."__peticion_archivos
                                                WHERE rel_archivo_peticion = '$sesion_idpet' AND tipo_archivo_peticion = 'FORMULARIO' ORDER BY id_archivo_peticion DESC";
                                                $rs_comprueba_archivo_formulario = mysqli_query($link, $sql_comprueba_archivo_formulario);
                                                $num_total_comprueba_archivo_formulario  = mysqli_num_rows($rs_comprueba_archivo_formulario);
                                                ?>
                                                <?php if($num_total_comprueba_archivo_formulario == 0){?>
                                                      <tr>
                                                        
                                                            <td class="col-md-4">
                                                                <form class="fileUpload" enctype="multipart/form-data" id="formulario-upload" method="post">
                                                                    <input type="file" name="archivo" id="file-formulario" class="inputfile inputfile-99" data-multiple-caption="{count} files selected" multiple/>
                                                                    <label for="file-formulario" id="button1" class="btn_upload"> <span><i class="fa fa-upload" aria-hidden="true"></i> Adjuntar Formulario</span></label>
                                                                    <input type="hidden" name="archivotipo" value="FORMULARIO"/>
                                                                    <input name="action" type="hidden" value="upload" />
                                                                    <input name="peti" type="hidden" value="<?php echo"$c_contacto_idcontador";?>" />
                                                                </form>  
                                                            </td>
                                                            <td class="col-md-2">
                                                                <button name="enviar" id="enviar-formulario" type="button" value="Upload File" class="btn btn-success" style="margin-top:0px;" />ENVIAR</button>
                                                            </td>
                                                            <td class="col-md-6">
                                                                <div id="msg-upload-formulario" class="col-md-12"></div>
                                                            </td>
                                                        
                                                     </tr>
                                                    
                                                <?php }else{ ?>
                                                    <tr>
                                                        <td>
                                                    <?php while ($row=mysqli_fetch_array($rs_comprueba_archivo_formulario)) { 
                                                            $archivo_formulario_id=$row["id_archivo_peticion"];
                                                            $archivo_formulario_rel=$row["rel_archivo_peticion"];
                                                            $archivo_formulario_nombre=$row["nombre_archivo_peticion"];
                                                            $archivo_formulario_estado=$row["estado_archivo_peticion"];
                                                            if($archivo_formulario_estado==0){
                                                                $archivo_formulario_estado_text="<span style='color:blue;'>Pendiente de revisión</span>";
                                                            }
                                                            if($archivo_formulario_estado==1){
                                                                $archivo_formulario_estado_text="<span style='color:red;'>Formulario erróneo</span>";
                                                            }
                                                            if($archivo_formulario_estado==2){
                                                                $archivo_formulario_estado_text="<span style='color:green;'>Formulario correcto</span>";
                                                            }
                                                            /* 0-revisar 1-erroneo 2-correcto*/
                                                ?>
                                                    
                                                            <b>Formulario:</b> <a target="_blank" href="archivos/<?php echo"$archivo_formulario_rel";?>/<?php echo"$archivo_formulario_nombre";?>">Ver Archivo</a><br>
                                                            <b>Estado:</b> <?php echo"$archivo_formulario_estado_text";?><br>
                                                       
                                                
                                                <?php   } ?> 
                                                    </td>
                                                    <td>
                                                        <?php if(($dias_desde_registro<16)&&(($archivo_formulario_estado==1)or($archivo_formulario_estado==0))&&(($c_contacto_estado==0)or($c_contacto_estado==1))){?>
                                                            
                                                                <table>
                                                                    <tr>
                                                                        <td class="col-md-4">
                                                                            <form class="fileUpload" enctype="multipart/form-data" id="formulario-upload" method="post">
                                                                                <input type="file" name="archivo" id="file-formulario" class="inputfile inputfile-99" data-multiple-caption="{count} files selected" multiple/>
                                                                                <label for="file-formulario" id="button1" class="btn_upload"> <span><i class="fa fa-upload" aria-hidden="true"></i> Adjuntar Formulario</span></label>
                                                                                <input type="hidden" name="archivotipo" value="FORMULARIO"/>
                                                                                <input name="action" type="hidden" value="upload" />
                                                                                <input name="peti" type="hidden" value="<?php echo"$c_contacto_idcontador";?>" />
                                                                            </form>
                                                                            
                                                                        </td>
                                                                        <td class="col-md-2">
                                                                            <button name="enviar" id="enviar-formulario" type="button" value="Upload File" class="btn btn-success" style="margin-top:0px;" />REENVIAR</button>
                                                                        </td>
                                                                        <td class="col-md-6">
                                                                            <div id="msg-upload-formulario" class="col-md-12"></div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            
                                                        <?php }?>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <?php }?>
                                                
                                            
                                             <?php 
                                            //////////DNI
                                                $sql_comprueba_archivo_DNI = "SELECT * FROM ".$table_prefix."__peticion_archivos
                                                WHERE rel_archivo_peticion = '$sesion_idpet' AND tipo_archivo_peticion = 'DNI' ORDER BY id_archivo_peticion DESC";
                                                $rs_comprueba_archivo_DNI = mysqli_query($link, $sql_comprueba_archivo_DNI);
                                                $num_total_comprueba_archivo_DNI  = mysqli_num_rows($rs_comprueba_archivo_DNI);
                                                ?>
                                                <?php if(($num_total_comprueba_archivo_DNI == 0) && ($dias_desde_registro<16)){?>
                                                     <tr>
                                                        
                                                            <td class="col-md-4">
                                                                <form class="fileUpload" enctype="multipart/form-data" id="dni-upload" method="post">
                                                                    <input type="file" name="archivo" id="file-dni" class="inputfile inputfile-99" data-multiple-caption="{count} files selected" multiple/>
                                                                    <label for="file-dni" id="button1" class="btn_upload"> <span><i class="fa fa-upload" aria-hidden="true"></i> Adjuntar DNI</span></label>
                                                                    <input type="hidden" name="archivotipo" value="DNI"/>
                                                                    <input name="action" type="hidden" value="upload" />
                                                                    <input name="peti" type="hidden" value="<?php echo"$c_contacto_idcontador";?>" />
                                                                </form>
                                                                
                                                            </td>
                                                            <td class="col-md-2">
                                                                <button name="enviar" id="enviar-dni" type="button" value="Upload File" class="btn btn-success" style="margin-top:0px;" />ENVIAR</button>
                                                            </td>
                                                            <td class="col-md-6">
                                                                <div id="msg-upload-DNI" class="col-md-12"></div>
                                                            </td>
                                                        
                                                    </tr>
                                                <?php }else{ ?>
                                                    <tr>
                                                        <td>
                                                    <?php while ($row=mysqli_fetch_array($rs_comprueba_archivo_DNI)) { 
                                                            $archivo_DNI_id=$row["id_archivo_peticion"];
                                                            $archivo_DNI_rel=$row["rel_archivo_peticion"];
                                                            $archivo_DNI_nombre=$row["nombre_archivo_peticion"];
                                                            $archivo_DNI_estado=$row["estado_archivo_peticion"];
                                                            if($archivo_DNI_estado==0){
                                                                $archivo_DNI_estado_text="<span style='color:blue;'>Pendiente de revisión</span>";
                                                            }
                                                            if($archivo_DNI_estado==1){
                                                                $archivo_DNI_estado_text="<span style='color:red;'>DNI erróneo</span>";
                                                            }
                                                            if($archivo_DNI_estado==2){
                                                                $archivo_DNI_estado_text="<span style='color:green;'>DNI correcto</span>";
                                                            }
                                                            /* 0-revisar 1-erroneo 2-correcto*/
                                                ?>
                                                    
                                                            <b>DNI:</b> <a target="_blank" href="archivos/<?php echo"$archivo_DNI_rel";?>/<?php echo"$archivo_DNI_nombre";?>">Ver Archivo</a><br>
                                                            <b>Estado:</b> <?php echo"$archivo_DNI_estado_text";?><br>
                                                       
                                                
                                                <?php   } ?> 
                                                    </td>
                                                    <td>
                                                        <?php if(($dias_desde_registro<16)&&(($archivo_DNI_estado==1)or($archivo_DNI_estado==0))&&(($c_contacto_estado==0)or($c_contacto_estado==1))){?>
                                                            
                                                                <table>
                                                                    <tr>
                                                                        <td class="col-md-4">
                                                                            <form class="fileUpload" enctype="multipart/form-data" id="dni-upload" method="post">
                                                                                <input type="file" name="archivo" id="file-dni" class="inputfile inputfile-99" data-multiple-caption="{count} files selected" multiple/>
                                                                                <label for="file-dni" id="button1" class="btn_upload"> <span><i class="fa fa-upload" aria-hidden="true"></i> Adjuntar DNI</span></label>
                                                                                <input type="hidden" name="archivotipo" value="DNI"/>
                                                                                <input name="action" type="hidden" value="upload" />
                                                                                <input name="peti" type="hidden" value="<?php echo"$c_contacto_idcontador";?>" />
                                                                            </form>
                                                                        </td>
                                                                        <td class="col-md-2">
                                                                            <button name="enviar" id="enviar-dni" type="button" value="Upload File" class="btn btn-success" style="margin-top:0px;" />REENVIAR</button>
                                                                        </td>
                                                                        <td class="col-md-6">
                                                                            <div id="msg-upload-DNI" class="col-md-12"></div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            
                                                        <?php }?>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <?php }?>
                                                

                                            
                                            <?php if ($num_total_acompanante == 1) { ?>
                                                <?php if($c_aco_menor!=2){?>
                                                 <?php 
                                                //////////libro de familia
                                                    $sql_comprueba_archivo_DNIACO = "SELECT * FROM ".$table_prefix."__peticion_archivos
                                                    WHERE rel_archivo_peticion = '$sesion_idpet' AND tipo_archivo_peticion = 'DNI ACOMPANANTE' ORDER BY id_archivo_peticion DESC";
                                                    $rs_comprueba_archivo_DNIACO = mysqli_query($link, $sql_comprueba_archivo_DNIACO);
                                                    $num_total_comprueba_archivo_DNIACO  = mysqli_num_rows($rs_comprueba_archivo_DNIACO);
                                                    ?>
                                                    <?php if(($num_total_comprueba_archivo_DNIACO == 0) && ($dias_desde_registro<16)){?>
                                                         <tr>
                                                            
                                                                <td class="col-md-4">
                                                                    <form class="fileUpload" enctype="multipart/form-data" id="dniaco-upload" method="post">
                                                                        <input type="file" name="archivo" id="file-dniaco" class="inputfile inputfile-99" data-multiple-caption="{count} files selected" multiple/>
                                                                        <label for="file-dniaco" id="button1" class="btn_upload"> <span><i class="fa fa-upload" aria-hidden="true"></i> Adjuntar DNI acompañante</span></label>
                                                                        <input type="hidden" name="archivotipo" value="DNI ACOMPANANTE"/>
                                                                        <input name="action" type="hidden" value="upload" />
                                                                        <input name="peti" type="hidden" value="<?php echo"$c_contacto_idcontador";?>" />
                                                                    </form>
                                                                    
                                                                </td>
                                                                <td class="col-md-2">
                                                                    <button name="enviar" id="enviar-dniaco" type="button" value="Upload File" class="btn btn-success" style="margin-top:0px;" />ENVIAR</button>
                                                                </td>
                                                                <td class="col-md-6">
                                                                    <div id="msg-upload-DNIACO" class="col-md-12"></div>
                                                                </td>
                                                            
                                                        </tr>
                                                    <?php }else{ ?>
                                                        <tr>
                                                            <td>
                                                        <?php while ($row=mysqli_fetch_array($rs_comprueba_archivo_DNIACO)) { 
                                                                $archivo_DNIACO_id=$row["id_archivo_peticion"];
                                                                $archivo_DNIACO_rel=$row["rel_archivo_peticion"];
                                                                $archivo_DNIACO_nombre=$row["nombre_archivo_peticion"];
                                                                $archivo_DNIACO_estado=$row["estado_archivo_peticion"];
                                                                if($archivo_DNIACO_estado==0){
                                                                    $archivo_DNIACO_estado_text="<span style='color:blue;'>Pendiente de revisión</span>";
                                                                }
                                                                if($archivo_DNIACO_estado==1){
                                                                    $archivo_DNIACO_estado_text="<span style='color:red;'>DNI acompañante erróneo</span>";
                                                                }
                                                                if($archivo_DNIACO_estado==2){
                                                                    $archivo_DNIACO_estado_text="<span style='color:green;'>DNI acompañante correcto</span>";
                                                                }
                                                                /* 0-revisar 1-erroneo 2-correcto*/
                                                    ?>
                                                        
                                                                <b>DNI acompañante:</b> <a target="_blank" href="archivos/<?php echo"$archivo_DNIACO_rel";?>/<?php echo"$archivo_DNIACO_nombre";?>">Ver Archivo</a><br>
                                                                <b>Estado:</b> <?php echo"$archivo_DNIACO_estado_text";?><br>
                                                           
                                                    
                                                    <?php   } ?> 
                                                        </td>
                                                        <td>
                                                            <?php if(($dias_desde_registro<16)&&(($archivo_DNIACO_estado==1)or($archivo_DNIACO_estado==0))&&(($c_contacto_estado==0)or($c_contacto_estado==1))){?>
                                                                
                                                                    <table>
                                                                        <tr>
                                                                            <td class="col-md-4">
                                                                                <form class="fileUpload" enctype="multipart/form-data" id="dniaco-upload" method="post">
                                                                                    <input type="file" name="archivo" id="file-dniaco" class="inputfile inputfile-99" data-multiple-caption="{count} files selected" multiple/>
                                                                                    <label for="file-dniaco" id="button1" class="btn_upload"> <span><i class="fa fa-upload" aria-hidden="true"></i> Adjuntar DNI acompañante</span></label>
                                                                                    <input type="hidden" name="archivotipo" value="DNI ACOMPANANTE"/>
                                                                                    <input name="action" type="hidden" value="upload" />
                                                                                    <input name="peti" type="hidden" value="<?php echo"$c_contacto_idcontador";?>" />
                                                                                </form>
                                                                            </td>
                                                                            <td class="col-md-2">
                                                                                <button name="enviar" id="enviar-dniaco" type="button" value="Upload File" class="btn btn-success" style="margin-top:0px;" />REENVIAR</button>
                                                                            </td>
                                                                            <td class="col-md-6">
                                                                                <div id="msg-upload-DNIACO" class="col-md-12"></div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                
                                                            <?php }?>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <?php }?>      
                                                    <?php }?>                             
                                                    
                                                    
                                                    <?php if(($c_aco_menor==1)or($c_aco_menor==2)){?>
                                                    <?php 
                                            //////////Autorización
                                                $sql_comprueba_archivo_AUTMENORES = "SELECT * FROM ".$table_prefix."__peticion_archivos
                                                WHERE rel_archivo_peticion = '$sesion_idpet' AND tipo_archivo_peticion = 'AUTORIZACIÓN-MENORES' ORDER BY id_archivo_peticion DESC";
                                                $rs_comprueba_archivo_AUTMENORES = mysqli_query($link, $sql_comprueba_archivo_AUTMENORES);
                                                $num_total_comprueba_archivo_AUTMENORES  = mysqli_num_rows($rs_comprueba_archivo_AUTMENORES);
                                                ?>
                                                <?php if(($num_total_comprueba_archivo_AUTMENORES == 0) &&($dias_desde_registro<16)){?>
                                                     <tr>
                                                        
                                                            <td class="col-md-4">
                                                                <form class="fileUpload" enctype="multipart/form-data" id="autmenores-upload" method="post">
                                                                    <input type="file" name="archivo" id="file-autmenores" class="inputfile inputfile-99" data-multiple-caption="{count} files selected" multiple/>
                                                                    <label for="file-autmenores" id="button1" class="btn_upload"> <span><i class="fa fa-upload" aria-hidden="true"></i> Adjuntar autorización de menores</span></label>
                                                                    <input type="hidden" name="archivotipo" value="AUTORIZACIÓN-MENORES"/>
                                                                    <input name="action" type="hidden" value="upload" />
                                                                    <input name="peti" type="hidden" value="<?php echo"$c_contacto_idcontador";?>" />
                                                                </form>
                                                                
                                                            </td>
                                                            <td class="col-md-2">
                                                                <button name="enviar" id="enviar-autmenores" type="button" value="Upload File" class="btn btn-success" style="margin-top:0px;" />ENVIAR</button>
                                                            </td>
                                                            <td class="col-md-6">
                                                                <div id="msg-upload-AUTMENORES" class="col-md-12"></div>
                                                            </td>
                                                        
                                                    </tr>
                                                <?php }else{ ?>
                                                    <tr>
                                                        <td>
                                                    <?php while ($row=mysqli_fetch_array($rs_comprueba_archivo_AUTMENORES)) { 
                                                            $archivo_AUTMENORES_id=$row["id_archivo_peticion"];
                                                            $archivo_AUTMENORES_rel=$row["rel_archivo_peticion"];
                                                            $archivo_AUTMENORES_nombre=$row["nombre_archivo_peticion"];
                                                            $archivo_AUTMENORES_estado=$row["estado_archivo_peticion"];
                                                            if($archivo_AUTMENORES_estado==0){
                                                                $archivo_AUTMENORES_estado_text="<span style='color:blue;'>Pendiente de revisión</span>";
                                                            }
                                                            if($archivo_AUTMENORES_estado==1){
                                                                $archivo_AUTMENORES_estado_text="<span style='color:red;'>Autorización de menores erróneo</span>";
                                                            }
                                                            if($archivo_AUTMENORES_estado==2){
                                                                $archivo_AUTMENORES_estado_text="<span style='color:green;'>Autorización de menores correcto</span>";
                                                            }
                                                            /* 0-revisar 1-erroneo 2-correcto*/
                                                ?>
                                                    
                                                            <b>Autorización de menores:</b> <a target="_blank" href="archivos/<?php echo"$archivo_AUTMENORES_rel";?>/<?php echo"$archivo_AUTMENORES_nombre";?>">Ver Archivo</a><br>
                                                            <b>Estado:</b> <?php echo"$archivo_AUTMENORES_estado_text";?><br>
                                                       
                                                
                                                <?php   } ?> 
                                                    </td>
                                                    <td>
                                                        <?php if(($dias_desde_registro<16)&&(($archivo_AUTMENORES_estado==1)or($archivo_AUTMENORES_estado==0))&&(($c_contacto_estado==0)or($c_contacto_estado==1))){?>
                                                            
                                                                <table>
                                                                    <tr>
                                                                        <td class="col-md-4">
                                                                            <form class="fileUpload" enctype="multipart/form-data" id="autmenores-upload" method="post">
                                                                                <input type="file" name="archivo" id="file-autmenores" class="inputfile inputfile-99" data-multiple-caption="{count} files selected" multiple/>
                                                                                <label for="file-autmenores" id="button1" class="btn_upload"> <span><i class="fa fa-upload" aria-hidden="true"></i> Adjuntar Aut. Menores</span></label>
                                                                                <input type="hidden" name="archivotipo" value="AUTORIZACIÓN-MENORES"/>
                                                                                <input name="action" type="hidden" value="upload" />
                                                                                <input name="peti" type="hidden" value="<?php echo"$c_contacto_idcontador";?>" />
                                                                            </form>
                                                                        </td>
                                                                        <td class="col-md-2">
                                                                            <button name="enviar" id="enviar-autmenores" type="button" value="Upload File" class="btn btn-success" style="margin-top:0px;" />REENVIAR</button>
                                                                        </td>
                                                                        <td class="col-md-6">
                                                                            <div id="msg-upload-AUTMENORES" class="col-md-12"></div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            
                                                        <?php }?>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <?php }?>
                                                <?php   } ?> 
                                                
                                            <?php } ?>
                                        </table>

                                        <div class="box_finish">
                                        <!--<a href="https://disfrutaunaexperienciaunica.com" data-toggle="modal" data-target="#fin-modal" class="btn btn-success">-->
                                        <a href="https://disfrutaunaexperienciaunica.com" class="btn btn-success">
                                        <span class="white">FINALIZAR</span>
                                        </a>
                                        </div>
                                       </div><!-- fi col3_box -->
                                        <div id="mensaje"></div>
                                    </div>
                                    
                                   <div class="col3_box">
                                    <div class="col3_box">
                                      <a href="descargatuparticipacion.php?<?php echo time(); ?>" target="_blank"> <button type="button" id="b1" class="btn_new1"><span class="icon1"><i class="fa fa-print fa-2x"></i></span><span class="text1">Imprimir</span></button></a>
       
                                    </div>
                                   </div>
                                   
                                    </hr>
                                </div><!-- fi container -->
                            </div> <!-- fi row -->
                        </div><!-- fi print div -->
                    </div><!-- fi box resumen -->
                </div><!-- fi form -->
            </div><!-- fi inner -->
        </section>




                </div><!-- Fi panel bottom -->
             </div><!-- Fi col 12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->

    </div><!-- /.back color -->

    
    <?php include("assets/includes/footer.php"); ?>

            <script src="assets/js/jquery.js"></script>
                
        <!-- remove this if you use Modernizr del file Input-->
        <script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>

<!-- Input Uploader -->
    <script src="assets/js/custom-file-input.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>
    
    <!-- Vertical tabs -->
    <script src="assets/js/tabs.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/popup.js"></script>

     <script type="text/javascript" src="assets/js/ajaxarchivos.js"></script>

<script>
function setColor(btn, color){
    var count=1;
    var property = document.getElementById(btn);
    if (count == 0){
        property.style.backgroundColor = "#FFFFFF"
        count=1;        
    }
    else{
        property.style.backgroundColor = "#7FFF00"
                property.style.background = "#df022b"
        count=0;
    }

}

$(function(){
    $( ".upload" ).change(function() {
       //get id
       var filenamePath = document.getElementById(this.id).value;
       //get last part
       var filenameArray = filenamePath.split('\\');
       var filenameLength = filenameArray.length;
       if (filenameLength > 0 ){
           var filenameString = filenameArray[filenameLength-1];
           $(".file-upload #"+this.id).parent().find($(".filename")).html( filenameString );
       }
       else{
           $(".file-upload .filename").html( "Ningún archivo seleccionado." );
       }
    });
});
</script>
        </body>
   </html> 

    
    
    